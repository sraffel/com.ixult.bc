/*jslint node: true */
'use strict';

module.exports = function (grunt) {
    require('jit-grunt')(grunt, {
        configureProxies: 'grunt-connect-proxy',
        jsdoc: 'grunt-jsdoc'
    });

    grunt.loadNpmTasks('grunt-openui5');
    // grunt.loadNpmTasks('grunt-nwabap-ui5uploader');

    grunt.initConfig({
        // Load Configurations
        project: grunt.file.readJSON('grunt-config.json'),
        // Task settings
        jshint: {
            all: [
                '<%= project.app.path %>/**/*.js',
                'test/**/*.js'
            ]
        },
        less: {
            development: {
                options: {
                    paths: ['']
                },
                files: {
                    '<%= project.app.path %>/<%= project.app.prefix %>/m/library.css':
                        '<%= project.app.path %>/<%= project.app.prefix %>/m/library.less',
                    '<%= project.app.path %>/<%= project.app.prefix %>/m/plugins/library.css':
                        '<%= project.app.path %>/<%= project.app.prefix %>/m/plugins/library.less'
                }
            }
        },
        watch: {
            options: {
                spawn: false
            },
            livereload: {
                options: {
                    livereload: {
                        host: 'localhost',
                        port: '<%= project.server.livereloadPort %>'
                    }
                },
                files: [
                    '<%= project.app.path %>/{,*/}*.{html, css, xml, js, properties, json}'
                ],
                tasks: ['less']
            },
            deploy: {
                options: {
                    livereload: false
                },
                files: [
                    '<%= project.deploy.path %>/{,*/}*.{html, xml, css, js, properties, json}'
                ]
            },
            doc: {
                files: ['<%= project.app.path %>/{,*/}*\.js'],
                tasks: ['jsdoc'],
                options: {
                    livereload: {
                        host: 'localhost',
                        port: '<%= project.doc.livereloadPort %>'
                    }
                }
            },
            less: {
                files: [
                    'css/**/*.less'
                ],
                tasks: ['less'],
                options: {
                    livereload: true
                }
            },
            css: {
                files: ['css/*.css']
            }
        },
        connect: {
            options: {
                hostname: '<%= project.server.host %>',
                port: '<%= project.server.port %>',
                livereload: true
            },
            rules: [{
                from: '^/(.*)-dbg\.(.*)\.js',
                to: '/$1\.$2.js'
            }, {
                from: '^<%= project.i18nPath %>/i18n/(.*)',
                to: '/i18n/$1'
            }],
            backendAll: {
                proxies: [{
                    context: '/sap/opu/odata/',
                    host: '<%= project.odata.server %>',
                    port: '<%= project.odata.port %>',
                    https: '<%= project.odata.protocol %>' === 'https',
                    changeOrigin: false,
                    rewrite: {
                        '&sap-client=\\d{3}': '',
                        'sap-client=\\d{3}&': '',
                        '\\?': '?sap-client=' + '<%= project.odata.client %>' + '&',
                        '(^((\?!\\?).)*$)': '$1?sap-client=' + '<%= project.odata.client %>' + ''
                    }
                }, {
                    context: '/resources',
                    host: '<%= project.resources.server %>',
                    port: '<%= project.resources.port %>',
                    https: '<%= project.resources.protocol %>' === 'https',
                    changeOrigin: true,
                    rewrite: {
                        '^/resources': '<%= project.resources.root %>' + '/resources',
                        '&sap-client=\\d{3}': '',
                        'sap-client=\\d{3}&': '',
                        '\\?': '?sap-client=' + '<%= project.resources.client %>' + '&sap-ui-debug=true&',
                        '(^((\?!\\?).)*$)': '$1?sap-client=' + '<%= project.resources.client %>' + '&sap-ui-debug=true'
                    }
                }, {
                    context: '/sap/bc/ui2/',
                    host: '<%= project.resources.server %>',
                    port: '<%= project.resources.port %>',
                    https: '<%= project.resources.protocol %>' === 'https',
                    changeOrigin: true,
                    rewrite: {
                        //'^/resources': '<%= project.resourcesRoot %>' + '/resources',
                        '&sap-client=\\d{3}': '',
                        'sap-client=\\d{3}&': '',
                        '\\?': '?sap-client=' + '<%= project.resources.client %>' + '&sap-ui-debug=true&',
                        '(^((\?!\\?).)*$)': '$1?sap-client=' + '<%= project.resources.client %>' + '&sap-ui-debug=true'
                    }
                }, {
                    context: '/sap/public/',
                    host: '<%= project.resources.server %>',
                    port: '<%= project.resources.port %>',
                    https: '<%= project.resources.protocol %>' === 'https',
                    changeOrigin: true,
                    rewrite: {
                        //'^/resources': '<%= project.resourcesRoot %>' + '/resources',
                        '&sap-client=\\d{3}': '',
                        'sap-client=\\d{3}&': '',
                        '\\?': '?sap-client=' + '<%= project.resources.client %>' + '&sap-ui-debug=true&',
                        '(^((\?!\\?).)*$)': '$1?sap-client=' + '<%= project.resources.client %>' + '&sap-ui-debug=true'
                    }
                }]
            },
            backendOData: {
                proxies: [{
                    context: '/sap/opu/odata/',
                    host: '<%= project.odata.server %>',
                    port: '<%= project.odata.port %>',
                    https: '<%= project.odata.protocol %>' === 'https',
                    changeOrigin: true,
                    rewrite: {
                        '&sap-client=\\d{3}': '',
                        'sap-client=\\d{3}&': '',
                        '\\?': '?sap-client=' + '<%= project.odata.client %>' + '&',
                        '(^((\?!\\?).)*$)': '$1?sap-client=' + '<%= project.odata.client %>' + ''
                    }
                }]
            },
            backendResources: {
                proxies: [{
                    context: '/resources',
                    host: '<%= project.resourcesServer %>',
                    port: '<%= project.resourcesPort %>',
                    https: '<%= project.resourcesProtocol %>' === 'https',
                    changeOrigin: true,
                    rewrite: {
                        '^/resources': '<%= project.resourcesRoot %>' + '/resources',
                        '&sap-client=\\d{3}': '',
                        'sap-client=\\d{3}&': '',
                        '\\?': '?sap-client=' + '<%= project.resourcesMandt %>' + '&sap-ui-debug=true&',
                        '(^((\?!\\?).)*$)': '$1?sap-client=' + '<%= project.resourcesMandt %>' + '&sap-ui-debug=true'
                    }
                }]
            },
            cdnResources: {
                proxies: [{
                    context: '/resources',
                    host: 'sapui5.hana.ondemand.com',
                    changeOrigin: true,
                    rewrite: {
                        '^/resources': '/' + '<%= project.resourcesVersion %>' + '/resources'
                    }
                }]
            },
            livereload: {
                options: {
                    livereload: false,
                    base: ['.tmp', '<%= project.app.path %>'],
                    middleware: function (connect, options, middlewares) {
                        middlewares.unshift(require('grunt-connect-proxy/lib/utils').proxyRequest);
                        middlewares.unshift(require('grunt-connect-rewrite/lib/utils').rewriteRequest);
                        middlewares.unshift(require('connect-livereload')({
                            port: grunt.config().project.server.livereloadPort,
                            ignore: [/\/sap\/opu\/.*/, /\.xml(\?.*)?$/, /\.js(\?.*)?$/, /\.css(\?.*)?$/, /\.svg(\?.*)?$/, /\.ico(\?.*)?$/,
                                /\.woff(\?.*)?$/, /\.png(\?.*)?$/, /\.jpg(\?.*)?$/, /\.jpeg(\?.*)?$/, /\.gif(\?.*)?$/, /\.pdf(\?.*)?$/,
                                /\.json(\?.*)?$/]
                        }));
                        return middlewares;
                    }
                }
            },
            deploy: {
                options: {
                    base: '<%= project.deploy.path %>',
                    middleware: function (connect, options, middlewares) {
                        middlewares.unshift(require('grunt-connect-proxy/lib/utils').proxyRequest);
                        middlewares.unshift(require('grunt-connect-rewrite/lib/utils').rewriteRequest);
                        middlewares.unshift(require('connect-livereload')({
                            port: grunt.config().project.server.livereloadPort,
                            ignore: [/\/sap\/opu\/.*/, /\.xml(\?.*)?$/, /\.js(\?.*)?$/, /\.css(\?.*)?$/, /\.svg(\?.*)?$/, /\.ico(\?.*)?$/,
                                /\.woff(\?.*)?$/, /\.png(\?.*)?$/, /\.jpg(\?.*)?$/, /\.jpeg(\?.*)?$/, /\.gif(\?.*)?$/, /\.pdf(\?.*)?$/,
                                /\.json(\?.*)?$/]
                        }));
                        return middlewares;
                    }
                }
            },
            doc: {
                options: {
                    base: '<%= project.doc.path %>',
                    port: '<%= project.doc.project.server.port %>',
                    middleware: function (connect, options, middlewares) {
                        middlewares.unshift(require('grunt-connect-proxy/lib/utils').proxyRequest);
                        middlewares.unshift(require('grunt-connect-rewrite/lib/utils').rewriteRequest);
                        middlewares.unshift(require('connect-livereload')({
                            port: grunt.config().project.doc.livereloadPort,
                            ignore: [/\/sap\/opu\/.*/, /\.js(\?.*)?$/, /\.css(\?.*)?$/, /\.svg(\?.*)?$/, /\.ico(\?.*)?$/,
                                /\.woff(\?.*)?$/, /\.png(\?.*)?$/, /\.jpg(\?.*)?$/, /\.jpeg(\?.*)?$/, /\.gif(\?.*)?$/, /\.pdf(\?.*)?$/,
                                /\.json(\?.*)?$/]
                        }));
                        return middlewares;
                    }
                }
            }
        },
        copy: {
            model: {
                files: [{
                    expand: true,
                    cwd: '<%= project.testPath %>/',
                    src: '**',
                    dest: '.tmp'
                }]
            },
            build: {
                files: [{
                    expand: true,
                    cwd: '<%= project.app.path %>',
                    src: [
                        '**/*.js',
                        '**/**/*.js'
                    ],
                    dest: '<%= project.deploy.path %>/',
                    rename: function (dest, src) {
                        // xx.view.js -> xx-dbg.view.js
                        // xx.fragment.js -> xx-dbg.fragment.js
                        // xx.controller.js -> xx-dbg.controller.js
                        return dest + src
                            .replace('\.js', '-dbg.js')
                            .replace('\.view-dbg\.js', '-dbg.view.js')
                            .replace('\.controller-dbg\.js', '-dbg.controller.js')
                            .replace('\.fragment-dbg\.js', '-dbg.fragment.js');
                    }
                }, {
                    expand: true,
                    cwd: '<%= project.app.path %>',
                    src: [
                        '**/*.*',
                    ],
                    dest: '<%= project.deploy.path %>/'
                }, {
                    expand: true,
                    cwd: '<%= project.app.path %>',
                    src: [
                        '**/.*',
                    ],
                    dest: '<%= project.deploy.path %>/'
                }]
            }
        },
        uglify: {
            deploy: {
                options: {
                    compress: true
                },
                files: [{
                    expand: true,
                    cwd: '<%= project.app.path %>',
                    src: [
                        '**/*.js',
                        '**/**/*.js',
                        '!doc/*'
                    ],
                    dest: '<%= project.deploy.path %>/'
                }]
            }
        },
        open: {
            server: {
                path: 'http://' + '<%= project.server.host %>:' + '<%= project.project.server.port %>/' + '<%= project.server.urlParameters %>'
            },
            doc: {
                path: 'http://' + '<%= project.server.host %>:' + '<%= project.doc.server.port %>'
            }
        },
        clean: {
            options: {
                force: true
            },
            temp: '<%= project.tempPath %>',
            deploy: '<%= project.deploy.path %>/**.*',
            preload: '<%= project.app.path %>/**/*-preload.js',
            doc: '<%= project.doc.path %>'
        },
        concurrent: {
            localTasks: [
                'clean:temp',
                'less',
                // 'jshint',
                'copy:model'
            ],
            serverTasks: [
                'connect:livereload',
                'open:server',
                'watch'
            ]
        },
        concat: {
            options: {
                banner: 'jQuery.sap.registerPreloadedModules({\n' +
                    '  "name": "<%= project.app.prefix.replace(/\\./g,\'/\') %>/library-preload",\n' +
                    '  "version" : "2.0",\n' +
                    '  "modules" : {\n',
                separator: ',\n',
                footer: '}});',
                process: function (src, filepath) {
                    return '"' + grunt.config().project.app.prefix.replace(/\./g, "/") + '/' + filepath.replace(grunt.config().project.app.path + "/", "") + '": function(){\n'
                        + src
                        + "\n}";
                }
            }
        },
        charset: {
            dist: {
                options: {
                    from: 'UTF-8',
                    to: '<%= project.codePage %>',
                    fileTypes: {
                        // Code replacement config (Optional)
                    }
                },
                files: [{
                    expand: true,
                    cwd: '<%= project.app.path %>',
                    src: [
                        '**/*.properties'
                    ],
                    dest: '<%= project.deploy.path %>'
                }]
            }
        },
        jsdoc: {
            doc: {
                src: [
                    '<%= project.app.path %>/<%= project.app.prefix %>/**/*.js',
                    '!<%= project.app.path %>/<%= project.app.prefix %>/<%= project.doc.path %>/*'
                ],
                dest: '<%= project.app.path %>/<%= project.app.prefix %>/<%= project.doc.path %>',
                options: {
                    configure: './jsdoc-config.json'
                }
            }
        },
        openui5_preload: {
            library: {
                options: {
                    resources: '<%= project.deploy.path %>',
                    dest: '<%= project.deploy.path %>/'
                },
                libraries: '*/**'
            },
            libdev: {
                options: {
                    resources: '<%= project.app.path %>/',
                    dest: '<%= project.app.path %>/'
                },
                libraries: '*/**'
            }
        },
        nwabap_ui5uploader: {
            options: {
                conn: {
                    server: '<%= project.upload.conn.server %>',
                    client: '<%= project.upload.conn.client %>',
                    useStrictSSL: '<%= project.upload.conn.useStrictSSL %>'
                },
                auth: {
                    user: '<%= project.upload.auth.user %>',
                    pwd: '<%= project.upload.auth.pwd %>'
                }
            },
            upload_build: {
                options: {
                    resources: {
                        cwd: '<%= project.upload.path %>',
                        src: '**/*.*'
                    },
                    ui5: {
                        language: '<%= project.upload.ui5.language %>',
                        package: '<%= project.upload.ui5.package %>',
                        bspcontainer: '<%= project.upload.ui5.bspcontainer %>',
                        bspcontainer_text: '<%= project.upload.ui5.bspcontainer_text %>',
                        transportno: '<%= project.upload.ui5.transportno %>',
                        transport_use_locked: '<%= project.upload.ui5.transport_use_locked %>',
                        calcappindex: '<%= project.upload.ui5.calcappindex %>'
                    }
                }
            }
        }
    });

    // GRUNT TASKS
    // Server
    // grunt.registerTask('server', function (target) {
    //     switch (target) {
    //         case 'local':
    //             grunt.task.run([
    //                 'concurrent:localTasks',
    //                 'configureProxies',
    //                 'concurrent:serverTasks'
    //             ]);
    //             break;
    //         case 'deploy':
    //             grunt.task.run([
    //                 'clean:temp',
    //                 'jshint',
    //                 'configureProxies:backendAll',
    //                 'build',
    //                 'charset',
    //                 'connect:deploy',
    //                 'open:server',
    //                 'watch:deploy'
    //             ]);
    //             break;
    //         case 'doc':
    //             grunt.task.run([
    //                 'clean:doc',
    //                 'jsdoc',
    //                 'connect:doc',
    //                 'open:doc',
    //                 'watch:doc'
    //             ]);
    //             break;
    //         default:
    //             grunt.task.run([
    //                 'clean:temp',
    //                 'clean:preload',
    //                 // 'jshint',
    //                 'less',
    //                 'configureProxies:backendAll',
    //                 'connect:livereload',
    //                 'open:server',
    //                 'watch:livereload'
    //             ]);
    //             break;
    //     }
    // });

    // Clean preload files in app folder
    grunt.registerTask('preload:clean', [
        'clean:preload'
    ]);

    // Generate preload files in app folder
    grunt.registerTask('preload:generate', [
        'clean:preload',
        'openui5_preload:libdev'
    ]);

    // Build process
    // grunt.registerTask('build', [
    //     'clean:preload',
    //     'clean:deploy',
    //     'jsdoc',
    //     'less',
    //     'concat',
    //     'copy:build',
    //     'uglify:deploy',
    //     'charset',
    //     'openui5_preload:library'
    // ]);

    // Build process
    // grunt.registerTask('deploy', [
    //     'nwabap_ui5uploader'
    // ]);
}
