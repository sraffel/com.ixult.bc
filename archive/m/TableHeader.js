sap.ui.define([
    "sap/m/Button",
    "sap/m/Popover",
    "sap/m/FlexBox",
    "sap/m/List",
    "sap/m/StandardListItem",
    "sap/m/SearchField",
    "sap/m/Text",
    "sap/m/Toolbar",
    "sap/ui/model/Sorter",
    "ixult/base/assert",
    "ixult/base/util/LibraryHelper",
    "ixult/ui/core/theming/ThemeHelper",
    "ixult/ui/model/util/SortHelper"
], function (
    Button,
    Popover,
    FlexBox,
    List,
    StandardListItem,
    SearchField,
    Text,
    Toolbar,
    Sorter,
    assert,
    LibraryHelper,
    ThemeHelper,
    SortHelper
) {
    "use strict";
    const Library = LibraryHelper.register("ixult/m", {
        styleSheet: "libary.css"
    });

    /**
     * Adjusts and enhances the TableHeader 'sortFields' property to look for incorrect required values
     * (like the 'path' attribute) and apply standard values for optional attributes if they were omitted
     * i.e. 'type' is a string by default and 'group' will be set to false
     * @param {object[]|map[]} aFields - the given 'fields' array as specified in the View XML
     * @return {object[]|map[]} the adjusted 'fields' array
     * @private
     */
    var _adjustSortFields = function (aFields) {
        var aSortFields = [];

        $.each(aFields, function (iIndex, oField) {
            if (!assert(oField.path && typeof oField.path === "string",
                "Error: Sort path must be a valid string value. ('" + oField.path + "')")) {
                return true;
            }

            oField.path = oField.path.toString().trim();
            oField.type = oField.stype ? oField.stype.toString().trim() : oButton.Type.String;
            oField.group = oField.group !== undefined ? Boolean(oField.group) : false;

            aSortFields.push(oField);
        }.bind(this));

        return aSortFields;
    }

    /**
     * Returns a custom sorter function if the 'type' attribute is supported
     * @param {string} sType - the fields 'type' attribute set in the XML View
     * @returns {function} - the sorter function
     */
    var _getSorterFunction = function (sType) {
        switch (sType) {
            case oButton.Type.Number:
                return SortHelper.compareNumbers;
            case oButton.Type.Currency:
                return SortHelper.compareCurrencies;
            case oButton.Type.Date:
                return SortHelper.compareDates;
            default:
                return SortHelper.compareStrings;
        }
    }

    /**
     * Table Header Button with sort and filter options displayed like 'Excel style'
     * @experimental since 0.2.1
     *
     * @property {string} sortOrder
     *  the initial sort order for the table column this Button is the header of;
     * @property {object[]|map[]} fields
     *  an array of fields that should be sorted; each containing an object with the following attributes:
     *  - {string} path: the name of the table field that should be sorted
     *  - {string} type: the field value type; @see {ixult.m.TableHeader#Types}
     *  - {boolean} group: true, if same values should be grouped along table rows, false by default
     * @class
     * @name ixult.m.TableHeader
     * @extends sap.m.Button
     * @see {ixult.m.TableHeader#SortOrder}
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.m.Button sap.m.Button}
     * @public
     */
    var oButton = Button.extend("ixult.m.TableHeader", /** @lends ixult.m.TableHeader */ {
        // The renderer to use; Important: Leave this empty,
        // so that the default Renderer is used
        renderer: {},

        // Metadata for the UI element;
        // Used to define custom properties
        metadata: {
            properties: {
                "sortFields": {
                    type: "object[]",
                    defaultValue: []
                },
                "sortOrder": {
                    type: "string",
                    defaultValue: "None"
                }
            }
        },

        // Attributes
        _oColumn: {},
        _oTable: {},
        _oPopover: null,
        _bInitialized: false
    });

    /**
     * Possible sort orders for the table column
     * @type {{ASCENDING: string, DESCENDING: string, NONE: string}}
     * @public
     * @static
     */
    oButton.SortOrder = {
        Ascending: "Ascending",
        Descending: "Descending",
        None: "None"
    };

    /**
     * Possible field types used in the table column used to specify the sort order;
     * - 'Number' will result in integer type sorting
     * - 'Currency' will assume float and respect negative and positive value
     * - 'String' can be used for everything else
     * @type {{Number: string, Currency: string, String: string, Date: string}}
     * @public
     * @static
     */
    oButton.Type = {
        Currency: "Currency",
        Date: "Date",
        Number: "Number",
        String: "String"
    };

    /**
     * Handles the 'beforeRendering' Event for the UI Control,
     * that is executed when the DOM elements are created but not yet shown
     * @memberOf ixult.m.TableHeader
     * @param {sap.ui.base.Event} oEvent - the UI event instance
     * @returns {void}
     * @protected
     */
    oButton.prototype.onBeforeRendering = function (oEvent) {
        // This has to be done only once, this is to avoid re-setup of the Control
        // because after executing sorting on the Table, the bound controls seem to rerender
        if (!this._bInitialized) {
            Button.prototype.onBeforeRendering.apply(this, arguments);
            this._initUiControl();
            this._bInitialized = true;
        }
    }

    /**
     * Initializes the Button Control
     * @returns {void}
     * @private
     */
    oButton.prototype._initUiControl = function () {
        // Get the table and column reference
        this._oColumn = this.getParent();
        this._oTable = this._oColumn.getParent();

        // Setup the style properties
        this._initUiStyle();

        // Evaluate and adjust the 'fields' property
        // so that it can be used correctly for sorting
        this.setSortFields(_adjustSortFields(this.getSortFields()));

        // Setup the initial sort direction and icon
        this.updateSortOrder(this.getSortOrder() || oButton.SortOrder.None);

        // Setup the event handlers
        this.attachPress(this.onButtonPress, this);
    }

    /**
     * Sets up the UI properties and styles for use with this Button Control
     * @returns {void}
     * @private
     */
    oButton.prototype._initUiStyle = function () {
        // Set the content density depending on the surrounding view (either 'compact' or 'cozy')
        var oView = Library.getViewFor(this);
        if (oView && oView.getController) {
            this.addStyleClass(ThemeHelper.getContentDensityClass(oView.getController()));
        }

        // Setup standard and custom CSS classes for this Control
        this.addStyleClass("sapMLabel");
        this.addStyleClass("ixultMSortButton");
        Library.loadStyleSheet();

        // Set the UI properties
        this.setType("Transparent");
        this.setIconFirst(false);
    }

    /**
     * TODO !
     */
    oButton.prototype._createPopover = function () {
        return new Popover({
            content: [new FlexBox({
                direction: "Row",
                fitContainer: true,
                items: [
                    new Button({
                        press: function (oEvent) {
                            this.onSortButtonPress(oEvent, oButton.SortOrder.Ascending);
                        }.bind(this)
                    }),
                    new Button({
                        press: function (oEvent) {
                            this.onSortButtonPress(oEvent, oButton.SortOrder.Descending);
                        }.bind(this)
                    }),
                    new Text({}),
                    new Button({
                        press: function (oEvent) {
                            this.onFilterResetButtonPress(oEvent);
                        }.bind(this)
                    }),
                    new List({
                        selectionChange: function (oEvent) {
                            this.onFilterListSelectionChange(oEvent);
                        }.bind(this),
                        infoToolbar: [new Toolbar({
                            content: [new SearchField({
                                liveChange: function (oEvent) {
                                    this.onFilterListSearch(oEvent);
                                }.bind(this)
                            })]
                        })],
                        items: [new StandardListItem({})]
                    })
                ]
            })],
            buttons: [
                new Button({
                    press: function (oEvent) {
                        this.onOkayButtonPress(oEvent);
                    }.bind(this)
                }),
                new Button({
                    press: function (oEvent) {
                        this.onCancelButtonPress(oEvent);
                    }.bind(this)
                })
            ]
        });
    }

    oButton.prototype._openPopover = function () {
        if (!this._oPopover) {
            this._oPopover = this._createPopover();
        }

        this._oPopover.openBy(this);
    }

    /**
     * Sets the Control property 'sortFields'
     * @protected
     * @override
     */
    oButton.prototype.setSortFields = function (aFields) {
        this.setProperty("sortFields", aFields || []);
    }

    /**
     * Returns the property value for 'sortFields'
     * or the bound property via 'BindingInfo'
     * @protected
     * @override
     */
    oButton.prototype.getSortFields = function () {
        var aBindings = this.getBindingInfo("sortFields");
        var aProperties = this.getProperty("sortFields");

        return aProperties && aProperties.length > 0 ? aProperties : aBindings ? aBindings.parts : [];
    }

    /**
     * Updates the sort order and sort icon
     * @memberOf ixult.m.TableHeader
     * @param {string} sOrder - the sort order; "Ascending", "Descending" or "None"
     * @returns {void}
     * @protected
     */
    oButton.prototype.updateSortOrder = function (sOrder) {
        this.setSortOrder(sOrder);
        this.setSortIcon(sOrder);
    }

    /**
     * Changes the Icon next to the Button to indicate the current sort order
     * @memberOf ixult.m.TableHeader
     * @param {string} [sOrder]
     *  the sort order; "Ascending", "Descending" or "None"
     *  optional; if not specified the current sort order will be used
     * @returns {void}
     * @protected
     */
    oButton.prototype.setSortIcon = function (sOrder) {
        sOrder = sOrder || this.getSortOrder();

        var sIcon = sOrder === oButton.SortOrder.Ascending ? "sap-icon://sort-ascending" :
            sOrder === oButton.SortOrder.Descending ? "sap-icon://sort-descending" :
                sOrder === oButton.SortOrder.None ? "sap-icon://sort" : "sap-icon://sort";

        this.setIcon(sIcon);
    }

    /**
     * Handles the 'press' Event on the Button
     * Changes the sort direction and sorts the elements
     * @memberOf ixult.m.TableHeader
     * @param {sap.ui.base.Event} oEvent - the Button 'press' Event instance
     * @returns {void}
     * @protected
     */
    oButton.prototype.onButtonPress = function (oEvent) {
        this._openPopover();
        return;

        // Toggle the sort order based on the current order
        var sOrder = this.getSortOrder();
        sOrder = sOrder === oButton.SortOrder.Ascending ? oButton.SortOrder.Descending :
            sOrder === oButton.SortOrder.Descending ? oButton.SortOrder.Ascending :
                sOrder === oButton.SortOrder.None ? oButton.SortOrder.Ascending :
                    oButton.SortOrder.None;

        // Update the sort state and the corresponding icon for all Table Columns with TableHeaders
        var aTableHeaderColumns = $.grep(this._oTable.getColumns(), function (oColumn, iIndex) {
            var oSortBtn = oColumn.getHeader();
            return oSortBtn && oSortBtn instanceof ixult.m.TableHeader;
        }.bind(this));
        $.each(aTableHeaderColumns, function (iIndex, oColumn) {
            var oSortBtn = oColumn.getHeader();
            // Set this current Button to the new sort order and every other TableHeader to 'None'
            oSortBtn.updateSortOrder(oSortBtn.getId() === this.getId() ? sOrder : oButton.SortOrder.None);
        }.bind(this));

        // Execute the sort function
        this.sortItems(sOrder);
    }

    /**
     * Sorts the list items
     * @memberOf ixult.m.TableHeader
     * @returns {void}
     * @protected
     */
    oButton.prototype.sortItems = function () {
        var aSorters = [];
        $.each(this.getSortFields(), function (iIndex, oField) {
            aSorters.push(this.createSorter(oField));
        }.bind(this));

        var oItemBinding = this._oTable.getBinding("items");
        oItemBinding.sort(aSorters);
    }

    /**
     * Creates a Sorter for the given table column field;
     * Sets up a custom sort function if the value is a number or currency value
     * @memberOf ixult.m.TableHeader
     * @see {ixult.m.TableHeader#SortOrder}
     * @see {ixult.m.TableHeader#Types}
     * @param {object|map} oField - a field object from the array specified in the 'fields' property
     * @param {string} oField.path
     *  the path to the model attribute which should be used for sorting
     * @param {string} oField.type
     *  the field value type used for sorting
     * @param {boolean} oField.group
     *  true, if the values should be grouped along table rows, false by default
     * @returns {sap.ui.model.Sorter}
     *  a model sorter instance to apply to the table binding
     * @protected
     */
    oButton.prototype.createSorter = function (oField) {
        var bDescending = this.getSortOrder() === oButton.SortOrder.Descending;
        return new Sorter(oField.path, bDescending, oField.group, _getSorterFunction(oField.type));
    }

    return oButton;
});