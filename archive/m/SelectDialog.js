/* jshint strict: true */
/* global com: false */
"use strict";

jQuery.sap.declare("ixult.m.SelectDialog");
jQuery.sap.require("sap.m.SelectDialog");

sap.m.SelectDialog.extend("ixult.m.SelectDialog", {
    // The renderer to use; Important: Leave this empty,
    // so that the default Renderer is used
    renderer: {},
    /**
     * Metadata
     */
    metadata: {
        events: {
            "afterOpen": {}
        }
    },

    /**
     * Initializes the control
     * @private
     */
    init: function () {
        sap.m.SelectDialog.prototype.init.apply(this, arguments);
        if (this._oList && this._oList instanceof sap.m.List) {
            this._oList.setGrowingThreshold(50);
            this._oList.setGrowingScrollToLoad(false);
        }
    },

    /**
     * Opens the internal dialog with a searchfield and a list.
     *
     * @name sap.m.SelectDialog#open
     * @function
     * @param {string} sSearchValue
     *         A value for the search can be passed to match with the filter applied to the list binding.
     * @type sap.m.SelectDialog
     * @public
     * @ui5-metamodel This method also will be described in the UI5 (legacy) designtime metamodel
     */
    open: function () {
        sap.m.SelectDialog.prototype.open.apply(this, arguments);

        // Set the cursor into the search field when the dialog has opened
        // This only applies to desktop devices (as touch devices will display the onscreen
        // keyboard when the input field is focused; and we don't want that to happen)
        if (sap.ui.Device.system.desktop) {
            jQuery.sap.delayedCall(500, this, function () {
                $("#" + this._oSearchField.sId + "-I").focus();
            });
        }

        // Fire the custom "afterOpen" event to allow post processing
        this.fireEvent("afterOpen");
    }
});