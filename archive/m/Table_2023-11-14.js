sap.ui.define([
    "sap/m/Table",
    "sap/m/library",
    "sap/ui/events/KeyCodes",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    "sap/ui/model/Sorter",
    "ixult/base/assert",
    "ixult/base/util/array",
    "ixult/base/util/object",
    "ixult/base/util/DateHelper",
    "ixult/base/util/LibraryHelper"
], function (
    Table,
    SapMLibrary,
    KeyCodes,
    Filter,
    FilterOperator,
    Sorter,
    assert,
    array,
    object,
    DateHelper,
    LibraryHelper
) {
    "use strict";
    const Library = LibraryHelper.register("ixult/m").loadStyleSheet();

    /**
     * @class ixult.m.Table
     * @extends sap.m.Table
     *
     * @classdesc Responsive Table Control with additional capabilities,
     *  like sortable and filterable columns
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.m.Table sap.m.Table}
     *
     * @public
     */
    return Table.extend("ixult.m.Table", /** @lends ixult.m.Table.prototype */ {
        // The renderer to use; Important: Leave this empty,
        // so that the default Renderer is used
        renderer: {},

        // Metadata for the UI element;
        // Used to define custom properties
        metadata: {
            properties: {
                /**
                 * @summary Allows the columns to be resizable when true
                 * @see {@link https://ui5.sap.com/sdk/#/api/sap.m.plugins.ColumnResizer sap.m.plugins.ColumnResizer}
                 * @type {boolean}
                 * @defaultValue false
                 */
                resizableColumns: {
                    type: "boolean",
                    defaultValue: false
                },
                /**
                 * @summary Sets the table layout to fixed
                 * @type {string}
                 * @defaultValue `false`
                 */
                fixedLayout: {
                    type: "String",
                    defaultValue: "false"
                },
                /**
                 * @summary Sets the table header to sticky allowing it
                 *  to stay in place when scrolling the table
                 * @type {boolean}
                 * @defaultValue `true`
                 */
                stickyHeader: {
                    type: "boolean",
                    defaultValue: true
                },
                /**
                 * @summary Sets the table footer to sticky allowing it
                 *  to stay in place when scrolling the table
                 * @type {boolean}
                 * @defaultValue `true`
                 */
                stickyFooter: {
                    type: "boolean",
                    defaultValue: true
                },
                /**
                 * @summary Sets the starting index for the keyboard
                 *  navigation index from which `tabindex` Controls
                 *  are counted from
                 * @type {int|Number}
                 * @defaultValue `1000`
                 */
                keyboardStart: {
                    type: "int",
                    defaultValue: 1000
                },
                /**
                 * @summary a string array of HTML control names
                 *  that should be used for keyboard navigation
                 * @type {string[]}
                 * @defaultValue `["Input","Button"]`
                 */
                keyboardElements: {
                    type: "string[]",
                    defaultValue: ["Input", "Button"]
                }
            },
            events: {
                /**
                 * @summary Event to notify calling views about
                 *  an update on the table filter
                 */
                filterUpdate: {
                    allowPreventDefault: true
                }
            }
        },

        // Attributes
        mAttributes: {
            LineId: "data-ui-line-id",
            ItemIndex: "data-ui-item-index",
            TabIndex: "data-ui-tab-index"
        },
        _mTableElements: {},
        _mTableIndices: {},
        // References
        _oView: null,
        _oController: null,
        // Arrays
        _aFilters: [],
        _aSorters: [],
        _aMergeableColumns: [],
        _aItems: [],
        _aItemDiff: [],
        _aDiffItems: [],
        // Booleans
        _bIsInitialized: false,
        _bIsRendered: false,

        /**
         * @summary Initializes the Table Control
         * @returns {void}
         * @protected
         */
        init: function () {
            Table.prototype.init.apply(this, arguments);

            // Attach custom internal event handler
            this.attachUpdateStarted(this.onUpdateStarted, this);
            this.attachUpdateFinished(this.onUpdateFinished, this);

            // Attach the jQuery 'keydown' event to listen to key presses
            $(document).keydown({
                table: this,
                keyCodes: KeyCodes
            }, function (oKeyEvent) {
                try {
                    this.onKeyPressed(oKeyEvent);
                } catch (oException) {
                    console.error("Error during keydown event handling.", oException);
                }
            }.bind(this));
        },

        /**
         * @summary Handles the 'beforeRendering' Event for the UI Control,
         *  that is executed when the DOM elements are created but not yet shown
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onBeforeRendering: function (oEvent) {
            // This has to be done only once, this is to avoid re-setup of the Control
            // because after executing sorting on the Table, the bound controls seem to rerender
            if (!this._bIsInitialized) {
                Table.prototype.onBeforeRendering.apply(this, arguments);

                // Save View and Controller instance for later access
                this._oView = Library.getViewFor(this);
                if (this._oView && this._oView.getController) {
                    this._oController = this._oView.getController();
                }

                // Attach an event listener to recognize changes in the bound items
                this.getBinding("items").attachChange(this.onItemDataChanged, this);

                // Add functionality for resizable columns if set
                if (this.getResizableColumns && this.getResizableColumns()) {
                    sap.ui.require(["ixult/m/plugins/ColumnResizer"], function (ColumnResizer) {
                        this.addDependent(new ColumnResizer());
                    }.bind(this));
                }

                // Setup custom CSS classes for this Control
                this.addStyleClass("ixultMTable");

                // Call a 'beforeRendering' handler on each applicable
                // custom Column because that Element has no such Event
                this._getCustomColumns().loop(function (oColumn) {
                    oColumn.fireBeforeRendering({
                        table: this
                    });
                }.bind(this));

                // Save the Columns that were set to 'mergeable' using the
                // sap.m.Column attribute 'mergeDuplicates'
                array(this.getColumns()).grep(oColumn => {
                    return oColumn.getMergeDuplicates &&
                        oColumn.getMergeDuplicates() === true;
                }).loop(function (oColumn, iIndex) {
                    this._aMergeableColumns.push({
                        iIndex: iIndex,
                        oColumn: oColumn
                    });
                }.bind(this));

                // ... and we're done :-)
                this._bIsInitialized = true;
            }
        },

        /**
         * @summary Handles the 'afterRendering' Event for the UI Control,
         *  that is executed when the Control is completely rendered and shown
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onAfterRendering: function (oEvent) {
            // This is for one time setups that should be executed
            // after the table has been initially rendered (and not
            // everytime e.g. after a resize)
            if (!this._bIsRendered) {
                Table.prototype.onAfterRendering.apply(this, arguments);

                // Set up a sticky Table Header using a specialized custom property
                if (this.getStickyHeader()) {
                    this.setSticky([sap.m.Sticky.ColumnHeaders]);
                }

                // ... and we're done :-)
                this._bIsRendered = true;
            }

            // Call an 'afterRendering' handler on each applicable
            // custom Column because that Element has no such Event
            this._getCustomColumns().loop(function (oColumn) {
                oColumn.fireAfterRendering({
                    table: this
                });
            }.bind(this));

            // Set up the Table Footer
            var oTableFooterElement = $("#" + this.getId() + " .sapMListTblFooter");
            if (oTableFooterElement && oTableFooterElement.hasClass) {
                if (!oTableFooterElement.hasClass("ixultMFooter")) {
                    oTableFooterElement.addClass("ixultMFooter");
                }
                if (this.getStickyFooter() && !oTableFooterElement.hasClass("ixultMStickyFooter")) {
                    oTableFooterElement.addClass("ixultMStickyFooter");
                }
            }
        },

        /**
         * @summary Handles the 'updateStarted' event on the table
         *  to allow custom internal postprocessing
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onUpdateStarted: function (oEvent) {
            // TODO: Refactor / Set obsolete
            // Save the items before evaluating the differences
            // this._aItemDiff = this._getBindingItemDifferences();
        },

        /**
         * @summary Handles the 'updateFinished' event on the table
         *  to allow custom internal postprocessing
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onUpdateFinished: function (oEvent) {
            // TODO: Refactor / Set obsolete
            // this._saveTableItems();
            //
            // const sReason = oEvent.getParameter("reason");
            // if (array(["Filter", "Sort", "Change"]).contains(sReason)) {
            //     this._adjustMergedDuplicatesDisplay(sReason);
            // }
        },

        /**
         * @summary Handles the 'change' Event on the Data
         *  Binding of the 'items' aggregation in this Table
         * @description This event will be fired between
         *  'updateStarted' and 'updateFinished'
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onItemDataChanged: function (oEvent) {
            // TODO: Refactor / Set obsolete
            // // Get the rows that have been inserted into the table
            // var aDiffInserts = array(this._aItemDiff).grep(function (oDiff) {
            //         return oDiff.sType === "insert";
            //     }),
            //     // ... and extract their model paths
            //     aDiffPaths = array(aDiffInserts).extractValues("sPath");
            //
            // // Get the table items of which the path matches the paths of the inserted rows
            // this._aDiffItems = array(this.getItems(true)).grep(function (oItem) {
            //     return aDiffPaths.contains(oItem.getBindingContextPath());
            // });
        },

        /**
         * @summary Handles a keypress event inside the table
         * @description Used e.g. to set the tab order for
         *  Input Controls inside this table
         * @param {jQuery.Event} oEvent
         *  the `keydown` event instance fired inside the document,
         *  including the following parameters:
         * @param {object} oEvent.target
         *  the HTML control where the key was pressed on
         * @param {map|object} oEvent.data
         *  additional data with event attributes
         * @param {map|object} oEvent.data.keyCodes
         *  the map of key codes for comparison to the pressed key
         * @param {map|object} oEvent.data.table
         *  the table control where the event was fired (= `this`)
         * @param {int} oEvent.keyCode
         *  the integer code of the key that has been pressed;
         *  can be compared against the `oEvent.data.keyCodes` map
         * @param {boolean} oEvent.shiftKey
         *  true, if the SHIFT key was pressed
         * @param {boolean} oEvent.ctrlKey
         *  true, if the CTRL key was pressed
         * @param {boolean} oEvent.altKey
         *  true, if the ALT key was pressed
         * @returns {void}
         * @protected
         */
        onKeyPressed: function (oEvent) {
            // TODO: SRL,2023-01-24: obsolet due to sap.m.Table
            //  property `keyboardMode` with value `Edit`
            // const mKeyCodes = oEvent.data.keyCodes,
            //     oElement = oEvent.target,
            //     oControl = $(oElement),
            //     sControlName = oElement ? oElement.localName || oElement.nodeName : null;
            // var iTabIndex = parseInt(oControl.attr(this.mAttributes.TabIndex));
            //
            // // Set the focus on the HTML Input Control with the next or previous tab index
            // if (oEvent.keyCode === mKeyCodes.TAB && sControlName === "input" && iTabIndex) {
            //     // Prevent default key press behavior
            //     oEvent.preventDefault();
            //
            //     // Calculate the next tab index based on the modifier key
            //     // TAB -> will move to the next input
            //     // SHIFT + TAB -> will move to the previous input
            //     iTabIndex = oEvent.shiftKey ? iTabIndex - 1 : iTabIndex + 1;
            //
            //     // Focus the input control with the corresponding tab index
            //     $('[' + this.mAttributes.TabIndex + '="' + (iTabIndex) + '"]').focus();
            // }
        },

        /**
         * @summary Retrieves the model data items corresponding
         *  to this Table's 'items' property
         * @returns {object[]}
         *  the data array
         * @private
         * @deprecated
         */
        _getBindingItems: function () {
            // TODO: Refactor / Set obsolete
            // var oBinding = this.getBinding("items"),
            //     aItems = [];
            //
            // this.getItems(true).forEach(function (oItem) {
            //     var oProperty = oBinding.getModel().getProperty(oItem.getBindingContextPath());
            //     if (oProperty) {
            //         aItems.push(oProperty);
            //     }
            // }.bind(this));
            //
            // return aItems;
        },

        /**
         * @summary Evaluates the Tables 'items' property against
         *  the model data to find differences
         * @returns {Array.<{iIndex: int, sType: string, oData: object}>}
         *  a diff array containing the index, data and type, where type can be on of the following
         *  <ul>
         *   <li>'delete' - in which case the item of the <b>old</b> array will be returned</li>
         *   <li>'insert' - in which case the item of the <b>new</b> array will be returned</li>
         *  </ul>
         * @private
         * @deprecated
         */
        _getBindingItemDifferences: function () {
            // TODO: Refactor / Set obsolete
            // var aOldItems = this._getBindingItems(),
            //     oBinding = this.getBinding("items"),
            //     aNewItems = oBinding.getModel().getProperty(oBinding.getPath()) || [],
            //     aDiff = [];
            //
            // array(aOldItems).diff(aNewItems).forEach(function (oDiff) {
            //     if (array(["insert", "delete"]).contains(oDiff.type)) {
            //         aDiff.push({
            //             iIndex: oDiff.index,
            //             sType: oDiff.type,
            //             sPath: oBinding.getPath() + "/" + oDiff.index,
            //             oData: oDiff.type === "insert" ? aNewItems[oDiff.index] :
            //                 oDiff.type === "delete" && aOldItems[oDiff.index]
            //         });
            //     } else {
            //         debugger;
            //     }
            // }.bind(this));
            //
            // return aDiff;
        },

        /**
         * @summary Saves the Table's HTML elements for
         *  later referencing, e.g. for keyboard navigation
         * @returns {void}
         * @private
         * @deprecated
         */
        _saveTableItems: function () {
            // TODO: Refactor / Set obsolete
            // var aItems = array(this.getItems()),
            //     sSelector = "";
            //
            // // Create the selector string that will be used to get
            // // all the table lines as well as the desired controls
            // // (i.e. Inputs and Buttons)
            // aItems.loop(function (oItem, iItemIndex) {
            //     // Add the table line id to the selector string
            //     sSelector += iItemIndex > 0 ? "," : "";
            //     sSelector += "#" + oItem.getId();
            //     // comma separator for `OR` selection in jQuery
            //     sSelector += ",";
            //     // Keyboard elements are by default
            //     // (HTML) `Input` and `Button` controls
            //     this.getKeyboardElements().forEach(function (sElement, iIndex) {
            //         sSelector += iIndex > 0 ? "," : "";
            //         // Selector for the control in a table row
            //         sSelector += "#" + oItem.getId() + " " + sElement.trim().toLowerCase();
            //         sSelector += ",";
            //         // Selector for the control in a table sub row
            //         // (i.e. when the table has popins in responsive
            //         // mode on e.g. mobile devices
            //         sSelector += "#" + oItem.getId() + " + tr " + sElement.trim().toLowerCase();
            //     }.bind(this));
            // }.bind(this));
            //
            // // Elements to be used in getting the controls
            // var oLineElement = null,
            //     sLineId = "",
            //     iLineIndex = 0,
            //     iControlIndex = 1;
            //
            // // Save all the controls into a map
            // $(sSelector).each(function (iIndex, oControl) {
            //     // Reset the counter and id's on each new table row
            //     if (oControl.localName === "tr") {
            //         oLineElement = oControl;
            //         sLineId = oControl.id;
            //         iLineIndex += 100;
            //         iControlIndex = 1;
            //         return true;
            //     }
            //
            //     // Table rows are incremented by 100
            //     // Controls are incremented by 1
            //     iControlIndex += 1;
            //     var iIndex = iLineIndex + iControlIndex;
            //
            //     // Save the control data
            //     this._mTableElements[oControl.id] = {
            //         lineId: sLineId,
            //         line: oLineElement,
            //         controlId: oControl.id,
            //         control: oControl,
            //         index: iIndex
            //     }
            //
            //     // Save the controls again by their
            //     // keyboard navigation index
            //     this._mTableIndices[iIndex] = {
            //         controlId: oControl.id,
            //         control: oControl,
            //     }
            // }.bind(this));
        },

        /**
         * @summary Updates the merging of duplicates handling because
         *  sap.m.Table's internal handling - sometimes - results in a cluttered display,
         * @description This method is executed after every update on the table to ensure
         *  correct display of merged columns after e.g. adding or deleting rows as well as
         *  sorting or filtering values
         * @param {string} sReason
         *  the reason on why the table was updated, can be 'Change', 'Sort' or 'Filter'
         * @private
         * @deprecated
         */
        _adjustMergedDuplicatesDisplay: function (sReason) {
            // TODO: Refactor / Set obsolete
            // const sDupClassName = "sapMListTblCellDup",
            //     sDupCntClassName = "sapMListTblCellDupCnt";
            //
            // // Duplicate merging does not need to happen if the table has
            // // no items or the table has active popins (i.e. sub rows)
            // // or if there are no columns with 'mergeDuplicates' defined
            // if (
            //     this.getItems(true).length === 0 ||
            //     this.hasPopin() ||
            //     this._aMergeableColumns.length === 0
            // ) {
            //     return;
            // }
            //
            // /**
            //  * The following paragraph get the table's items and looks for each cell's values
            //  * inside a 'duplicate-merge-able' column, for the correct comparison the applicable
            //  * values of each row are stored and in the next iteration compared to the previous
            //  * values on the same cell in the applicable column; if the values match, the cell
            //  * HTML element gets a corresponding class to hide it
            //  */
            // var oModel = this.getBinding("items").getModel(),
            //     aPreviousData = [];
            //
            // // Loop over the table's items
            // array(this.getItems(true)).loop(function (aColumnInfo, oItem, iRowIndex) {
            //     var oRowData = {};
            //
            //     // Loop over the columns of interest (i.e. the ones with 'mergeDuplicates=true')
            //     aColumnInfo.forEach(function (oColumnInfo, iIndex) {
            //         var oColumn = oColumnInfo.oColumn,
            //             iColIndex = oColumnInfo.iIndex;
            //
            //         // For the evaluation to work we have to get the column's cell's value;
            //         // But since we are inside the _column_ we have to know the model attribute
            //         // identifying the cell binding; For this we re-use the 'filterItem' or
            //         // 'sortItems' attribute; if they don't exist, we quit processing on this column
            //         var oItemInfo = oColumn.getFilterItem ? oColumn.getFilterItem() :
            //             oColumn.getSortItems && oColumn.getSortItems()[0] ? oColumn.getSortItems()[0] : null;
            //         if (!oItemInfo || !oItemInfo.itemPath) {
            //             return true;
            //         }
            //
            //         // Get the item binding property out of the cell using the context path
            //         var vData = oModel.getProperty(oItem.getBindingContextPath() + "/" + oItemInfo.itemPath);
            //         if (!vData) {
            //             return true;
            //         }
            //
            //         // ...and save it for the next iteration
            //         oRowData[oItemInfo.itemPath] = vData;
            //
            //         // Get the cell and its HTML parent (which should be the <TD> element)
            //         var oCell = oItem.getCells()[iColIndex],
            //             oParent = $("#" + oCell.getId()).parent();
            //
            //         // Compare the current value to the one of the last row on the same column index
            //         if (iRowIndex >= 1 && aPreviousData[iRowIndex - 1]) {
            //             var vPrevData = aPreviousData[iRowIndex - 1][oItemInfo.itemPath],
            //                 bEquals = vData.toString() === vPrevData.toString();
            //
            //             // If they match, apply the 'duplicate' CSS classes
            //             if (bEquals) {
            //                 oCell.addStyleClass(sDupCntClassName);
            //                 oParent.addClass(sDupClassName);
            //             } else if (oCell.hasStyleClass(sDupCntClassName)) {
            //                 // If they don't match, but the cell is marked as a 'duplicate'
            //                 // remove the corresponding CSS classes
            //                 oCell.removeStyleClass(sDupCntClassName);
            //                 oParent.removeClass(sDupClassName);
            //             }
            //         }
            //     }.bind(this));
            //
            //     // Save the row data for the next iteration
            //     aPreviousData.push(oRowData);
            // }.bind(this, this._aMergeableColumns));
        },

        /**
         * @summary Returns an array of the custom Column type {ixult.m.Column}
         * @returns {ixult.m.Column[]}
         *  the array of custom Columns
         * @private
         */
        _getCustomColumns: function () {
            return array(this.getColumns()).grep(oColumn => {
                return oColumn.isA("ixult.m.Column");
            });
        },

        /**
         * @summary Applies the sorter to the table's items
         * @param {map} mParameters - a parameter map, containing the following attributes:
         * @param {object[]} mParameters.aSortItems
         *  an object list with the sort item information
         * @param {sap.ui.core.SortOrder} mParameters.sSortOrder
         *  the sort order, can be 'Ascending', 'Descending' or 'None'
         * @returns {void}
         * @public
         */
        setSorter: function (mParameters) {
            this._aSorters = [];

            // Create the model sorters from the given information
            $.each(mParameters.aSortItems, function (iIndex, oSortItem) {
                this._aSorters.push(new Sorter(
                    oSortItem.itemPath,
                    mParameters.sSortOrder ? mParameters.sSortOrder === "Descending" : false,
                    oSortItem.group !== undefined ? oSortItem.group : false
                ));
            }.bind(this));

            // Execute the sorting
            this.getBinding("items").sort(this._aSorters);
        },

        /**
         * @summary Returns the list of current table Sorters
         * @returns {sap.ui.model.Sorter[]}
         *  the array of model Sorters applied to this Table
         * @public
         */
        getSorter: function () {
            return this._aSorters;
        },

        /**
         * @summary Applies Filters to the table's items
         * @param {map} mParameters
         *  a parameter map containing the following attributes:
         * @param {object|map} mParameters.oFilterData
         *  the data object resp. map with filter input data and information
         * @param {boolean} mParameters.bReset
         *  a flag to indicate a reset of the given filter in the table
         * @returns {void}
         * @public
         */
        setFilter: function (mParameters) {
            var oFilterData = mParameters.oFilterData,
                aFilters = [];

            // Get the Column that matches the current filter
            const oColumn = this._getCustomColumns().grep(oColumn => {
                const oFilterProperty = oColumn.getProperty("filterItem");
                return oFilterProperty && oFilterProperty.itemPath &&
                    oFilterProperty.itemPath === oFilterData.Item.itemPath;
            })[0];

            // Get all other filters except for the one we want to create below
            this._aFilters = $.grep(this.getFilter(), function (oFilter) {
                return oFilter.Item.itemPath !== oFilterData.Item.itemPath;
            });
            if (!mParameters.bReset) {
                // Create the filter and add it to the filter list
                const oFilter = this._createFilter(oFilterData);
                if (oFilter) {
                    this._aFilters.push({
                        Item: oFilterData.Item,
                        Filter: oFilter
                    });
                } else if (oColumn) {
                    oColumn.setFiltered(false);
                }
            } else if (oColumn) {
                oColumn.setFiltered(false);
            }

            // Extract the Filter instances from the filter list and apply them
            array(this._aFilters).loop(function (oFilterItem) {
                aFilters.push(oFilterItem.Filter);
                // Set the Column as `filtered` to show the corresponding filter icon
                if (oColumn && !oColumn.getFiltered()) {
                    oColumn.setFiltered(true);
                }
            }.bind(this));

            // Apply the table filter
            this.getBinding("items").filter(aFilters);

            // Notify any listeners about the filter change
            this.fireFilterUpdate({
                filterData: oFilterData,
                column: oColumn
            });
        },

        /**
         * @summary Returns the list of current table Filters
         * @param {string} sItemPath
         *  an optional 'itemPath' value to retrieve a single filter item
         * @returns {
         *      Object.<{Item:object,Filter:sap.ui.model.Filter}>|
         *      Array.<{Item:object,Filter:sap.ui.model.Filter}>|
         *      null
         *  }
         *  an object list with filter information and Filter instances applied to this table
         *  or a single object thereof for a given 'sItemPath' parameter (null, if not found)
         * @public
         */
        getFilter: function (sItemPath) {
            return sItemPath ? $.grep(this._aFilters, function (oFilterItem) {
                return oFilterItem.Item && oFilterItem.Item.itemPath === sItemPath;
            }) : this._aFilters;
        },

        /**
         * @summary Creates a {sap.ui.model.Filter} based on the given data
         * @param {object} oFilterData
         *  the filter input data to analyze
         * @returns {sap.ui.model.Filter|null}
         *  the Filter to be applied to the table data (or null)
         * @private
         */
        _createFilter: function (oFilterData) {
            const oFilterItem = oFilterData.Item;

            // Create a list based filter with a custom compare function
            if (oFilterData.SelectedItems && oFilterData.SelectedItems.length > 0) {
                var aSelectedKeys = [];
                $.each(oFilterData.SelectedItems, function (iIndex, oItem) {
                    aSelectedKeys.push(oItem.Key);
                });
                return new Filter({
                    path: oFilterItem.itemPath,
                    operator: FilterOperator.EQ,
                    value1: oFilterData.InputValue || aSelectedKeys,
                    comparator: function (mParameters, vValue, vCompareValue) {
                        // Filter Date and Time values depending on their filter format
                        if (mParameters.filterFormat && array(["Date", "Time"]).contains(mParameters.itemType)) {
                            vValue = mParameters.DateHelper.format(vValue, mParameters.filterFormat);
                        }
                        const aCompareValues = Array.isArray(vCompareValue) ? vCompareValue : [vCompareValue];
                        return array(aCompareValues).contains(vValue) ? 0 : -1;
                    }.bind(this, {
                        DateHelper: DateHelper,
                        itemType: oFilterItem.itemType,
                        filterFormat: oFilterItem.filterFormat
                    })
                });
            } else if (oFilterData.InputValue) {
                return new Filter({
                    path: oFilterItem.itemPath,
                    operator: FilterOperator.Contains,
                    value1: oFilterData.InputValue
                });
            } else {
                return null;
            }
        }
    });
});
