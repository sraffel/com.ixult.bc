sap.ui.define([
    "sap/m/DatePicker",
    "sap/m/ResponsivePopover",
    "sap/m/Panel",
    "sap/ui/core/Fragment",
    "sap/ui/core/mvc/ViewType",
    "sap/ui/layout/VerticalLayout",
    "sap/ui/unified/Calendar",
    "sap/ui/unified/DateRange",
    "sap/ui/unified/DateTypeRange",
    "sap/ui/unified/CalendarDayType",
    "sap/ui/unified/CalendarLegend",
    "sap/ui/unified/CalendarLegendItem",
    "com/ixult/travel/companion/core/ui/unified/CalendarNew"
], function (
  DatePicker,
  ResponsivePopover,
  Panel,
  Fragment,
  ViewType,
  VerticalLayout,
  Calendar,
  DateRange,
  DateTypeRange,
  CalendarDayType,
  CalendarLegend,
  CalendarLegendItem,
  CalendarNew
) {
    /* jshint strict: true */
    /* global com: false */
    "use strict";
    return DatePicker.extend("ixult.m.DatePicker", {
        // The renderer to use; Important: Leave this empty,
        // so that the default Renderer is used
        renderer: {},
        // Metadata for the UI element;
        // Used to define custom properties
        metadata: {
            properties: {
                "specialDateSet": {
                    type: "object[]",
                    defaultValue: []
                },
                "specialDateTexts": {
                    type: "string"
                }
            }
        },
        // Attributes
        bIsMonthPicker: false,

        /**
         * Initializes the UI element;
         * Basically calls the super method
         * @returns {void}
         * @public
         */
        init: function () {
            DatePicker.prototype.init.apply(this, arguments);
        },

        /**
         * Creates the Calender Popup on the DatePicker
         * @returns {void}
         * @private
         */
        _createPopup: function () {
            var sDisplayFormat = this.getProperty("displayFormat").toString();
            if (sDisplayFormat.indexOf("d") === -1 && sDisplayFormat.indexOf("M") !== -1) {
                this.bIsMonthPicker = true;
            }

            // Create the calendar popup (including the legend)
            this._initCalendarPopup();

            // Init the calendar mode
            this._initCalendarIMode();

            // Check if the click happened in the calender icon
            /* if (jQuery(oEvent.target).hasClass("sapUiIcon") && this._oPopup && this._oPopup.isOpen()) {
                this._updateCalendar();
            } */
        },

        /**
         * Handles the click event in the DatePicker element;
         * Extended to format special dates on the calender control
         * @param {object} oEvent - the Javascript UI event object
         * @returns {void}
         * @protected
         */
        onclick: function () {
            if (!this._oPopup) {
                this._createPopup();
            }

            // Call super method
            if (DatePicker.prototype.onclick) {
                DatePicker.prototype.onclick.apply(this, arguments);
            }
        },

        /**
         * Initializes the calendar popup for the DatePicker UI element;
         * Adds custom dates to the calendar and displays a legend
         * @returns {sap.m.ResponsivePopover} the Calender Popover UI control (for method chaining)
         * @private
         */
        _initCalendarPopup: function () {
            // ------------------------------------------------------------ SRL,2020-08-25 >>>
            // Check for an existing Calender Popup for the current field
            // (e.g. when the view was not destroyed before)
            this._oPopup = sap.ui.getCore().byId(this.getId() + "-RP");
            if (this._oPopup) {
                this._oPopup.destroyContent();
            } else {
                this._oPopup = new ResponsivePopover(this.getId() + "-RP", {
                    showCloseButton: false,
                    showArrow: false,
                    showHeader: false,
                    placement: "Bottom" //,
                    // afterOpen: [this._handleOpened, this]
                }).attachAfterOpen(this._handleOpened, this).addStyleClass("sapMRPCalendar");
            }
            // ------------------------------------------------------------ SRL,2020-08-25 <<<

            // jQuery.sap.require("sap.ui.core.Popup");
            var oPopupPanel = new Panel({
                content: [new VerticalLayout({
                    content: [this._createCalendar(), this._createCalendarLegend()]
                })]
            }).addStyleClass("calendarPopupPanel");
            this._oPopup.addContent(oPopupPanel);

            return this._oPopup;
        },

        /**
         * Creates the Calendar UI element of the popup
         * @returns {sap.ui.unified.Calendar} the Calendar UI element (for method chaining)
         * @private
         */
        _createCalendar: function () {
            if (this.bIsMonthPicker) {
                this._oCalendar = new CalendarNew(this.getId() + "-calNew", {
                    intervalSelection: this._bIntervalSelection
                });
            } else {
                this._oCalendar = new Calendar(this.getId() + "-cal", {
                    intervalSelection: this._bIntervalSelection
                });
            }

            // Setup the date range
            this._oDateRange = new DateRange();
            this._oCalendar.addSelectedDate(this._oDateRange);

            // Add the calendar events
            this._oCalendar.attachSelect(this._selectDate, this);
            this._oCalendar.attachCancel(this._cancel, this);
            if (this.bIsMonthPicker) {
                this._oCalendar.attachEvent("_renderMonth", this._resizeCalendar, this);
            }

            // Adjust the styles
            if (this.$().closest(".sapUiSizeCompact").length > 0) {
                this._oCalendar.addStyleClass("sapUiSizeCompact");
            }

            this._oCalendar.setPopupMode(true);
            this._oCalendar.setParent(this, "content", true); // don't invalidate DatePicker

            if (this.bIsMonthPicker) {
                // var oHeader = this._oCalendar.getAggregation("header");
                // oHeader.attachEvent("pressButton1", this._handleButton1, this);
                var oMonthPicker = this._oCalendar.getAggregation("monthPicker");
                oMonthPicker.attachEvent("select", this._handleSelectMonth, this);
            }

            // Apply the special date set to colorize existing dates
            this._applySpecialDates();

            return this._oCalendar;
        },

        /**
         * Creates a legend for the calendar including the special dates
         * @returns {sap.ui.core.Fragment} the Calendar Legend UI element (for method chaining)
         * @private
         */
        _createCalendarLegend: function () {
            var aSpecialDates = this.getProperty("specialDateSet") || [];
            var sDateTexts = this.getProperty("specialDateTexts");
            var aTexts = [];
            var bHasRequests = false;
            var bHasExpenses = false;
            var bHasCashExpenses = false;

            if (sDateTexts) {
                aTexts = sDateTexts.toString().split(",");
                for (var i = 0; i < aSpecialDates.length; i++) {
                    bHasRequests = (aSpecialDates[i].Request === true) ? true : bHasRequests;
                    bHasExpenses = (aSpecialDates[i].Expenses === true && !aSpecialDates[i].IsCashExpense === false) ?
                      true : bHasExpenses;
                    bHasCashExpenses = (aSpecialDates[i].Expenses === true && !aSpecialDates[i].IsCashExpense === true) ?
                      true : bHasCashExpenses;
                }
            }

            this._oCalendarLegend = new Fragment({
                fragmentName: "com.ixult.travel.companion.view.CalendarLegend",
                type: ViewType.XML
            });

            this._oCalendarLegend.setModel(new sap.ui.model.json.JSONModel({
                RequestText: aTexts[0] && aTexts[0].length > 0 ? aTexts[0] : "-",
                RequestVisible: bHasRequests && aTexts[0].length > 0,
                ExpenseText: aTexts[1] && aTexts[1].length > 0 ? aTexts[1] : "-",
                ExpenseVisible: bHasExpenses && aTexts[1].length > 0,
                CashExpenseText: aTexts[2] && aTexts[2].length > 0 ? aTexts[2] : "-",
                CashExpenseVisible: bHasCashExpenses && aTexts[2].length > 0
            }), "model");

            return this._oCalendarLegend;
        },

        /**
         * Modifies the calendar popup element in the DatePicker control to apply special dates;
         * Reads the given array of trips and sets up the day type style for requests and expenses
         * @returns {void}
         * @private
         */
        _applySpecialDates: function () {
            var aSpecialDates = this.getProperty("specialDateSet") || [];
            var i = 0;

            // we need to remove what's added already to avoid duplicates:
            this._oCalendar.removeAllSpecialDates();

            // Apply the styles for travel requests
            var aRequests = $.grep(aSpecialDates, function (oItem) {
                return oItem.Request === true;
            }) || [];
            for (i = 0; i < aRequests.length; i++) {
                this._applySpecialDate({
                    Data: aRequests[i],
                    Type: sap.ui.unified.CalendarDayType.Type01
                });
            }

            // Apply the styles for cash expenses
            var aCashExpenses = $.grep(aSpecialDates, function (oItem) {
                return oItem.Expenses === true && oItem.IsCashExpense === true;
            }) || [];
            for (i = 0; i < aCashExpenses.length; i++) {
                this._applySpecialDate({
                    Data: aCashExpenses[i],
                    Type: sap.ui.unified.CalendarDayType.Type03
                });
            }

            // Apply the styles for trip expenses
            var aExpenses = $.grep(aSpecialDates, function (oItem) {
                return oItem.Expenses === true && oItem.IsCashExpense === false;
            }) || [];
            for (i = 0; i < aExpenses.length; i++) {
                this._applySpecialDate({
                    Data: aExpenses[i],
                    Type: sap.ui.unified.CalendarDayType.Type02
                });
            }
        },

        /**
         * Applies "Datedep" and "Datearr" field values of a single trip object
         * as special DateRange object to the calendar
         * @param {map} mParameters - a parameter map containing the following attributes:
         * @param {object} mParameters.Data - the data object featuring the "Datedep" and "Datearr" fields
         * @param {sap.ui.unified.CalendarDayType} mParameters.Type - a calendar legend type
         * @returns {void}
         * @private
         */
        _applySpecialDate: function (mParameters) {
            var dStartDate = mParameters.Data.Datedep;
            var dEndDate = mParameters.Data.Datearr;
            var oDateTimeRange = new DateTypeRange({
                type: mParameters.Type,
                startDate: dStartDate,
                endDate: dEndDate
            });

            this._oCalendar.insertSpecialDate(oDateTimeRange);
        },

        /**
         * Handles the open event of the calender popup; Basically includes the
         * important code of the function found in {sap.m.DatePicker}
         * @returns {void}
         * @private
         */
        _handleOpened: function () {
            if (this._oCalendar) {
                this._renderedDays = this._oCalendar.$("-Month0-days").find(".sapUiCalItem").length;
                this._oCalendar.focus();
            }
            if (this.bIsMonthPicker) {
                this._showMonthPicker();
            }
        },

        /**
         * Handles the selection of a month in the MonthPicker UI part
         * @returns {void}
         * @private
         */
        _handleSelectMonth: function () {
            if (this.bIsMonthPicker) {
                this._showMonthPicker();
                var iMonth = this._oCalendar.mAggregations.monthPicker.getMonth();
                var iYear = this._oCalendar.mAggregations.header.getTextButton2();
                this.setDateValue(new Date(iYear, iMonth, 1));
                this._oPopup.close();
            }
        },

        /**
         * Cancels the calendar and closes the popup window;
         * Basically a copy of the function found in {sap.m.DatePicker}
         * @returns {void}
         * @private
         */
        _cancel: function () {
            if (this._oPopup && this._oPopup.isOpen()) {
                this._oPopup.close();
                this._bFocusNoPopup = true;
                this.focus();
            }
        },

        /**
         * Resizes the calender according to the rendered days;
         * Basically a copy of the function found in {sap.m.DatePicker}
         * @param {object|jQuery.Event|sap.ui.base.Event} oEvent the UI event object
         * @returns {void}
         * @private
         */
        _resizeCalendar: function (oEvent) {
            if (!this.bIsMonthPicker) {
                var iDays = oEvent.getParameter("days");
                if (iDays > this._renderedDays) {
                    // calendar gets larger, so it could move out of the page -> reposition
                    this._renderedDays = iDays;
                    this._oPopup._applyPosition(this._oPopup._oLastPosition);
                }
            }
        },

        /**
         * Shows the MonthPicker UI part of the calendar element;
         * Used to override the standard calendar display when
         * only a month should be selected
         * @returns {void}
         * @private
         */
        _showMonthPicker: function () {
            var oMonthPicker = this._oCalendar.getAggregation("monthPicker");
            var oRm = sap.ui.getCore().createRenderManager();
            var $Container = this._oCalendar.$("content");
            oRm.renderControl(oMonthPicker);
            oRm.flush($Container[0], false, true); // insert it
            oRm.destroy();
        },

        /**
         * Changes _IMode property depending on the "bIsMonthPicker" value;
         * Change to "1" for start with MohthPicker; Only a month should be selected
         * @returns {void}
         * @private
         */
        _initCalendarIMode: function () {
            if (this.bIsMonthPicker) {
                this._oCalendar._iMode = 1;
            }
        }

    });
});
