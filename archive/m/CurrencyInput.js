/* jshint strict: true */
/* global com: false */
"use strict";

jQuery.sap.declare("ixult.m.CurrencyInput");
jQuery.sap.require("sap.m.Input");

sap.m.Input.extend("ixult.m.CurrencyInput", {
    // The renderer to use; Important: Leave this empty,
    // so that the default Renderer is used
    renderer: {},
    // Metadata for the UI element;
    // Used to define custom properties
    metadata: {
        properties: {}
    },

    /**
     * Initializes the UI element
     * @returns {void}
     * @protected
     */
    init: function () {
        sap.m.Input.prototype.init.apply(this, arguments);
    },

    /**
     * Called when the Control has been rendered (so its HTML is part of the document).
     * Post-rendering manipulations of the HTML could be done here.
     * This hook is the same one that SAPUI5 controls get after being rendered.
     * @param {sap.ui.base.Event} oEvent the UI event object
     * @returns {void}
     * @protected
     */
    onAfterRendering: function () {
        sap.m.Input.prototype.onAfterRendering.apply(this, arguments);
        this.attachSuggestionItemSelected(null, this.onSuggestionItemSelected, this);

        if (this.getProperty("showSuggestion")) {
            // Set the suggestion to start everytime on value help only inputs
            this.setStartSuggestion(0);
            this.setFilterSuggests(false);

            if (this._oSuggestionPopup) {
                this._oSuggestionPopup.addStyleClass("inputSuggestionList");
            }

            // The suggestion shows up at clicking on the input field
            // (includes also inputs with value help only)
            this.ontap = function (oEvent) {
                if (oEvent.target.id !== this.getId() + "__vhi" && this.getShowSuggestion() && this._oSuggestionPopup) {
                    this._triggerSuggest();
                } else if (oEvent.target.id === this.getId() + "__vhi") {
                    sap.m.Input.prototype.ontap.apply(this, arguments);
                }
            };
        }
    },

    /**
     * Handles the suggestion click event in the input field
     * @param {sap.ui.base.Event} oEvent the UI event object
     * @returns {void}
     * @protected
     */
    onSuggestionItemSelected: function (oEvent) {
        var oItem = oEvent.getParameter("selectedItem");
        var sCurrency = oItem.getProperty("key");
        var sCurrText = oItem.getProperty("text");
        sCurrText = sCurrText.toString().split(" - ")[1].trim();

        var aCustomData = this.getCustomData();
        for (var i = 0; i < aCustomData.length; i++) {
            switch (aCustomData[i].mProperties.key) {
                case "RecCurr":
                    aCustomData[i].mProperties.value = sCurrency;
                    aCustomData[i].mBindingInfos.value.binding.setValue(sCurrency);
                    break;
                case "RecCurrName":
                    aCustomData[i].mProperties.value = sCurrText;
                    aCustomData[i].mBindingInfos.value.binding.setValue(sCurrText);
                    break;
            }
        }
    }

});