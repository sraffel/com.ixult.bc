sap.ui.define([
    "sap/m/Table",
    "ixult/base/assert",
    "ixult/base/util/LibraryHelper",
    "ixult/ui/core/theming/ThemeHelper"
], function (
    Table,
    assert,
    LibraryHelper,
    ThemeHelper
) {
    "use strict";
    const Library = LibraryHelper.register("ixult/m");

    /**
     * Responsive Table with additional capabilities, like sortable and filterable Columns
     * @class
     * @name ixult.m.Table
     * @extends sap.m.Table
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.m.Table sap.m.Table}
     * @public
     */
    return Table.extend("ixult.m.Table", /** @lends ixult.m.Table */ {
        // The renderer to use; Important: Leave this empty,
        // so that the default Renderer is used
        renderer: {},

        // Metadata for the UI element;
        // Used to define custom properties
        metadata: {
            properties: {
                stickyHeader: {
                    type: "boolean",
                    defaultValue: true
                },
                stickyFooter: {
                    type: "boolean",
                    defaultValue: true
                }
            }
        },

        // Attributes
        _oView: null,
        _oController: null,
        _bInitialized: false,
        _vFilters: null,
        _vSorters: null,

        /**
         * Handles the 'beforeRendering' Event for the UI Control,
         * that is executed when the DOM elements are created but not yet shown
         * @param {sap.ui.base.Event} oEvent - the UI event instance
         * @returns {void}
         * @protected
         */
        onBeforeRendering: function (oEvent) {
            // This has to be done only once, this is to avoid re-setup of the Control
            // because after executing sorting on the Table, the bound controls seem to rerender
            if (!this._bInitialized) {
                Table.prototype.onBeforeRendering.apply(this, arguments);
                try {
                    // Save View and Controller instance for later access
                    this._oView = Library.getViewFor(this);
                    if (this._oView && this._oView.getController) {
                        this._oController = this._oView.getController();
                    }

                    // Setup custom CSS classes for this Control
                    this.addStyleClass("ixultMTable");
                    Library.loadStyleSheet();

                    // Call a 'beforeRendering' handler on each applicable
                    // custom Column because this Element has no such Event
                    $.each(this._getCustomColumns(), function (iIndex, oColumn) {
                        oColumn.onBeforeTableRendering(this);
                    }.bind(this));
                } catch (oException) {
                    console.error("Error during rendering of Control '" + this.getId() + "'", oException);
                }
                this._bInitialized = true;
            }
        },

        /**
         * Handles the 'afterRendering' Event for the UI Control,
         * that is executed when the Control is completely rendered and shown
         * @param {sap.ui.base.Event} oEvent - the UI event instance
         * @returns {void}
         * @protected
         */
        onAfterRendering: function (oEvent) {
            Table.prototype.onAfterRendering.apply(this, arguments);
            this._setupTable();

            // Call an 'afterRendering' handler on each applicable
            // custom Column because this Element has no such Event
            $.each(this._getCustomColumns(), function (iIndex, oColumn) {
                oColumn.onAfterTableRendering(this);
            }.bind(this));
        },

        /**
         * Returns an array of the custom Column type {ixult.m.Column}
         * @returns {ixult.m.Column[]}
         *  the array of custom Columns
         * @private
         */
        _getCustomColumns: function () {
            return $.grep(this.getColumns(), function (oColumn) {
                return oColumn.isA("ixult.m.Column");
            });
        },

        /**
         * Initializes and enhances the Table Control
         * @returns {void}
         * @private
         */
        _setupTable: function () {
            // Setup a sticky Table Header using a specialized custom property
            if (this.getStickyHeader()) {
                this.setSticky([sap.m.Sticky.ColumnHeaders]);
            }

            // Setup the Table Footer
            var oTableFooterElement = $("#" + this.getId() + " .sapMListTblFooter");
            if (oTableFooterElement && oTableFooterElement.attr) {
                var sClasses = oTableFooterElement.attr("class");
                sClasses += " ixultMFooter";
                sClasses += this.getStickyFooter() ? " ixultMStickyFooter" : "";
                oTableFooterElement.attr("class", sClasses);
            }
        },

        /**
         * Applies Sorters to the table's items
         * @param {sap.ui.model.Sorter|sap.ui.model.Sorter[]} vSorters
         *  the Sorter instance or an array of Sorters
         * @returns {void}
         * @public
         */
        setSorter: function (vSorters) {
            // Save the sorters, e.g. so that they can later be reapplied after table update
            this._vSorters = vSorters;

            // Execute the sort
            this.getBinding("items").sort(vSorters);
        },

        /**
         * Applies Filters to the table's items
         * @param {sap.ui.model.Filter|sap.ui.model.Filter[]} vFilters
         *  the Filters instance or an array of Filters
         * @returns {void}
         * @public
         */
        setFilter: function (vFilters) {
            // Save the sorters, e.g. so that they can later be reapplied after table update
            this._vFilters = Filters;

            // Execute the sort
            this.getBinding("items").filter(vFilters);
        }

    });
});


