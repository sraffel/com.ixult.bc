sap.ui.define([
    "sap/m/Column",
    "sap/ui/core/CustomData",
    "sap/ui/core/Fragment",
    "sap/ui/core/syncStyleClass",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    "sap/ui/model/Sorter",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/resource/ResourceModel",
    "ixult/base/assert",
    "ixult/base/util/DateHelper",
    "ixult/base/util/LibraryHelper"
], function (
    Column,
    CustomData,
    Fragment,
    syncStyleClass,
    Filter,
    FilterOperator,
    Sorter,
    JSONModel,
    ResourceModel,
    assert,
    DateHelper,
    LibraryHelper
) {
    "use strict";
    const Library = LibraryHelper.register("ixult/m");

    /**
     * Column for the responsive ixult.m.Table Control with
     * additional properties to enable or disable sorting and filtering
     * @class
     * @name ixult.m.Column
     * @extends sap.m.Column
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.m.Column sap.m.Column}
     * @see {ixult.m.Table}
     * @public
     */
    return Column.extend("ixult.m.Column", /** @lends ixult.m.Column */ {

        // Metadata for the UI element;
        // Used to define custom properties
        metadata: {
            properties: {
                sortable: {
                    type: "boolean",
                    defaultValue: true
                },
                sortItems: {
                    type: "object"
                },
                filterable: {
                    type: "boolean",
                    defaultValue: true
                },
                filterItems: {
                    type: "object"
                },
                filterOptions: {
                    type: "object"
                },
                headerAlign: {
                    type: "sap.ui.core.TextAlign",
                    defaultValue: "Center"
                },
                headerPopupHeight: {
                    type: "string",
                    defaultValue: "25%"
                }
            }
        },

        // Attributes
        _oView: null,
        _oTable: null,
        _oPopover: null,
        _sSortOrder: undefined,
        _bIsFiltered: false,

        /**
         * Creates a new ixult.m.Column instance
         * @constructor ixult.m.Column
         * @param {string} [sId]
         *  id for the new control, generated automatically if no id is given
         * @param {object} [mSettings]
         *  initial settings for the new control
         * @returns {void}
         * @public
         */
        constructor: function (sId, mSettings) {
            Column.prototype.constructor.apply(this, arguments);
            this._evaluateProperties();
            this.setColumnData({
                Filtered: this.getFiltered().toString(),
                SortOrder: this.getSortOrder().toLowerCase()
            });
        },

        /**
         *
         * @private
         */
        _evaluateProperties: function () {
            var oFilterOptions = this.getFilterOptions(),
                vSortItems = this.getSortItems();

            // Check 'filterOptions' if the Column is 'filterable'
            if (this.getFilterable()) {
                assert(oFilterOptions && oFilterOptions.itemPath,
                    "Can't filter column '" + this.getId() + "' without 'filterOptions'.")
            }

            // Check 'sortItems' if the Column is 'sortable'
            if (this.getSortable()) {
                // Use the 'filterOptions' property's attributes like 'itemPath'
                // if this Column is sortable, but no sortItems were given
                if (!vSortItems && this.getFilterable() && oFilterOptions && oFilterOptions.itemPath) {
                    this.setSortItems({
                        itemPath: oFilterOptions.itemPath,
                        itemType: oFilterOptions.itemType || "String"
                    });
                }
                this._sSortOrder = this.getSortOrder();
                this._applySort({
                    sSortOrder: this._sSortOrder
                })
            }
        },

        /**
         * Called during 'beforeRendering' of an applicable {ixult.m.Table};
         * allows for custom processing before rendering of this Column
         * because there's no such event on this Element
         * @param {ixult.m.Table} oTable - the Table Control
         * @returns {void}
         * @public
         */
        onBeforeTableRendering: function (oTable) {
            this._oView = Library.getViewFor(this);
            this._oTable = oTable;

            // Add CustomData to the Column to set data model properties
            // into the DOM element that can be evaluated to apply styles
            this.addCustomData(new CustomData({
                key: "ui-filtered",
                writeToDom: true
            }).bindProperty("value", {
                path: "ColumnData>/Filtered"
            }));
            this.addCustomData(new CustomData({
                key: "ui-sorted",
                writeToDom: true
            }).bindProperty("value", {
                path: "ColumnData>/SortOrder"
            }));
        },

        /**
         * Called during 'afterRendering' of an applicable {ixult.m.Table};
         * allows for custom processing after rendering of this Column
         * because there's no such event on this Element
         * @param {ixult.m.Table} oTable - the Table Control
         * @returns {void}
         * @public
         */
        onAfterTableRendering: function (oTable) {
            var oColumnElement = $("#" + this.getId());

            // Setup custom CSS classes for the Column
            this._addStyleClass(oColumnElement, [
                "ixultMColumn",
                this.getSortable() ? "ixultMSortableColumn" : "",
                this.getFilterable() ? " ixultMFilterableColumn" : ""
            ]);
            if (this.getHeader()) {
                this._addStyleClass($("#" + this.getId() + " > [class*='sapMColumnHeader']"), [
                    "ixultMColumnHeader",
                    "ixultMColumnHeader" + this.getHeaderAlign()
                ]);
            }

            // Attach a 'click' Event to the Column Header if the conditions apply, i.e.
            // (1) only works with custom Table (2) has to have a header Control and
            // (3) needs to be filter- or sortable
            if (this._oTable && this._oTable.isA("ixult.m.Table") &&
                oColumnElement && this.getHeader() &&
                (this.getSortable() || this.getFilterable())
            ) {
                oColumnElement.click(function (oEvent) {
                    this.onHeaderClick(oEvent);
                }.bind(this));
            }
        },

        /**
         * Adds style classes to a given HTML element
         * @param {jQuery.Selector|Q.fn.init} oElement
         *  the jQuery selector for the HTML element that should be enhanced
         * @param {string|string[]} vClass
         *  the style class or array of classes to be applied
         *  @returns {void}
         * @private
         */
        _addStyleClass: function (oElement, vClass) {
            // Check that the Element was found and the style classes are given
            if (!(vClass && oElement && oElement.length && oElement.length > 0 && oElement.attr)) {
                return;
            }

            // Get the current element 'class' attribute string
            // and evaluate the given class(es)
            var sClassString = oElement.attr("class"),
                aClasses = $.grep(typeof vClass === "string" ? [vClass] :
                    $.isArray(vClass) ? vClass : [], function (sClass) {
                    return sClass && sClass.length > 0;
                }.bind(this));

            // Add the style classes to the class string
            $.each(aClasses, function (iIndex, sClass) {
                if (sClassString.length > 0) {
                    sClassString += " ";
                }
                sClassString += sClass;
            }.bind(this));

            // Overwrite the attribute inside the HTML element
            oElement.attr("class", sClassString);
        },

        /**
         * Sets the given data object into the 'ColumnData' JSONModel
         * that allows for applying custom data to this Column
         * @param {object} oData - the custom Data to set
         * @returns {void}
         * @protected
         */
        setColumnData: function (oData) {
            if (!this.getModel("ColumnData")) {
                this.setModel(new JSONModel({}), "ColumnData");
            }
            this.getModel("ColumnData").setData(oData, true);
        },

        /**
         * Returns the custom data object for this Column
         * @returns {object} the custom data stored in the 'ColumnData' model
         * @protected
         */
        getColumnData: function () {
            this.getModel("ColumnData").getData();
        },

        /**
         * Returns the property 'filterOptions' for this Column;
         * overridden to include certain checks and postprocessing
         * @returns {{itemType: string, formatInfo: string, itemPath: string, formatText: string}|null}
         *  the 'filterOptions' data for this Column (or null if not found)
         * @public
         * @override
         */
        getFilterOptions: function () {
            var oFilterOptions = this.getProperty("filterOptions");

            // Check for required attributes
            if (!assert(oFilterOptions && oFilterOptions.itemPath,
                "Can't filter column '" + this.getId() + "' without 'filterOptions'.")) {
                return null;
            }

            // Adjust unset but optional attributes
            oFilterOptions.itemType = oFilterOptions.itemType || "String";

            return oFilterOptions;
        },

        /**
         * Returns the current sort order for this Column
         * @returns {sap.ui.core.SortOrder}
         *  the sort order, can be 'Ascending', 'Descending' or 'None'
         * @protected
         */
        getSortOrder: function () {
            if (!this._sSortOrder) {
                var vSortItems = this.getSortItems(),
                    sDefaultOrder = "None";
                if (vSortItems && $.isArray(vSortItems)) {
                    var aItems = $.grep(vSortItems, function (oItem) {
                        return oItem.sortOrder !== undefined;
                    }) || [];
                    this._sSortOrder = aItems.length > 0 ? aItems[0].sortOrder : sDefaultOrder;
                } else if (vSortItems) {
                    this._sSortOrder = vSortItems.sortOrder || sDefaultOrder;
                } else {
                    // Fallback
                    this._sSortOrder = sDefaultOrder;
                }
            }
            return this._sSortOrder;
        },

        /**
         * Sets this Column as 'filtered', i.e. changes
         * the corresponding ColumnData property
         * which results in a changed DOM property which
         * shows or hides a Filter Icon in the Column Header
         * @param {boolean} bFiltered
         *  true if the Column is filtered, false otherwise
         * @returns {void}
         * @public
         */
        setFiltered: function (bFiltered) {
            this._bIsFiltered = bFiltered;

            // Update the ColumnData property
            this.setColumnData({
                Filtered: bFiltered.toString()
            });
        },

        /**
         * Returns the current filter state
         * @returns {boolean}
         *  true, is the Column is currently filtered
         * @public
         */
        getFiltered: function () {
            return this._bIsFiltered;
        },

        /**
         * Handles the jQuery/JavaScript 'click' Event on the Column Header
         * @param {jQuery.Event} oEvent
         *  the jQuery 'click' Event
         * @returns {void}
         * @protected
         */
        onHeaderClick: function (oEvent) {
            this._createColumnHeaderPopover(oEvent).then(function (oPopover) {
                oPopover.openBy(this.getHeader());
            }.bind(this));
        },

        /**
         * Creates the Popover that is displayed by the Column
         * @param {jQuery.Event} oEvent
         *  the jQuery 'click' Event
         * @returns {Promise}
         *  a Promise handler with 'resolve' ('then') and ('reject') 'fail' handlers
         *  to be executed after the Popover has been retrieved or created;
         * @private
         */
        _createColumnHeaderPopover: function (oEvent) {
            var oDeferred = $.Deferred();

            // Create or retrieve the Column Header Popover Control
            if (!this._oPopover) {
                Fragment.load({
                    id: this.getHeader().getId() + "--popover",
                    name: "ixult.m.ColumnHeaderPopover",
                    type: "XML",
                    controller: {
                        onBeforePopoverOpen: this.onBeforePopoverOpen.bind(this),
                        onAfterPopoverOpen: this.onAfterPopoverOpen.bind(this),
                        onSortButtonPress: this.onSortButtonPress.bind(this),
                        onFilterResetButtonPress: this.onFilterResetButtonPress.bind(this),
                        onFilterSearchInputChange: this.onFilterSearchInputChange.bind(this),
                        onFilterSelectAllButtonToggle: this.onFilterSelectAllButtonToggle.bind(this),
                        onFilterListItemPress: this.onFilterListItemPress.bind(this),
                        onPopoverOkayButtonPress: this.onPopoverOkayButtonPress.bind(this),
                        onPopoverCancelButtonPress: this.onPopoverCancelButtonPress.bind(this)
                    }
                }).then(function (oFragment) {
                    this._oPopover = oFragment;
                    oDeferred.resolve(oFragment);
                }.bind(this));
            } else {
                oDeferred.resolve(this._oPopover);
            }

            return oDeferred.promise();
        },

        /**
         * Handles the 'beforeOpen' Event on the Column's Header Popover
         * @param {sap.ui.base.Event} oEvent - the UI event instance
         * @returns {void}
         * @protected
         */
        onBeforePopoverOpen: function (oEvent) {
            // Set the Popover data
            this._oPopover.setModel(new JSONModel({
                Sortable: this.getSortable(),
                SortOrder: this.getSortOrder(),
                Filterable: this.getFilterable(),
                FilterSearchValue: "",
                FilterSearchInvert: false,
                FilterSelectAll: true,
                FilterItems: this._getFilterListItems({
                    bFilterSelectAll: true
                }),
                HeaderPopupHeight: this.getHeaderPopupHeight()
            }), "PopupData");

            // Set the texts for the Popover
            this._oPopover.setModel(new ResourceModel({
                bundle: Library.getResourceBundle()
            }), "i18n");
        },

        /**
         * Handles the 'afterOpen' Event on the Column's Header Popover
         * @param {sap.ui.base.Event} oEvent - the UI event instance
         * @returns {void}
         * @protected
         */
        onAfterPopoverOpen: function (oEvent) {
            // Set the initial focus into the filter search Input (if found)
            var oSelector = $("#" + this._oPopover.getId() + " [data-ui-id='filterSearchInput']"),
                sElementId = oSelector && oSelector[0] ? oSelector[0].id : null,
                oControl = sElementId ? sap.ui.getCore().byId(sElementId) : null;
            if (oControl && oControl.isA("sap.m.Input")) {
                oControl.focus();
            }
        },

        /**
         * Returns a list of filterable values for this Column
         * @param {map} mParameters
         *  a parameter map, containing the following attributes
         * @param {boolean} [mParameters.bFilterSelectAll=true]
         *  sets all items selected by default if true
         * @returns {object[]}
         *  the item list of filterable values
         * @private
         */
        _getFilterListItems: function (mParameters) {
            var oFilterOptions = this.getFilterOptions();
            if (!oFilterOptions) {
                return [];
            }

            var oBinding = this._oTable.getBinding("items"),
                sArrayName = oBinding.getPath().replace("/", ""),
                oModel = oBinding.getModel(),
                oData = oModel.getData(),
                aResults = [],
                bFilterSelectAll = mParameters && mParameters.bFilterSelectAll !== undefined ?
                    mParameters.bFilterSelectAll : true;

            // Get the 'results' set containing the table values
            if (!assert(oData.hasOwnProperty(sArrayName) && $.isArray(oData[sArrayName]),
                "No filterable result set found.")) {
                return [];
            }
            aResults = oData[sArrayName];

            // Extract the filter values using the 'itemPath' property
            var aFilterValues = [],
                aFilterItems = [];
            $.each(aResults, function (iIndex, oResult) {
                // Get the value by context binding (that's why we need the item path)
                var vValue = oModel.getProperty("/" + sArrayName + "/" + iIndex + "/" + oFilterOptions.itemPath),
                    oFilterItem = this._createFilterListItem({
                        vValue: vValue,
                        oOptions: oFilterOptions,
                        bSelected: bFilterSelectAll
                    });
                // Add the value only, if it does not exist
                var bExists = $.grep(aFilterValues, function (vFilterValue) {
                    return oFilterOptions.itemType === "Time" ?
                        vFilterValue.getTime() === oFilterItem.Value.getTime() :
                        vFilterValue.toString() === oFilterItem.Value.toString();
                }.bind(this)).length !== 0;
                if (!bExists) {
                    aFilterValues.push(oFilterItem.Value);
                    aFilterItems.push(oFilterItem);
                }
            }.bind(this));

            // Return the list of filter items containing 'Value' and 'Text'
            return aFilterItems;
        },

        /**
         * Creates and filter item to apply to the Popover's filter list
         * @param {map} mParameters
         *  a parameter map containing the following attributes:
         * @param {string|Number|Date|Time} mParameters.vValue
         *  the filter value
         * @param {object} [mParameters.oOptions]
         *  the 'filterOptions' property
         * @param {boolean} [mParameters.bSelected=false]
         *  sets the item to selected if true
         * @returns {{Value: *, Text: string, Info: string, Selected: boolean}}
         *  the filter item map to be used with the filter list
         * @private
         */
        _createFilterListItem: function (mParameters) {
            var sText = "",
                sInfo = "",
                vValue = mParameters.vValue,
                oOptions = mParameters.oOptions || this.getFilterOptions(),
                bSelected = mParameters.bSelected !== undefined ? mParameters.bSelected : false;

            // Set the day inside of values of type 'Time' to a fixed value
            // so that only distinct time values remain for filtering
            if (oOptions.itemType === "Time") {
                const mTime = DateHelper.toMap(vValue);
                vValue = DateHelper.toDate({
                    iYear: mTime.iYear,
                    iMonth: mTime.iMonth,
                    iDay: 1,
                    iHours: mTime.iHours,
                    iMinutes: mTime.iMinutes
                });
            }

            // Format the filter list values according to the
            // 'itemType' and optionally a given 'filterFormat'
            switch (oOptions.itemType) {
                case "Date":
                case "Time":
                    // 'formatText' and 'formatInfo' can be date or time patterns like 'dd.MM.yyyy', 'HH:mm' etc.
                    sText = oOptions.formatText ? DateHelper.format(vValue, oOptions.formatText) : vValue.toString();
                    sInfo = oOptions.formatInfo ? DateHelper.format(vValue, oOptions.formatInfo) : "";
                    break;
                case "Number":
                case "String":
                default:
                    sText = vValue.toString().trim();
                    break;
            }

            // Return the value and texts inside a map
            return {
                Value: vValue,
                Text: sText,
                Info: sInfo,
                Selected: bSelected
            };
        },

        /**
         * Handles the 'press' event on one of the 'Sort' Buttons
         * @param {sap.ui.base.Event} oEvent - the UI event instance
         * @returns {void}
         * @protected
         */
        onSortButtonPress: function (oEvent) {
            this._applySort({
                sSortOrder: oEvent.getSource().data("SortOrder")
            });
        },

        /**
         * Update the sort order, the dependent UI settings and set the sorter into the table
         * @param {{sSortOrder}} mParameters
         *  a parameter map containing the following attributes:
         * @param {sap.ui.core.SortOrder} mParameters.sSortOrder
         *  the sort order to set, can be 'Ascending', 'Descending' or 'None'
         * @returns {void}
         * @private
         */
        _applySort: function (mParameters) {
            this._sSortOrder = mParameters && mParameters.sSortOrder ?
                mParameters.sSortOrder : this._sSortOrder;

            // Update the sort indicator and data model properties
            this.setSortIndicator(this._sSortOrder);
            this.setColumnData({
                SortOrder: this._sSortOrder.toLowerCase()
            });
            if (this._oPopover) {
                this._oPopover.getModel("PopupData").setData({
                    SortOrder: this._sSortOrder
                }, true);
            }

            // Return if there's no need for actual sorting to be done
            if (this._sSortOrder === "None") {
                return;
            }

            // Reset the sort order for every other applicable {ixult.m.Column}
            $.each(this._oTable.getColumns(), function (iIndex, oColumn) {
                if (oColumn.isA("ixult.m.Column") && oColumn !== this) {
                    oColumn.setSortOrder("None");
                }
            }.bind(this));

            var vSortItems = this.getSortItems(),
                vSorters = null;
            if ($.isArray(vSortItems)) {
                vSorters = [];
                $.each(vSortItems, function (iIndex, oSortItem) {
                    vSorter.push(this._createItemSorter(oSortItem));
                }.bind(this));
            } else if (!$.isEmptyObject(vSortItems)) {
                vSorters = this._createItemSorter(vSortItems);
            }

            // Apply the sort order to the Table
            if (this._oTable.isA("ixult.m.Table")) {
                // Set the Sorters into an {ixult.m.Table} to allow further processing
                this._oTable.setSorter(vSorters);
            } else if (this._oTable.isA("sap.m.Table")) {
                // Or apply the sorting directly if it is a standard sap.m.Table
                this._oTable.getBinding("items").sort(vSorters);
            }
        },

        /**
         * Returns a Sorter instance using the given sort item attributes
         * @param {object} oSortItem
         *  the sort item data
         * @returns {sap.ui.model.Sorter}
         *  the model Sorter instance
         * @private
         */
        _createItemSorter: function (oSortItem) {
            return new Sorter(
                oSortItem.itemPath,
                this._sSortOrder === "Descending",
                oSortItem.group !== undefined ? oSortItem.group : false
            );
        },

        /**
         * Handles the 'press' event on the 'clear filter' Button in the Header Popover
         * @param {sap.ui.base.Event} oEvent - the UI event instance
         * @returns {void}
         * @protected
         */
        onFilterResetButtonPress: function (oEvent) {
            this._oPopover.getModel("PopupData").setData({
                FilterSearchValue: ""
            }, true);
            this._updateFilterList();

            // TODO: on 'reset' close the popover and reset the filter on the table
            this._oPopover.close();
        },

        /**
         * Handles the 'liveChange' event in the filter search Input;
         * Applies the value to the list of filterable items
         * @param {sap.ui.base.Event} oEvent - the UI event instance
         * @returns {void}
         * @protected
         */
        onFilterSearchInputChange: function (oEvent) {
            this._updateFilterList({
                sFilterValue: oEvent.getSource().getValue()
            });
        },

        /**
         * TODO Column filtering
         * @param {sap.ui.base.Event} oEvent - the UI event instance
         * @returns {void}
         * @protected
         */
        onFilterSelectAllButtonToggle: function (oEvent) {

        },

        /**
         * TODO Column filtering
         * @param {sap.ui.base.Event} oEvent - the UI event instance
         * @returns {void}
         * @protected
         */
        onFilterListItemPress: function (oEvent) {

        },

        /**
         * Applies the current filter search values to the list of filterable items
         * @param {map} [mParameters]
         *  a parameter map containing the following attributes:
         * @param {string} [mParameters.sFilterValue]
         *  the current filter value (e.g. when updated via 'liveChange' event)
         * @returns {void}
         * @private
         */
        _updateFilterList: function (mParameters) {
            mParameters = mParameters || {};
            var oData = this._oPopover.getModel("PopupData").getData(),
                bSearchInvert = false,
                sFilterValue = mParameters.sFilterValue || oData.FilterSearchValue;

            // Retrieve the filter list UI Control using customData and DOM attributes
            var oSelector = $("#" + this._oPopover.getId() + " [data-ui-id='filterList']"),
                sElementId = oSelector && oSelector[0] ? oSelector[0].id : null,
                oControl = sElementId ? sap.ui.getCore().byId(sElementId) : null;
            if (!assert(oControl && oControl.getBinding("items"), "FilterList Control not found.")) {
                return;
            }

            // Reset the filter on the filter list
            if (sFilterValue === "") {
                oControl.getBinding("items").filter([]);
                return;
            }

            // Filter the items according to the given filter value
            oControl.getBinding("items").filter(new Filter({
                path: "Text",
                operator: bSearchInvert ? FilterOperator.NotContains : FilterOperator.Contains,
                value1: sFilterValue,
            }));
        },

        /**
         * TODO Popover actions
         * @param {sap.ui.base.Event} oEvent - the UI event instance
         * @returns {void}
         * @protected
         */
        onPopoverOkayButtonPress: function (oEvent) {

            this._oPopover.close();
        },

        /**
         * Update the sort order, the dependent UI settings and set the sorter into the table
         * @param {{sSortOrder}} mParameters
         *  a parameter map containing the following attributes:
         * @param {sap.ui.model.Filter} mParameters.sSortOrder
         *  the sort order to set, can be 'Ascending', 'Descending' or 'None'
         * @returns {void}
         * @private
         */
        _applyFilter: function (mParameters) {
            // Apply the sort order to the Table
            if (this._oTable.isA("ixult.m.Table")) {
                // Set the Sorters into an {ixult.m.Table} to allow further processing
                this._oTable.setFilter(vFilters);
            } else if (this._oTable.isA("sap.m.Table")) {
                // Or apply the sorting directly if it is a standard sap.m.Table
                this._oTable.getBinding("items").sort(vFilters);
            }
        },

        /**
         * TODO Popover actions
         * @param {sap.ui.base.Event} oEvent - the UI event instance
         * @returns {void}
         * @protected
         */
        onPopoverCancelButtonPress: function (oEvent) {
            this._oPopover.close();
        }

    });
});
