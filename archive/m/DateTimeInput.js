/* jshint strict: true */
/* global com: false */
"use strict";

jQuery.sap.declare("ixult.m.DateTimeInput");
jQuery.sap.require("com.ixult.travel.companion.util.Formatter");
jQuery.sap.require("sap.m.DateTimeInput");
jQuery.sap.require("sap.m.DateTimeInputType");
jQuery.sap.require("sap.m.HBox");
jQuery.sap.require("sap.m.Panel");
jQuery.sap.require("sap.ui.core.CSSSize");
jQuery.sap.require("sap.ui.core.TextAlign");
jQuery.sap.require("sap.ui.core.TextDirection");
jQuery.sap.require("sap.ui.core.ValueState");
jQuery.sap.require("sap.ui.core.format.DateFormat");
jQuery.sap.require("sap.ui.layout.FixFlex");
jQuery.sap.require("sap.ui.model.odata.type.Time");

sap.m.Panel.extend("ixult.m.DateTimeInput", {
    // The renderer to use; Important: Leave this empty,
    // so that the default Renderer is used
    renderer: {},
    // Metadata
    metadata: {
        properties: {
            "type": {
                type: "sap.m.DateTimeInputType",
                defaultValue: sap.m.DateTimeInputType.Date
            },
            "displayFormat": {
                type: "string"
            },
            "valueFormat": {
                type: "string"
            },
            "dateValue": {
                type: "object"
            },
            "value": {
                type: "string"
            },
            "width": {
                type: "sap.ui.core.CSSSize"
            },
            "enabled": {
                type: "boolean",
                defaultValue: true
            },
            "name": {
                type: "string"
            },
            "placeholder": {
                type: "string"
            },
            "blankValuesAllowed": {
                type: "boolean",
                defaultValue: true
            },
            "editable": {
                type: "boolean",
                defaultValue: true
            },
            "valueState": {
                type: "sap.ui.core.ValueState",
                defaultValue: sap.ui.core.ValueState.None
            },
            "valueStateText": {
                type: "string"
            },
            "showValueStateMessage": {
                type: "boolean",
                defaultValue: true
            },
            "textAlign": {
                type: "sap.ui.core.TextAlign",
                defaultValue: "Initial"
            },
            "textDirection": {
                type: "sap.ui.core.TextDirection",
                defaultValue: "Inherit"
            },
            "visible": {
                type: "boolean",
                defaultValue: true
            }
        },
        events: {
            // This event gets fired when the selection has finished and the value has changed.
            "change": {
                "parameters": {
                    // The string value of the control in given valueFormat(or locale format).
                    "value": {
                        type: "string"
                    },
                    // The value of control as JavaScript Date Object or null if value is empty.
                    "dateValue": {
                        type: "object"
                    }
                }
            }
        }
    },
    // Attributes
    bIsDesktop: undefined,
    bHasContent: false,
    oComponent: null,
    oInput: null,
    oValueHelpInput: null,
    oValueHelpButton: null,
    mLastValue: null,

    /**
     * Initializes the UI element
     * @public
     */
    init: function () {
        this.bIsDesktop = !sap.ui.Device.support.touch || sap.ui.Device.system.desktop;
        this.oComponent = sap.ui.core.Component.getOwnerComponentFor(this);
        this.addStyleClass("ixultUiDateTimeInputPanel");
    },

    /**
     * Function is called before the rendering of the control is started.
     * Applications must not call this hook method directly, it is called by the framework.
     * Subclasses of Control should override this hook to implement any necessary actions before the rendering.
     * @param {sap.ui.base.Event} oEvent the UI event object
     * @protected
     */
    onBeforeRendering: function (oEvent) {
        // Stop the control from being rerendered when the surrounding
        // control (e.g. a responsive table) is rerendered
        if (this.bHasContent) {
            oEvent.stopPropagation();
            oEvent.stopImmediatePropagation();
            oEvent.preventDefault();
            return;
        }

        // Set the Panel attributes
        this.setExpandable(false);
        this.setExpanded(true);

        // Create the panel contents;
        // This has to be done only once
        if (!this.bHasContent) {
            this.createContent(oEvent);
        }
    },

    /**
     * Called when the Control has been rendered (so its HTML is part of the document).
     * Post-rendering manipulations of the HTML could be done here.
     * This hook is the same one that SAPUI5 controls get after being rendered.
     * @param {sap.ui.base.Event} oEvent the event object
     * @public
     */
    onAfterRendering: function (oEvent) {
        // Clear the last value on re-rendering
        this.mLastValue = null;

        // Update the contents and their values
        //if (this.oInput.getValueState() === sap.ui.core.ValueState.None) {
        this.updateContent(oEvent);
        //}

        $("#" + this.oValueHelpButton.sId).attr("tabindex", -1);
        $("#" + this.oValueHelpButton.sId + "-inner").attr("tabindex", -1);
    },

    /**
     * Handles the SAPUI5 "afterRendering" UI event in the input field;
     * Calls the original super method and adjusts the field for the time input
     * @param {sap.ui.base.Event} oEvent the UI event object
     * @protected
     */
    onAfterRenderingInput: function (oEvent) {
        try {

            //var v1 = this.getProperty("value");

            //>>>>>>>>>>>>>>>>>>>>>>
            var v1 = this.getValue();
            if (v1 != null) {
                v1 = this._parseDateTime(v1).string;
            }
            var v2 = sap.m.DateTimeInput.prototype.getProperty.apply(this.oInput, ["value"]);
            if (v2 != null) {
                v2 = this._parseDateTime(v2).string;
            }

            // If needed -> update the property value
            if ((v1 != null) && (v1 !== v2)) {
                sap.m.DateTimeInput.prototype.setProperty.apply(this.oInput, ["value", v1]);
            }

            // Call Method of Super Class
            sap.m.DateTimeInput.prototype.onAfterRendering.apply(this.oInput, arguments);

            // Disable the time input "scroller" popup on desktop devices
            if (this.oInput._$input.scroller) {
                this.oInput._$input.scroller("destroy");
            }

            // Reset the values to blanks after rendering;
            // Fixes event bubbling from parent elements and value reset to "00:00"
            if (this.mProperties.blankValuesAllowed && this.mLastValue && this.mLastValue.string === "") {
                this.oInput._$input.value = "";
                this.oInput._$input[0].value = "";
            }
        } catch (oException) {
            this.handleException(oException);
        }
    },

    /**
     * Handles the SAPUI5 "afterRendering" UI event in the hidden value help input field;
     * Calls the original super method and adjusts the field for the time input
     * @param {sap.ui.base.Event} oEvent the UI event object
     * @protected
     */
    onAfterRenderingValueHelpInput: function ( /*oEvent*/) {
        try {
            sap.m.DateTimeInput.prototype.onAfterRendering.apply(this.oValueHelpInput, arguments);
            $("#" + this.oValueHelpInput.sId).attr("tabindex", -1);
            $("#" + this.oValueHelpInput.sId + "-inner").attr("tabindex", -1);
        } catch (oException) {
            this.handleException(oException);
        }
    },

    /**
     * Creates the panel contents, consisting of:
     *   - the main input field (editable on desktop devices),
     *   - the "Value Help" input field (for displaying the "scroller" popup) and
     *   - the "Value Help" button which activates the "scroller" popup
     * @param {sap.ui.base.Event} oEvent the UI event object
     * @protected
     */
    createContent: function () {
        // Clear the contents
        this.removeAllContent();

        // Attach event handler to recognize value and value state changes in the field
        // Has to be done BEFORE setting the BindingInfo values into the input field
        this._attachBindingListener("valueState", this.onValueStateChange);
        this._attachBindingListener("valueStateText", this.onValueStateTextChange);

        // Add the input field and additional controls
        this.addContent(new sap.ui.layout.FixFlex({
            fixFirst: false,
            vertical: false,
            flexContent: this._createDateTimeInput(),
            fixContent: [
                this._createDateTimeValueHelpInput(),
                this._createDateTimeValueHelpButton()
            ],
            visible: this.mBindingInfos.visible || this.mProperties.visible
        }));

        this.bHasContent = true;
    },

    /**
     * Attaches event listeners to the value bindings to recognize model changes
     * @param {string} sFieldName the field name to look up in the BindingInfos
     * @param {function} fnEventHandler the local function to execute on the event
     * @private
     */
    _attachBindingListener: function (sFieldName, fnEventHandler) {
        if (this.mBindingInfos[sFieldName] && this.mBindingInfos[sFieldName].binding) {
            var oBinding = this.mBindingInfos[sFieldName].binding;
            var bHasListener = false;
            if (oBinding.mEventRegistry && oBinding.mEventRegistry.change) {
                bHasListener = $.grep(oBinding.mEventRegistry.change, $.proxy(function (oElement) {
                    return oElement.oListener === this;
                }, this)).length > 0 ? true : false;
            }
            if (!bHasListener) {
                this.mBindingInfos[sFieldName].binding.attachChange($.proxy(fnEventHandler, this), this);
            }
        }
    },

    /**
     * Creates the DateTimeInput UI element that is used to enter dates or times
     * @returns {sap.m.DateTimeInput} the DateTimeInput UI element instance
     * @private
     */
    _createDateTimeInput: function () {
        // ------------------------------------------------------------
        // Create the DateTimeInput UI element for date or time input
        // ------------------------------------------------------------
        this.oInput = new sap.m.DateTimeInput(this.sId + "-input", {
            value: this.mBindingInfos.value || this.mProperties.value,
            dateValue: this.mBindingInfos.dateValue || this.mProperties.dateValue,
            valueState: this.mBindingInfos.valueState || this.mProperties.valueState,
            valueStateText: this.mBindingInfos.valueStateText || this.mProperties.valueStateText,
            enabled: this.mBindingInfos.enabled || this.mProperties.enabled,
            visible: this.mBindingInfos.visible || this.mProperties.visible,
            name: this.mProperties.name,
            type: this.mProperties.type,
            displayFormat: this.mProperties.displayFormat,
            valueFormat: this.mProperties.valueFormat,
            placeholder: this.mProperties.placeholder || "",
            editable: true, // this.mProperties.editable,
            showValueStateMessage: this.mProperties.showValueStateMessage || true,
            textAlign: this.mProperties.textAlign,
            textDirection: this.mProperties.textDirection
        });
        this.oInput.bindValue(this.mBindingInfos.value);

        // Setup custom styles
        this.oInput.addStyleClass("ixultUiDateTimeInput");

        // ------------------------------------------------------------
        // Attach Events to the input field
        // ------------------------------------------------------------
        this.oInput.onAfterRendering = $.proxy(this.onAfterRenderingInput, this);
        this.oInput.attachChange({}, this.onChange, this);
        if (this.mEventRegistry && this.mEventRegistry.change && this.mEventRegistry.change[0]) {
            // Redirect the original change event to a custom event (if set)
            // to allow pre-processing in this class (see method "onChange")
            var oChangeEvent = this.mEventRegistry.change[0];
            this.oInput.attachEvent(
              "afterChange",
              oChangeEvent.oData,
              oChangeEvent.fFunction,
              oChangeEvent.oListener
            );
        }
        if (this.bIsDesktop) {
            // Hide the "scroller" popup in the input field on desktop devices
            this.oInput.attachBrowserEvent("click", $.proxy(this.onClick, this), this);
            this.oInput.attachBrowserEvent("tap", $.proxy(this.onTap, this), this);
            this.oInput.attachBrowserEvent("keydown", $.proxy(this.onKeyDown, this), this);
            this.oInput.attachBrowserEvent("focusout", $.proxy(this.onFocusOut, this), this);
            this.oInput.attachBrowserEvent("sapfocusleave", $.proxy(this.onSapFocusLeave, this), this);
            // Copy the keyboard events of the "Value Help" input to the input field
            // This opens the "scroller" popup on [F4] or [ALT]+[DOWN]
            this.oInput.onsapshow = $.proxy(this.onSapShow, this);
            this.oInput.onsaphide = $.proxy(this.onSapShow, this);
        }

        return this.oInput;
    },

    /**
     * Creates the DateTimeInput UI element that is used as a "Value Help" and displays the "scroller" popup
     * @returns {sap.m.DateTimeInput} the DateTimeInput UI element instance
     * @private
     */
    _createDateTimeValueHelpInput: function () {
        // ------------------------------------------------------------
        // Create a DateTimeInput element for the "Value Help"
        // ------------------------------------------------------------
        this.oValueHelpInput = new sap.m.DateTimeInput(this.sId + "-vh-input", {
            value: this.mBindingInfos.value || this.mProperties.value,
            dateValue: this.mBindingInfos.dateValue || this.mProperties.dateValue,
            enabled: this.mBindingInfos.enabled || this.mProperties.enabled,
            visible: this.mBindingInfos.visible || this.mProperties.visible,
            name: this.mProperties.name,
            type: this.mProperties.type,
            displayFormat: this.mProperties.displayFormat,
            valueFormat: this.mProperties.valueFormat,
            placeholder: this.mProperties.placeholder,
            editable: this.mProperties.editable,
            showValueStateMessage: false,
            textAlign: this.mProperties.textAlign,
            textDirection: this.mProperties.textDirection
        });

        // Setup custom styles
        this.oValueHelpInput.addStyleClass("ixultUiDateTimeValueHelpInput");

        // Attach Events to the input field
        this.oValueHelpInput.onAfterRendering = $.proxy(this.onAfterRenderingValueHelpInput, this);
        this.oValueHelpInput.attachChange({}, this.onValueHelpSelect, this);

        // Disable any input in the "Value Help" field
        this._changeInputAttributes(this.oValueHelpInput.sId, false, true);

        return this.oValueHelpInput;
    },

    /**
     * Creates the button that opens the "scroller" popup on the "Value Help" input field
     * @returns {sap.m.Button} the Button UI element instance
     * @private
     */
    _createDateTimeValueHelpButton: function () {
        // Create a Button UI element to access the "scroller" popup
        this.oValueHelpButton = new sap.m.Button(this.sId + "-vh-button", {
            type: sap.m.ButtonType.Transparent,
            icon: "sap-icon://history",
            enabled: this.mBindingInfos.enabled || this.mProperties.enabled,
            visible: this.mBindingInfos.visible || this.mProperties.visible
        });
        this.oValueHelpButton.attachPress({}, this.onValueHelpRequest, this);
        this.oValueHelpButton.addStyleClass("ixultUiDateTimeValueHelpButton");
        this.oValueHelpButton.removeStyleClass("sapMFocusable");

        return this.oValueHelpButton;
    },

    /**
     * Changes the attributes of the HTML element of an input field
     * @param {string} sId the id of the HTML input field
     * @param {boolean} [bEnabled] true if the field should be editable; true by default
     * @param {boolean} [bReadable] true if the field should be readable; true by default
     * @private
     */
    _changeInputAttributes: function (sId, bEnabled, bReadable) {
        bEnabled = bEnabled || true;
        bReadable = bReadable || true;
        if (!sId) {
            return;
        }
        if (sId.charAt(0) !== "#") {
            sId = "#" + sId;
        }

        // Set the input field to editable (or not)
        $(sId).prop("disabled", !bEnabled);
        $(sId).attr("disabled", !bEnabled);

        // Set the input field to readable (or not)
        $(sId).prop("readonly", !bReadable);
        $(sId).attr("readonly", !bReadable);
    },

    /**
     * Updates the properties on the controls on re-rendering
     * @param {sap.ui.base.Event|jQuery.Event} oEvent the UI event object
     * @protected
     */
    updateContent: function (oEvent) {
        if (this.oInput) {
            this.oInput.setEnabled(this.mProperties.enabled);
            this.oInput.setVisible(this.mProperties.visible);
            this.oInput.setValueState(this.mProperties.valueState);
            this.oInput.setValueStateText(this.mProperties.valueStateText);
        }

        // Update the UI element properties on re-rendering
        if (this.oValueHelpInput) {
            this.oValueHelpInput.setEnabled(this.mProperties.enabled);
            this.oValueHelpInput.setVisible(this.mProperties.visible);
        }

        // Update the UI element properties on re-rendering
        if (this.oValueHelpButton) {
            this.oValueHelpButton.setEnabled(this.mProperties.enabled);
            this.oValueHelpButton.setVisible(this.mProperties.visible);
        }

        // Insert the initial date value
        if (oEvent.srcControl.mBindingInfos && oEvent.srcControl.mBindingInfos.value) {
            this.oInput.bindValue(oEvent.srcControl.mBindingInfos.value);
            this.oValueHelpInput.bindValue(oEvent.srcControl.mBindingInfos.value);
            //this.oInput.updateBindings();
        }

        // this.updateValue(oEvent, this.mProperties.value);
        if (this.mProperties.blankValuesAllowed && this.oInput.getValue() === "00:00") {
            this.updateValue(oEvent, this.oInput.getValue());
        }

        // this._attachBindingListener("value", this.onChange);
    },

    /**
     * Handles the browser "click" event in the input field
     * @param {jQuery.Event} oEvent the jQuery event object
     * @protected
     */
    onClick: function (oEvent) {
        try {
            // Select the whole text when clicking into the input field
            oEvent.target.setSelectionRange(0, oEvent.target.value.length);

            // Set the input field to editable
            this._changeInputAttributes(oEvent.target.id, true, true);

            // Stop event bubbling (firing following events like "focusout")
            oEvent.stopPropagation();
        } catch (oException) {
            this.handleException(oException);
        }
    },

    /**
     * Handles the browser "tap" event when entering the input field
     * @param {jQuery.Event} oEvent the jQuery event object
     * @returns {void}
     * @protected
     */
    onTap: function (oEvent) {
        try {
            // Hide the scroller of the input field
            if (this.oInput._$input.scroller) {
                this.oInput._$input.scroller("destroy");
            }

            // Set the input field to editable
            this._changeInputAttributes(oEvent.target.id, true, true);
        } catch (oException) {
            this.handleException(oException);
        }
    },

    /**
     * Handles the browser "keydown" event when typing into the input field
     * @param {jQuery.Event} oEvent the jQuery event object
     * @returns {void}
     * @protected
     */
    onKeyDown: function (oEvent) {
        try {
            // Applies to all non special keycodes (ignores backspace, return etc.)
            if (oEvent.keyCode > 46 && oEvent.originalEvent) {
                var oOriginalEvent = oEvent.originalEvent;

                // Special: Handle the numpad number keys
                if (oEvent.keyCode >= 96 && oEvent.keyCode <= 105 &&
                  oOriginalEvent.code.toString().indexOf("Numpad") !== -1) {
                    oEvent.keyCode -= 48;
                }

                // Special: Handle the colon sign ":" on different Locales
                var bColonEntered = false;
                var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage().toString();
                if (sCurrentLocale.indexOf("de") !== -1 && oOriginalEvent.code === "Period" && oOriginalEvent.shiftKey) {
                    bColonEntered = true;
                } else if (sCurrentLocale.indexOf("en") !== -1 && oOriginalEvent.code === "Semicolon" && oOriginalEvent.shiftKey) {
                    bColonEntered = true;
                } else if (oOriginalEvent.key === ":") {
                    bColonEntered = true;
                } else if (String.fromCharCode(oEvent.keyCode) === "¾") {
                    bColonEntered = true;
                }

                // Only allow valid times entered into the field as well as the ":" sign
                var bSpecialKeyEntered = oEvent.originalEvent.shiftKey || oEvent.originalEvent.altKey || oEvent.originalEvent.ctrlKey;
                var sChar = String.fromCharCode(oEvent.keyCode);
                if ("0123456789".indexOf(sChar) >= 0 && !bSpecialKeyEntered || bColonEntered) {
                    oEvent.stopPropagation();
                } else {
                    oEvent.stopPropagation();
                    oEvent.stopImmediatePropagation();
                    oEvent.preventDefault();
                    return false;
                }
            } else {
                oEvent.stopPropagation();
            }
        } catch (oException) {
            this.handleException(oException);
        }
    },

    /**
     * Handles the browser "focusout" event when leaving the input field
     * @param {jQuery.Event} oEvent the jQuery event object
     * @returns {void}
     * @protected
     */
    onFocusOut: function (oEvent) {
        try {
            this.onChange(oEvent);
        } catch (oException) {
            this.handleException(oException);
        }
    },

    /**
     * Handles the browser "sapfocusleave" event when leaving the input field
     * @param {jQuery.Event} oEvent the jQuery event object
     * @protected
     */
    onSapFocusLeave: function () {
        try {
            jQuery.sap.log.debug("sapfocusleave");
        } catch (oException) {
            this.handleException(oException);
        }
    },

    /**
     * Handles the "change" event on the input field when dates or times have been entered manually
     * @param {sap.ui.base.Event|jQuery.Event} oEvent the event object
     * @protected
     */
    onChange: function (oEvent) {
        this.updateValue(oEvent);

        // Inform portal, that changes are pending
        com.ixult.travel.companion.util.Util.setDataWasChanged(true);
    },

    /**
     * Handles the "press" event on the "Value Help" button;
     * Opens the "scroller" popup in the "Value Help" input field
     * @param {jQuery.Event} oEvent the jQuery event object
     * @protected
     */
    onValueHelpRequest: function () {
        // Insert the empty default time value when nothing is set
        var sValue = this.oValueHelpInput.getValue();
        if (sValue === "") {
            //this.oValueHelpInput.setValue("00:00");
            //debugger;
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            this.oValueHelpInput.setDateValue(this._parseDateTime("00:00").date);
        }

        // Insert the current time for the scroller when the initial value is blank
        // if (this.mProperties.blankValuesAllowed && (sValue === "" || sValue === "00:00")) {
        // 	this.oValueHelpInput.setValue(this._getFormattedDateTime(new Date()));
        // }

        // Show the time scroller popup
        jQuery.sap.delayedCall(10, sValue, $.proxy(function () {
            $("#" + this.oValueHelpInput.sId + "-inner").scroller("show");
        }, this, sValue));
    },

    /**
     * Handles the "change" event on the "Value Help" input scroller,
     * i.e. when a new value was chosen in the "scroller" popup
     * @param {jQuery.Event} oEvent the jQuery event object
     * @protected
     */
    onValueHelpSelect: function (oEvent) {
        this.updateValue(oEvent, this.oValueHelpInput.getValue());

        // Inform portal, that changes are pending
        com.ixult.travel.companion.util.Util.setDataWasChanged(true);
    },

    /**
     * Handles the "sapshow" event on the input field;
     * This is fired when pressing [F4] or [ALT]+[DOWN] on the keyboard
     * @param {jQuery.Event} oEvent the jQuery event object
     * @protected
     */
    onSapShow: function (oEvent) {
        // Open the "scroller" popup to choose a value
        if (this.oValueHelpInput && $("#" + this.oValueHelpInput.sId).scroller) {
            $("#" + this.oValueHelpInput.sId).scroller("show");
        }
        oEvent.preventDefault();
        oEvent.setMarked();
    },

    /**
     * Handles the change of a bound value state;
     * Sets the corresponding value into the input field
     * @param {sap.ui.base.Event} oEvent the UI event object
     * @protected
     */
    onValueStateChange: function (oEvent) {
        this.oInput.setValueState(oEvent.oSource.oValue);
        if (oEvent.oSource.oValue !== sap.ui.core.ValueState.None) {
            this.oInput.setShowValueStateMessage(true);
        }
    },

    /**
     * Handles the change of a bound value state message;
     * Sets the corresponding value into the input field
     * @param {sap.ui.base.Event} oEvent the UI event object
     * @protected
     */
    onValueStateTextChange: function (oEvent) {
        this.oInput.setValueStateText(oEvent.oSource.oValue);
    },

    /**
     * Inserts the date value into input field and value help
     * @param {sap.ui.base.Event|jQuery.Event} oEvent the event object
     * @param {string|number|date} [vValue] the date value, optionally
     * @protected
     */
    updateValue: function (oEvent, vValue) {
        try {
            this._setValueState(sap.ui.core.ValueState.None);

            // Check the given input value (if set) and parse the date value
            var mValue = this._setDateValue(oEvent, vValue);

            // Copy the value to the properties section
            this.mProperties.value = mValue.string;

            /*
            // Execute the event handler for the "Change" event
            // that was specified in the UI element
            var iTime = (!mValue.string) ? 0 : mValue.date.getTime();
            this.oInput.fireEvent("afterChange", {
                value: iTime
            });

            // Stop event bubbling
            if (oEvent instanceof jQuery.Event) {
                oEvent.stopPropagation();
                oEvent.stopImmediatePropagation();
            }
            if (oEvent.bPreventDefault !== null) {
                oEvent.bPreventDefault = true;
            }
            oEvent.preventDefault();
            */

        } catch (oException) {
            this.handleException(oException);
        }
    },

    /**
     * Updates the date value into the input field
     * @param {jQuery.Event} oEvent the jQuery event object
     * @param {string|number|date} [vValue] the date value, optionally
     * @returns {map} a map with the date value, consisting of
     *    - {date} date the date value object
     *    - {string} string the formatted date value in the input field
     * @private
     */
    _setDateValue: function (oEvent, vValue) {
        this._setValueState(sap.ui.core.ValueState.None);

        // Evaluate the given input value (if set)
        vValue = (vValue && typeof vValue !== "object") ? vValue :
          this.oInput._$input ? this.oInput._$input.val() :
            oEvent.target ? oEvent.target.value : null;

        // Fallback 1: Set the value to 00:00 if nothing could be found
        if (!vValue) {
            vValue = "00:00";
        }

        // Fallback 2: Reset the value to the last valid saved value
        // if there has to be a value set into the field
        if (!this.mProperties.blankValuesAllowed && vValue === "00:00" && this.mLastValue) {
            vValue = this.mLastValue.string;
        }

        // Get the value and parse it into a date/time
        var mValue = this._parseDateTime(vValue);
        if (!mValue || isNaN(mValue.date)) {
            // Show an error message if the value is not a valid date/time
            this._setValueState(sap.ui.core.ValueState.Error);
            return;
        }
        mValue.date = new Date(mValue.date);

        // Insert the values into the input field
        this.oInput.setDateValue(mValue.date);

        //>>>>>>>>>>>>>>>>>>>>>>>>>>>
        sap.m.DateTimeInput.prototype.setProperty.apply(this.oInput, ["dateValue", mValue.date]);
        sap.m.DateTimeInput.prototype.setProperty.apply(this.oInput, ["value", mValue.string]);

        // ------------------------------------------------------------ MPN -> destroyed the data binding in some cases
        // this.oInput.setValue(mValue.string);
        // ------------------------------------------------------------
        if (this.oInput._$input) {
            this.oInput._$input.value = mValue.string;
            if (this.oInput._$input[0]) {
                this.oInput._$input[0].value = mValue.string;
            }
        }

        //>>>>>>>>>>>>>>>>>>>>>>
        /*
        var v1 = this.getValue();
        var v2 = sap.m.DateTimeInput.prototype.getProperty.apply(this.oInput, ["value"]);
        if ( (v1 != null) && (v1 !== v2) ) {
            sap.m.DateTimeInput.prototype.setProperty.apply(this.oInput, ["value", v1]);
        }
        */

        this.oInput.updateBindings();

        // Copy the values into the "Value Help" input field
        this.oValueHelpInput.setDateValue(mValue.date);
        // ------------------------------------------------------------ MPN -> destroyed the data binding in some cases
        // this.oValueHelpInput.setValue(mValue.string);
        // ------------------------------------------------------------
        this.oValueHelpInput.updateBindings();

        // Save the valid value as "last value"
        this.mLastValue = mValue;
        return mValue;
    },

    /**
     * Parses the given string into a valid date/time object;
     * Using patterns for standard ("HH:mm") and american format ("HH:mm AM/PM")
     * @param {string} sValue the field value to parse
     * @returns {map} a map with the date value, consisting of
     *    - {date} date the date value object
     *    - {string} string the formatted date value in the input field
     * @private
     */
    _parseDateTime: function (sValue) {
        try {
            sValue = sValue.toString().trim();

            // Search for the time values in the string value;
            // Uses regular expressions to find hours, minutes and "am/pm"
            var sPattern = /([0-9]*):([0-9]*)\s{0,}(am|pm){0,1}/i;
            var aMatches = sValue.match(sPattern);

            // Try again by evaluating erratic user input
            // (like "8" should convert to "8:00" etc.)
            if (!aMatches) {
                sValue = this._createDateTimeFromErraticUserInput(sValue);
                aMatches = sValue.match(sPattern);
            }

            // Try again with the last input value if no matches were found
            if (!aMatches && this.oInput) {
                sValue = this.oInput.getValue() || "00:00";
                aMatches = sValue.match(sPattern);
            }

            // Find the values in the array of regex matches
            var iHours = parseInt(aMatches[1], 10);
            var iMinutes = parseInt(aMatches[2], 10);
            var sAMPM = aMatches[3];
            if (sAMPM) {
                if (sAMPM.toString().toLowerCase() === "pm") {
                    if (iHours !== 12) {
                        iHours = iHours + 12;
                    }
                } else if ((sAMPM.toString().toLowerCase() === "am") && iHours === 12) {
                    // 12:00 AM = 00:00
                    iHours = 0;
                }
            }

            // Create the time value
            var dDateTime = new Date();
            dDateTime.setHours(iHours, iMinutes, 0, 0);

            // Set the date string to show in the input field
            var sDateString = "00:00";
            if (iHours === 0 && iMinutes === 0 && this.mProperties.blankValuesAllowed) {
                // Clear the string
                sDateString = "";
            } else {
                sDateString = this._getFormattedDateTime(dDateTime);
            }

            // Return the time values as string and date object
            return {
                string: sDateString,
                date: dDateTime
            };
        } catch (oException) {
            this.handleException(oException);
            return null;
        }
    },

    /**
     * Evaluates the user input and tries to create a valid datetime,
     * thus allowing to e.g. enter "8" which is converted to "8:00"
     * @param {string} sInputValue the given user input value
     * @returns {string} the evaluated value
     * @private
     */
    _createDateTimeFromErraticUserInput: function (sInputValue) {
        // Match the value against a broader regex pattern
        var sPattern = /([0-9]*)\s{0,}(a|p|am|pm){0,1}/i;
        var aMatches = sInputValue.match(sPattern);
        if (!aMatches || !aMatches[1]) {
            return sInputValue;
        }
        var sValue = aMatches[1].toString();

        // Delete a leading Zero at the begin (for hours like "08", "09", ...)
        // if (sValue.length >= 2 && sValue.indexOf("0") === 0 && sValue.substring(1, 2) !== "0") {
        // 	sValue = sValue.substring(1, sValue.length);
        // }

        // Determine the values for hours and minutes
        // Supports up to 4 numbers to be evaluated as time
        var iHour1 = parseInt(sValue.substring(0, 1), 10);
        var iHour2 = (sValue.length >= 2) ? parseInt(sValue.substring(1, 2), 10) : null;
        var iMinute1 = (sValue.length >= 3) ? parseInt(sValue.substring(2, 3), 10) : null;
        var iMinute2 = (sValue.length >= 4) ? parseInt(sValue.substring(3, 4), 10) : null;
        var iHour = 0;
        var iMinute = 0;

        // Check the hour numbers for valid input
        iHour = (iHour1 !== null && iHour2 !== null) ? iHour1 * 10 + iHour2 : 0;
        if (iHour > 23 || iHour2 === null) {
            iHour = iHour1;
            iMinute2 = iMinute1;
            iMinute1 = iHour2;
        }

        // Check the minute numbers for valid input
        iMinute = (iMinute1 !== null && iMinute2 !== null) ? iMinute1 * 10 + iMinute2 : iMinute1 ? iMinute1 : 0;
        if (iMinute > 59 || iMinute2 === null) {
            iMinute = iMinute1 || 0;
        }

        // Return the evaluated time string
        sValue = "" + iHour + ":" + iMinute + "";
        return sValue;
    },

    /**
     * Returns a date time string that is formatted according to the input pattern
     * @param {date} dDateTime the date time object
     * @returns {string} the formatted string
     * @private
     */
    _getFormattedDateTime: function (dDateTime) {
        // Get the display format pattern
        var sDisplayPattern = this.oInput.getDisplayFormat();
        if (!this.mBindingInfos.visible || !this.mProperties.visible) {
            sDisplayPattern = "HH:mm";
        }

        // Transform the date value into a valid formatted date
        var oDateFormat = sap.ui.core.format.DateFormat.getDateInstance({
            pattern: sDisplayPattern
        });

        return oDateFormat.format(dDateTime);
    },

    /**
     * Sets the value state (like error or warning) of the date/time input field;
     * Formats the CSS attributes and shows the error message for the input field
     * @param {sap.ui.core.ValueState} sValueState the vlaue state constant value
     * @private
     */
    _setValueState: function (sValueState) {
        this.oInput.setValueState(sValueState);
        switch (sValueState) {
            case sap.ui.core.ValueState.Error:
                this.oInput.setShowValueStateMessage(true);
                this.addStyleClass("UiError");
                return;
            default:
                this.oInput.setShowValueStateMessage(false);
                this.removeStyleClass("UiError");
                return;
        }
    },

    /**
     * Handles an exception thrown by a try catch block
     * @param {object} oException the error object
     * @protected
     */
    handleException: function (oException) {
        console.error(oException.name + ": " + oException.message, oException);
    }
});