sap.ui.define([
    "./array",
    "sap/ui/base/ManagedObject"
], function (
    array,
    ManagedObject
) {
    "use strict";

    /**
     * @class ixult.base.util.Error
     *
     * @summary Enhanded Error handling class
     * @returns {ixult.base.util.Error}
     *
     * @public
     */
    return ManagedObject.extend("ixult.base.util.Error", /** @lends ixult.base.util.Error.prototype */{
        _oInternalError: null,
        name: "",
        message: "",
        results: [],

        constructor: function (mParameters) {
            if (mParameters instanceof Error) {
                this.name = mParameters.name;
                this.message = mParameters.message;
            }

            this.results = mParameters.results || []
        },

        getResults: function () {
            return this.results;
        }


    });
});
