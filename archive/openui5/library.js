sap.ui.define([
    "sap/ui/core/Core",
    "sap/ui/core/library"
], function (
  Core,
  Library
) {
    "use strict";

    return sap.ui.getCore().initLibrary({
        name: "openui5",
        noLibraryCSS: true,
        dependencies: [
            "sap.ui.core",
            "sap.m",
        ],
        types: [],
        interfaces: [],
        controls: [],
        elements: [],
        version: "1.0.0"
    });

}, /* bExport= */ false);