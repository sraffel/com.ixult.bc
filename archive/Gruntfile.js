/*jslint node: true */
'use strict';

module.exports = function (grunt) {
    require('jit-grunt')(grunt, {
        configureProxies: 'grunt-connect-proxy',
        jsdoc: 'grunt-jsdoc'
    });

    // ------------------------------------------------------------
    // Grunt Config
    // ------------------------------------------------------------
    grunt.initConfig({
        // Load Configurations
        project: grunt.file.readJSON('config.json'),
        // Task settings
        jshint: {
            all: [
                '<%= project.appPath %>/**/*.js',
                'test/**/*.js'
            ]
        },
        less: {
            development: {
                options: {
                    paths: ['']
                },
                files: {
                    // '<%= project.appPath %>/css/base.css': '<%= project.appPath %>/css/base.less',
                    // '<%= project.appPath %>/css/styles.css': '<%= project.appPath %>/css/styles.less',
                    // '<%= project.appPath %>/css/companion.css': '<%= project.appPath %>/css/companion.less'
                }
            }
        },
        watch: {
            options: {
                spawn: false
            },
            livereload: {
                options: {
                    livereload: {
                        host: 'localhost',
                        port: '<%= project.livereloadPort %>'
                    }
                },
                files: [
                    '<%= project.appPath %>/{,*/}*.{html, css, xml, js, properties, json}'
                ],
                tasks: ['less']
            },
            deploy: {
                options: {
                    livereload: false
                },
                files: [
                    '<%= project.deployPath %>/{,*/}*.{html, xml, css, js, properties, json}'
                ]
            },
            doc: {
                files: ['<%= project.appPath %>/{,*/}*\.js'],
                tasks: ['jsdoc'],
                options: {
                    livereload: {
                        host: 'localhost',
                        port: '<%= project.docLivereloadPort %>'
                    }
                }
            },
            less: {
                files: [
                    'css/**/*.less'
                ],
                tasks: ['less'],
                options: {
                    livereload: true
                }
            },
            css: {
                files: ['css/*.css']
            }
        },
        connect: {
            options: {
                hostname: '<%= project.host %>',
                port: '<%= project.serverPort %>',
                livereload: true
            },
            rules: [{
                from: '^/(.*)-dbg\.(.*)\.js',
                to: '/$1\.$2.js'
            }, {
                from: '^<%= project.i18nPath %>/i18n/(.*)',
                to: '/i18n/$1'
            }],
            backendAll: {
                proxies: [{
                    context: '/sap/opu/odata/',
                    host: '<%= project.odataServer %>',
                    port: '<%= project.odataPort %>',
                    https: '<%= project.odataProtocol %>' === 'https',
                    changeOrigin: false,
                    rewrite: {
                        '&sap-client=\\d{3}': '',
                        'sap-client=\\d{3}&': '',
                        '\\?': '?sap-client=' + '<%= project.odataMandt %>' + '&',
                        '(^((\?!\\?).)*$)': '$1?sap-client=' + '<%= project.odataMandt %>' + ''
                    }
                }, {
                    context: '/resources',
                    host: '<%= project.resourcesServer %>',
                    port: '<%= project.resourcesPort %>',
                    https: '<%= project.resourcesProtocol %>' === 'https',
                    changeOrigin: true,
                    rewrite: {
                        '^/resources': '<%= project.resourcesRoot %>' + '/resources',
                        '&sap-client=\\d{3}': '',
                        'sap-client=\\d{3}&': '',
                        '\\?': '?sap-client=' + '<%= project.resourcesMandt %>' + '&sap-ui-debug=true&',
                        '(^((\?!\\?).)*$)': '$1?sap-client=' + '<%= project.resourcesMandt %>' + '&sap-ui-debug=true'
                    }
                }, {
                    context: '/sap/bc/ui2/',
                    host: '<%= project.resourcesServer %>',
                    port: '<%= project.resourcesPort %>',
                    https: '<%= project.resourcesProtocol %>' === 'https',
                    changeOrigin: true,
                    rewrite: {
                        //'^/resources': '<%= project.resourcesRoot %>' + '/resources',
                        '&sap-client=\\d{3}': '',
                        'sap-client=\\d{3}&': '',
                        '\\?': '?sap-client=' + '<%= project.resourcesMandt %>' + '&sap-ui-debug=true&',
                        '(^((\?!\\?).)*$)': '$1?sap-client=' + '<%= project.resourcesMandt %>' + '&sap-ui-debug=true'
                    }
                }, {
                    context: '/sap/public/',
                    host: '<%= project.resourcesServer %>',
                    port: '<%= project.resourcesPort %>',
                    https: '<%= project.resourcesProtocol %>' === 'https',
                    changeOrigin: true,
                    rewrite: {
                        //'^/resources': '<%= project.resourcesRoot %>' + '/resources',
                        '&sap-client=\\d{3}': '',
                        'sap-client=\\d{3}&': '',
                        '\\?': '?sap-client=' + '<%= project.resourcesMandt %>' + '&sap-ui-debug=true&',
                        '(^((\?!\\?).)*$)': '$1?sap-client=' + '<%= project.resourcesMandt %>' + '&sap-ui-debug=true'
                    }
                }]
            },
            backendOData: {
                proxies: [{
                    context: '/sap/opu/odata/',
                    host: '<%= project.odataServer %>',
                    port: '<%= project.odataPort %>',
                    https: '<%= project.odataProtocol %>' === 'https',
                    changeOrigin: true,
                    rewrite: {
                        '&sap-client=\\d{3}': '',
                        'sap-client=\\d{3}&': '',
                        '\\?': '?sap-client=' + '<%= project.odataMandt %>' + '&',
                        '(^((\?!\\?).)*$)': '$1?sap-client=' + '<%= project.odataMandt %>' + ''
                    }
                }]
            },
            backendResources: {
                proxies: [{
                    context: '/resources',
                    host: '<%= project.resourcesServer %>',
                    port: '<%= project.resourcesPort %>',
                    https: '<%= project.resourcesProtocol %>' === 'https',
                    changeOrigin: true,
                    rewrite: {
                        '^/resources': '<%= project.resourcesRoot %>' + '/resources',
                        '&sap-client=\\d{3}': '',
                        'sap-client=\\d{3}&': '',
                        '\\?': '?sap-client=' + '<%= project.resourcesMandt %>' + '&sap-ui-debug=true&',
                        '(^((\?!\\?).)*$)': '$1?sap-client=' + '<%= project.resourcesMandt %>' + '&sap-ui-debug=true'
                    }
                }]
            },
            cdnResources: {
                proxies: [{
                    context: '/resources',
                    host: 'sapui5.hana.ondemand.com',
                    changeOrigin: true,
                    rewrite: {
                        '^/resources': '/' + '<%= project.resourcesVersion %>' + '/resources'
                    }
                }]
            },
            livereload: {
                options: {
                    livereload: false,
                    base: ['.tmp', '<%= project.appPath %>'],
                    middleware: function (connect, options, middlewares) {
                        middlewares.unshift(require('grunt-connect-proxy/lib/utils').proxyRequest);
                        middlewares.unshift(require('grunt-connect-rewrite/lib/utils').rewriteRequest);
                        middlewares.unshift(require('connect-livereload')({
                            port: grunt.config().project.livereloadPort,
                            ignore: [/\/sap\/opu\/.*/, /\.xml(\?.*)?$/, /\.js(\?.*)?$/, /\.css(\?.*)?$/, /\.svg(\?.*)?$/, /\.ico(\?.*)?$/,
                                /\.woff(\?.*)?$/, /\.png(\?.*)?$/, /\.jpg(\?.*)?$/, /\.jpeg(\?.*)?$/, /\.gif(\?.*)?$/, /\.pdf(\?.*)?$/,
                                /\.json(\?.*)?$/]
                        }));
                        return middlewares;
                    }
                }
            },
            deploy: {
                options: {
                    base: '<%= project.deployPath %>',
                    middleware: function (connect, options, middlewares) {
                        middlewares.unshift(require('grunt-connect-proxy/lib/utils').proxyRequest);
                        middlewares.unshift(require('grunt-connect-rewrite/lib/utils').rewriteRequest);
                        middlewares.unshift(require('connect-livereload')({
                            port: grunt.config().project.livereloadPort,
                            ignore: [/\/sap\/opu\/.*/, /\.xml(\?.*)?$/, /\.js(\?.*)?$/, /\.css(\?.*)?$/, /\.svg(\?.*)?$/, /\.ico(\?.*)?$/,
                                /\.woff(\?.*)?$/, /\.png(\?.*)?$/, /\.jpg(\?.*)?$/, /\.jpeg(\?.*)?$/, /\.gif(\?.*)?$/, /\.pdf(\?.*)?$/,
                                /\.json(\?.*)?$/]
                        }));
                        return middlewares;
                    }
                }
            },
            doc: {
                options: {
                    base: '<%= project.docPath %>',
                    port: '<%= project.docServerPort %>',
                    middleware: function (connect, options, middlewares) {
                        middlewares.unshift(require('grunt-connect-proxy/lib/utils').proxyRequest);
                        middlewares.unshift(require('grunt-connect-rewrite/lib/utils').rewriteRequest);
                        middlewares.unshift(require('connect-livereload')({
                            port: grunt.config().project.docLivereloadPort,
                            ignore: [/\/sap\/opu\/.*/, /\.js(\?.*)?$/, /\.css(\?.*)?$/, /\.svg(\?.*)?$/, /\.ico(\?.*)?$/,
                                /\.woff(\?.*)?$/, /\.png(\?.*)?$/, /\.jpg(\?.*)?$/, /\.jpeg(\?.*)?$/, /\.gif(\?.*)?$/, /\.pdf(\?.*)?$/,
                                /\.json(\?.*)?$/]
                        }));
                        return middlewares;
                    }
                }
            }
        },
        copy: {
            model: {
                files: [{
                    expand: true,
                    cwd: '<%= project.testPath %>/',
                    src: '**',
                    dest: '.tmp'
                }]
            },
            build: {
                files: [{
                    expand: true,
                    cwd: '<%= project.appPath %>',
                    src: [
                        '**/*.js',
                        '**/**/*.js'
                    ],
                    dest: '<%= project.deployPath %>/',
                    rename: function (dest, src) {
                        // xx.view.js -> xx-dbg.view.js
                        // xx.fragment.js -> xx-dbg.fragment.js
                        // xx.controller.js -> xx-dbg.controller.js
                        var newDest = dest + src.replace('\.js', '-dbg.js')
                          .replace('\.view-dbg\.js', '-dbg.view.js')
                          .replace('\.controller-dbg\.js', '-dbg.controller.js')
                          .replace('\.fragment-dbg\.js', '-dbg.fragment.js');
                        return newDest;
                    }
                }, {
                    expand: true,
                    cwd: '<%= project.appPath %>',
                    src: [
                        '**/*.xml',
                        '**/*.json',
                        '**/*.properties',
                        '**/*.html',
                        '**/*.css',
                        '**/*.jpg',
                        '**/*.png',
                        '**/*.svg',
                        '**/*.ico',
                        '.Ui5RepositoryTextFiles',
                        '.Ui5RepositoryBinaryFiles',
                        '.Ui5RepositoryUploadParameters',
                        'css/**'
                    ],
                    dest: '<%= project.deployPath %>/'
                }]
            }
        },
        uglify: {
            deploy: {
                options: {
                    compress: true
                },
                files: [{
                    expand: true,
                    cwd: '<%= project.appPath %>',
                    src: [
                        '**/*.js',
                        '**/**/*.js',
                        '!theme/**/*.js'
                    ],
                    dest: '<%= project.deployPath %>/'
                }]
            }
        },
        open: {
            server: {
                path: 'http://' + '<%= project.host %>:' + '<%= project.serverPort %>/' + '<%= project.urlParameters %>'
            },
            doc: {
                path: 'http://' + '<%= project.host %>:' + '<%= project.docServerPort %>'
            }
        },
        clean: {
            options: {
                force: true
            },
            temp: '<%= project.tempPath %>',
            deploy: '<%= project.deployPath %>/**.*',
            preload: '<%= project.appPath %>/Component-preload*.js',
            doc: '<%= project.docPath %>'
        },
        concurrent: {
            localTasks: [
                'clean:temp',
                'less',
                // 'jshint',
                'copy:model'
            ],
            serverTasks: [
                'connect:livereload',
                'open:server',
                'watch'
            ]
        },
        concat: {
            options: {
                banner: 'jQuery.sap.registerPreloadedModules({\n' +
                  '  "name": "<%= project.appPrefix.replace(/\\./g,\'/\') %>/Component-preload",\n' +
                  '  "version" : "2.0",\n' +
                  '  "modules" : {\n',
                separator: ',\n',
                footer: '}});',
                process: function (src, filepath) {
                    return '"' + grunt.config().project.appPrefix.replace(/\./g, "/") + '/' + filepath.replace(grunt.config().project.appPath + "/", "") + '": function(){\n'
                      + src
                      + "\n}";
                }
            },
            dev: {
                src: '<%= project.preloadFiles %>',
                dest: '<%= project.appPath %>/Component-preload.js'
            }
        },
        charset: {
            dist: {
                options: {
                    from: 'UTF-8',
                    to: '<%= project.codePage %>',
                    fileTypes: {
                        // Code replacement config (Optional)
                    }
                },
                files: [{
                    expand: true,
                    cwd: '<%= project.appPath %>',
                    src: [
                        '**/*.properties'
                    ],
                    dest: '<%= project.deployPath %>'
                }]
            }
        },
        jsdoc: {
            doc: {
                src: ['<%= project.appPath %>/**/*.js'],
                dest: '<%= project.docPath %>',
                options: {
                    configure: './jsdoc-conf.json'
                }
            }
        },
        openui5_preload: {
            library: {
                options: {
                    resources: {
                        cwd: '<%= project.appPath %>',
                        src: '**/**'
                    },
                    dest: '<%= project.deployPath %>'
                },
                libraries: 'ixult'
            }
        },
        nwabap_ui5uploader: {
            options: {
                conn: {
                    server: '<%= project.deployProtocol %>' + '://' + '<%= project.deployServer %>' + ':' + '<%= project.deployPort %>',
                    client: '<%= project.deployMandt %>',
                    useStrictSSL: false
                },
                auth: {
                    user: 'SRA999',
                    password: 'feb2021%FEB'
                }
            },
            upload_build: {
                options: {
                    resources: {
                        cwd: '<%= project.deployPath %>',
                        src: 'ixult'
                    },
                    ui5: {
                        language: "EN",
                        package: "/IXULT/BC_UI",
                        bspcontainer: "/IXULT/BC_UI_LIB",
                        bspcontainer_text: "conovum UI5 common library",
                        transportno: "S4QK902615",
                        transport_use_locked: false,
                        calc_appindex: true
                    }
                },
            }
        }
    });

    // ------------------------------------------------------------
    // Grunt Loading
    // ------------------------------------------------------------
    grunt.loadNpmTasks('grunt-openui5');
    grunt.loadNpmTasks('grunt-nwabap-ui5uploader');

    // ------------------------------------------------------------
    // Grunt Tasks
    // ------------------------------------------------------------
    // Server
    grunt.registerTask('server', function (target) {
        switch (target) {
            case 'local':
                grunt.task.run([
                    'concurrent:localTasks',
                    'configureProxies',
                    'concurrent:serverTasks'
                ]);
                break;
            case 'deploy':
                grunt.task.run([
                    'clean:temp',
                    'jshint',
                    'configureProxies:backendAll',
                    'build',
                    'charset',
                    'connect:deploy',
                    'open:server',
                    'watch:deploy'
                ]);
                break;
            case 'doc':
                grunt.task.run([
                    'clean:doc',
                    'jsdoc',
                    'connect:doc',
                    'open:doc',
                    'watch:doc'
                ]);
                break;
            default:
                grunt.task.run([
                    'clean:temp',
                    'clean:preload',
                    // 'jshint',
                    'less',
                    'configureProxies:backendAll',
                    'connect:livereload',
                    'open:server',
                    'watch:livereload'
                ]);
                break;
        }
    });
    // Default Task
    // grunt.registerTask('default', ['server:local']);
    // Server deployment
    // grunt.registerTask('serverDeploy', ['server:deploy']);
    // Documentation
    // grunt.registerTask('doc', ['server:doc']);
    // Build process
    grunt.registerTask('build', [
        'clean:deploy',
        'clean:preload',
        'less',
        'concat',
        'copy:build',
        'uglify:deploy',
        'charset',
        'openui5_preload'
    ]);
    // Deploy using UI5 deployer
    grunt.registerTask('deploy-ui5', [
        'clean:deploy',
        'clean:preload',
        'less',
        // 'concat',
        'copy:build',
        'uglify:deploy',
        'charset',
        'openui5_preload',
        'nwabap_ui5uploader'
    ]);
};
