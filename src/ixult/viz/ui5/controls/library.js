sap.ui.define([
    "sap/ui/core/Core",
    "sap/ui/core/library"
], function (
    Core,
    Library
) {
    "use strict";

    /**
     * @namespace controls
     * @memberOf ixult.viz.ui5
     *
     * @summary SAPUI5 library for use with 'visualization'
     *  Controls, e.g. to display diagrams
     *
     * @public
     */
    sap.ui.getCore().initLibrary({
        name: "ixult.viz.ui5.controls",
        noLibraryCSS: true,
        dependencies: [],
        types: [
            "ixult.viz.ui5.controls.VizType"
        ],
        interfaces: [],
        controls: [],
        elements: [],
        version: "0.8.1"
    });

    var thisLib = ixult.viz.ui5.controls;

    /**
     * @summary Possible VizFrame Types for use
     *  with ChartContainers
     * @type {map}
     * @public
     */
    thisLib.VizType = {
        Column: "column",
        // dual_column: "dual_column",
        Bar: "bar",
        // dual_bar: "dual_bar",
        StackedBar: "stacked_bar",
        StackedColumn: "stacked_column",
        Line: "line",
        // dual_line: "dual_line",
        // combination: "combination",
        // bullet: "bullet",
        // time_bullet: "time_bullet",
        Bubble: "bubble",
        // time_bubble: "time_bubble",
        Pie: "pie",
        Donut: "donut"
        // timeseries_column: "timeseries_column",
        // timeseries_line: "timeseries_line",
        // timeseries_scatter: "timeseries_scatter",
        // timeseries_bubble: "timeseries_bubble",
        // timeseries_stacked_column: "timeseries_stacked_column",
        // timeseries_100_stacked_column: "timeseries_100_stacked_column",
        // timeseries_bullet: "timeseries_bullet",
        // timeseries_waterfall: "timeseries_waterfall",
        // timeseries_stacked_combination_scatter: "timeseries_stacked_combination_scatter",
        // vertical_bullet: "vertical_bullet",
        // dual_stacked_bar: "dual_stacked_bar",
        // 100_stacked_bar: "100_stacked_bar",
        // 100_dual_stacked_bar: "100_dual_stacked_bar",
        // dual_stacked_column: "dual_stacked_column",
        // 100_stacked_column: "100_stacked_column",
        // 100_dual_stacked_column: "100_dual_stacked_column",
        // stacked_combination: "stacked_combination",
        // horizontal_stacked_combination: "horizontal_stacked_combination",
        // dual_stacked_combination: "dual_stacked_combination",
        // dual_horizontal_stacked_combination: "dual_horizontal_stacked_combination",
        // heatmap: "heatmap",
        // treemap: "treemap",
        // waterfall: "waterfall",
        // horizontal_waterfall: "horizontal_waterfall",
        // area: "area",
        // radar: "radar"
    }

    return thisLib;

}, /* bExport= */ false);