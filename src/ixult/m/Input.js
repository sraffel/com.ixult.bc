sap.ui.define([
    "sap/m/Input",
    "ixult/base/util/array",
    "ixult/base/util/LibraryHelper",
    "./library"
], function (
    Input,
    array,
    LibraryHelper,
    MyLibrary
) {
    "use strict";
    const Library = LibraryHelper.register("ixult/m").loadStyleSheet();

    /**
     * @class Input
     * @extends sap.m.Input
     * @memberOf ixult.m
     *
     * @summary Input class with enhancements for the standard SAPUI5 control
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.m.Input sap.m.Input}
     *
     * @public
     */
    return Input.extend("ixult.m.Input", /** @lends ixult.m.Input.prototype */ {
        // The renderer to use; Important: Leave this empty,
        // so that the default Renderer is used
        renderer: {},

        // Metadata for the UI element; Used to define custom properties
        metadata: {
            properties: {
                /**
                 * @name ixult.m.Input#filterContains
                 * @type {boolean}
                 * @default true
                 * @description Changes the filter function to allow
                 *  the search for containing substrings
                 * @public
                 */
                filterContains: {
                    type: "boolean",
                    defaultValue: true
                }
            }
        },

        // Attributes
        oSuggestionPopover: null,

        /**
         * @summary Creates a new ixult.m.Input instance
         * @param {string} [sId]
         *  id for the new control, generated automatically if no id is given
         * @param {object} [mSettings]
         *  initial settings for the new control
         * @returns {void}
         * @public
         */
        constructor: function (sId, mSettings) {
            Input.prototype.constructor.apply(this, arguments);

            this.attachSuggest(this.onSuggest, this);
        },

        /**
         * @summary Handles the `beforeRendering` Event for the UI Control,
         *  that is executed when the DOM elements are created but not yet shown
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onBeforeRendering: function (oEvent) {
            Input.prototype.onBeforeRendering.apply(this, arguments);

            // Set an additional custom style class
            this.addStyleClass("ixultMInput");

            // Adjust the filter suggestion comparison function
            this._setSuggestionFilterFunction();
        },

        /**
         * @summary Handles the 'suggest' Event for the Input Control
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onSuggest: function (oEvent) {
            setTimeout(function () {
                // Get the suggestion Popover by looking for a corresponding
                // element that consists of this Input's id and `-popup`
                const oHtmlElement = $("#" + this.getId() + "-popup");
                if (oHtmlElement && oHtmlElement[0] && oHtmlElement[0].id) {
                    // Save the Popover element for later access
                    this.oSuggestionPopover = sap.ui.getCore().byId(oHtmlElement[0].id);
                    if (this.oSuggestionPopover) {
                        // Add a custom style class to the Popover
                        this.oSuggestionPopover.addStyleClass("ixultMInputSuggestionPopover");
                    }
                }
            }.bind(this), 100);
        },

        /**
         * @summary Sets the filter function to allow
         *  searching for containing substrings
         * @returns {void}
         * @private
         */
        _setSuggestionFilterFunction: function () {
            if (!this.getFilterContains()) {
                return;
            }

            // The filter comparison function for a
            // case-insensitive 'string contains' filter
            var fnFilterContains = function (sTerm, oItem) {
                var oRegExp = new RegExp(sTerm, "i");
                return oItem.getText ? oItem.getText().match(oRegExp) :
                    oItem.getKey ? oItem.getKey().match(oRegExp) : false;
            }

            // Apply the enhanced filter function
            this.setFilterFunction(function (sTerm, oItem) {
                // Check if the filter suggestion item is
                // e.g. a tabular `ColumnListItem` with cells
                if (oItem.getCells) {
                    var aCells = oItem.getCells();
                    for (var i = 0; i < aCells.length; i++) {
                        var oCellItem = aCells[i];
                        // Apply the filter function to each cell value
                        if (fnFilterContains(sTerm, oCellItem)) {
                            return true;
                        }
                    }
                    return false;
                } else {
                    // Execute the filter function
                    // directly on the suggestion item
                    return fnFilterContains(sTerm, oItem);
                }
            });
        }

    });
});
