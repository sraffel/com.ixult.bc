sap.ui.define([
    "sap/m/ColumnListItem",
    "ixult/base/util/array",
    "ixult/base/util/LibraryHelper",
    "./library"
], function (
    ColumnListItem,
    array,
    LibraryHelper,
    MyLibrary
) {
    "use strict";
    const Library = LibraryHelper.register("ixult/m").loadStyleSheet();

    /**
     * @class ColumnListItem
     * @extends sap.m.ColumnListItem
     * @memberOf ixult.m
     *
     * @classdesc ColumnListItem class with enhanced functionality
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.m.ColumnListItem sap.m.ColumnListItem}
     * @see {ixult.m.Table}
     *
     * @public
     */
    return ColumnListItem.extend("ixult.m.ColumnListItem", /** @lends ixult.m.ColumnListItem.prototype */ {
        // The renderer to use; Important: Leave this empty,
        // so that the default Renderer is used
        renderer: {},

        // Metadata for the UI element; Used to define custom properties
        metadata: {
            properties: {
                /**
                 * @name ixult.m.ColumnListItem#context
                 * @type {string}
                 * @description The data model context
                 *  path for this ListItem
                 * @public
                 */
                context: {
                    type: "string"
                },
                /**
                 * @name ixult.m.ColumnListItem#enabled
                 * @type {boolean}
                 * @default true
                 * @description Enables or disables the table row
                 *  by enabling or disabling Controls like Input
                 *  fields within
                 * @public
                 */
                enabled: {
                    type: "boolean",
                    defaultValue: true
                },
                /**
                 * @name ixult.m.ColumnListItem#highlighted
                 * @type {boolean}
                 * @default true
                 * @description adds the attribute `data-ui-selected`
                 *  to the rendered HTML element for this control, which
                 *  can be evaluated to set highlight colors
                 * @public
                 */
                highlighted: {
                    type: "boolean",
                    defaultValue: false
                },
                /**
                 * @name ixult.m.ColumnListItem#draggable
                 * @type {boolean}
                 * @default true
                 * @description Sets or removes the SAPUI5 property
                 *  of this ColumnListItem to be dragged
                 * @public
                 */
                draggable: {
                    type: "boolean",
                    defaultValue: true
                },
                /**
                 * @name ixult.m.ColumnListItem#droppable
                 * @type {boolean}
                 * @default true
                 * @description Determines, if this ColumnListItem
                 *  can be a drop source
                 * @public
                 */
                droppable: {
                    type: "boolean",
                    defaultValue: true
                }
            }
        },

        // Attributes
        _sModelName: null,
        _oHTMLElement: null,

        /**
         * @summary Initializes the UI Control
         * @returns {void}
         * @protected
         */
        init: function () {
            ColumnListItem.prototype.init.apply(this, arguments);

            // Attach custom event handlers
            this.attachModelContextChange(function (oEvent) {
                if (oEvent.getSource().getId() !== this.getId()) {
                    return;
                }
                if (this.getContext() !== undefined && this.getContext() > 0) {
                    return;
                }
                // Save the name of the bound data model
                if (!this._sModelName) {
                    const aKeys = Object.keys(this.oBindingContexts);
                    if (aKeys.length > 0) {
                        this._sModelName = aKeys[0];
                    }
                }
                var sContextPath = this.getBindingContextPath(this._sModelName);
                this.setContext(sContextPath);
            }, this);
        },

        /**
         * @summary Handles the `afterRendering` Event for the UI Control,
         *  that is executed when the Control is completely rendered and shown
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onAfterRendering: function (oEvent) {
            ColumnListItem.prototype.onAfterRendering.apply(this, arguments);
            this._oHTMLElement = $("#" + this.getId());

            // Insert an attribute for the `highlighted` property
            this._oHTMLElement.attr("data-ui-highlighted", "false");

            // Remove the HTML tag property that determines, if this element
            // can be dragged (basically just applies a different CSS style)
            if (this._oHTMLElement && !this.getDraggable()) {
                this._oHTMLElement.attr("data-sap-ui-draggable", "false");
                this._oHTMLElement.attr("draggable", "false");
            }
        },

        /**
         * @summary Sets this Control to `enabled` or `disabled`
         * @param {boolean} bEnabled
         *  true, if the Control should be enabled, false otherwise
         * @returns {void}
         * @public
         */
        setEnabled: function (bEnabled) {
            array(this.getCells()).loop(oCell => {
                var aContents = oCell.getContent ? array(oCell.getContent()) :
                    oCell.getItems ? array(oCell.getItems()) : array([oCell]);
                aContents.loop(oContent => {
                    if (oContent.isA("sap.m.Input") && oContent.setEnabled) {
                        oContent.setEnabled(bEnabled);
                    }
                });
            });
        },

        /**
         * @summary Sets this ColumnListItem to `highlighted`
         *  by changing the corresponding HTML attribute
         * @param {boolean} bHighlight
         *  The value to be set into the HTML `data-ui-highlighted`
         *  tag of this ColumnListItem
         * @returns {void}
         * @public
         * @override
         */
        setHighlighted: function (bHighlight) {
            this._oHTMLElement.attr("data-ui-highlighted", bHighlight.toString());
        }


    });
});
