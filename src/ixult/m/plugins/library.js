sap.ui.define([
    "sap/ui/core/Core",
    "sap/ui/core/library",
    "ixult/base/util/LibraryHelper"
], function (
    Core,
    Library,
    LibraryHelper
) {
    "use strict";
    LibraryHelper.register("ixult/m/plugins").loadStyleSheet();

    /**
     * @namespace plugins
     * @memberOf ixult.m
     *
     * @description Contains classes and functions with UI
     *  plugins to enhance mobile-first SAPUI5 Controls
     *
     * @public
     * @experimental
     */
    return sap.ui.getCore().initLibrary({
        name: "ixult.m.plugins",
        noLibraryCSS: true,
        dependencies: [
            "ixult.base",
            "ixult.ui",
            "sap.m",
            "sap.ui.core"
        ],
        types: [],
        interfaces: [],
        controls: [],
        elements: [],
        version: "0.8.1"
    });

}, /* bExport= */ false);