sap.ui.define([
    "sap/ui/core/Core",
    "sap/ui/core/library"
], function (
    Core,
    Library
) {
    "use strict";

    /**
     * @namespace m
     * @memberOf ixult
     *
     * @description The main conovum UI5 controls library, with responsive controls
     *  that can be used in touch devices as well as desktop browsers.
     *
     * @public
     */
    sap.ui.getCore().initLibrary({
        name: "ixult.m",
        noLibraryCSS: true,
        dependencies: [
            "ixult.base",
            "ixult.ui",
            "sap.m",
            "sap.ui.core"
        ],
        types: [
            "ixult.m.DisplayType",
            "ixult.m.TimeInputSteps"
        ],
        interfaces: [],
        controls: [
            "ixult.m.Clipboard",
            "ixult.m.Column",
            "ixult.m.ColumnListItem",
            "ixult.m.ComboBox",
            "ixult.m.Input",
            "ixult.m.JSONViewer",
            "ixult.m.MarkdownViewer",
            "ixult.m.OverflowToolbar",
            "ixult.m.Table",
            "ixult.m.TimeInput"
        ],
        elements: [],
        version: "0.8.1"
    });

    var thisLib = ixult.m;

    /**
     * @name ixult.m#DisplayType
     * @type {{
     *     List: string,
     *     ComboBox: string
     * }}
     * @summary Possible Display Types for use with
     *  Controls like {ixult.m.Column}
     * @public
     * @static
     */
    thisLib.DisplayType = {
        List: "List",
        ComboBox: "ComboBox"
    };

    /**
     * @name ixult.m#TimeInputSteps
     * @type {{
     *     Zero: string,
     *     Fifteen: string,
     *     Thirty: string
     * }}
     * @summary Steps to be used for the minute values inside the ixult.m.TimeInput UI Control
     * @description Specifies the steps available inside the ixult.m.TimeInput UI Control;
     *  <ul>
     *    <li>'00': Only allows full hour values. Renders the TimeInput Popover
     *     with a corresponding option to just input '00' minute values.</li>
     *    <li>'15': sets the minute steps to 15 minute intervalls. Renders the TimeInput Popover
     *     with corresponding options to input '15', '30', '45' and '00' minute values.</li>
     *    <li>'30': sets the minute steps to 30 minute intervalls. Renders the TimeInput Popover
     *     with corresponding options to input '30' and '00' minute values.</li>
     *  </ul>
     * @public
     * @static
     */
    thisLib.TimeInputSteps = {
        Zero: '00',
        Fifteen: '15',
        Thirty: '30'
    };


    return thisLib;

}, /* bExport= */ false);