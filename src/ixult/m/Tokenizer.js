sap.ui.define([
    "sap/m/Tokenizer",
    "ixult/ui/util/LocalStorage"
], function (
    Tokenizer,
    LocalStorage
) {
    "use strict";

    /**
     * @class ixult.m.Tokenizer
     * @extends sap.m.Tokenizer
     *
     * @summary Handler class for setting and getting Tokens
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.m.Tokenizer sap.m.Tokenizer}
     *
     * @param {string} [sId]
     *  ID for the new control, generated automatically if no ID is given
     * @param {object|map} [mSettings]
     *  Initial settings for the new control
     *
     * @public
     */
    return Tokenizer.extend("ixult.m.Tokenizer", /** @lends ixult.m.Tokenizer.prototype */ {
        /**
         * @description local storage session handler (type 'session')
         * @type {ixult.ui.util.LocalStorage}
         * @private
         */
        _oLocalStorage: null,
        /**
         * @summary Token handler object
         * @type {map}
         * @private
         */
        _oToken: {},

        /**
         * @summary Instantiates the Tokenizer instance
         * @param {string} [sId]
         *  ID for the new control, generated automatically if no ID is given
         * @param {object|map} [mSettings]
         *  Initial settings for the new control
         * @public
         * @hideconstructor
         */
        constructor: function (sId, mSettings) {
            Tokenizer.prototype.constructor.apply(this, arguments);

            // Save the local Storage config
            if (mSettings.storageConfig) {
                var oStorageConfig = this.getComponent().getManifestEntry(mSettings.localStorage);
                if (oStorageConfig) {
                    this._oLocalStorage = new LocalStorage(oStorageConfig);
                }
            }

            // Setup session and local Storage handler
            if (mSettings.sessionStorage) {
                this._oToken.session = new LocalStorage({
                    name: this.getId() + ".session",
                    type: LocalStorage.Type.Session
                });
            }
            if (mSettings.localStorage) {
                this._oToken.local = new LocalStorage({
                    name: this.getId() + ".local",
                    type: LocalStorage.Type.Local
                });
            }
        },

        /**
         * @summary Returns the LocalStorage if set
         * @returns {ixult.ui.util.LocalStorage}
         *  the LocalStorage handler instance
         * @public
         */
        getLocalStorage: function () {
            return this._oLocalStorage;
        },

        /**
         * @summary Parses a simple JSON object token or
         *  decodes a (base64 encoded) JSON Web Token and returns its contents
         * @param {string} sToken
         *  the (encoded) token string
         * @returns {map|object}
         *  the JSON parsed token contents
         * @see [Stack Overflow: How to decode jwt token in javascript without using a library?]
         *  (https://stackoverflow.com/questions/38552003)
         * @protected
         */
        parseToken: function (sToken) {
            if (typeof sToken !== "string") {
                console.error("Token is not a string, empty or invalid.", sToken);
                return sToken;
            }
            var oToken = null;
            try {
                oToken = JSON.parse(sToken);
            } catch (oException) {
                console.warn("Token is not a JSON object, trying JWT");
                try {
                    var sBase64String = sToken.split(".")[1]
                            .replace(/-/g, "+")
                            .replace(/_/g, "/"),
                        sJsonPayload = atob(sBase64String);
                    oToken = JSON.parse(sJsonPayload);
                } catch (oException) {
                    console.error("Error parsing token string", oException);
                }
            }
            return oToken;
        },

        /**
         * @summary Parses a given jqXHR object to extract its header fields and returns them as a map
         * @param {jqXHR} jqXHR
         *  the jqXHR object
         * @returns {map}
         *  the parsed header fields
         * @protected
         */
        getJqXhrResponseHeaders: function (jqXHR) {
            var mHeaders = {};
            var aFields = [];

            jQuery.each(jqXHR.getAllResponseHeaders().split("\r\n"), function (iIndex, sHeader) {
                aFields = sHeader.split(": ");
                if (aFields.length === 2) {
                    var sKey = aFields[0].trim();
                    var sValue = aFields[1].trim();
                    mHeaders[sKey] = sValue;
                    // Try to convert date strings (to allow something like session timer checks)
                    if (sKey.indexOf("date") !== -1) {
                        try {
                            mHeaders[sKey] = !isNaN(Date.parse(sValue)) ? Date(Date.parse(sValue)) : sValue;
                        } catch (oException) {
                            console.error("'" + aFields[1] + "' could not be parsed into a date.", oException);
                        }
                    }
                }
            }.bind(this));

            return mHeaders;
        },

        /**
         * @summary Formats a given parameter map coming from a successful or failed jQuery ajax request
         * @param {map} mParameters
         *  the parameter map containing the following attributes
         * @param {string} mParameters.textStatus
         *  the ajax call status text
         * @param {jqXHR} mParameters.jqXHR
         *  the ajax call jqXHR object
         * @param {object} [mParameters.data]
         *  the ajax call data object if the call was successful
         * @param {string} [mParameters.errorThrown]
         *  the ajax call error text if the call failed
         * @param {string} [mParameters.username]
         *  an optional username
         * @returns {map}
         *  a formatted map containing the source attributes as well as processed data
         * @protected
         */
        formatJqXhrResponse: function (mParameters) {
            if (!mParameters) {
                console.error("Error processing jqXHR response: No parameters found!");
                return null;
            }

            // Create the response object from every possible option
            var mResponse = {
                iStatus: mParameters.jqXHR ? mParameters.jqXHR.status : undefined,
                sStatusText: mParameters.textStatus,
                jqXHR: mParameters.jqXHR,
                oHeaders: mParameters.jqXHR ? _getJqXhrResponseHeaders(mParameters.jqXHR) : null,
                sUsername: mParameters.username,
                oData: mParameters.data,
                sErrorThrown: mParameters.errorThrown
            }

            // And then delete every attribute that has no values
            $.each(Object.keys(mResponse), function (iIndex, sKey) {
                if (mResponse[sKey] === null || mResponse[sKey] === undefined) {
                    delete mResponse[sKey];
                }
            });

            return mResponse;
        }

    });
});