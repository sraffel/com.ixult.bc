sap.ui.define([
    "sap/m/Panel",
    "sap/ui/core/Fragment",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/resource/ResourceModel",
    "ixult/base/util/array",
    "ixult/base/util/LibraryHelper",
    "ixult/lib/ui/util/Marked"
], function (
    Panel,
    Fragment,
    JSONModel,
    ResourceModel,
    array,
    LibraryHelper,
    LibMarked
) {
    "use strict";
    const Library = LibraryHelper.register("ixult/m").loadStyleSheet();

    /**
     * @class MarkdownViewer
     * @extends sap.m.Panel
     * @memberOf ixult.m
     *
     * @classdesc
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.m.Panel sap.m.Panel}
     *
     * @public
     *
     * @since 0.8.1
     */
    return Panel.extend("ixult.m.MarkdownViewer", /** @lends ixult.m.MarkdownViewer.prototype */ {
        // The renderer to use; Important: Leave this empty,
        // so that the default Renderer is used
        renderer: {},

        // Metadata for the UI element; Used to define custom properties
        metadata: {
            properties: {
                /**
                 * @name ixult.m.MarkdownViewer#markdown
                 * @type {string}
                 * @description The markdown formatted content
                 *  string that should be displayed
                 * @public
                 */
                markdown: {
                    type: "string",
                    defaultValue: ""
                },
                /**
                 * @name ixult.m.MarkdownViewer#showCopyButton
                 * @type {string}
                 * @description Shows the `Copy` Button to allow
                 *  copying the contents to the Clipboard
                 * @default `false`
                 * @public
                 */
                showCopyButton: {
                    type: "boolean",
                    defaultValue: false
                }
            }
        },

        // Attributes
        oFragment: null,
        oMarkdownViewer: null,
        _bIsInitialized: false,

        /**
         * @constructor
         * @summary Creates a new MarkdownViewer (Panel) Control
         * @param {string} sId
         *  ID for the new control, generated automatically if no ID is given
         * @param {map|object} mSettings
         *  Initial settings for the new Control
         * @public
         * @hideconstructor
         */
        constructor: function (sId, mSettings) {
            Panel.prototype.constructor.apply(this, arguments);
        },

        /**
         * @summary Initializes the UI control
         * @returns {void}
         * @protected
         */
        init: function () {
            Panel.prototype.init.apply(this, arguments);

            // Attach custom event handler
            this.attachExpand(this.onExpand, this);
        },

        /**
         * @summary Handles the `beforeRendering` Event for the UI Control,
         *  that is executed when the DOM elements are created but not yet shown
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onBeforeRendering: function (oEvent) {
            if (this._bIsInitialized) {
                return;
            }

            // Set the texts for this Control
            this.setModel(new ResourceModel({
                bundle: Library.getResourceBundle()
            }), "messagebundle");

            // Apply the properties for this Control
            this.setModel(new JSONModel({
                headerText: this.getHeaderText(),
                showCopyButton: this.getShowCopyButton()
            }), "properties");

            // Load the XML fragment to display the formatted JSON data
            Fragment.load({
                name: "ixult.m.MarkdownViewer",
                type: "XML",
                controller: this
            }).then(function (oFragment) {
                this.oFragment = oFragment;

                // Add the custom header Toolbar
                this.setHeaderToolbar(oFragment.getHeaderToolbar());

                // Add the contents for the Markdown Viewer
                this.removeAllContent();
                array(oFragment.getContent()).loop(function (oContent) {
                    this.addContent(oContent);
                }.bind(this));

                // Execute the parent `beforeRendering` __after__
                // this Toolbar has been set into place, because it
                // influences the way the `Expand` Button will be rendered
                Panel.prototype.onBeforeRendering.apply(this, arguments);

                this._bIsInitialized = true;
            }.bind(this));
        },

        /**
         * @summary Handles the `afterRendering` Event for the UI Control,
         *  that is executed when the Control is completely rendered and shown
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onAfterRendering: function (oEvent) {
            Panel.prototype.onAfterRendering.apply(this, arguments);

            this.addStyleClass("ixultMMarkdownViewer");
        },

        /**
         * @summary Handles the `expand` Event for this Panel
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onExpand: function (oEvent) {
            // Nothing to do here at the moment :-)
        },

        /**
         * @summary Handles the `afterRendering` Event for
         *  the inner HTML UI Control, that should display
         *  the Markdown formatted contents
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onAfterRenderingContent: function (oEvent) {
            if (oEvent.getSource().getContent()) {
                return;
            }
            if (!marked) {
                console.error("Couldn't find markdown renderer. Exiting.");
            }
            oEvent.getSource().setContent(marked.parse(this.getMarkdown()));
        },

        /**
         * @summary Handles the `press` Event on the Header Toolbar
         * @param {sap.ui.base.Event} oEvent
         *  the UI Event instance
         * @returns {void}
         * @protected
         * @param oEvent
         */
        onToolbarPress: function (oEvent) {
            this.setExpanded(!this.getExpanded());
        },

        /**
         * @summary Handles the `press` Event on the `Copy`
         *  Button in the Header Toolbar; Copies the JSON
         *  string to the Clipboard
         * @param {sap.ui.base.Event} oEvent
         *  the UI Event instance
         * @returns {void}
         * @protected
         */
        onCopyButtonClick: function (oEvent) {
            navigator.clipboard.writeText(this.getMarkdown());
        }
    });
});