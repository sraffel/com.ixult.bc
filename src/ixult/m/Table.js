sap.ui.define([
    "sap/m/Table",
    "sap/ui/core/SortOrder",
    "sap/ui/events/KeyCodes",
    "sap/ui/model/Filter",
    "sap/ui/model/Sorter",
    "ixult/base/assert",
    "ixult/base/util/array",
    "ixult/base/util/object",
    "ixult/base/util/DateHelper",
    "ixult/base/util/LibraryHelper"
], function (
    Table,
    SortOrder,
    KeyCodes,
    Filter,
    Sorter,
    assert,
    array,
    object,
    DateHelper,
    LibraryHelper
) {
    "use strict";
    const Library = LibraryHelper.register("ixult/m").loadStyleSheet();

    /**
     * @class ixult.m.Table
     * @extends sap.m.Table
     *
     * @classdesc Responsive Table Control with additional capabilities,
     *  like sortable and filterable columns
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.m.Table sap.m.Table}
     *
     * @public
     */
    return Table.extend("ixult.m.Table", /** @lends ixult.m.Table.prototype */ {
        // The renderer to use; Important: Leave this empty,
        // so that the default Renderer is used
        renderer: {},

        // Metadata for the UI element; Used to define custom properties
        metadata: {
            properties: {
                /**
                 * @summary Allows the columns to be resizable when true
                 * @see {@link https://ui5.sap.com/sdk/#/api/sap.m.plugins.ColumnResizer sap.m.plugins.ColumnResizer}
                 * @type {boolean}
                 * @default false
                 */
                resizableColumns: {
                    type: "boolean",
                    defaultValue: false
                },
                /**
                 * @summary Sets the table layout to fixed
                 * @type {string}
                 * @default `false`
                 */
                fixedLayout: {
                    type: "string",
                    defaultValue: false
                },
                /**
                 * @summary Sets the table header to sticky allowing it
                 *  to stay in place when scrolling the table
                 * @type {boolean}
                 * @default `true`
                 */
                stickyHeader: {
                    type: "boolean",
                    defaultValue: true
                },
                /**
                 * @summary Sets the table footer to sticky allowing it
                 *  to stay in place when scrolling the table
                 * @type {boolean}
                 * @default `true`
                 */
                stickyFooter: {
                    type: "boolean",
                    defaultValue: true
                },
                /**
                 * @summary Sets the starting index for the keyboard
                 *  navigation index from which `tabindex` Controls
                 *  are counted from
                 * @type {int|Number}
                 * @default `1000`
                 */
                keyboardStart: {
                    type: "int",
                    defaultValue: 1000
                },
                /**
                 * @summary a string array of HTML control names
                 *  that should be used for keyboard navigation
                 * @type {string[]}
                 * @default `["Input","Button"]`
                 */
                keyboardElements: {
                    type: "string[]",
                    defaultValue: ["Input", "Button"]
                }
            },
            events: {
                /**
                 * @summary Fired before the Control is rendered
                 *  and its contents are placed into the DOM
                 * @type {sap.ui.base.Event}
                 */
                beforeRendering: {
                    allowPreventDefault: true
                },
                /**
                 * @summary Fired after the Control is rendered
                 * @type {sap.ui.base.Event}
                 */
                afterRendering: {
                    allowPreventDefault: true
                },
                /**
                 * @summary Event to notify calling views about
                 *  an update on the table sorter
                 * @type {sap.ui.base.Event}
                 */
                sortUpdate: {
                    allowPreventDefault: true
                },
                /**
                 * @summary Event to notify calling views about
                 *  an update on the table filter
                 * @type {sap.ui.base.Event}
                 */
                filterUpdate: {
                    allowPreventDefault: true
                },
                /**
                 * @summary Event to allow handling of shortcuts
                 *  or detect other keyboard press events; Adds the
                 *  type of keyboard event as well as a keymap to
                 *  make key comparisons easier
                 * @type {sap.ui.base.Event}
                 */
                keyPress: {
                    allowPreventDefault: true
                }
            }
        },

        // Attributes
        mAttributes: {
            LineId: "data-ui-line-id",
            ItemIndex: "data-ui-item-index",
            TabIndex: "data-ui-tab-index"
        },
        // References
        _oView: null,
        _oController: null,
        // Maps
        _mFilter: null,
        // Arrays
        _aFilters: [],
        _aMergeableColumns: [],
        // Booleans
        _bIsInitialized: false,
        _bIsRendered: false,

        /**
         * @summary Initializes the Table Control
         * @returns {void}
         * @protected
         */
        init: function () {
            Table.prototype.init.apply(this, arguments);
        },

        /**
         * @summary Handles the `beforeRendering` Event for the UI Control,
         *  that is executed when the DOM elements are created but not yet shown
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onBeforeRendering: function (oEvent) {
            // This has to be done only once, this is to avoid re-setup of the Control
            // because after executing sorting on the Table, the bound controls seem to rerender
            if (!this._bIsInitialized) {
                Table.prototype.onBeforeRendering.apply(this, arguments);

                // Save View and Controller instance for later access
                this._oView = Library.getViewFor(this);
                if (this._oView && this._oView.getController) {
                    this._oController = this._oView.getController();
                }

                // Add functionality for resizable columns if set
                if (this.getResizableColumns && this.getResizableColumns()) {
                    sap.ui.require(["ixult/m/plugins/ColumnResizer"], function (ColumnResizer) {
                        this.addDependent(new ColumnResizer());
                    }.bind(this));
                }

                // Setup custom CSS classes for this Control
                this.addStyleClass("ixultMTable");

                // Call a 'beforeRendering' handler on each applicable
                // custom Column because that Element has no such Event
                this._getCustomColumns().loop(function (oColumn) {
                    // Attach event listeners to handle updates for
                    // sorters and filters (initiated by choosing
                    // corresponding options in the Column's HeaderPopover)
                    oColumn.attachSortUpdate(this.onSortUpdate, this);
                    oColumn.attachFilterUpdate(this.onFilterUpdate, this);
                    // Initiate rendering of the Column UI Control
                    oColumn.fireBeforeRendering({
                        oTable: this
                    });
                }.bind(this));

                // Save the Columns that were set to 'mergeable' using the
                // sap.m.Column attribute 'mergeDuplicates'
                array(this.getColumns()).grep(oColumn => {
                    return oColumn.getMergeDuplicates &&
                        oColumn.getMergeDuplicates() === true;
                }).loop(function (oColumn, iIndex) {
                    this._aMergeableColumns.push({
                        iIndex: iIndex,
                        oColumn: oColumn
                    });
                }.bind(this));

                // ... and we're done :-)
                this._bIsInitialized = true;
            }
        },

        /**
         * @summary Handles the `afterRendering` Event for the UI Control,
         *  that is executed when the Control is completely rendered and shown
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onAfterRendering: function (oEvent) {
            // This is for one time setups that should be executed
            // after the table has been initially rendered (and not
            // everytime e.g. after a resize)
            if (!this._bIsRendered) {
                Table.prototype.onAfterRendering.apply(this, arguments);

                // Set up a sticky Table Header using a specialized custom property
                if (this.getStickyHeader()) {
                    this.setSticky([sap.m.Sticky.ColumnHeaders]);
                }

                // Only attach the keyboard event listeners if there's an implementation
                if (this.hasListeners("keyPress")) {
                    // Attach the `keydown` event; useful to recognize
                    // continuously pressed keys like CTRL or SHIFT
                    $("#" + this.getId()).keydown(function (oKeyEvent) {
                        this.fireKeyPress({
                            type: "keydown",
                            keyEvent: oKeyEvent,
                            originalEvent: oKeyEvent.originalEvent,
                            keyCodes: KeyCodes
                        });
                    }.bind(this));
                    // Attach the `keypress` event; useful
                    // to recognize single keystrokes
                    $("#" + this.getId()).keypress(function (oKeyEvent) {
                        this.fireKeyPress({
                            type: "keypress",
                            keyEvent: oKeyEvent,
                            originalEvent: oKeyEvent.originalEvent,
                            keyCodes: KeyCodes
                        });
                    }.bind(this));
                    // Attach the `keyup` event; useful
                    // to handle both of the above events
                    $("#" + this.getId()).keyup(function (oKeyEvent) {
                        this.fireKeyPress({
                            type: "keyup",
                            keyEvent: oKeyEvent,
                            originalEvent: oKeyEvent.originalEvent,
                            keyCodes: KeyCodes
                        });
                    }.bind(this));
                }

                // ... and we're done :-)
                this._bIsRendered = true;
            }

            // Call an 'afterRendering' handler on each applicable
            // custom Column because that Element has no such Event
            this._getCustomColumns().loop(function (oColumn) {
                oColumn.fireAfterRendering({
                    oTable: this
                });
            }.bind(this));

            // Set up the Table Footer
            var oTableFooterElement = $("#" + this.getId() + " .sapMListTblFooter");
            if (oTableFooterElement && oTableFooterElement.hasClass) {
                if (!oTableFooterElement.hasClass("ixultMFooter")) {
                    oTableFooterElement.addClass("ixultMFooter");
                }
                if (this.getStickyFooter() && !oTableFooterElement.hasClass("ixultMStickyFooter")) {
                    oTableFooterElement.addClass("ixultMStickyFooter");
                }
            }
        },

        /**
         * @function
         * @name ixult.m.Table.prototype.onKeyPress
         * @summary Handles the 'keyPress' event attached to this Table instance
         *  allows for handling `keypress`, `keydown` or `keyup` events
         * @param {sap.ui.base.Event} oEvent
         *  the keyboard press event
         * @param {map} oEvent.mParameters
         *  a map of parameters included in this event
         * @param {string} oEvent.mParameters.type
         *  type of event; `keypress`, `keydown` or `keyup`
         * @param {jQuery.Event} oEvent.mParameters.keyEvent
         *  the jQuery `keypress`, `keydown` or `keyup` event
         * @param {KeyboardEvent} oEvent.mParameters.originalEvent
         *  the original keyboard event
         * @param {sap.ui.events.KeyCodes} oEvent.mParameters.keyCodes
         *  a map of key codes to allow for easier comparison
         * @returns {void}
         * @protected
         * @abstract
         */

        /**
         * @summary Returns the ListBinding for this `items` aggregation
         * @see {@link https://ui5.sap.com/sdk/#/api/sap.ui.model.ClientListBinding sap.ui.model.ClientListBinding}
         * @see {@link https://ui5.sap.com/sdk/#/api/sap.ui.model.JSONListBinding sap.ui.model.JSONListBinding}
         * @returns {sap.ui.model.ListBinding|sap.ui.model.Binding}
         *  the ListBinding for the Table's `items`
         * @protected
         */
        getItemBinding: function () {
            return this.getBinding("items");
        },

        /**
         * @summary Returns an array of the custom Column type {ixult.m.Column}
         * @returns {ixult.m.Column[]}
         *  the array of custom Columns
         * @private
         */
        _getCustomColumns: function () {
            return array(this.getColumns()).grep(oColumn => {
                return oColumn.isA("ixult.m.Column");
            });
        },

        /**
         * @summary Handles a `sortUpdate` Event initiated by an
         *  applicable {ixult.m.Column} Control
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onSortUpdate: function (oEvent) {
            var sColumnId = oEvent.getParameter("sColumnId"),
                vSorter = oEvent.getParameter("vSorter");

            // Reset the sort order for all other Columns
            this._getCustomColumns().grep(oColumn => {
                return oColumn.getId() !== sColumnId;
            }).loop(oColumn => {
                oColumn.setSorted(false);
            });

            // Apply the Sorter
            this.getItemBinding().sort(vSorter);

            // Notify implementing listeners about the
            // sorter update on this Table
            this.fireSortUpdate({
                sColumnId: sColumnId,
                vSorter: vSorter
            });
        },

        /**
         * @summary Handles a `filterUpdate` Event initiated by an
         *  applicable {ixult.m.Column} Control
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @private
         */
        onFilterUpdate: function (oEvent) {
            var sColumnId = oEvent.getParameter("sColumnId"),
                vFilter = oEvent.getParameter("vFilter");

            // Update the `filtered` property the Columns
            this._getCustomColumns().grep(oColumn => {
                return oColumn.getId() !== sColumnId;
            }).loop(oColumn => {
                oColumn.setFiltered(false);
            });

            // Apply the Filter
            this.getItemBinding().filter(vFilter);

            // Notify implementing listeners about the
            // filter update on this Table
            this.fireFilterUpdate({
                sColumnId: sColumnId,
                vFilter: vFilter
            });
        }

    });
});
