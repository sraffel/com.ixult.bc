sap.ui.define([
    "sap/m/Popover",
    "sap/ui/Device",
    "sap/ui/core/CustomData",
    "sap/ui/core/Fragment",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/resource/ResourceModel",
    "ixult/base/assert",
    "ixult/base/util/array",
    "ixult/base/util/object",
    "ixult/base/util/DateHelper",
    "ixult/base/util/LibraryHelper"
], function (
    Popover,
    Device,
    CustomData,
    Fragment,
    SortOrder,
    JSONModel,
    ResourceModel,
    assert,
    array,
    object,
    DateHelper,
    LibraryHelper
) {
    "use strict";
    const Library = LibraryHelper.register("ixult/m").loadStyleSheet();

    /**
     * @class Clipboard
     * @extends sap.m.Popover
     * @memberOf ixult.m
     *
     * @summary Popover displaying a simple Clipboard Control
     *  that allows dynamic copy and paste actions
     *
     * @public
     */
    return Popover.extend("ixult.m.Clipboard", /** @lends ixult.m.Clipboard.prototype */ {

        // Metadata for the UI element; Used to define custom properties
        metadata: {
            properties: {}
        },

        // the array that contains the clipboard's entries
        aClipboard: array([]),

        // the source UI Control that the popover is opened next to
        _oSource: null,

        // the actual Popover UI Control
        _oPopover: null,

        /**
         * @summary Opens the Popover Control
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance, containing the UI Control
         *  that the Popover is opened next to
         * @returns {Promise}
         *  a Promise handler with `resolve` (`then`) and `reject` (`fail`)
         *  handlers to be executed after the Popover has been opened
         *  or created
         * @public
         * @overwrite
         */
        openBy: function (oEvent) {
            this._oSource = oEvent.getSource();

            // Create or retrieve the Column Header Popover Control
            if (!this._oPopover) {
                Fragment.load({
                    id: this.getId() + "-popover",
                    name: "ixult.m.ClipboardPopover",
                    type: "XML",
                    controller: {
                        onBeforePopoverOpen: this.onBeforePopoverOpen.bind(this),
                        onAfterPopoverOpen: this.onAfterPopoverOpen.bind(this),
                        onPopoverOkayButtonPress: this.onPopoverOkayButtonPress.bind(this),
                        onPopoverCancelButtonPress: this.onPopoverCancelButtonPress.bind(this),
                        onPopoverCloseButtonPress: this.onPopoverCloseButtonPress.bind(this)
                    }
                }).then(function (oFragment) {
                    this._oPopover = oFragment;
                    // Set the texts for the Popover
                    this._oPopover.setModel(new ResourceModel({
                        bundle: Library.getResourceBundle()
                    }), "messagebundle");
                    this._oPopover.openBy(this._oSource);
                }.bind(this));
            } else {
                this._oPopover.openBy(this._oSource);
            }
        },

        /**
         * @summary Handles the `beforeOpen` Event on the Popover
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onBeforePopoverOpen: function (oEvent) {
        },

        /**
         * @summary Handles the `afterOpen` Event on the Popover
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onAfterPopoverOpen: function (oEvent) {
        },

        /**
         * @summary Handles the `press` event on the 'Close' Button
         *  in the Popover Header (used on mobile devices)
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onPopoverCloseButtonPress: function (oEvent) {
            this._oPopover.close();
        },

        /**
         * @summary Handles the `press` event on the 'Okay' Button in the Header Popover
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onPopoverOkayButtonPress: function (oEvent) {
            this._oPopover.close();
        },

        /**
         * @summary Handles the `press` event on the 'Cancel' Button in the Header Popover
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onPopoverCancelButtonPress: function (oEvent) {
            this._oPopover.close();
        }


    });
});