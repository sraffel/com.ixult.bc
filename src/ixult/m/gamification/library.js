sap.ui.define([
    "sap/ui/core/Core",
    "sap/ui/core/library"
], function (
    Core,
    Library
) {
    "use strict";

    /**
     * @namespace gamification
     * @memberOf ixult.m
     *
     * @description Contains classes and functions to allow the implementation
     *  of 'gamification' Controls and UI enhancements, e.g. XBox like
     *  achievements
     *
     * @public
     * @experimental
     */
    return sap.ui.getCore().initLibrary({
        name: "ixult.m.gamification",
        noLibraryCSS: true,
        dependencies: [
            "ixult.base",
            "ixult.ui",
            "sap.m",
            "sap.ui.core"
        ],
        types: [],
        interfaces: [],
        controls: [],
        elements: [],
        version: "0.8.1"
    });

}, /* bExport= */ false);