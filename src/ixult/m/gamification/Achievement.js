sap.ui.define([
    "sap/ui/base/ManagedObject",
    "sap/m/List",
    "sap/m/CustomListItem",
    "sap/m/StandardListItem",
    "sap/m/MessageBox",
    "sap/ui/model/json/JSONModel",
    "ixult/base/assert",
    "ixult/base/util/DateHelper",
    "ixult/base/util/LibraryHelper"
], function (
    ManagedObject,
    List,
    CustomListItem,
    StandardListItem,
    MessageBox,
    JSONModel,
    assert,
    DateHelper,
    LibraryHelper
) {
    "use strict";
    const Library = LibraryHelper.register("ixult/m/gamification");

    /**
     * @class Achievement
     * @memberOf ixult.m.gamification
     * @extends sap.ui.base.ManagedObject
     *
     * @classdesc Achievement management class, offering create and display
     *  gaming like achievements to users for a little fun at work :-)
     *  | ❗❗ Note : This class is experimental and not yet implemented! |
     *  |:---|
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.ui.base.ManagedObject sap.ui.base.ManagedObject}
     *
     * @param {map} mParameters
     *  a parameter map, containing the following attributes:
     * @returns {void}
     *
     * @public
     * @experimental
     */
    return ManagedObject.extend("ixult.m.gamification.Achievement", /** @lends ixult.m.gamification.Achievement.prototype */ {

        /**
         * @constructor
         * @summary Creates a new ixult.m.gamification.Achievement instance
         * @hideconstructor
         */
        constructor: function (mParameters) {
            ManagedObject.prototype.constructor.apply(this);

            // TODO: implement :->
        }

    });
});