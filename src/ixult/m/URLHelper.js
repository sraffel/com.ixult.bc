sap.ui.define([
    "sap/base/security/encodeURL",
    "sap/m/library"
], function (
    encodeURL,
    MLibrary
) {
    "use strict"

    /**
     * @namespace URLHelper
     * @memberOf ixult.m
     *
     * @summary Static helper providing methods
     *  for advanced handling and manipulation of URLs
     *
     * @public
     */
    var oURLHelper = MLibrary.URLHelper;

    /**
     * @function splitURL
     * @memberOf ixult.m.URLHelper
     * @summary Splits a given URL string into its parts
     * @param {string} sURL
     *  the URL to split
     * @returns {map<{url: string, parameters: map}>}
     *  a map containing the url and its parameters (if found)
     * @public
     * @static
     */
    oURLHelper.splitURL = function (sURL) {
        // Separate URL and parameters from the given string
        var aUrlParts = sURL.split("?"),
            mUrlData = {
                url: aUrlParts[0],
                parameters: {}
            };

        // Extract the URL parameters
        if (aUrlParts[1]) {
            var aUrlParams = aUrlParts[1].split("&");
            aUrlParams.forEach(function (sParam) {
                var aParam = sParam.split("=");
                mUrlData.parameters[aParam[0]] = aParam[1];
            });
        }

        return mUrlData;
    }

    /**
     * @function encodeURL
     * @memberOf ixult.m.URLHelper
     * @summary Encodes a given URL string
     * @description Convenience method forwarding to sap.base.security.encodeURL
     * @see {@link https://ui5.sap.com/sdk/#/api/module:sap/base/security/encodeURL sap.base.security.encodeURL}
     * @param {string} sURL
     *  the URL to encode
     * @returns {string}
     *  the encoded URL
     * @public
     * @static
     */
    oURLHelper.encodeURL = function (sURL) {
        return encodeURL(sURL);
    }

    return oURLHelper;
});