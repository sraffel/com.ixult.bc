sap.ui.define([
    "sap/m/Column",
    "sap/ui/Device",
    "sap/ui/core/CustomData",
    "sap/ui/core/Fragment",
    "sap/ui/core/SortOrder",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/resource/ResourceModel",
    "sap/ui/model/Filter",
    "sap/ui/model/Sorter",
    "ixult/base/assert",
    "ixult/base/util/array",
    "ixult/base/util/object",
    "ixult/base/util/DateHelper",
    "ixult/base/util/LibraryHelper",
    "./library"
], function (
    Column,
    Device,
    CustomData,
    Fragment,
    SortOrder,
    JSONModel,
    ResourceModel,
    Filter,
    Sorter,
    assert,
    array,
    object,
    DateHelper,
    LibraryHelper,
    MyLibrary
) {
    "use strict";
    const Library = LibraryHelper.register("ixult/m").loadStyleSheet();

    /**
     * @class Column
     * @extends sap.m.Column
     * @memberOf ixult.m
     *
     * @classdesc Column for the responsive ixult.m.Table Control
     *  featuring additional properties like sorting and filtering
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.m.Column sap.m.Column}
     * @see {ixult.m.Table}
     *
     * @public
     */
    return Column.extend("ixult.m.Column", /** @lends ixult.m.Column.prototype */ {

        // Metadata for the UI element; Used to define custom properties
        metadata: {
            properties: {
                /**
                 * @name ixult.m.Column#name
                 * @type {string}
                 * @description An optional name for this Column to be
                 *  identified easier by the parent Table and allowing it
                 *  to trigger certain events like a `filterRequest`
                 * @public
                 */
                name: {
                    type: "string",
                    defaultValue: ""
                },
                /**
                 * @name ixult.m.Column#filterable
                 * @type {object}
                 * @description Determines, if the Column can be filtered,
                 *  which shows or hides the corresponding filter options
                 *  in the Column Header Popover
                 * @default `true`
                 * @public
                 */
                filterable: {
                    type: "boolean",
                    group: "Filter",
                    defaultValue: true
                },
                /**
                 * @name ixult.m.Column#filtered
                 * @type {boolean}
                 * @description Allows to set this Column as filtered,
                 *  which will be indicated by a corresponding filter icon
                 * @default `false`
                 * @public
                 */
                filtered: {
                    type: "boolean",
                    group: "Filter",
                    defaultValue: false
                },
                /**
                 * @name ixult.m.Column#headerAlign
                 * @type {sap.ui.core.TextAlign}
                 * @description Align of the Column Header;
                 *  Will add the CSS style class `ixultMColumnHeader<value>`
                 *  to this Column, with `<value>` being one of the valid
                 *  {sap.ui.core.TextAlign} options, e.g. `Center`, `Begin`,
                 *  `End`; `Center` by default
                 * @default `Center`
                 * @public
                 */
                headerAlign: {
                    type: "sap.ui.core.TextAlign",
                    defaultValue: "Center",
                    group: "Display"
                },
                /**
                 * @name ixult.m.Column#headerPopoverHeight
                 * @type {string}
                 * @description The height of the Header Popover content
                 * @default `25%`
                 * @public
                 */
                headerPopoverHeight: {
                    type: "string",
                    defaultValue: "25%",
                    group: "Display"
                },
                /**
                 * @name ixult.m.Column#headerPopoverPlacement
                 * @type {sap.m.PlacementType}
                 * @description the Header Popover placement relative to the
                 *  Column Header; `Bottom` by default
                 * @default `Bottom`
                 * @public
                 */
                headerPopoverPlacement: {
                    type: "sap.m.PlacementType",
                    defaultValue: "Bottom",
                    group: "Display"
                },
                /**
                 * @name ixult.m.Column#headerTitle
                 * @type {string}
                 * @description The Header Popover's title;
                 *  Used as a Label above the filter list if specified
                 * @public
                 */
                headerTitle: {
                    type: "string"
                },
                /**
                 * @name ixult.m.Column#sortable
                 * @type {boolean}
                 * @description Sets the Column to sortable, thereby
                 *  enabling the Column Header Popover with sort options
                 * @default `true`
                 * @public
                 */
                sortable: {
                    type: "boolean",
                    defaultValue: true,
                    group: "Sorter"
                }
            },
            aggregations: {
                /**
                 * @name ixult.m.Column#filter
                 * @type {ixult.ui.core.Filter}
                 * @description Specifies the Filter that can be applied
                 *  to this Column; Determines the `filter` action in
                 *  the `ColumnHeaderPopover`
                 * @public
                 * @since 0.8.1
                 */
                filter: {
                    type: "ixult.ui.core.Filter",
                    multiple: false
                },
                /**
                 * @name ixult.m.Column#sorter
                 * @type {ixult.ui.core.Sorter}
                 * @description Specifies the Sorter or an array of Sorters,
                 *  that can be applied to this Column; Determines the `sort`
                 *  action in the `ColumnHeaderPopover`
                 * @public
                 * @since 0.8.1
                 */
                sorter: {
                    type: "ixult.ui.core.Sorter",
                    multiple: false
                }
            },
            events: {
                /**
                 * @name ixult.m.Column#beforeRendering
                 * @type {sap.ui.base.Event}
                 * @description Called during `beforeRendering` of an applicable
                 *  {ixult.m.Table}; Allows for custom processing in this Column
                 *  on `beforeRendering` of the Table, because there's no such
                 *  Event on this Element
                 * @param {ixult.m.Table} table
                 *  The table that initiated this Column rendering
                 * @public
                 */
                beforeRendering: {
                    allowPreventDefault: true,
                    parameters: {
                        table: {
                            type: "ixult.m.Table"
                        }
                    }
                },
                /**
                 * @name ixult.m.Column#afterRendering
                 * @type {sap.ui.base.Event}
                 * @description Called during `afterRendering` of an applicable
                 *  {ixult.m.Table}; Allows for custom processing in this Column
                 *  on `afterRendering` of the Table, because there's no such #
                 *  Event on this Element
                 * @param {ixult.m.Table} table
                 *  The table that initiated this Column rendering
                 * @public
                 */
                afterRendering: {
                    allowPreventDefault: true,
                    parameters: {
                        table: {
                            type: "ixult.m.Table"
                        }
                    }
                },
                /**
                 * @name ixult.m.Column#sortUpdate
                 * @type {sap.ui.base.Event}
                 * @description Informs all applicable Listeners that a Sort option in the
                 *  HeaderPopover was chosen to allow initiating corresponding actions;
                 *  Used e.g. in applicable {ixult.m.Table} Controls to update the
                 *  ItemBinding sort
                 * @public
                 * @since 0.8.1
                 */
                sortUpdate: {
                    allowPreventDefault: true
                },
                /**
                 * @name ixult.m.Column#filterUpdate
                 * @type {sap.ui.base.Event}
                 * @description Informs all applicable Listeners that a Filter option in the
                 *  HeaderPopover was chosen to allow initiating corresponding actions;
                 *  Used e.g. in applicable {ixult.m.Table} Controls to update the
                 *  ItemBinding filter
                 * @public
                 * @since 0.8.1
                 */
                filterUpdate: {
                    allowPreventDefault: true
                }
            }
        },

        // Attributes
        _oView: null,
        _oTable: null,
        _oPopover: null,
        _bIsInitialized: false,
        _bFiltered: false,

        /**
         * @summary Creates a new ixult.m.Column instance
         * @param {string} [sId]
         *  id for the new control, generated automatically if no id is given
         * @param {object} [mSettings]
         *  initial settings for the new control
         * @returns {void}
         * @public
         * @hideconstructor
         */
        constructor: function (sId, mSettings) {
            Column.prototype.constructor.apply(this, arguments);

            // Create model data for this Column
            this.setColumnData({
                Filtered: this.getFiltered().toString(),
                SortOrder: this.getSortOrder().toLowerCase()
            });
        },

        /**
         * @summary Initializes the UI control
         * @returns {void}
         * @protected
         */
        init: function () {
            Column.prototype.init.apply(this);

            // Attach internal event handler to handle the
            // 'beforeRendering' and 'afterRendering' events
            // from the parent ixult.m.Table
            this.attachBeforeRendering(this.onBeforeRendering, this);
            this.attachAfterRendering(this.onAfterRendering, this);
        },

        /**
         * @summary Internal handling of the 'beforeRendering' event on this
         *  Control, before possible custom event listeners are executed
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onBeforeRendering: function (oEvent) {
            // Setup needs to be done only once
            // when the Column is initialized
            if (!this._bIsInitialized) {
                this._oView = Library.getViewFor(this);
                this._oTable = oEvent.getParameter("oTable");

                // Add CustomData to the Column to set data model properties
                // into the DOM element that can be evaluated to apply styles
                this.addCustomData(new CustomData({
                    key: "ui-filtered",
                    writeToDom: true
                }).bindProperty("value", {
                    path: "ColumnData>/Filtered"
                }));
                this.addCustomData(new CustomData({
                    key: "ui-sorted",
                    writeToDom: true
                }).bindProperty("value", {
                    path: "ColumnData>/SortOrder"
                }));

                // Fire an initial `sortUpdate` event and let the Listeners
                // (i.e. an applicable `ixult.m.Table`) handle the rest
                // - if one Sorter has been set as a `default` Sorter
                if (this.getSorter() && this.getSorter().getDefault()) {
                    this.updateSorter();
                }

                this._bIsInitialized = true;
            }
        },

        /**
         * @summary Internal handling of the 'afterRendering' event on this Control
         *  before possible custom event listeners are executed
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onAfterRendering: function (oEvent) {
            var oColumnElement = $("#" + this.getId());

            // Setup custom CSS classes for the Column
            this._applyStyleClass(oColumnElement, [
                "ixultMColumn",
                this.getSortable() ?
                    "ixultMColumnSortable" :
                    "ixultMColumnNotSortable",
                this.getFilterable() ?
                    "ixultMColumnFilterable" :
                    "ixultMColumnNotFilterable"
            ]);
            if (this.getHeader()) {
                var oColumnHeader =
                    $("#" + this.getId() + " > [class*='sapMColumnHeader']");
                this._applyStyleClass(oColumnHeader, [
                    "ixultMColumnHeader",
                    "ixultMColumnHeader" + this.getHeaderAlign()
                ]);
            }

            // Attach a `click` Event to the Column Header
            // if the following conditions apply:
            // (1) This Column is inside an {ixult.m.Table},
            // (2) This Column has to have a Header Control
            // (3) This Column needs to be filterable or sortable
            var bIsValid = this._oTable &&
                this._oTable.isA("ixult.m.Table") &&
                oColumnElement && this.getHeader() &&
                (this.getSortable() || this.getFilterable());
            if (bIsValid) {
                oColumnElement.click(function (oEvent) {
                    this.onHeaderClick(oEvent);
                }.bind(this));
            }
        },

        /**
         * @summary Returns a property setup in the metadata of this Column
         * @param {string} sName - the property name
         * @returns {*|null}
         *  the property value, or null if not found
         * @private
         */
        _getProperty: function (sName) {
            const mBindingInfo = this.getBindingInfo(sName),
                mProperty = this.getProperty(sName);

            return mBindingInfo && mBindingInfo.parts ?
                mBindingInfo.parts : mProperty || null;
        },

        /**
         * @summary Adds style classes to a given HTML element
         * @param {jQuery.Selector|Q.fn.init} oElement
         *  the jQuery selector for the HTML element that should be enhanced
         * @param {string|string[]} vClass
         *  the style class or array of classes to be applied
         *  @returns {void}
         * @private
         */
        _applyStyleClass: function (oElement, vClass) {
            // Check that the Element was found and the style classes are given
            if (!vClass || !oElement || !oElement.attr) {
                return;
            }

            // Get the current element 'class' attribute string
            // and evaluate the given class(es)
            var sClassString = oElement.attr("class") || "",
                aClasses = array(vClass).grep(sClass => {
                    return sClass !== undefined && sClass !== "";
                });

            // Add the style classes to the class string
            aClasses.loop(sClass => {
                if (sClassString.length > 0) {
                    sClassString += " ";
                }
                sClassString += sClass;
            });

            // Overwrite the attribute inside the HTML element
            oElement.attr("class", sClassString);
        },

        /**
         * @summary Sets the given data object into the 'ColumnData' JSONModel
         *  that allows for applying custom data to this Column
         * @param {object} oData
         *  the custom Data to set
         * @returns {void}
         * @protected
         */
        setColumnData: function (oData) {
            if (!this.getModel("ColumnData")) {
                this.setModel(new JSONModel({}), "ColumnData");
            }
            this.getModel("ColumnData").setData(oData, true);
        },

        /**
         * @summary Returns the custom data object for this Column
         * @returns {object} the custom data stored in the 'ColumnData' model
         * @protected
         */
        getColumnData: function () {
            this.getModel("ColumnData").getData();
        },

        /**
         * @summary Returns the Column Header Title set via property 'headerTitle'
         * @description If not found, it will search inside the Column Header
         *  for the next applicable Control that has a Text inside; That's why
         *  this overrides the default generated method of the property 'headerTitle'
         * @returns {string}
         *  the title, or an empty string if not found
         * @public
         * @override
         */
        getHeaderTitle: function () {
            var sTitle = this.getProperty("headerTitle");
            if (sTitle === undefined) {
                var oControl = this.getHeader();
                if (oControl && oControl.getText) {
                    sTitle = oControl.getText();
                }
            }
            return sTitle;
        },

        /**
         * @summary Handles a sorter update by creating the
         *  {sap.ui.model.Sorter} based on the given data,
         *  updating the Indicator and firing the corresponding
         *  `sortUpdate` event
         * @public
         */
        updateSorter: function () {
            this.setSortIndicator(this.getSortOrder());
            var oSorter = this.getSorter();

            // Fire the `sortUpdate` event on this Column
            // to hopefully notify attached listeners, e.g.
            // a parent {ixult.m.Table}
            this.fireSortUpdate({
                sColumnId: this.getId(),
                vSorter: new Sorter(
                    oSorter.getPath(),
                    oSorter.getDescending(),
                    oSorter.getGroup() === true
                ),
                bDescending: this.getSortOrder() === SortOrder.Descending
            });
        },

        /**
         * @summary Sets the sort order for this Column
         * @param {sap.ui.core.SortOrder} sSortOrder
         *  The sort order, can be `Ascending`, `Descending` or `None`
         * @returns {void}
         * @public
         */
        setSortOrder: function (sSortOrder) {
            if (this.getSorter()) {
                this.getSorter().setDescending(
                    sSortOrder === SortOrder.Descending
                );
            }
            this.setSortIndicator(sSortOrder);
        },

        /**
         * @summary Returns the current sort order for this Column
         * @returns {sap.ui.core.SortOrder}
         *  The sort order, can be `Ascending`, `Descending` or `None`
         * @public
         */
        getSortOrder: function () {
            return this.getSorter() ? this.getSorter().getDescending() ?
                SortOrder.Descending : SortOrder.Ascending : SortOrder.None;
        },

        /**
         * @summary Sets the sort order using a boolean `true` or `false`
         * @description `true` will reapply the current order,
         *  `false` will set the sort order to `None`
         * @param {boolean} bSorted
         *  `false`, if the sort order should be cleared
         * @returns {void}
         * @public
         */
        setSorted: function (bSorted) {
            this.setSortOrder(bSorted ? this.getSortOrder() : SortOrder.None);
        },

        /**
         * @summary Sets this Column as `filtered`
         * @description Overrides the generated `setFiltered` function
         *  to additionally set the `filtered` value into the Column's
         *  data model and update the display options (i.e. show a
         *  filter icon)
         * @param {boolean} bFiltered
         *  true if the Column is set to be filtered, false otherwise
         * @public
         * @override
         */
        setFiltered: function (bFiltered) {
            this._bFiltered = bFiltered;
            this.setColumnData({
                Filtered: this._bFiltered.toString()
            });
        },

        /**
         * @summary Returns the value of the `filtered` property
         * @description Overridden, because of function `setFiltered`
         *  additionally setting the property into the Column's model,
         *  to allow showing the corresponding filter icon in the
         *  Column Header
         * @returns {boolean}
         *  the value of the `filtered` property
         * @public
         * @override
         */
        getFiltered: function () {
            return this._bFiltered;
        },

        /**
         * @summary Handles a filter update by creating the
         *  {sap.ui.model.Filter} based on the given data
         *  and firing the corresponding `filterUpdate` event
         * @param {map} mParameters
         *  a parameter map containing the following attributes
         * @param {map} [mParameters.aItems]
         *  the filter options used in the Header Popover
         * @param {map} [mParameters.bReset=false]
         *  `true` if the filter should be reset
         * @returns {void}
         * @public
         */
        updateFilter: function (mParameters) {
            var aItems = array(mParameters.aItems),
                aValues = [],
                bReset = mParameters.bReset === true,
                oFilter = null;

            this.setFiltered(!bReset);

            // Get the filter values (including '' for empty strings)
            aItems.loop(oItem => {
                aValues.push(oItem.Value);
            });

            // Create the sap.ui.model.Filter from the given data
            if (!bReset) {
                oFilter = new Filter({
                    path: this.getFilter().getPath(),
                    operator: this.getFilter().getOperator(),
                    value1: aValues,
                    comparator: this.getFilter().getComparator()
                });
            }

            // Fire the `filterUpdate` event on this Column
            // to hopefully notify attached listeners, e.g.
            // a parent {ixult.m.Table}
            this.fireFilterUpdate({
                sColumnId: this.getId(),
                vFilter: oFilter,
                bReset: bReset
            });
        },

        /**
         * @summary Handles the jQuery/JavaScript 'click' Event on the Column Header
         * @param {jQuery.Event} oEvent
         *  the jQuery 'click' Event
         * @returns {void}
         * @protected
         */
        onHeaderClick: function (oEvent) {
            this._createColumnHeaderPopover(oEvent).then(function (oPopover) {
                if (oPopover.isA("sap.m.Popover")) {
                    oPopover.openBy(this.getHeader());
                } else if (oPopover.isA("sap.m.Dialog")) {
                    oPopover.open();
                }
            }.bind(this));
        },

        /**
         * @summary Creates the Popover that is displayed by the Column
         * @param {jQuery.Event} oEvent
         *  the jQuery `click` Event
         * @returns {Promise}
         *  a Promise handler with 'resolve' ('then') and ('reject')
         *  'fail' handlers to be executed after the Popover has been
         *  retrieved or created
         * @private
         */
        _createColumnHeaderPopover: function (oEvent) {
            var oDeferred = $.Deferred();

            // Create or retrieve the Column Header Popover Control
            if (!this._oPopover) {
                Fragment.load({
                    id: this.getHeader().getId() + "-popover",
                    name: Device.system.phone ?
                        "ixult.m.ColumnHeaderDialog" :
                        "ixult.m.ColumnHeaderPopover",
                    type: "XML",
                    controller: this
                }).then(function (oFragment) {
                    this._oPopover = oFragment;
                    oDeferred.resolve(oFragment);
                }.bind(this));
            } else {
                oDeferred.resolve(this._oPopover);
            }

            return oDeferred.promise();
        },

        /**
         * @summary Handles the `beforeOpen` event
         *  of the Column Header Popover / Dialog
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onBeforeOpen: function (oEvent) {
            var aValues = this.getFilter().getValues(),
                aSuggestionItems = array([]);

            // Create the list of suggested
            // (i.e. possible) filter values
            aValues.loop(function (oValue) {
                if (oValue.Value === "") {
                    oValue.Text = Library.getBundleText("column.header.filter.empty");
                }
                aSuggestionItems.push(object(oValue).extend({
                    Info: "",
                    Selected: false
                }));
            }.bind(this));

            // Set the data into the Popover
            this._oPopover.setModel(new JSONModel({
                Sortable: this.getSortable(),
                SortOrder: this.getSortOrder(),
                Filterable: this.getFilterable(),
                Filter: {
                    Label: this.getHeaderTitle(),
                    Items: aSuggestionItems,
                    InputValue: ""
                },
                Height: this.getHeaderPopoverHeight(),
                Placement: this.getHeaderPopoverPlacement(),
                Title: this.getHeaderTitle(),
                ShowHeader: Device.system.phone,
                ShowActions: this.getFilterable(),
            }), "PopupData");

            // Set the texts for the Popover
            this._oPopover.setModel(new ResourceModel({
                bundle: Library.getResourceBundle()
            }), "messagebundle");
        },

        /**
         * @summary Handles the `afterOpen` event
         *  of the Column Header Popover / Dialog
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onAfterOpen: function (oEvent) {
            // Set the initial focus into the filter search
            // Input (if existing and found)
            var oSelector = $(
                    "#" + this._oPopover.getId() +
                    " [data-ui-id='filterSearchInput']"
                ),
                sElementId = oSelector && oSelector[0] ?
                    oSelector[0].id : null,
                oControl = sElementId ?
                    sap.ui.getCore().byId(sElementId) : null;
            if (oControl && oControl.focus) {
                oControl.focus();
            }
        }
        ,

        /**
         * @summary Handles the `press` event on one of
         *  the `Sort` Buttons (Ascending or Descending)
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onSortButtonPress: function (oEvent) {
            this._oPopover.close();

            var sOrder = oEvent.getSource().data("SortOrder");
            setTimeout(function () {
                this.setSortOrder(sOrder);
                this.updateSorter();
            }.bind(this), 500);
        },

        /**
         * @summary Handles the `press` event on the
         *  `Clear Filter` Button in the Header Popover
         * @param {sap.ui.base.Event} oEvent - the UI event instance
         * @returns {void}
         * @protected
         */
        onFilterResetButtonPress: function (oEvent) {
            this._oPopover.close();

            // Reset the suggestion item list selection in the Popover data
            var oData = this._oPopover.getModel("PopupData").getData();

            // Reset the filter input value and the selected items
            oData.Filter.InputValue = "";
            array(oData.Filter.SuggestionItems).loop(oItem => {
                oItem.Selected = false;
            });
            this._oPopover.getModel("PopupData").setData(oData, true);

            // Fire the `filterUpdate` event and let the Listeners
            // (i.e. an applicable `ixult.m.Table`) handle the rest
            this.updateFilter({
                bReset: true
            });
        }
        ,

        /**
         * @summary Handles the `press` event on the
         *  `Okay` Button in the Header Popover
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onOkayButtonPress: function (oEvent) {
            this._oPopover.close();
            var oPopupData =
                this._oPopover.getModel("PopupData").getData();

            this.updateFilter({
                aItems: oPopupData.Filter.Items.grep(oItem => {
                    return oItem.Selected === true;
                })
            });
        },

        /**
         * @summary Handles the `press` event on the
         *  `Cancel` Button in the Header Popover
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onCancelButtonPress: function (oEvent) {
            this._oPopover.close();
        },

        /**
         * @summary Handles the `press` event on the
         *  `Close` Button in the Popover Header
         *  (used on mobile devices)
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onCloseButtonPress: function (oEvent) {
            this._oPopover.close();
        }

    });
})
;
