sap.ui.define([
    "sap/m/OverflowToolbar",
    "ixult/base/assert",
    "ixult/base/util/array",
    "ixult/base/util/LibraryHelper",
    "./library"
], function (
    OverflowToolbar,
    assert,
    array,
    LibraryHelper
) {
    "use strict";
    const Library = LibraryHelper.register("ixult/m").loadStyleSheet();

    /**
     * @class OverflowToolbar
     * @memberOf ixult.m
     * @extends sap.m.OverflowToolbar
     *
     * @classdesc TODO
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.m.OverflowToolbar sap.m.OverflowToolbar}
     *
     * @public
     */
    return OverflowToolbar.extend("ixult.m.OverflowToolbar", /** @lends ixult.m.OverflowToolbar.prototype */ {
        // The renderer to use; Important: Leave this empty,
        // so that the default Renderer is used
        renderer: {},

        // Metadata for the UI element; Used to define custom properties
        metadata: {
            aggregations: {
                /**
                 * @name ixult.m.OverflowToolbar#items
                 * @type {sap.ui.core.Control[]}
                 * @description An array of UI Controls that will be set as
                 *  the contents of the OverflowToolbar, allowing it to be
                 *  set up like a horizontal `sap.m.List` Control
                 * @public
                 */
                items: {
                    type: "sap.ui.core.Control",
                    multiple: true,
                    singularName: "item",
                    bindable: "bindable",
                    selector: "#{id}",
                    dnd: true
                }
            }
        },

        /**
         * @summary Initializes the UI control
         * @returns {void}
         * @protected
         */
        init: function () {
            OverflowToolbar.prototype.init.apply(this, arguments);
        },

        /**
         * @summary Called during `beforeRendering` of an applicable
         *  `ixult.m.OverflowToolbar`; Allows for custom processing in
         *  this Toolbar on `beforeRendering` of the Control
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onBeforeRendering: function (oEvent) {
            OverflowToolbar.prototype.onBeforeRendering.apply(this, arguments);
        },

        /**
         * @summary Called during `afterRendering` of an applicable
         *  {ixult.m.OverflowToolbar}; Allows for custom processing in
         *  this Toolbar on `afterRendering` of the Control
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onAfterRendering: function (oEvent) {
            OverflowToolbar.prototype.onAfterRendering.apply(this, arguments);
        },

        /**
         * @summary Adds some item to the aggregation `items`
         * @description Convenience method to forward to `addContent`
         *  allowing the OverflowToolbar to have `sap.m.List` style
         *  content based on a model bound list and a template Control
         * @param {sap.ui.core.Control} oItem
         *  The item to add; if empty, nothing is inserted
         * @returns {this}
         *  Reference to this in order to allow method chaining
         * @public
         */
        addItem: function (oItem) {
            return this.addContent(oItem);
        },

        /**
         * @summary Inserts an item into the aggregation items
         * @description Convenience method to forward to `insertContent`
         *  allowing the OverflowToolbar to have `sap.m.List` style
         *  content based on a model bound list and a template Control
         * @param {sap.ui.core.Control} oItem
         *  The item to insert; if empty, nothing is inserted
         * @param {int} iIndex
         *  The 0-based index the item should be inserted at; for a
         *  negative value of iIndex, the item is inserted at position 0;
         *  for a value greater than the current size of the aggregation,
         *  the item is inserted at the last position
         * @returns {this}
         *  Reference to this in order to allow method chaining
         * @public
         */
        insertItem: function(oItem, iIndex) {
            return this.insertContent(oItem, iIndex);
        },

        /**
         * @summary Gets content of aggregation `items`
         * @description Defines the items contained within this control
         * @description Convenience method to forward to `getContent`
         *  allowing the OverflowToolbar to have `sap.m.List` style
         *  content based on a model bound list and a template Control
         * @returns {sap.ui.core.Control[]}
         *  The array of UI Controls from the `items` aggregation
         * @public
         */
        getItems: function() {
            return this.getContent();
        },

        /**
         * @summary Removes an item from the aggregation `items`
         * @description Convenience method to forward to `removeItem`
         *  allowing the OverflowToolbar to have `sap.m.List` style
         *  content based on a model bound list and a template Control
         * @param {int|string|sap.ui.core.Control} vItem
         *  The item to remove or its index or id
         * @returns {sap.ui.core.Control|null}
         *  The removed item or null
         * @public
         */
        removeItem: function (vItem) {
            return this.removeContent(vItem);
        },

        /**
         * @summary Removes all the controls from the aggregation `items`;
         *  Additionally, it unregisters them from the hosting UIArea
         * @description Convenience method to forward to `removeAllContent`
         *  allowing the OverflowToolbar to have `sap.m.List` style
         *  content based on a model bound list and a template Control
         * @returns {sap.ui.core.Control[]}
         *  An array of the removed elements (might be empty)
         * @public
         */
        removeAllItems: function () {
            return this.removeAllContent();
        }

    });
});