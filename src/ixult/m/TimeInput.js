sap.ui.define([
    "sap/m/Input",
    "sap/ui/Device",
    "sap/ui/core/CustomData",
    "sap/ui/core/Fragment",
    "sap/ui/core/ValueState",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/resource/ResourceModel",
    "ixult/base/assert",
    "ixult/base/util/array",
    "ixult/base/util/DateHelper",
    "ixult/base/util/LibraryHelper",
    "./library"
], function (
    Input,
    Device,
    CustomData,
    Fragment,
    ValueState,
    JSONModel,
    ResourceModel,
    assert,
    array,
    DateHelper,
    LibraryHelper,
    MyLibrary
) {
    "use strict";
    const Library = LibraryHelper.register("ixult/m").loadStyleSheet();

    /**
     * @class TimeInput
     * @extends sap.m.Input
     * @memberOf ixult.m
     *
     * @classdesc TODO
     * @description TODO
     *
     * @param {string} [sId]
     *  id for the new control, generated automatically if no id is given
     * @param {object} [mSettings]
     *  initial settings for the new control
     *
     * @public
     */
    return Input.extend("ixult.m.TimeInput", /** @lends ixult.m.TimeInput.prototype */ {
        // Renderer for the UI Control; Important: Leave this empty
        // so that the default renderer of the sap.m.Input can be used
        renderer: {},

        // Metadata for the UI element;vUsed to define custom properties
        metadata: {
            properties: {
                /**
                 * @name ixult.m.TimeInput#dateValue
                 * @type {object}
                 * @default null
                 * @summary Holds a reference to a JavaScript Date Object. The `value` (string)
                 *  property will be set according to it. Alternatively, if the `value`
                 *  and `valueFormat` pair properties are supplied instead,
                 *  the `dateValue` will be instantiated according to the parsed
                 *  `value`.
                 * @description Use `dateValue` as a helper property to easily obtain
                 *  the day, month, year, hours, minutes and seconds of the chosen date and time.
                 *  Although possible to bind it, the recommendation is not to do it.
                 *  When binding is needed, use `value` property instead.
                 * @public
                 */
                dateValue: {
                    type: "object",
                    group: "Data",
                    defaultValue: null
                },
                /**
                 * @name ixult.m.TimeInput#mask
                 * @type {string}
                 * @default null
                 * @summary Mask defined by its characters type (respectively, by its length).
                 * @description You should consider the following important facts:
                 *  1. The mask characters normally correspond to an existing rule (one rule per unique char).
                 *  Characters which don't, are considered immutable characters (for example, the mask '2099', where '9' corresponds to a rule
                 *  for digits, has the characters '2' and '0' as immutable).
                 *  2. Adding a rule corresponding to the `placeholderSymbol` is not recommended and would lead to an unpredictable behavior.
                 *  3. You can use the special escape character '^' called "Caret" prepending a rule character to make it immutable.
                 *  Use the double escape '^^' if you want to make use of the escape character as an immutable one.
                 * @public
                 */
                mask: {
                    type: "string",
                    group: "Display",
                    defaultValue: null
                },
                /**
                 * @name ixult.m.TimeInput#displayFormat
                 * @type {string}
                 * @default HH:mm
                 * @summary Determines the format, displayed in the input field.
                 * @public
                 */
                displayFormat: {
                    type: "string",
                    group: "Display",
                    defaultValue: "HH:mm"
                },
                /**
                 * @name ixult.m.TimeInput#valueFormat
                 * @type {string}
                 * @default HH:mm
                 * @summary Determines the format of the value property.
                 * @public
                 */
                valueFormat: {
                    type: "string",
                    group: "Data",
                    defaultValue: "HH:mm"
                },
                /**
                 * @name ixult.m.TimeInput#minutesStep
                 * @type {int}
                 * @default 30
                 * @summary Sets the minutes step. If step is less than 1,
                 *  it will be automatically converted back to 1. The
                 *  minutes clock is populated only by multiples of the step.
                 * @public
                 */
                minutesStep: {
                    type: "int",
                    group: "Data",
                    defaultValue: MyLibrary.TimeInputSteps.Fifteen
                },
                /**
                 * @name ixult.m.TimeInput#emptyValuesSupported
                 * @type {boolean}
                 * @default true
                 * @summary Defines if an empty string in the 'value' property
                 *  is displayed as such. This corresponds to a 'dateValue'
                 *  of '00:00'.
                 * @public
                 */
                emptyValuesSupported: {
                    type: "boolean",
                    group: "Display",
                    defaultValue: true
                }
            }
        },

        // Attributes
        _oPopover: null,
        _oInputField: null,
        _oValueHelpIcon: null,

        /**
         * @summary Creates a new ixult.m.TimeInput instance
         * @returns {void}
         * @public
         * @hideconstructor
         */
        constructor: function (sId, mSettings) {
            Input.prototype.constructor.apply(this, arguments);

            // Set up the default properties of this Input control, i.e. enable or disable
            // certain properties that we'll need for this to be an 'TimeInput' like Control
            this.setShowValueHelp(true);
            this.setValueHelpOnly(Device.system.phone);
        },

        /**
         * @summary Initializes the UI control
         *  and attaches internal event handlers
         * @description This method is used to attach internal
         *  event handlers allowing pre-processing data; They
         *  need to be attached *before* calling the
         *  `Input.protoype.init` to ensure they are handled
         *  *before* custom event handlers in the event registry
         *  (i.e. before the events that are set in the app
         *  using this Control)
         * @returns {void}
         * @protected
         */
        init: function () {
            // Attach events for this Control
            this.attachChange(this.onValueChange, this);
            this.attachValueHelpRequest(this.onValueHelpRequest, this);

            Input.prototype.init.apply(this);
        },

        /**
         * @summary Handles the `afterRendering` Event for the UI Control,
         *  that is executed when the Control is completely rendered and shown
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onAfterRendering: function (oEvent) {
            Input.prototype.onAfterRendering.apply(this);

            // Change the icon of the Input to look more likely like the 'TimeInput' Control
            var oIconSelector = $("#" + this.getId() + " [class*='sapUiIcon']");
            if (oIconSelector && oIconSelector.length > 0) {
                this._oValueHelpIcon = sap.ui.getCore().byId(oIconSelector[0].id);
                this._oValueHelpIcon.setSrc("sap-icon://time-entry-request");
            }

            // Save the HTML input control (for later access if needed)
            this._oInputField = $("#" + this.getId() + " input");
            if (this._oInputField) {
                // Select the whole text on focusin to allow direct overwrite
                this._oInputField.focusin(function (oEvent) {
                    this._oInputField.select();
                }.bind(this));
            }
        },

        /**
         * @summary Handles the 'valueHelpRequest' Event on the UI Control
         *  fired, when using the value help icon to open the Input's Popover
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onValueHelpRequest: function (oEvent) {
            this._createTimeTickerPopover(oEvent).then(function (oPopover) {
                if (oPopover.isA("sap.m.Popover")) {
                    oPopover.openBy(this._oValueHelpIcon ? this._oValueHelpIcon : this);
                } else if (oPopover.isA("sap.m.Dialog")) {
                    oPopover.open();
                }
            }.bind(this));
        },

        /**
         * @summary Creates the Popover that is displayed by the value help icon of the TimeInput
         * @param {sap.ui.base.Event} oEvent
         *  the UI 'press' event instance on the value help icon
         * @returns {Promise}
         *  a Promise handler with 'resolve' ('then') and ('reject') 'fail' handlers
         *  to be executed after the Popover has been retrieved or created
         * @private
         */
        _createTimeTickerPopover: function (oEvent) {
            var oDeferred = $.Deferred();

            // Create or retrieve the TimeInput Popover Control
            if (!this._oPopover) {
                Fragment.load({
                    id: this.getId() + "-input",
                    name: Device.system.phone ? "ixult.m.TimeInputDialog" : "ixult.m.TimeInputPopover",
                    type: "XML",
                    controller: {
                        onAfterOpen: function (oEvent) {
                            // --------------------------------------------------------------------------------
                            // TODO: TimeTicker Popover adjustment --- experimental function, needs refinement
                            // --------------------------------------------------------------------------------
                            var sPopoverSelector = "#" + oEvent.getSource().getId(),
                                oPopoverElement = $(sPopoverSelector),
                                oPopoverDownArrowElement = $(sPopoverSelector + " [class*='sapMPopoverArrDown']");

                            // Popover is facing upwards (has a `down` arrow)
                            if (oPopoverElement.length > 0 && oPopoverDownArrowElement.length > 0) {
                                // Adjust the offset of a downwards facing Popover, because it is usually rendered
                                // at the bottom of the Input, thereby overlaying the input field; uses the Input's
                                // height to reset the Popover position to the top of the Input field
                                try {
                                    var iInputHeight = $("#" + this.getId()).height() || 26,
                                        oOffset = oPopoverElement.offset();

                                    // Determine if there's a `top` or `bottom` stylesheet attribute in the CSS
                                    // this is due to downwards facing Popover Controls at the bottom of the
                                    // screen having a `bottom` setting, while Popovers that are *slightly* higher
                                    // but still flipped have a `top` setting for some reason
                                    var iTopStyle = oOffset && oOffset.top ? oOffset.top : oPopoverElement.css("top") ?
                                            Number(oPopoverElement.css("top").replace("px", "")) : undefined,
                                        iBottomStyle = oOffset && oOffset.bottom ? oOffset.bottom :
                                            oPopoverElement.attr("style").indexOf("bottom") !== -1 && oPopoverElement.css("bottom") ?
                                                Number(oPopoverElement.css("bottom").replace("px", "")) : undefined;

                                    // Update the CSS and offset values
                                    if (iBottomStyle && iBottomStyle !== "NaN") {
                                        oPopoverElement.css("bottom", (iBottomStyle + iInputHeight) + "px");
                                    } else if (iTopStyle && iTopStyle !== "NaN") {
                                        oPopoverElement.css("top", (iTopStyle - iInputHeight) + "px");
                                        oPopoverElement.offset({
                                            top: iTopStyle - iInputHeight,
                                            left: oOffset.left
                                        });
                                    }
                                } catch (oException) {
                                    console.error("Error during positioning Popover: ", oException);
                                }
                            }
                        }.bind(this),
                        onPreviewTimeDigitToggle: this.onPopoverPreviewTimeDigitToggle.bind(this),
                        onPreviewAmPmSwitchChange: this.onPopoverPreviewAmPmSwitchChange.bind(this),
                        onNumberButtonPress: this.onPopoverNumberButtonPress.bind(this),
                        onOkayButtonPress: this.onPopoverOkayButtonPress.bind(this),
                        onCancelButtonPress: function (oEvent) {
                            this._oPopover.close();
                        }.bind(this)
                    }
                }).then(function (oFragment) {
                    this._oPopover = oFragment;
                    this._setupTimeTickerPopover();
                    oDeferred.resolve(this._oPopover);
                }.bind(this));
            } else {
                this._setupTimeTickerPopover();
                oDeferred.resolve(this._oPopover);
            }

            return oDeferred.promise();
        },

        /**
         * @summary Collects the Data used in the Popover
         *  and sets it into respective JSON models
         * @returns {void}
         * @private
         */
        _setupTimeTickerPopover: function () {
            // Set the Popover data
            this._oPopover.setModel(new JSONModel({
                Title: Library.getBundleText("timeinput.popup.title"),
                MinutesStep: this.getMinutesStep().toString(),
                PreviewValues: this._createPopoverPreviewValues()
            }), "PopupData");

            // Set the texts for the Popover
            this._oPopover.setModel(new ResourceModel({
                bundle: Library.getResourceBundle()
            }), "messagebundle");
        },

        /**
         * @summary Creates the preview values for the value help Popover
         *  by evaluating the current time value and splitting it for the display
         * @todo: evaluate the regex pattern to support any (valid) time format
         *  (like what about 'a H:mm' which would be sth. like 'AM 5:03');
         *  maybe use the 'displayFormat' to construct the pattern
         * @returns {Array.<{Index: integer, Text: string, Pressed: boolean}>}
         *  an array of preview values to be used by the UI Controls inside the Popover;
         *  Array indices 0 to 3 represent the corresponding time digits (1 to 4);
         *  Array index 4 is reserved to detect 'AM' or 'PM'
         * @private
         */
        _createPopoverPreviewValues: function () {
            var aResults = [];
            for (var i = 0; i < 5; i++) {
                aResults.push({Index: i, Text: "", Pressed: false});
            }

            // Split the time value using a regex pattern;
            // supports standards like 'HH:mm' and 'HH:mm a
            var sPattern = /(\d{0,1})(\d{1}):(\d{1})(\d{1})\s{0,}(am|pm|AM|PM){0,1}/i,
                sValue = this.getValue() !== "" ? this.getValue() :
                    DateHelper.format(this.getDateValue(), this.getDisplayFormat(), false),
                aMatches = sValue.match(sPattern);

            // Build the results array using the string match indices
            aResults.forEach(function (oResult, iIndex) {
                oResult.Text = aMatches[iIndex + 1] || "0";
            });
            // First button is initially pressed
            aResults[0].Pressed = true;

            // Create the special last result element
            // for use with the 'AM/PM' Switch Control
            aResults[4].Text = aMatches[5] ? aMatches[5].toUpperCase() : "";
            aResults[4].Pressed = aResults[4].Text === "PM";

            return aResults;
        },

        /**
         * @summary Handles the `press` event on one of the time digit
         *  ToggleButtons in the TimeInput Popover's preview
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onPopoverPreviewTimeDigitToggle: function (oEvent) {
            var oButton = oEvent.getSource(),
                iIndex = oButton.data("Index"),
                oPopupData = this._oPopover.getModel("PopupData").getData();

            // Deselect all time digit ToggleButtons
            for (var i = 0; i <= 3; i++) {
                oPopupData.PreviewValues[i].Pressed = false;
            }
            // ... and set the currently pressed Button to selected after that
            oPopupData.PreviewValues[iIndex].Pressed = true;

            // Update the model data
            this._oPopover.getModel("PopupData").setData(oPopupData, true);
        },

        /**
         * @summary Handles the 'change' event on the AM/PM Switch
         *  in the TimeInput Popover's preview
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onPopoverPreviewAmPmSwitchChange: function (oEvent) {
            var bPM = oEvent.getSource().getState() === true,
                oPopupData = this._oPopover.getModel("PopupData").getData();

            // Update the value text of the model value
            // to AM or PM based on the Switch state
            oPopupData.PreviewValues[4].Text = bPM ? "PM" : "AM";

            // Update the model data
            this._oPopover.getModel("PopupData").setData(oPopupData, true);
        },

        /**
         * @summary Handles the `press` event on one of the time digit
         *  ToggleButtons in the TimeInput Popover's preview
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onPopoverNumberButtonPress: function (oEvent) {
            var sNumber = oEvent.getSource().getText(),
                oPopupData = this._oPopover.getModel("PopupData").getData(),
                aPreviewValues = oPopupData.PreviewValues,
                iPressedIndex = 0;

            if (array(["+1h", "-1h"]).contains(sNumber)) {
                // Calculate the hour value based on the '+1h' or '-1h' Buttons
                var iHour = parseInt(aPreviewValues[0].Text) * 10 + parseInt(aPreviewValues[1].Text);
                iHour = sNumber === "+1h" ? iHour + 1 : sNumber === "-1h" ? iHour - 1 : iHour;
                sNumber = iHour < 0 ? "23" : iHour > 23 ? "00" :
                    iHour < 10 ? "0" + iHour.toString() : iHour.toString();
                aPreviewValues[0].Text = sNumber[0];
                aPreviewValues[0].Pressed = false;
                aPreviewValues[1].Text = sNumber[1];
                aPreviewValues[1].Pressed = false;
                iPressedIndex = 1;
            } else if (array([":15", ":30", ":45", ":00"]).contains(sNumber)) {
                // Set the minutes value based on the pressed button value (text)
                sNumber = sNumber.replaceAll(":", "");
                aPreviewValues[2].Text = sNumber[0];
                aPreviewValues[2].Pressed = false;
                aPreviewValues[3].Text = sNumber[1];
                aPreviewValues[3].Pressed = false;

                iPressedIndex = 3;
            } else {
                // Get the ToggleButton Control from the preview display that is currently active (i.e. 'pressed')
                var aPressedElements = $.grep(aPreviewValues, function (oValue) {
                        return oValue.Pressed === true;
                    }),
                    oPressedElement = aPressedElements && aPressedElements.length > 0 ?
                        aPressedElements[0] : null;
                iPressedIndex = oPressedElement.Index;
                if (!oPressedElement || !array([0, 1, 2, 3]).contains(iPressedIndex)) {
                    return;
                }

                if (!isNaN(sNumber)) {
                    // Adjust the hour and minute values based on the number input
                    var iNumber = parseInt(sNumber),
                        // Set the first hour digit to be aware of AM/PM being enabled
                        iMaxHour = aPreviewValues[4].Text !== "" ? 1 : 2;
                    if ((iPressedIndex === 0 && iNumber > iMaxHour) || (iPressedIndex === 2 && iNumber > 5)) {
                        // If the first hour digit is > 2 OR the first minute digit is > 5:
                        // Set it to '0', insert the value into the next digit and move the digit field
                        // after that (i.e. from hours to minutes and from minutes back to hours)
                        aPreviewValues[iPressedIndex].Text = "0";
                        aPreviewValues[iPressedIndex].Pressed = false;
                        iPressedIndex = iPressedIndex + 1;
                    } else if (iPressedIndex === 1 && aPreviewValues[0].Text === "2" && iNumber > 3) {
                        // Do nothing, if the second hour digit will result in the hour being > 23
                        return;
                    }
                } else {
                    // Not a valid number
                    console.error("Not a valid number: '" + sNumber + "'");
                    return;
                }

                // Set the given or adjusted value into the field and deselect it
                oPopupData.PreviewValues[iPressedIndex].Text = sNumber;
                oPopupData.PreviewValues[iPressedIndex].Pressed = false;
            }

            // Got to the next Button and set this one to active, so the user can
            // toggle through the time digit Buttons by pressing the 'numpad' numbers
            var iNewIndex = array([0, 1, 2]).contains(iPressedIndex) ? iPressedIndex + 1 : 0;
            oPopupData.PreviewValues[iNewIndex].Pressed = true;

            // Update the model data
            this._oPopover.getModel("PopupData").setData({
                PreviewValues: aPreviewValues
            }, true);
        },

        /**
         * @summary Handles the `press` event on the 'Okay' Button in the value help Popover
         * @see {@link ixult.m.TimeInput.html#.onValueUpdate ixult.m.TimeInput.html#.onValueUpdate}
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onPopoverOkayButtonPress: function (oEvent) {
            var oPopupData = this._oPopover.getModel("PopupData").getData(),
                aPreviewValues = oPopupData.PreviewValues,
                sTimeText = "";

            // Simply concatenate the result text out of the preview values,
            // so that the result is e.g. '1235' or '700PM' or '345AM'
            aPreviewValues.forEach(function (oValue) {
                sTimeText += oValue.Text;
            });

            // Update the time value by passing the new value string from the Popover
            // to the standard value change handler; see function `.onValueUpdate`
            this.fireChange({
                time: sTimeText
            });

            // ... and we're done :-)
            this._oPopover.close();
        },

        /**
         * @summary Handles the `press` event on the 'Cancel' Button in the value help Popover
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onPopoverCancelButtonPress: function (oEvent) {
            this._oPopover.close();
        },

        /**
         * @summary Handles the 'change' Event
         *  on the Input Control
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @param {map} oEvent.mParameters
         *  a standard parameter map coming with the UI Event; can be
         *  evaluated using the `getParameter` or `getParameters` methods
         * @param {string} [oEvent.mParameters.time]
         *  a string value containing numbers for a new time to set;
         *  used with the TimePicker Popover; see `onPopoverOkayButtonPress`
         * @returns {void}
         * @protected
         */
        onValueChange: function (oEvent) {
            var sValue = oEvent.getParameter("time") || oEvent.getSource().getValue(),
                dOldValue = this.getDateValue();

            // Reset the value state and the text
            this.setValueState(ValueState.None);
            this.setValueStateText("");

            // Look for empty values if supported
            if (sValue === "" && this.getEmptyValuesSupported()) {
                this.setDateValue(dOldValue, {
                    sReason: "change",
                    sValue: sValue
                });
                return;
            }

            // Evaluate the user input and 'guess' a time out of it
            var dNewValue = DateHelper.getTimeFromString(sValue, {
                dDate: dOldValue
            });

            // Show an error and don't change the value
            // if the date/time could not be determined
            if (!dNewValue) {
                this.setValueState(ValueState.Error);
                this.setValueStateText(Library.getBundleText("timeinput.valuestate.error.notadate"));
                return;
            }

            // Otherwise update the 'dateValue'
            this.setDateValue(dNewValue, {
                sReason: "change",
                sValue: sValue
            });
        },

        /**
         * @summary Sets the property 'dateValue' into the TimeInput field
         * @description Updates the property or the model bound 'dateValue' of
         *  the TimeInput Control with the given date/time value;
         *  Also converts the date/time value based on the property 'displayFormat'
         *  and sets the result into into the Input's property 'value'
         * @param {Date} dValue
         *  the date/time value to set
         * @param {map} [mParameters]
         *  a map with additional parameters used to handle certain changes
         *  on the time input value and containing the following parameters:
         * @param {string} [mParameters.sReason]
         *  the reason for the value update, e.g. 'change' when
         *  coming from the Input's 'change' event
         * @param {map} [mParameters.sValue]
         *  the Input value specified during the given event; if omitted,
         *  the current Input 'value' property will be used
         * @returns {void}
         * @public
         */
        setDateValue: function (dValue, mParameters) {
            var sValue = mParameters && mParameters.sValue ? mParameters.sValue : this.getValue();

            // Value has been changed by the user to ''; Set the 'dateValue' to '00:00'
            if (this.getEmptyValuesSupported() && sValue === "") {
                dValue.setHours(0, 0);
            }

            // Update the 'dateValue' property or the bound value
            if (this.getProperty("dateValue")) {
                this.setProperty("dateValue", dValue);
            } else if (this.getBindingInfo("dateValue")) {
                var oBinding = this.getBindingInfo("dateValue");
                oBinding.binding.setValue(dValue);
            }

            // If empty values are supported, return; 'dateValue' should be '00:00' then
            if (this.getEmptyValuesSupported() && sValue === "") {
                this.setValue("");
                return;
            }

            // Format the value according to the given pattern and update the Input value with it
            sValue = DateHelper.format(dValue, this.getDisplayFormat(), false);
            this.setValue(sValue);
        },

        /**
         * @summary Returns the value fpr the property 'dateValue'
         * @description Convenience method to retrieve the static 'dateValue' property if set
         *  or lookup the binding value if the date was bound to a model
         * @returns {Date|null}
         *  the date value
         * @public
         * @override
         */
        getDateValue: function () {
            var dValue = this.getProperty("dateValue");
            if (!dValue) {
                var oBindingInfo = this.getBindingInfo("dateValue");
                dValue = oBindingInfo.binding.getValue();
            }
            return dValue;
        }

    });
});