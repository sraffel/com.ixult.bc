sap.ui.define([
    "sap/m/routing/Router",
    "sap/base/util/UriParameters",
    "sap/ui/core/routing/HashChanger",
    "sap/ui/core/routing/History"
], function (
    Router,
    UriParameters,
    HashChanger,
    History
) {
    "use strict";

    /**
     * @class Router
     * @memberOf ixult.m.routing
     * @extends sap.m.routing.Router
     *
     * @classdesc Router class providing general event handler, navigation
     *  and utility functions for common routing purposes
     * @summary Instantiates the Router
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.m.routing.Router sap.m.routing.Router}
     * @see {sap.m.routing.Router}
     *
     * @param {object|object[]} [oRoutes]
     *  may contain many Route configurations as sap.ui.core.routing.Route
     * @param {object} [oConfig]
     *  Default values for route configuration
     * @param {sap.ui.core.UIComponent} [oOwner]
     *  the Component of all the views that will be created by this Router
     * @param {object} [oTargetsConfig]
     *  the target configuration
     * @returns {void}
     *
     * @public
     */
    return Router.extend("ixult.m.routing.Router", /** @lends ixult.m.routing.Router.prototype */ {
        oComponent: null,
        oHashChanger: null,
        oHistory: null,
        mConfig: null,

        /**
         * @summary Instantiates the Router
         * @hideconstructor
         */
        constructor: function (oRoutes, oConfig, oOwner, oTargetsConfig) {
            // Call super method
            Router.prototype.constructor.apply(this, arguments);

            // Save the attributes
            this.oComponent = oOwner;
            this.oHashChanger = new HashChanger(); // this.getHashChanger();
            this.oHistory = new History(this.oHashChanger);
            this.mConfig = this.getComponent().getManifestEntry("/sap.ui5/routing");

            // Initialize the routeMatched handler
            this.attachRoutePatternMatched({}, this._handleRoutePatternMatched, this);

            // Setup event handler to recognize target display events
            $.each(this.getTargetsConfig(), function (sTargetName, oTarget) {
                this.getTarget(sTargetName).attachDisplay(oTarget, this._handleTargetDisplayMatched, this);
            }.bind(this));

            // Register the Router for the application
            this.register(this.mConfig.config.routerName);
        },

        /**
         * @summary Returns the configured routes from the manifest file
         * @returns {map}
         *  the routes map
         * @protected
         */
        getRoutesConfig: function () {
            return this.mConfig ? this.mConfig.routes :
                this.getComponent().getManifestEntry("/sap.ui5/routing/routes");
        },

        /**
         * @summary Returns the configured routing targets from the manifest file
         * @returns {map}
         *  the targets map
         * @protected
         */
        getTargetsConfig: function () {
            return this.mConfig ? this.mConfig.targets :
                this.getComponent().getManifestEntry("/sap.ui5/routing/targets");
        },

        /**
         * @summary Retrieves the app's Component
         * @returns {ixult.ui.core.Component|sap.ui.core.UIComponent}
         *  the Component
         * @protected
         */
        getComponent: function () {
            return this.oComponent;
        },

        /**
         * @summary Returns the Router's history
         * @returns {sap.ui.core.routing.History}
         *  the Routing History
         * @protected
         */
        getHistory: function () {
            return this.oHistory;
        },

        /**
         * @summary Handles the `routePatternMatched` event of the current route;
         *  Method Stub, can be implemented in deriving Classes
         * @see {ixult.ui.core.UIComponent~fireRouteMatched}
         * @param {sap.ui.base.Event} oEvent
         *  the routing Event
         * @returns {boolean} [true]
         *  if true, the event will be delegated further to notify all listing
         *  views of the routing change using the corresponding method
         *  of the UIComponent
         * @protected
         * @abstract
         */

        /**
         * @summary Internal handler for the 'routePatternMatched' event of the current route;
         *  Additionally fires the 'routeMatched' event allowing listening view
         *  controllers to react on the routing event
         * @see {@link ixult.ui.core.UIComponent.html#.fireRouteMatched ixult.ui.core.UIComponent#.fireRouteMatched}
         * @param {sap.ui.base.Event} oEvent
         *  the routing Event
         * @returns {void}
         * @private
         */
        _handleRoutePatternMatched: function (oEvent) {
            var bEventDelegate = true;

            // Execute the handle method that can be implemented in derived classes
            if (this.handleRoutePatternMatched) {
                bEventDelegate = this.handleRoutePatternMatched(oEvent) !== false;
            }

            // Notify all listening views about the routing update
            if (bEventDelegate && this.getComponent() && this.getComponent().fireRouteMatched) {
                this.getComponent().fireRouteMatched(oEvent);
            }
        },

        /**
         * @summary Handles the `targetMatched` event of this current routing target (view)
         *  Method Stub, can be implemented in deriving Classes
         * @see {@link ixult.ui.core.UIComponent.html#.fireTargetMatched ixult.ui.core.UIComponent#.fireTargetMatched}
         * @param {sap.ui.base.Event} oEvent
         *  the target display event
         * @returns {boolean} [true]
         *  if true, the event will be delegated further to notify all listing
         *  views of the target display change using the corresponding method
         *  of the UIComponent
         * @protected
         * @abstract
         */

        /**
         * @summary Internal handler for the `display` event of this current
         *  routing target (view); Additionally fires the `targetMatched` event
         *  allowing listening view controllers to react on their corresponding
         *  view being called
         * @see {@link ixult.ui.core.UIComponent.html#.fireTargetMatched ixult.ui.core.UIComponent#.fireTargetMatched}
         * @param {sap.ui.base.Event} oEvent
         *  the target display event
         * @returns {void}
         * @private
         */
        _handleTargetDisplayMatched: function (oEvent) {
            var bEventDelegate = true;

            // Execute the handle method that can be implemented in derived classes
            if (this.handleTargetDisplayMatched) {
                bEventDelegate = this.handleTargetDisplayMatched(oEvent) !== false;
            }

            // Notify all listening views about the target display update
            if (bEventDelegate && this.getComponent() && this.getComponent().fireTargetMatched) {
                this.getComponent().fireTargetMatched(oEvent);
            }
        },

        /**
         * @summary Convenience method: Displays a routing target
         * @param {string} sTarget
         *  the routing target as defined in the manifest
         * @param {map} [mParameters]
         *  a map with additional parameters
         * @returns {void}
         * @public
         */
        display: function (sTarget, mParameters) {
            this.getTargets().display(sTarget, mParameters);
        },

        /**
         * Returns the base App URL without any hashes
         * @returns {string}
         *  the base URL
         * @public
         */
        getAppUrl: function () {
            var iHashPos = window.location.href.indexOf("#");
            return iHashPos !== -1 ?
                window.location.href.substring(0, iHashPos) :
                window.location.href;
        },

        /**
         * @summary Retrieves the parameters from the current URL
         * @param {string} [sUrl]
         *  a given URL or the current one, if omitted
         * @returns {map}
         *  a value map containing the URL parameter names and values
         * @public
         */
        getUrlParameters: function (sUrl) {
            sUrl = sUrl || window.location.href;
            var mUrlParameters = {};

            // Extract the SAPUI5 route and add it as a special parameter
            var sRoute = sUrl.substring(sUrl.lastIndexOf("#"), sUrl.length);
            sUrl = sUrl.replace(sRoute, "");
            mUrlParameters["sap-ui-route"] = sRoute;

            // Use the UriParameters interface to simply extract the parameters
            try {
                var oUriParameters = UriParameters.fromURL(sUrl),
                    oIterator = oUriParameters.keys(),
                    mParam = oIterator.next();
                while (!mParam.done) {
                    mUrlParameters[mParam.value] = oUriParameters.get(mParam.value);
                    mParam = oIterator.next();
                }
            } catch (oException) {
                console.error("Error while retrieving URL parameters. Trying again ...", oException);
                // Fallback / Alternative:
                // Split the parameters from the URL "manually"
                var aParams = [],
                    aURLs = sUrl.split("?");
                if (Array.isArray(aURLs) && aURLs.length > 1) {
                    // Split the parameters
                    aParams = aURLs[1].split("&");
                    if (Array.isArray(aParams) && aParams.length > 0) {
                        // Add each parameter as '<name>: <value>' pair to the output map
                        $.each(aParams, function (iIndex, sParam) {
                            var aValues = sParam.split("=");
                            if (Array.isArray(aValues) && aValues.length > 0) {
                                mUrlParameters[aValues[0]] = aValues[1] ? aValues[1] : "";
                            }
                        });
                    }
                }
            }

            // Return the parameter map
            return mUrlParameters;
        },

        /**
         * @summary Navigates back to a given route or the previous page
         * @param {string} [sRoute]
         *  the route to navigate to, the previous route hash by default
         * @param {object} [oData]
         *  an OData object to pass to the target page; optionally
         * @returns {void}
         * @public
         */
        navBack: function (sRoute, oData) {
            sRoute = sRoute || this.getHistory().getPreviousHash();
            oData = oData || {};

            if (sRoute) {
                // Go to the given Route or the previous History Hash
                this.navTo(sRoute, oData, true);
            } else {
                // Otherwise, go backwards using the standard Window Handler
                window.history.back();
            }
        },

        /**
         * @summary Navigates to an (external) Website by a
         *  given URL, optionally with parameters
         * @param {map} mParameters
         *  a parameter map, containing the following attributes:
         * @param {string} mParameters.sUrl
         *  the URL to got to
         * @param {map} [mParameters.mUrlParameters]
         *  additional map containing the URL parameters, optionally
         * @param {string} [mParameters.sSuffix]
         *  an optional suffix string to add at the end of the URL
         * @param {boolean} [mParameters.bNewWindow]
         *  opens the URL in a new window if true, false by default
         * @returns {void}
         * @public
         */
        navToUrl: function (mParameters) {
            var bNewWindow = mParameters && mParameters.bNewWindow !== undefined ?
                    mParameters.bNewWindow : false,
                sUrl = mParameters.sUrl.toString().trim();

            // Add URL parameters
            if (mParameters.mUrlParameters) {
                Object.keys(mParameters.mUrlParameters).forEach(function (sKey, iIndex) {
                    sUrl += iIndex === 0 ? "?" : "&";
                    sUrl += sKey + "=" + mParameters.mUrlParameters.sKey.toString() + "";
                }.bind(this));
            }
            sUrl = sUrl.replace("//?", "/?");

            // Append a suffix (like a routing target) to the URL
            if (mParameters.sSuffix) {
                sUrl += mParameters.sSuffix;
            }

            sap.m.URLHelper.redirect(sUrl, bNewWindow);
        },

        /**
         * @summary Navigates to the Fiori Launchpad using the
         *  CrossApplicationNavigation framework or closes the window,
         *  if no `UShell` - i.e. no Launchpad - was found
         * @returns {void}
         * @public
         */
        navToFLP: function () {
            debugger;
            if (sap.ushell && sap.ushell.Container) {
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                oCrossAppNavigator.toExternal({
                    target: {
                        semanticObject: "#Shell-home"
                    }
                });
            } else {
                setTimeout(function () {
                    window.close();
                }, 1000);
            }
        }

    });
});