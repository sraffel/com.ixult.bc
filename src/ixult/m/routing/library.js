sap.ui.define([
    "sap/ui/core/Core",
    "sap/ui/core/library"
], function (
    Core,
    Library
) {
    "use strict";

    /**
     * @namespace routing
     * @memberOf ixult.m
     *
     * @description Contains classes and functions to
     *  handle routing events in SAPUI5 applications
     *
     * @public
     */
    return sap.ui.getCore().initLibrary({
        name: "ixult.m.routing",
        noLibraryCSS: true,
        dependencies: [
            "sap.ui.core",
            "sap.m",
        ],
        types: [],
        interfaces: [],
        controls: [],
        elements: [],
        version: "0.8.1"
    });

}, /* bExport= */ false);