sap.ui.define([
    "sap/m/ComboBox",
    "ixult/base/util/LibraryHelper",
    "./library"
], function (
    ComboBox,
    LibraryHelper,
    MyLibrary
) {
    "use strict";
    const Library = LibraryHelper.register("ixult/m").loadStyleSheet();

    /**
     * @class ComboBox
     * @extends sap.m.ComboBox
     * @memberOf ixult.m
     *
     * @summary ComboBox class with enhancements for the standard SAPUI5 control
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.m.ComboBox sap.m.ComboBox}
     *
     * @public
     */
    return ComboBox.extend("ixult.m.ComboBox", /** @lends ixult.m.ComboBox.prototype */ {
        // The renderer to use; Important: Leave this empty,
        // so that the default Renderer is used
        renderer: {},

        // Metadata for the UI element; Used to define custom properties
        metadata: {
            properties: {
                /**
                 * @name ixult.m.ComboBox#filterContains
                 * @type {boolean}
                 * @default true
                 * @description Changes the filter function to allow
                 *  the search for containing substrings
                 * @public
                 */
                filterContains: {
                    type: "boolean",
                    defaultValue: true
                }
            }
        },

        /**
         * @summary Creates a new ixult.m.ComboBox instance
         * @param {string} [sId]
         *  id for the new control, generated automatically if no id is given
         * @param {object} [mSettings]
         *  initial settings for the new control
         * @returns {void}
         * @public
         */
        constructor: function (sId, mSettings) {
            ComboBox.prototype.constructor.apply(this, arguments);
        },

        /**
         * @summary Handles the `beforeRendering` Event for the UI Control,
         *  that is executed when the DOM elements are created but not yet shown
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onBeforeRendering: function (oEvent) {
            ComboBox.prototype.onBeforeRendering.apply(this, arguments);

            // Set the filter function to allow search for containing substrings
            if (this.getFilterContains()) {
                this.setFilterFunction(function (sTerm, oItem) {
                    // A case-insensitive 'string contains' filter
                    return oItem.getText().match(new RegExp(sTerm, "i")) ||
                        oItem.getKey().match(new RegExp(sTerm, "i"));
                });
            }
        }

    });
});
