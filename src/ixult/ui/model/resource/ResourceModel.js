sap.ui.define([
    "sap/base/i18n/ResourceBundle",
    "sap/ui/core/IconPool",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/resource/ResourceModel"
], function (
    ResourceBundle,
    IconPool,
    JSONModel,
    ResourceModel
) {
    "use strict";

    /**
     * @summary Possible types of resources for the ResourceModel
     * @type {{
     *      fonts: string,
     *      files: string
     * }}
     * @public
     * @static
     */
    const Type = {
        fonts: "fonts",
        files: "files"
    }

    /**
     * @class ResourceModel
     * @memberOf ixult.ui.model.resource
     * @extends sap.ui.model.resource.ResourceModel
     *
     * @summary Model class for resource files like i18n.properties, but also other files like fonts and (changelog) files
     * @description This ResourceModel class allows the usage of assets files inside the app using the manifest descriptor
     *  model to instantiate corresponding instances of e.g. font icons or (text) files.
     *  For this to work you have to execute the following steps:
     *  0. *For fonts*: Create your designated symbol font from custom (`svg`) images using programs like
     *  [FontForge](https://fontforge.org/)
     *  1. *For fonts*: Create or move fonts or files to the directory of our choice inside the app. To use font icons
     *  inside the app the fonts need at least to have a `woff2` and `ttf` representation. To get the needed file formats
     *  for fonts you can use online converters e.g. at [cloudconvert.com](https://cloudconvert.com/).
     *  *Files* should be placed in a correspondingly named directory and placed as simple text (or compatible) files.
     *  An example project structure could look like below.
     *  ```markdown
     *  webapp
     *  - assets
     *    - fonts
     *      - Icomoon.ttf
     *      - Icomoon.woff2
     *      - Icomoon.json
     *    - logs
     *      - version069.md
     *      - version070.md
     *    - fonts.properties
     *    - logs.properties
     *  ```
     *  2. *For fonts only*: Create CSS styles entries in your corresponding CSS files defining the font face, name and
     *  source urls to use. It's recommended to name the font-family _exactly_ matching to its file name. For example:
     *  ```css
     *   &#64;font-face {
     *      font-family: "Icomoon";
     *      src: url("../assets/fonts/Icomoon.ttf") format("truetype");
     *      src: url("../assets/fonts/Icomoon.eot") format("embedded-opentype");
     *      src: url("../assets/fonts/Icomoon.woff") format("woff");
     *      src: url("../assets/fonts/Icomoon.woff2") format("woff2");
     *      src: url("../assets/fonts/Icomoon.svg") format("svg");
     *      font-weight: normal;
     *      font-style: normal;
     *  }
     *  ```
     *  3. *For fonts only*: Create a json file with the _exact_ name of the font file name inside the fonts' directory.
     *  (See project structure above.) The file has at least to have an empty `{}` statement to be interpreted as a
     *  valid JSON file. Here you can define the icons you want to use inside the app. These have to be specified as
     *  `<name>: "<value>"` (JSON-compatible) attributes, where `<name>` is the icon name you want to use, and
     *  `<value>` refers to the hexadecimal unicode id, the icon is indexed inside the font. To find this Unicode id,
     *  you can use font viewers like [this one](https://us.fontviewer.de). Example:
     *  ```json
     *  {
     *      "curr-dollar": "E93B",
     *      "curr-euro": "E93C",
     *      "curr-pound": "E93D",
     *      "curr-yen": "E93E",
     *      "hourglass": "E979",
     *      "spinner-1": "E97A",
     *      "spinner-2": "E97B",
     *      "spinner-3": "E97C",
     *      "spinner-4": "E97D",
     *      "spinner-5": "E97E",
     *      "spinner-6": "E97F",
     *      "spinner-7": "E980",
     *      "spinner-8": "E981",
     *      "spinner-9": "E982",
     *      "spinner-0": "E983",
     *      "graph-1": "E99B",
     *      "graph-2": "E99C",
     *      "graph-3": "E99D"
     *  }
     *  ```
     *  4. *For fonts*: Create a `fonts.properties` file at the _same level_ as the fonts' directory.
     *  (See project structure above.) Make sure the filename _exactly_ matches the directory name. Specify font
     *  collection name and font file name in a `<key> = <value>` like scheme (analog i18n files), where `<key>`
     *  is the collection name that will be used in the UI5 app, and `<value>` is the font name used for the
     *  actual font file. Example:
     *  ```xml
     *  conovum = conovumSymbols
     *  fluent  = SegoeFluentIcons
     *  holosym = HoloSymMDL2
     *  icofont = Icofont
     *  icomoon = Icomoon
     *  ```
     *  *For files*: Apply the same method used for fonts, i.e. create a `properties` files named _exactly_
     *  like the files' directory (See project structure above.) and specify the used files using an
     *  i18n-like approach. **Important**: File specifications have to include the filetype suffix! Example:
     *  ```xml
     *  version069 = version069.md
     *  version070  = version070.md
     *  ```
     *  5. Define an instance of this `RecourceModel` class (analog to existing
     *  `sap.ui.model.resource.ResourceModel` definitions) in the `sap.ui5.models` section in the app's
     *  `manifest.json` descriptor file. The model `type` has the fixed value of
     *  `ixult.ui.model.resource.ResourceModel` to create an instance of this resource handler class.
     *  Define a `bundleName` and `type` attribute (required) inside the `settings` section to set up the
     *  ResourceModel correctly, where `type` always is an instance of this ResourceModel class using the
     *  value `ixult.ui.model.resource.ResourceModel` and type can be either `fonts` or `files`.
     *  The `bundleName` attribute has to be specified as directory where fonts or files are placed, with
     *  the apps' namespace used as a prefix. (See project structure above.) The ResourceModel will look
     *  for a `properties` file as well as a correspondingly named directory using the bundleName path.
     *  (Hence the naming requirement in step 3.) Example:
     *  ```json
     *  "sap.ui5": {
     *      "models": {
     *          "fonts": {
     *              "type": "ixult.ui.model.resource.ResourceModel",
     *              "settings": {
     *                  "bundleName": "com.ixult.time.companion.assets.fonts",
     *                  "type": "fonts"
     *              }
     *              "changelog": {
     *                  "type": "ixult.ui.model.resource.ResourceModel",
     *                  "settings": {
     *                      "bundleName": "com.ixult.time.companion.assets.logs",
     *                      "type": "files"
     *                 }
     *             }
     *          }
     *      }
     *  }
     *  ```
     *  6. *For fonts*: Use the font's icons and symbols inside your app by referring to the `<key>`
     *  attribute specified in the `fonts.properties` file as the collection name used for custom icons
     *  and then referring to the icon name defined in the fonts `json` file named using the `<value>`
     *  property in the `fonts.properties` file and placed inside the directory that is specified using
     *  the `bundleName` property inside the model `sap.ui5.models` section. Example:
     *  ```xml
     *  <core:Icon src="sap-icon://icomoon/hourglass" />
     *  ```
     *  *For files*: Access the `ResourceModel` instance using its name specified in the manifest descriptor
     *  file. The ResourceModel's `getData` method can then retrieve the file contents as an array of objects
     *  with a `title` attribute matching the properties file key and a `content` attribute containing the
     *  file contents. Example:
     *  ```javascript
     *  var oChangelogData = this.getModel("changelog").getData()
     *  ```
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.ui.model.resource.ResourceModel https://ui5.sap.com/sdk/#/api/sap.ui.model.resource.ResourceModel}
     *
     * @param {map} mParameters
     *  the parameter map used in the original ResourceModel;
     *  Most importantly containing the following attributes
     * @param {string} [mParameters.bundleName]
     *  UI5 module name in dot notation referring to the base ".properties" file;
     *  this name is resolved to a path just as for normal UI5 modules, to which
     *  ".properties" is then appended (e.g. a name like "myapp.i18n.myBundle"
     *  can be given); relative module names are not supported
     * @param {string} [mParameters.bundleUrl]
     *  URL pointing to the base ".properties" file of a bundle (".properties"
     *  file without any locale information, e.g. "../../i18n/mybundle.properties");
     *  relative URLs are evaluated relative to the document.baseURI
     *  "i18n/messagebundle.properties"
     * @param {string} mParameters.type
     *  Type of ResourceModel to be created;
     *  can be `fonts` or `files`
     * @returns {void}
     *
     * @public
     */
    return ResourceModel.extend("ixult.ui.model.resource.ResourceModel", /** @lends ixult.ui.model.resource.ResourceModel.prototype */ {
        _sType: "",
        _oFileModel: {},

        /**
         * @constructor
         * @summary Instantiates the model class
         * @public
         * @hideconstructor
         */
        constructor: function (mParameters) {
            ResourceModel.prototype.constructor.apply(this, arguments);
            this._sType = mParameters.type;

            if (this._sType === Type.fonts) {
                this.registerFonts();
            } else if (this._sType === Type.files) {
                this._oFileModel = new JSONModel({
                    results: []
                });
                this.registerFiles();
            }
        },

        /**
         * @summary *For files only*: Sets (adjusted) data into the JSONModel
         *  used for files contents
         * @param {object} oData
         *  the data to set into the file JSONModel
         * @returns {void}
         * @public
         */
        setData: function (oData) {
            if (this._sType === Type.files) {
                this._oFileModel.setData(oData);
            }
        },

        /**
         * @summary *For files only*: Returns the JSONModel containing the
         *  file contents read from the files inside the specified directory
         * @returns {sap.ui.model.json.JSONModel}
         *  the JSONModel containing the file contents
         * @public
         */
        getData: function () {
            if (this._sType === Type.files) {
                return this._oFileModel.getData();
            }
            return null;
        },

        /**
         * @summary Creates the data model for files and reads the specified file contents into it
         * @description Reads the (sub) directory specified as `bundleName` when creating this
         *  ResourceModel instance and extracts the file contents from the files specified
         *  in the `properties` file inside the `bundleName` path. Creates a JSONModel containing
         *  a `title` attribute with the `properties` key value and a `contents` attribute
         *  containing the files' contents.
         * @returns {void}
         * @protected
         */
        registerFiles: function () {
            var oBundle = this.getResourceBundle(),
                sFilePath = oBundle.oUrlInfo.prefix,
                mContents = oBundle.aPropertyFiles[0].mProperties;
            Object.keys(mContents).forEach(function (sKey) {
                try {
                    var sUrl = "/" + sFilePath + "/" + mContents[sKey];
                    sUrl = sUrl.replaceAll("//", "/")
                        .replaceAll("/./", "/");
                    jQuery.ajax({
                        type: "GET",
                        url: document.location.origin + sUrl,
                        success: function (sContent) {
                            var aResults = this._oFileModel.getProperty("/results");
                            aResults.push({
                                title: sKey,
                                content: sContent
                            });
                            this._oFileModel.setData({
                                results: aResults
                            }, true);
                        }.bind(this),
                        error: function (oError) {
                            console.error("Error registering file '" + sKey + "' at '" + sUrl + "'", oError);
                        }.bind(this)
                    });
                } catch (oException) {
                    console.error("Error registering file '" + sKey + "' at '" + sUrl + "'", oException);
                }
            }.bind(this));
        },

        /**
         * @summary Registers the (icon) fonts according to the attributes defined in the model
         *  definiton inside the apps' manifest descriptor file
         * @description Reads the (sub) directory specified as `bundleName` when creating this
         *  ResourceModel instance and uses attributes specified in the fonts' properties file
         *  to create a `sap.ui.core.IconPool` font collection, where the `key` will be used as
         *  the collection name and the `value` will be used as the `font-family` name that
         *  matches
         *  - the font file name,
         *  - the fonts' json descriptor and
         *  - the font-family name
         *  used in the CSS file.
         * @see {@link https://ui5.sap.com/sdk/#/api/sap.ui.core.IconPool sap.ui.core.IconPool}
         * @returns {void}
         * @protected
         */
        registerFonts: function () {
            var oBundle = this.getResourceBundle(),
                sFilePath = oBundle.oUrlInfo.prefix,
                mContents = oBundle.aPropertyFiles[0].mProperties;
            Object.keys(mContents).forEach(sKey => {
                try {
                    IconPool.registerFont({
                        fontFamily: mContents[sKey],
                        collectionName: sKey,
                        fontURI: sFilePath
                    });
                } catch (oException) {
                    console.error("Error registering font collection '" + sKey + "'", oException);
                }
            });
        },

        /**
         * @summary Returns the metamodel associated with this model
         *  if it is available for the concrete model type.
         * @returns {sap.ui.model.MetaModel}
         *  The metamodel or `undefined` if no metamodel exists.
         * @public
         */
        getMetaModel: function () {
            return ResourceModel.prototype.getMetaModel.apply(this, arguments);
        },

        /**
         * @summary Returns a property from the model
         * @param {string} sPath
         *  Path to where to read the object
         * @param {object} [oContext=null]
         *  Context with which the path should be resolved
         * @param {object} [mParameters]
         *  Additional model specific parameters
         * @returns {any}
         *  The value for the given path/context or
         *  `undefined` if data could not be found
         * @public
         */
        getObject: function (sPath, oContext, mParameters) {
            return ResourceModel.prototype.getObject.apply(this, arguments);
        }

    });
});