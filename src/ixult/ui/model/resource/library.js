sap.ui.define([
    "sap/ui/core/Core",
    "sap/ui/core/library"
], function (
    Core,
    Library
) {
    "use strict";

    /**
     * @namespace resource
     * @memberOf ixult.ui.model
     *
     * @description Library for handling file resources
     *
     * @public
     */
    return sap.ui.getCore().initLibrary({
        name: "ixult.ui.model.resource",
        noLibraryCSS: true,
        dependencies: [
            "sap.ui.core",
            "sap.m",
        ],
        types: [],
        interfaces: [],
        controls: [],
        elements: [],
        version: "0.8.1"
    });

}, /* bExport= */ false);