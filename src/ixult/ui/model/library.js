sap.ui.define([
    "sap/ui/core/Core",
    "sap/ui/core/library"
], function (
    Core,
    Library
) {
    "use strict";

    /**
     * @namespace model
     * @memberOf ixult.ui
     *
     * @description Main library for handles models,
     *  e.g. contains the Model handler class
     *
     * @public
     */
    return sap.ui.getCore().initLibrary({
        name: "ixult.ui.model",
        noLibraryCSS: true,
        dependencies: [
            "sap.m",
            "sap.ui.core"
        ],
        types: [],
        interfaces: [],
        controls: [],
        elements: [],
        version: "0.8.1"
    });

}, /* bExport= */ false);