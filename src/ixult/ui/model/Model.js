sap.ui.define([
    "sap/base/i18n/ResourceBundle",
    "sap/ui/Device",
    "sap/ui/model/Model",
    "sap/ui/model/json/JSONModel",
    "ixult/base/assert",
    "ixult/base/util/ObjectHelper",
    "ixult/ui/model/odata/ODataUtils",
    "ixult/ui/util/LocalStorage"
], function (
    ResourceBundle,
    Device,
    Model,
    JSONModel,
    assert,
    ObjectHelper,
    ODataUtils,
    LocalStorage
) {
    "use strict";

    /**
     * @class Model
     * @memberof ixult.ui.model
     * @extends sap.ui.model.Model
     *
     * @summary Model handler class for general data processing and business logic
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.ui.model.Model sap.ui.model.Model}
     *
     * @param {(ixult.ui.core.UIComponent|sap.ui.core.UIComponent)} oComponent
     *  the App's Component
     * @returns {void}
     *
     * @public
     */
    return Model.extend("ixult.ui.model.Model", /** @lends ixult.ui.model.Model.prototype */ {
        _oComponent: null,
        _oLocalStorage: null,
        mModelRegistry: {},

        /**
         * @constructor
         * @summary Instantiates the model class
         * @public
         * @hideconstructor
         */
        constructor: function (oComponent) {
            Model.prototype.constructor.apply(this, arguments);
            this._oComponent = oComponent;
            this.mModelRegistry = {
                i18n: this.getComponent().getModel("i18n")
            };

            // Setup device model
            this.setModel({
                sName: "Device",
                oData: {
                    isTouch: Device.support.touch,
                    isDesktop: Device.system.desktop,
                    isTablet: Device.system.tablet,
                    isPhone: Device.system.phone,
                    listMode: Device.system.phone ? "None" : "SingleSelectMaster",
                    listItemType: Device.system.phone ? "Active" : "Inactive"
                }
            });

            // Setup browser model
            this.setModel({
                sName: "Browser",
                oData: {
                    name: Device.browser.name,
                    version: Device.browser.version,
                    isEdge: Device.browser.edge,
                    isSafari: Device.browser.safari
                }
            });

            // Add a local storage handler
            var oStorageConfig = this.getComponent().getManifestEntry("/sap.ui5/config/localStorage");
            this._oLocalStorage = new LocalStorage(oStorageConfig);
        },

        /**
         * Returns the main application component
         * @returns {sap.ui.core.UIComponent} - the component instance
         * @public
         */
        getComponent: function () {
            return this._oComponent;
        },

        /**
         * Initializes or updates a JSONModel with a given Name and oData
         * @param {map} mParameters
         *  a parameter map with the following attributes
         * @param {string} mParameters.sName
         *  the model name
         * @param {object} [mParameters.oData]
         *  the OData to set into the model, optionally
         * @param {boolean} [mParameters.bMerge=false]
         *  true, if the oData should be merged with existing data, false otherwise
         * @returns {sap.ui.model.json.JSONModel|null}
         *  the JSONModel instance, for method chaining - or null
         * @protected
         */
        setModel: function (mParameters) {
            if (!mParameters || !mParameters.sName) {
                console.error("Cannot create model without at least a name!", mParameters);
                return null;
            }

            // Check the input parameters
            mParameters.bMerge = mParameters.bMerge === null || mParameters.bMerge === undefined ?
                false : mParameters.bMerge;
            if (!mParameters.sName) {
                console.error("Model name must be specified!");
                return null;
            }

            // Get or create the JSON model
            var oModel = this.getComponent().getModel(mParameters.sName);
            if (!oModel) {
                this.getComponent().setModel(new sap.ui.model.json.JSONModel({}), mParameters.sName);
                oModel = this.getComponent().getModel(mParameters.sName);
                this.mModelRegistry[mParameters.sName] = oModel;
            }

            // Update the data model
            oModel.setData(mParameters.oData, mParameters.bMerge);

            // Return the model for method chaining
            return oModel;
        },

        /**
         * @summary Sets up a list of initial local models
         *  given by the model name map
         * @description Convenience method to allow setting up
         *  multiple empty models; inserts empty oData
         * @param {map<string>} mModelNames
         *  a map with the names for the local models
         * @returns {void}
         * @protected
         */
        setInitialModels: function(mModelNames) {
            Object.keys(mModelNames).forEach(function (sKey) {
                this.setModel({
                    sName: mModelNames[sKey],
                    oData: {}
                });
            }.bind(this));
        },

        /**
         * Returns a (registered) JSON or OData model
         * @param {string} sName - the model name
         * @returns {sap.ui.model.odata.v2.ODataModel|sap.ui.model.json.JSONModel} the model instance
         * @public
         */
        getModel: function (sName) {
            return this.mModelRegistry[sName] ? this.mModelRegistry[sName] : this.getComponent().getModel(sName);
        },

        /**
         * Registers a Model with the internal 'Model Registry' map
         * @param {map} mParameters
         *  a parameter map with the following attributes
         * @param {string} mParameters.sName
         *  the model name
         * @param {sap.ui.model.Model|sap.ui.model.json.JSONModel|sap.ui.model.odata.v2.ODataModel} [mParameters.oModel]
         *  the Model instance; will be retrieved via the Component if not given
         * @returns {void}
         * @public
         */
        registerModel: function (mParameters) {
            if (!assert(mParameters && mParameters.sName, "Can't register Model without a name.")) {
                return;
            }

            mParameters.sName = mParameters.sName.toString().trim();
            mParameters.oModel = mParameters.oModel || this.getComponent().getModel(mParameters.sName);

            if (!this.mModelRegistry[mParameters.sName]) {
                this.mModelRegistry[mParameters.sName] = mParameters.oModel;
            }
        },

        /**
         * Returns the local storage handler
         * @returns {ixult.ui.util.LocalStorage} - the local storage handler
         * @public
         */
        getLocalStorage: function () {
            return this._oLocalStorage;
        },

        /**
         * Returns the 'i18n' language model instance
         * @returns {sap.ui.model.resource.ResourceModel} the i18n language model
         * @public
         */
        getLanguageModel: function () {
            return this.mModelRegistry.i18n;
        },

        /**
         * Convenience method for getting the resource bundle
         * @returns {sap.ui.model.resource.ResourceModel} the ResourceModel of the component
         * @protected
         */
        getResourceBundle: function () {
            var oBundle = this.getLanguageModel().getResourceBundle();
            if (!oBundle) {
                ResourceBundle.create({
                    url: "../i18n/i18n.properties",
                    async: false
                }).then(function (oResourceBundle) {
                    oBundle = oResourceBundle;
                }).fail(function (oError) {
                    console.error("Resource bundle not found!", oError);
                });
            }
            return oBundle;
        },

        /**
         * Retrieves a text from the language model
         * @param {string} sTextId - the text id to search for in the language model
         * @returns {string} sText - the text for the given id
         * @public
         */
        getText: function (sTextId) {
            try {
                return this.getResourceBundle().hasText(sTextId) ?
                    this.getResourceBundle().getText(sTextId) : sTextId
            } catch (oException) {
                return sTextId;
            }
        },

        /**
         * @summary Returns the property of a locally registered (JSON) model
         * @param {string} sPath
         *  either the name of an existing (most probably JSON-) model
         *  registered with this model or the path in form of 'model>property'
         * @param {string} [sAttribute]
         *  the attribute name - optionally (if sPath contains a model name)
         * @returns {*} the property value
         * @protected
         */
        findProperty: function (sPath, sAttribute) {
            var sModelName = "",
                sPropertyName = "";
            if (sPath.indexOf(">") !== -1) {
                // Split the path if given
                var aData = sPath.toString().split(">");
                sModelName = aData[0];
                sPropertyName = aData[1];
            } else if (sAttribute) {
                // ... or use the model and attribute parameter format
                sModelName = sPath;
                sPropertyName = sAttribute;
            }

            // Search for the property inside the model and return its value
            if (this.mModelRegistry[sModelName] && this.mModelRegistry[sModelName].getData) {
                var oData = this.mModelRegistry[sModelName].getData(),
                    sPropertyPath = sPropertyName.replaceAll("/", ".");
                if(sPropertyPath.indexOf(".") === 0) {
                    sPropertyPath = sPropertyPath.substring(1, sPropertyPath.length);
                }
                return ObjectHelper.getNestedProperty(oData, sPropertyPath) || null;
            }

            return null;
        },

        /**
         * @summary Returns the meta model associated with this model
         *  if it is available for the concrete model type.
         * @returns {sap.ui.model.MetaModel}
         *  The meta model or `undefined` if no meta model exists.
         * @public
         */
        getMetaModel: function () {
            return undefined;
        },

        /**
         * @summary Returns a property from the model
         * @param {string} sPath
         *  Path to where to read the object
         * @param {object} [oContext=null]
         *  Context with which the path should be resolved
         * @param {object} [mParameters]
         *  Additional model specific parameters
         * @returns {any}
         *  The value for the given path/context or
         *  `undefined` if data could not be found
         * @public
         */
        getObject: function (sPath, oContext, mParameters) {
            return this.findProperty(sPath);
        },

        /**
         * @summary Returns a property from the model
         * @param {string} sPath
         *  the path to where to read the attribute value
         * @param {object} [oContext=null]
         *  the context with which the path should be resolved
         * @public
         */
        getProperty(sPath, oContext) {
            return this.findProperty(sPath);
        }


    });
});