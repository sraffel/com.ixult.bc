sap.ui.define([
    "sap/ui/core/Core",
    "sap/ui/core/library"
], function (
    Core,
    Library
) {
    "use strict";

    /**
     * @namespace util
     * @memberOf ixult.ui.model
     *
     * @description Library for utility functions related to
     *  data and model classes and functions, e.g. containing
     *  helper methods for sorting data
     *
     * @public
     */
    return sap.ui.getCore().initLibrary({
        name: "ixult.ui.model.util",
        noLibraryCSS: true,
        dependencies: [
            "ixult.base",
            "sap.ui.core"
        ],
        types: [],
        interfaces: [],
        controls: [],
        elements: [],
        version: "0.8.1"
    });

}, /* bExport= */ false);