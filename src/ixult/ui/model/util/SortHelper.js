sap.ui.define([
    "sap/ui/model/Sorter",
    "ixult/base/assert"
], function (
    Sorter,
    assert
) {
    "use strict";

    /**
     * @summary Checks the sorter values for 'undefined' or 'null' values
     * @param {any} vValue1
     *  the first comparison value to check
     * @param {any} vValue2
     *  the second comparison value to check
     * @returns {integer}
     *  the parsed number value
     * @private
     * @static
     * @internal
     */
    var _assertValues = function (vValue1, vValue2) {
        return assert([
            vValue1 !== undefined && vValue1 !== null,
            "Sorter value '" + vValue1 + "is null or invalid.",
            vValue2 !== undefined && vValue2 !== null,
            "Sorter value '" + vValue2 + "is null or invalid."
        ]);
    }

    /**
     * @summary Parses a string value into an integer number;
     *  Removes possible decimal value signs like "," and "."
     * @param {string|integer|float} vValue
     *  the number value, most likely a string
     * @returns {integer}
     *  the parsed number value
     * @private
     * @static
     * @internal
     */
    var _parseInteger = function (vValue) {
        var sValue = vValue.toString() || "0";

        sValue = sValue.replace(",", "");
        sValue = sValue.replace(".", "");

        return parseInt(sValue, 10);
    }

    /**
     * @summary Parses currency strings like `USD 1,234.00`
     *  or `91,45 EUR` and retrieves the number part in it
     * @param {string|integer|float} vValue
     *  the number value, most likely a string
     * @returns {string}
     *  the extracted number string
     * @private
     * @static
     * @internal
     */
    var _getCurrencyValue = function (vValue) {
        var aMatches = [];
        aMatches = vValue.toString().match(/([0-9][^a-z ]*)/i);

        return aMatches && aMatches.length > 0 ? aMatches[0] : 0;
    }

    /**
     * @namespace SortHelper
     * @memberOf ixult.ui.model.util
     * @summary Utility collection for value sorting
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.ui.model.Sorter sap.ui.model.Sorter}
     * @public
     * @static
     */
    var oSortHelper = {};

    /**
     * @function compareStrings
     * @memberOf ixult.ui.model.util.SortHelper
     * @summary Compares two strings against each other
     * @param {any} vValue1
     *  the first value to compare
     * @param {any} vValue2
     *  the second value to compare
     * @returns {integer}
     *  the result of the comparison
     *  - -1 if the first value is less than the second one
     *  -  1 if the first value is greater than the second one
     *  -  0 if both are equal
     * @public
     * @static
     */
    oSortHelper.compareStrings = function (vValue1, vValue2) {
        if (!_assertValues(vValue1, vValue2)) {
            return 0;
        }

        var sValue1 = vValue1.toString().trim();
        var sValue2 = vValue2.toString().trim();

        return sValue1 < sValue2 ? -1 : sValue1 > sValue2 ? 1 : 0;
    }

    /**
     * @function compareNumbers
     * @memberOf ixult.ui.model.util.SortHelper
     * @summary Compares two (integer) number values against each other;
     *  float values or currency values will be stripped of signs like `,` or `.`
     * @param {any} vValue1
     *  the first value to compare
     * @param {any} vValue2
     *  the second value to compare
     * @returns {integer} - the result of the comparison
     *  - -1 if the first value is less than the second one
     *  -  1 if the first value is greater than the second one
     *  -  0 if both are equal
     * @public
     * @static
     */
    oSortHelper.compareNumbers = function (vValue1, vValue2) {
        if (!_assertValues(vValue1, vValue2)) {
            return 0;
        }
        var iNumber1, iNumber2;

        try {
            iNumber1 = _parseInteger(vValue1.toString().trim());
            iNumber2 = _parseInteger(vValue2.toString().trim());
        } catch (oException) {
            console.error("Could not compare numbers for values '" +
                vValue1 + "' and '" + vValue2 + "'");
            return oSortHelper.compareStrings(vValue1, vValue2);
        }

        return iNumber1 < iNumber2 ? -1 :
            iNumber1 > iNumber2 ? 1 : 0;
    }

    /**
     * @function compareCurrencies
     * @memberOf ixult.ui.model.util.SortHelper
     * @summary Compares two currency values like `USD 1,234.00`
     *  or `91,45 EUR` against each other
     * @param {any} vValue1
     *  the first value to compare
     * @param {any} vValue2
     *  the second value to compare
     * @returns {integer}
     *  the result of the comparison
     *  - -1 if the first value is less than the second one
     *  -  1 if the first value is greater than the second one
     *  -  0 if both are equal
     * @public
     * @static
     */
    oSortHelper.compareCurrencies = function (vValue1, vValue2) {
        return oSortHelper.compareNumbers(
            _getCurrencyValue(vValue1),
            _getCurrencyValue(vValue2)
        );
    }

    /**
     * @function compareDates
     * @memberOf ixult.ui.model.util.SortHelper
     * @summary Compares dates with handling empty
     *  values as being the lowest
     * @param {any} vValue1
     *  the first value to compare
     * @param {any} vValue2
     *  the second value to compare
     * @returns {integer}
     *  the result of the comparison
     *  - -1 if the first value is less than the second one
     *  -  1 if the first value is greater than the second one
     *  -  0 if both are equal
     * @public
     * @static
     */
    oSortHelper.compareDates = function (vValue1, vValue2) {
        vValue1 = vValue1 !== undefined && vValue1 !== null ? vValue1 : "";
        vValue2 = vValue2 !== undefined && vValue2 !== null ? vValue2 : "";

        var dDate1 = new Date(vValue1.toString().trim()) || new Date(0),
            dDate2 = new Date(vValue2.toString().trim()) || new Date(0);

        return dDate1.getTime() < dDate2.getTime() ? -1 : dDate1.getTime() > dDate2.getTime() ? 1 : 0;
    }


    return oSortHelper;
});