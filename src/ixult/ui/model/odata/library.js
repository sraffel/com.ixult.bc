sap.ui.define([
    "sap/ui/core/Core",
    "sap/ui/core/library"
], function (
    Core,
    Library
) {
    "use strict";

    /**
     * @namespace odata
     * @memberOf ixult.ui.model
     *
     * @description Library to handle OData service requests
     *  as well as pre- or post-processing
     *
     * @public
     */
    return sap.ui.getCore().initLibrary({
        name: "ixult.ui.model.odata",
        noLibraryCSS: true,
        dependencies: [
            "sap.ui.core"
        ],
        types: [],
        interfaces: [],
        controls: [],
        elements: [],
        version: "0.8.1"
    });

}, /* bExport= */ false);