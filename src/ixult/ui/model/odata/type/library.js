sap.ui.define([
    "sap/ui/core/Core",
    "sap/ui/core/library"
], function (
    Core,
    Library
) {
    "use strict";

    /**
     * @namespace type
     * @memberOf ixult.ui.model.odata
     *
     * @description Library to handle OData data types
     *
     * @public
     */
    return sap.ui.getCore().initLibrary({
        name: "ixult.ui.model.odata.type",
        noLibraryCSS: true,
        dependencies: [
            "sap.ui.core"
        ],
        types: [],
        interfaces: [],
        controls: [],
        elements: [],
        version: "0.8.1"
    });

}, /* bExport= */ false);