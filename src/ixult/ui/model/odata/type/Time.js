sap.ui.define([
    "ixult/base/util/DateHelper",
    "sap/ui/model/odata/type/Time"
], function (
    DateHelper,
    Time
) {

    /**
     * TODO: comment
     * @see {@link https://www.w3schools.blog/xsd-date-and-time-data-types https://www.w3schools.blog/xsd-date-and-time-data-types}
     */
    return Time.extend("ixult.ui.model.odata.type.Time", /** @lends ixult.ui.model.odata.type.Time */ {
        oFormatOptions: null,

        /**
         * TODO: comment
         */
        constructor: function (oFormatOptions, oConstraints) {
            Time.prototype.constructor.apply(this, arguments);

            this.oFormatOptions = oFormatOptions;
            this.oFormatOptions.pattern =
                this.oFormatOptions.pattern || "HH:mm:ss";
        },

        /**
         * TODO: comment
         * @param bUTC
         * @returns {*|boolean}
         * @private
         */
        _getUTC(bUTC) {
            return bUTC !== undefined ? bUTC : false;
        },

        /**
         * TODO: comment
         * @param dTime
         * @param bUTC
         * @returns {object}
         */
        toEdmTime: function (dTime, bUTC) {
            bUTC = bUTC !== undefined ? bUTC : false;
            return this.parseValue(
                DateHelper.format(dTime, this.oFormatOptions.pattern, this._getUTC(bUTC)),
                "string"
            );
        },

        /**
         * TODO: comment
         * @param {Date|map} vTime
         * @param bUTC
         * @returns {string}
         */
        toXsdString: function (vTime, bUTC) {
            var dTime = DateHelper.isDate(vTime) ? vTime : DateHelper.toDate(vTime);
            return DateHelper.format(dTime, "%1%%5%HH%6%mm%7%ss%8%", this._getUTC(bUTC))
                .replace("%1%", "P").replace("%5%", "T")
                .replace("%6%", "H").replace("%7%", "M").replace("%8%", "S");
        },

        /**
         * TODO: comment
         * @param oEdmTime
         * @param bUTC
         * @returns {string}
         */
        edmToXsd: function (oEdmTime, bUTC) {
            var dDate = DateHelper.toDate({
                iTimeMs: oEdmTime.ms
            });
            return this.toXsdString(dDate, this._getUTC(bUTC));
        }


    });
});
