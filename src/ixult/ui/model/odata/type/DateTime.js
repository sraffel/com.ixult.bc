sap.ui.define([
    "ixult/base/util/DateHelper",
    "sap/ui/model/odata/type/DateTime"
], function (
    DateHelper,
    DateTime
) {

    /**
     * TODO: comment
     * @see {@link https://www.w3schools.blog/xsd-date-and-time-data-types https://www.w3schools.blog/xsd-date-and-time-data-types}
     */
    return DateTime.extend("ixult.ui.model.odata.type.DateTime", /** @lends ixult.ui.model.odata.type.DateTime */ {
        oFormatOptions: null,

        /**
         * TODO: comment
         */
        constructor: function (oFormatOptions, oConstraints) {
            DateTime.prototype.constructor.apply(this, arguments);

            this.oFormatOptions = oFormatOptions;
            this.oFormatOptions.pattern =
                this.oFormatOptions.pattern || "yyyy-MM-dd HH:mm:ss";
        },

        /**
         * TODO: comment
         */
        toFilterString: function (dDateTime) {
            return "datetime'" + DateHelper.format(dDateTime, "yyyy-MM-ddTHH:mm:ss") + "'";
        },

        /**
         * TODO: comment
         */
        toXsdString: function (dDateTime) {
            return DateHelper.format(dDateTime, "%1%yy%2%MM%3%dd%4%%5%HH%6%mm%7%ss%8%")
                .replace("%1%", "P").replace("%2%", "Y").replace("%3%", "M").replace("%4%", "D")
                .replace("%5%", "T").replace("%6%", "H").replace("%7%", "M").replace("%8%", "S");
        }

    });
});
