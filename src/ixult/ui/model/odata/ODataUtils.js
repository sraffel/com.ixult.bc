sap.ui.define([
    "sap/base/util/uid",
    "sap/ui/model/odata/ODataUtils",
    "ixult/base/assert",
    "ixult/base/util/array",
    "ixult/base/util/object",
    "ixult/base/util/DateHelper",
    "ixult/ui/base/Error"
], function (
    uid,
    ODataUtils,
    assert,
    array,
    object,
    DateHelper,
    Error
) {
    "use strict";

    /**
     * @summary Checks a given ODataModel for its version
     * @param {sap.ui.model.odata.ODataModel|sap.ui.model.odata.v2.ODataModel} oDataModel
     *  the ODataModel, either version 1 or 2
     * @returns {Number}
     *  1 or 2 depending on the ODataModel version,
     *  0 if the model version does not match
     *  or if an error occurred
     * @private
     * @static
     * @internal
     */
    const _fnGetODataModelVersion = function (oDataModel) {
        if (!assert(oDataModel, "No ODataModel found.")) {
            return -1;
        }

        const iVersion = oDataModel.isA("sap.ui.model.odata.ODataModel") ? 1 :
            oDataModel.isA("sap.ui.model.odata.v2.ODataModel") ? 2 : 0;

        assert(iVersion > 0, "No valid Model of type 'sap.ui.model.odata.ODataModel' " +
            "or 'sap.ui.model.odata.v2.ODataModel' was found.");

        return iVersion;
    }

    /**
     * @summary Returns oData batch responses in a structured object
     * @description Evaluates given oData coming from a OData batch service call and returns
     *  them as a single object with the results as sub elements assigned by a result id
     *  given inside the 'operations' array
     * @param oData
     * @param {map} mParameters
     *  a map of parameters containing the following attributes
     * @param {boolean} [mParameters.bDeleteMetadata=true]
     *  automatically deletes the metadata when parsing the result; true by default
     * @param {boolean} [mParameters.bAsArray=false]
     *  groups the result data of one `sResultId` group into a `results` array
     * @param {Array.<{
     *      sPath: string,
     *      sMethod: string,
     *      sResultId: string,
     *      oData: object,
     *      oParameters: object
     *  }>} mParameters.aOperations
     *  the set of batch operations with attributes to apply to the ODataModel batch methods
     * @param {jQuery.Deferred} [mParameters.oDeferred]
     *  the Deferred instance used in the batch submit method; can optionally
     *  be used to create progress notifications via the `Deferred.notify` function
     * @returns {object|null}
     *  the result object with sub objects containing the assigned 'batchResponses' data
     * @private
     * @static
     * @internal
     */
    const _fnCreateBatchResult = function (oData, mParameters) {
        var oResult = {};
        if (!assert([
            mParameters.aOperations, "Invalid parameters or no parameters found.",
            oData, "No response found."
        ])) {
            return null;
        }

        // Return with a warning if there have been no batch responses
        // (might happen due to invalid or missing backend entries)
        if (!oData.__batchResponses) {
            console.warn("No batch response found.");
            return oData;
        }

        // Evaluate the batch responses and move them to a
        // combined result assigned on given identifiers
        array(oData.__batchResponses).loop(function (oData, iIndex) {
            // Extract the result data and post-process them
            var oOperation = mParameters.aOperations[iIndex],
                sId = oOperation && oOperation.sResultId ? oOperation.sResultId : uid(),
                oResultData = mParameters.bDeleteMetadata === true ?
                    oDataUtils.deleteMetadata(oData.data) : oData.data;

            // Continue if there's no result data, e.g. when
            // the entry is a technical `__changeResponses`)
            if (oResultData === undefined) {
                return true;
            }

            // Optional: create status notifications for the Deferred object
            if (mParameters.oDeferred && mParameters.oDeferred.notify) {
                mParameters.oDeferred.notify({
                    sId: sId,
                    iIndex: iIndex,
                    oData: oResultData
                });
            }

            // Return the result data in one group as an array
            if (mParameters.bAsArray === true) {
                if (!oResult[sId]) {
                    oResult[sId] = {
                        results: []
                    }
                }
                oResult[sId].results.push(oResultData);
                return true;
            }

            // Insert the result object into the set
            oResult[sId] = !oResult[sId] ? oResultData :
                oResult[sId].results && oResultData.results ? {
                    results: oResult[sId].results.concat(oResultData.results)
                } : object(oResult[sId]).extend(oResultData);
        });

        return oResult;
    }

    /**
     * @namespace ODataUtils
     * @memberOf ixult.ui.model.odata
     * @summary Utility collection to handle and/or manipulate OData,
     *  based on the `sap.ui.model.odata.ODataUtils` collection
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.ui.model.odata.ODataUtils sap.ui.model.odata.ODataUtils}
     * @public
     * @static
     */
    var oDataUtils = ODataUtils;

    /**
     * @name Method
     * @memberOf ixult.ui.model.odata.ODataUtils
     * @summary Possible OData or ajax call methods
     *  that can be executed in batch operations
     * @type {map}
     * @public
     */
    ODataUtils.Method = {
        POST: "POST",
        GET: "GET",
        PUT: "PUT",
        MERGE: "MERGE",
        DELETE: "DELETE",
    };

    /**
     * @name Operation
     * @memberOf ixult.ui.model.odata.ODataUtils
     * @summary Possible OData operations for
     *  ODataModel methods, i.e. CRUD
     * @type {map}
     * @public
     */
    ODataUtils.Operation = {
        Create: "Create",
        Read: "Read",
        Update: "Update",
        Delete: "Delete",
    };


    /**
     * @namespace v1
     * @memberOf ixult.ui.model.odata.ODataUtils
     *
     * @summary Utility collection to handle and/or manipulate ODataModels in version 1,
     *  based on the SAPUI5 sap.ui.model.odata.ODataUtils collection
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.ui.model.odata.ODataUtils sap.ui.model.odata.ODataUtils}
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.ui.model.odata.v2.ODataModel sap.ui.model.odata.v2.ODataModel}
     *
     * @public
     * @static
     *
     * @deprecated use `sap.ui.model.odata.v2.ODataModel` methods instead;
     *  will be automatically applied when given an ODataModel v2 as parameter
     */
    oDataUtils.v1 = {

        /**
         * @function createBatchOperations
         * @memberOf ixult.ui.model.odata.ODataUtils.v1
         * @summary Creates batch operations for OData batch service calls
         * @description Note: This method only supports sap.ui.model.odata.ODataModel
         *  and thus is deprecated; use sap.ui.model.odata.v2.ODataModel instead and create batch operations
         *  using {#ixult.ui.model.odata.ODataUtils.v2.createBatchOperations}
         * @see {@link https://ui5.sap.com/sdk/#/api/sap.ui.model.odata.ODataModel sap.ui.model.odata.ODataModel}
         * @param {map} mParameters
         *  a map of parameters containing the following attributes
         * @param {sap.ui.model.odata.ODataModel} mParameters.oDataModel
         *  the ODataModel; note: this only supports v1 ODataModel and thus is deprecated
         * @param {Array.<{
         *      sPath: string,
         *      sMethod: string,
         *      sResultId: string,
         *      oData: object,
         *      Parameters: object
         *  }>} mParameters.aOperations
         *  the set of batch operations with attributes to apply to the ODataModel batch methods
         * @returns {sap.ui.model.odata.ODataModel|null}
         *  the changed ODataModel (for method chaining), e.g. to allow calling 'submitBatch'
         *  directly after this method; returns null if an error occurred
         * @protected
         * @static
         * @deprecated
         */
        createBatchOperations: function (mParameters) {
            if (!assert([
                mParameters && mParameters.oDataModel && mParameters.aOperations,
                "Input for batch operations not found or invalid.",
                _fnGetODataModelVersion(mParameters.oDataModel) === 1,
                "This method only supports 'sap.ui.model.odata.ODataModel' (v1)."
            ])) {
                return null;
            }
            var oDataModel = mParameters.oDataModel,
                aBatchOperations = [];

            // Prepare the ODataModel for the batch operations
            oDataModel.clearBatch();

            // Create OData batch operations from the given parameter array
            mParameters.aOperations.forEach(function (oOperation) {
                aBatchOperations.push(oDataModel.createBatchOperation(
                    oOperation.sPath,
                    oOperation.sMethod,
                    oOperation.oData,
                    oOperation.oParameters
                ));
            })

            // Add 'change' oder 'read' operations to the ODataModel depending on the 'method' value
            const aBatchChangesArray = array([
                ODataUtils.Method.PUT,
                ODataUtils.Method.MERGE,
                ODataUtils.Method.POST,
                ODataUtils.Method.DELETE
            ]);
            oDataModel.addBatchChangeOperations($.grep(aBatchOperations, function (oOperation) {
                return aBatchChangesArray.contains(oOperation.sMethod);
            }));
            oDataModel.addBatchReadOperations($.grep(aBatchOperations, function (oOperation) {
                return oOperation.method === ODataUtils.Method.GET;
            }.bind(this)));

            // Return the changed ODataModel for method chaining
            return oDataModel;
        }

    };

    /**
     * @namespace v2
     * @memberOf ixult.ui.model.odata.ODataUtils
     * @summary Utility collection to handle and/or manipulate ODataModels in version 2,
     *  based on the SAPUI5 sap.ui.model.odata.ODataUtils collection
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.ui.model.odata.ODataUtils sap.ui.model.odata.ODataUtils}
     * @public
     * @static
     */
    oDataUtils.v2 = {

        /**
         * @function createBatchOperations
         * @memberOf ixult.ui.model.odata.ODataUtils.v2
         * @summary Creates batch operations for OData batch service calls
         * @description Note: This method only supports sap.ui.model.odata.v2.ODataModel
         * @see {@link https://ui5.sap.com/sdk/#/api/sap.ui.model.odata.v2.ODataModel sap.ui.model.odata.v2.ODataModel}
         * @param {map} mParameters
         *  a map of parameters containing the following attributes
         * @param {sap.ui.model.odata.v2.ODataModel} mParameters.oDataModel
         *  the ODataModel
         * @param {string} [mParameters.sGroupId]
         *  the group id for the batch to create; will be generated if omitted
         * @param {Array.<{
         *      sPath: string,
         *      sMethod: string,
         *      sResultId: string,
         *      oData: object,
         *      oParameters: object
         *  }>} mParameters.aOperations
         *  the set of batch operations with attributes to apply to the ODataModel batch methods
         * @returns {sap.ui.model.odata.v2.ODataModel|null}
         *  the changed ODataModel (for method chaining), e.g. to allow calling 'submitChanges'
         *  directly after this method; returns null if an error occurred
         * @protected
         * @static
         */
        createBatchOperations: function (mParameters) {
            // Check the input parameters
            if (!assert([
                mParameters && mParameters.oDataModel && mParameters.aOperations,
                "Input for batch operations not found or invalid.",
                _fnGetODataModelVersion(mParameters.oDataModel) === 2,
                "This method only supports 'sap.ui.model.odata.v2.ODataModel' (v2)."
            ])) {
                return null;
            }

            // Prepare the ODataModel for the batch processing
            var oDataModel = mParameters.oDataModel,
                sGroupId = mParameters.sGroupId !== undefined ?
                    mParameters.sGroupId.toString().trim() : uid();
            oDataModel.setDeferredGroups([sGroupId]);

            // Create OData batch operations from the given parameter array
            mParameters.aOperations.forEach(function (oOperation) {
                var oParameters = $.extend(true, oOperation.oParameters || {}, {
                    groupId: sGroupId
                });
                switch (oOperation.sMethod) {
                    case ODataUtils.Method.POST:
                        oDataModel.create(oOperation.sPath, oOperation.oData, oParameters);
                        break;
                    case ODataUtils.Method.GET:
                        oDataModel.read(oOperation.sPath, oParameters);
                        break;
                    case ODataUtils.Method.PUT:
                    case ODataUtils.Method.MERGE:
                        oDataModel.update(oOperation.sPath, oOperation.oData, oParameters);
                        break;
                    case ODataUtils.Method.DELETE:
                        oDataModel.remove(oOperation.sPath, oParameters)
                        break;
                }
            });

            // Return the changed ODataModel for method chaining
            return oDataModel;
        },

        /**
         * @function getEntityType
         * @memberOf ixult.ui.model.odata.ODataUtils.v2
         * @summary Reads the ODataModel's metadata to extract
         *  an entity type with the given name
         * @description Reads the metadata to look for the child object
         *  `dataServices.schema[0].entityType` where usually the entity types
         *   are stored; then looks up the entity type and its attributes.
         *   The attributes are then transferred into the return map.
         *   Supports the following OData Edm types at the moment:
         *   - `Edm.String`: will be created empty
         *   - `Edm.Int32`: will be set to `0`
         *   - `Edm.Time`: wil be initiated as structure `{ ms: 0 }`
         * @param {sap.ui.model.odata.v2.ODataModel} oDataModel
         *  the ODataModel instance
         * @param {map} mParameters
         *  a parameter map containing the following attributes
         * @param {string} mParameters.sEntityName
         *  the name of the entity to look for
         * @returns {object|map}
         *  a map for the entity type with the extracted attributes
         *  and initial values
         * @public
         * @static
         * @experimental
         */
        getEntityType: function (oDataModel, mParameters) {
            oDataModel = oDataModel || this.getODataModel();

            // Get the service metadata and lookup the entity for 'ActivityType'
            // which is the type that contains time record entries in the backend
            var oTemplate = {},
                oMetadata = oDataModel.getServiceMetadata(),
                oEntityType = $.grep(oMetadata.dataServices.schema[0].entityType, function (oEntity) {
                    return oEntity.name === mParameters.sEntityName;
                }.bind(this))[0];

            // Copy each of the attributes we need in the template to the template object
            oEntityType.property.forEach(function (oProperty) {
                switch (oProperty.type) {
                    case "Edm.String":
                        oTemplate[oProperty.name] = "";
                        break;
                    case "Edm.Time":
                        oTemplate[oProperty.name] = {ms: 0};
                        break;
                    case "Edm.Int32":
                        oTemplate[oProperty.name] = 0;
                        break;
                }
            }.bind(this));

            return oTemplate;
        }

    };

    /**
     * @function deleteMetadata
     * @memberOf ixult.ui.model.odata.ODataUtils
     * @summary Strips an OData object from unnecessary '__metadata' and '__deferred' entries
     * @param {object} oData
     *  the OData object
     * @returns {object}
     *  the modified OData object
     * @public
     * @static
     */
    oDataUtils.deleteMetadata = function (oData) {
        // Delete the metadata and deferred properties inside the object
        for (var sProperty in oData) {
            if (!oData.hasOwnProperty(sProperty) ||
                oData[sProperty] === null ||
                oData[sProperty] === undefined) {
                continue;
            }
            if (sProperty === "__deferred" || sProperty === "__metadata") {
                delete oData[sProperty];
            } else if (DateHelper.isDate(oData[sProperty])) {
                // do nothing
            } else if (typeof oData[sProperty] === "object") {
                // Delete 'deferred' and 'metadata' from sub-properties
                // and delete the property if nothing else remains
                oData[sProperty] = object(oDataUtils.deleteMetadata(oData[sProperty]));
                if (oData[sProperty].isEmpty() && !Array.isArray(oData[sProperty])) {
                    delete oData[sProperty];
                }
            }
        }

        // Delete the metadata and deferred properties inside the 'results' list if existing
        if (oData && oData.results && Array.isArray(oData.results) && oData.results.length > 0) {
            oData.results.forEach(function (oResult) {
                oResult = oDataUtils.deleteMetadata(oResult);
            }.bind(this));
        }

        return oData;
    }

    /**
     * @function createBatchOperations
     * @memberOf ixult.ui.model.odata.ODataUtils
     * @summary Creates batch operations for OData batch service calls
     * @description This is a convenience method redirecting to
     *  {ixult.ui.model.odata.ODataUtils.v1#createBatchOperations} or
     *  {ixult.ui.model.odata.ODataUtils.v2#createBatchOperations}
     *  depending on the given ODataModel version
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.ui.model.odata.ODataModel sap.ui.model.odata.ODataModel}
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.ui.model.odata.v2.ODataModel sap.ui.model.odata.v2.ODataModel}
     * @param {map} mParameters
     *  a map of parameters containing the following attributes
     * @param {sap.ui.model.odata.ODataModel|sap.ui.model.odata.v2.ODataModel} mParameters.oDataModel
     *  the ODataModel, either version 1 or 2
     * @param {string} [mParameters.sGroupId]
     *  the group id for the batch to create when using sap.ui.model.odata.v2.ODataModel; will be generated if omitted
     * @param {Array.<{
     *      sPath: string,
     *      sMethod: string,
     *      sResultId: string,
     *      oData: object,
     *      oParameters: object
     *  }>} mParameters.aOperations
     *  the set of batch operations with attributes to apply to the ODataModel batch methods
     * @returns {sap.ui.model.odata.ODataModel|sap.ui.model.odata.v2.ODataModel}
     *  The changed ODataModel, enriched with the given batch operations;
     *  Hint: Use method {ixult.ui.model.odata.ODataUtils~submitBatchOperations} to directly
     *  execute batch processing and get the result in form of a Promise than can be evaluated
     * @protected
     * @static
     */
    oDataUtils.createBatchOperations = function (mParameters) {
        const iVersion = _fnGetODataModelVersion(mParameters.oDataModel);
        if (!assert(mParameters && mParameters.oDataModel && mParameters.aOperations && iVersion > 0,
            "Input for batch operations not found or invalid.")) {
            return null;
        }

        return iVersion === 1 ? oDataUtils.v1.createBatchOperations(mParameters) :
            iVersion === 2 ? oDataUtils.v2.createBatchOperations(mParameters) : null;
    }

    /**
     * @function submitBatchOperations
     * @memberOf ixult.ui.model.odata.ODataUtils
     * @summary Creates batch operations for OData batch service calls and directly submits them
     * @description This method allows to create batch operations but also directly to submit them
     *  which also includes post-processing the 'batchResponses' data and assign the results based
     *  on given ids (specified in mParameters.aOperations > 'sResultId')
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.ui.model.odata.ODataModel sap.ui.model.odata.ODataModel}
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.ui.model.odata.v2.ODataModel sap.ui.model.odata.v2.ODataModel}
     * @param {map} mParameters
     *  a map of parameters containing the following attributes
     * @param {sap.ui.model.odata.ODataModel|sap.ui.model.odata.v2.ODataModel} mParameters.oDataModel
     *  the ODataModel instance, either version 1 or 2
     * @param {string} [mParameters.sGroupId]
     *  the group id for the batch to create when using sap.ui.model.odata.v2.ODataModel; will be generated if omitted
     * @param {boolean} [mParameters.bDeleteMetadata=true]
     *  automatically deletes the metadata when parsing the result; true by default
     *  experimental
     * @param {boolean} [mParameters.bAsArray=false]
     *  groups the result data of one `sResultId` group into a `results` array
     * @param {Array.<{
     *      sPath: string,
     *      sMethod: string,
     *      sResultId: string,
     *      oData: object,
     *      oParameters: object
     *  }>} mParameters.aOperations
     *  the set of batch operations with attributes to apply to the ODataModel batch methods
     * @returns {Promise}
     *  a Promise object with resolve and reject handler methods to asynchronously
     *  post process the OData service result in the calling instance;
     *  an OData success can be handled via 'then' using the 'oResult' parameter,
     *  where oResult is an object containing the batch responses,
     *  each assigned to the given ids in the aOperations parameter
     *  an OData service error can be handled via 'fail' using the 'oError' parameter
     * @public
     * @static
     */
    oDataUtils.submitBatchOperations = function (mParameters) {
        var oDeferred = $.Deferred(),
            mResultParameters = {
                bDeleteMetadata: mParameters.bDeleteMetadata === false ?
                    mParameters.bDeleteMetadata : true,
                bAsArray: mParameters.bAsArray === true,
                aOperations: mParameters.aOperations,
                oDeferred: oDeferred
            },
            iDateNow = Date.now();

        // Return 'empty-handed' when there's nothing to be done
        if (mParameters.aOperations.length === 0) {
            oDeferred.resolve();
            return oDeferred.promise();
        }

        // Check the ODataModel version
        const iVersion = _fnGetODataModelVersion(mParameters.oDataModel);
        if (!mParameters || !mParameters.oDataModel || !mParameters.aOperations || !iVersion) {
            oDeferred.reject(new Error({
                message: "Input for batch operations not found or invalid."
            }));
            return oDeferred.promise();
        }

        // Internal handler to check for error messages
        // or parse the result and resolve the operation
        const fnResolve = function (oData) {
            var aMessages = sap.ui.getCore().getMessageManager().getMessageModel().getData(),
                aErrors = array(aMessages).grep(function (oMessage) {
                    return oMessage.date > iDateNow && oMessage.validation === false;
                });
            if (aErrors.length === 0) {
                oDeferred.resolve(_fnCreateBatchResult(oData, mResultParameters));
            } else {
                oDeferred.reject(new Error({
                    response: {
                        statusCode: aErrors[0].technicalDetails.statusCode,
                        statusText: aErrors[0].message
                    },
                    results: aErrors
                }));
            }
        }.bind(this);

        // Enable batch processing
        mParameters.oDataModel.setUseBatch(true);

        // Create the batch operations depending on the given ODataModel version
        /** @type {sap.ui.model.odata.ODataModel|sap.ui.model.odata.v2.ODataModel} */
        var oDataModel = oDataUtils.createBatchOperations(mParameters);
        if (!oDataModel) {
            oDeferred.reject(new Error({
                message: "Could not create operations for batch processing!"
            }));
            return oDeferred.promise();
        }

        // For v1 we get the ODataModel back after creating the batch operations
        if (iVersion === 1) {
            // TODO test ODataModel v1 (although it's deprecated)
            // Use 'submitBatch' for OData v1
            oDataModel.submitBatch(
                // Success
                function (oData, oResponse) {
                    oDataModel.clearBatch();
                    oDataModel.setUseBatch(false);
                    fnResolve(oData);
                }.bind(this),
                // Error
                function (oError) {
                    oDataModel.clearBatch();
                    oDataModel.setUseBatch(false);
                    oDeferred.reject(new Error(oError));
                }.bind(this)
            );
        } else if (iVersion === 2) {
            // Use 'submitChanges' for OData v2
            oDataModel.submitChanges({
                groupId: mParameters.sGroupId,
                success: function (oData, oResponse) {
                    oDataModel.setUseBatch(false);
                    if (oDataModel.hasPendingChanges(true)) {
                        oDataModel.resetChanges(null, true).then(function () {
                            fnResolve(oData);
                        }.bind(this));
                    } else {
                        fnResolve(oData);
                    }
                }.bind(this),
                error: function (oError) {
                    oDataModel.setUseBatch(false);
                    oDataModel.resetChanges(null, true).then(function () {
                        if (oDataModel.hasPendingChanges(true)) {
                            oDataModel.resetChanges(null, true).then(function () {
                                oDeferred.reject(new Error(oError));
                            }.bind(this));
                        } else {
                            oDeferred.reject(new Error(oError));
                        }
                    }.bind(this));
                }.bind(this)
            });
        }

        // Return this 'submitBatch' Promise
        return oDeferred.promise();
    }


    /**
     * @function getEntityType
     * @memberOf ixult.ui.model.odata.ODataUtils
     * @summary Reads the ODataModel's metadata to extract
     *  an entity type with the given name
     * @description Reads the metadata to look for the child object
     *  `dataServices.schema[0].entityType` where usually the entity types
     *   are stored; then looks up the entity type and its attributes.
     *   The attributes are then transferred into the return map.
     *   Supports the following OData Edm types at the moment:
     *   - `Edm.String`: will be created empty
     *   - `Edm.Int32`: will be set to `0`
     *   - `Edm.Time`: wil be initiated as structure `{ ms: 0 }`
     * @param {sap.ui.model.odata.ODataModel|sap.ui.model.odata.v2.ODataModel} oDataModel
     *  the ODataModel instance
     * @param {map} mParameters
     *  a parameter map containing the following attributes
     * @param {string} mParameters.sEntityName
     *  the name of the entity to look for
     * @returns {object|map}
     *  a map for the entity type with the extracted attributes
     *  and initial values
     * @public
     * @static
     * @experimental
     */
    oDataUtils.getEntityType = function (oDataModel, mParameters) {
        const iVersion = _fnGetODataModelVersion(oDataModel);
        if (iVersion === 1) {
            console.log("getEntityType only supports ODataModel v2 at the moment.");
            return {};
        } else if (iVersion === 2) {
            return oDataUtils.v2.getEntityType(oDataModel, mParameters);
        }

        return null;
    }

    return oDataUtils;
});
