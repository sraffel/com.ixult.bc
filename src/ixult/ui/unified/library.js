sap.ui.define([
    "sap/ui/core/Core",
    "sap/ui/core/library"
], function (
    Core,
    Library
) {
    "use strict";

    /**
     * @namespace unified
     * @memberOf ixult.ui
     * @summary main library for unified controls and helper classes
     * @public
     */
    return sap.ui.getCore().initLibrary({
        name: "ixult.ui.unified",
        noLibraryCSS: true,
        dependencies: [
            "sap.ui.core",
            "sap.m",
            "ixult.ui.core"
        ],
        types: [],
        interfaces: [],
        controls: [
            "ixult.ui.unified.FileUploader"
        ],
        "resources": {
            "js": [
                {"uri": "ixult/lib/ui/unified/jquery.csv.js"},
                {"uri": "ixult/lib/ui/unified/simple-excel.js"},
                {"uri": "ixult/lib/ui/unified/xlsx-populate.js"},
                {"uri": "ixult/lib/ui/unified/xlsx.js"},
                {"uri": "ixult/lib/ui/unified/jszip.js"},
            ]
        },
        elements: [],
        version: "0.8.2"
    });

}, /* bExport= */ false);