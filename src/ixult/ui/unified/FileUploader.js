sap.ui.define([
    "sap/m/FlexBox",
    "sap/ui/unified/FileUploader",
    "sap/ui/unified/FileUploaderHttpRequestMethod",
    "sap/ui/Device",
    "sap/ui/core/CustomData",
    "sap/ui/core/Fragment",
    "sap/ui/core/ValueState",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/resource/ResourceModel",
    "ixult/base/assert",
    "ixult/base/util/array",
    "ixult/base/util/object",
    "ixult/base/util/string",
    "ixult/base/util/DateHelper",
    "ixult/base/util/LibraryHelper",
    "ixult/ui/core/util/file/type/ExcelDocument",
    "ixult/ui/core/util/file/library",
    "./library"
], function (
    FlexBox,
    FileUploader,
    HttpRequestMethod,
    Device,
    CustomData,
    Fragment,
    ValueState,
    JSONModel,
    ResourceModel,
    assert,
    array,
    object,
    string,
    DateHelper,
    LibraryHelper,
    ExcelDocument,
    MyLibrary
) {
    "use strict";
    const Library = LibraryHelper.register("ixult/ui/unified", {
        loadStyleSheet: true,
        loadFonts: true
    });

    /**
     * @class FileUploader
     * @extends sap.m.FlexBox
     * @implements sap.ui.unified.FileUploader
     * @memberOf ixult.ui.unified
     *
     * @summary Extended FileUploader Control
     *  supporting HTML Drag and Drop
     *
     * @public
     */
    return FlexBox.extend("ixult.ui.unified.FileUploader", /** @lends ixult.ui.unified.FileUploader */ {
        // The renderer to use; Important: Leave this empty,
        // so that the default Renderer is used
        renderer: {},

        // Metadata for the UI element; Used to define custom properties
        metadata: {
            properties: {
                // Inherited properties from sap.ui.unified.FileUploader
                // Will be passed down to the `FileUploader` Control
                // inside the surrounding `FlexBox` Control
                value: {
                    type: "string",
                    group: "Data",
                    defaultValue: "",
                    bindable: true
                },
                enabled: {type: "boolean", group: "Behavior", defaultValue: true},
                uploadUrl: {type: "sap.ui.core.URI", group: "Data", defaultValue: ''},
                name: {type: "string", group: "Data", defaultValue: null},
                width: {type: "sap.ui.core.CSSSize", group: "Misc", defaultValue: ''},
                uploadOnChange: {type: "boolean", group: "Behavior", defaultValue: false},
                additionalData: {type: "string", group: "Data", defaultValue: null},
                sameFilenameAllowed: {type: "boolean", group: "Behavior", defaultValue: false},
                buttonText: {type: "string", group: "Misc", defaultValue: null},
                fileType: {type: "string[]", group: "Data", defaultValue: null},
                multiple: {type: "boolean", group: "Behavior", defaultValue: false},
                maximumFileSize: {type: "float", group: "Data", defaultValue: null},
                mimeType: {type: "string[]", group: "Data", defaultValue: null},
                sendXHR: {type: "boolean", group: "Behavior", defaultValue: false},
                httpRequestMethod: {type: "sap.ui.unified.FileUploaderHttpRequestMethod", group: "Behavior", defaultValue: HttpRequestMethod.Post},
                placeholder: {type: "string", group: "Appearance", defaultValue: null},
                style: {type: "string", group: "Appearance", defaultValue: null},
                buttonOnly: {type: "boolean", group: "Appearance", defaultValue: false},
                useMultipart: {type: "boolean", group: "Behavior", defaultValue: true},
                maximumFilenameLength: {type: "int", group: "Data", defaultValue: null},
                valueState: {type: "sap.ui.core.ValueState", group: "Data", defaultValue: ValueState.None},
                valueStateText: {type: "string", group: "Misc", defaultValue: null},
                icon: {type: "sap.ui.core.URI", group: "Appearance", defaultValue: ''},
                iconHovered: {type: "sap.ui.core.URI", group: "Appearance", defaultValue: ''},
                iconSelected: {type: "sap.ui.core.URI", group: "Appearance", defaultValue: ''},
                iconFirst: {type: "boolean", group: "Appearance", defaultValue: true},
                iconOnly: {type: "boolean", group: "Appearance", defaultValue: false},
                directory: {type: "boolean", group: "Behavior", defaultValue: false},
                // Custom properties
                /**
                 * @name ixult.ui.unified.FileUploader#showLabel
                 * @type {boolean}
                 * @default `false`
                 * @description Shows a Label in front of the
                 *  `FileUploader` Control if set to `true`;
                 *  Default is `false`
                 * @public
                 */
                showLabel: {
                    type: "boolean",
                    group: "Appearance",
                    defaultValue: false
                },
                /**
                 * @name ixult.ui.unified.FileUploader#labelText
                 * @type {boolean}
                 * @description Text on the Label that is shown
                 *  in front of the `FileUploader` Control; Will
                 *  be shown if `showLabel` is set to `true`
                 * @public
                 */
                labelText: {
                    type: "string",
                    group: "Appearance",
                    defaultValue: null
                },
                /**
                 * @name ixult.ui.unified.FileUploader#uploadText
                 * @type {string}
                 * @description Text show on the `Upload` Button
                 *  that will be shown when `uploadOnChange`
                 *  is set to `false`
                 * @public
                 */
                uploadText: {
                    type: "string",
                    group: "Appearance",
                    defaultValue: ""
                },
                /**
                 * @name ixult.ui.unified.FileUploader#dragDrop
                 * @type {boolean}
                 * @default `false`
                 * @description Enabled this Control to support
                 *  Drag & Drop of files onto it; The area will
                 *  be marked accordingly; Default is `false`
                 * @public
                 */
                dragDrop: {
                    type: "boolean",
                    group: "Behavior",
                    defaultValue: false
                },
                /**
                 * @name ixult.ui.unified.FileUploader#parseOnUpload
                 * @type {boolean}
                 * @default `true`
                 * @description Parses an uploaded file right away
                 *  if the type can be recognized and a valid parser
                 *  for this exists: Currently supports `XLSX`
                 *  and `CSV` documents
                 * @public
                 */
                parseOnUpload: {
                    type: "boolean",
                    group: "Behavior",
                    defaultValue: true
                }
            },
            events: {
                // Events used in the `FileUploader` Control; passed onto
                // this Control to allow setting them into the `FileUploader`
                // that is placed inside this surrounding `FlexBox`
                change: {
                    parameters: {
                        newValue: {type: "string"},
                        files: {type: "object[]"}
                    }
                },
                typeMissmatch: {
                    parameters: {
                        fileName: {type: "string"},
                        fileType: {type: "string"},
                        mimeType: {type: "string"}
                    }
                },
                fileAllowed: {},
                fileEmpty: {
                    parameters: {
                        fileName: {type: "string"}
                    }
                },
                filenameLengthExceed: {
                    parameters: {
                        fileName: {type: "string"}
                    }
                },
                fileSizeExceed: {
                    parameters: {
                        fileName: {type: "string"},
                        fileSize: {type: "string"}
                    }
                },
                uploadStart: {
                    parameters: {
                        fileName: {type: "string"},
                        requestHeaders: {type: "object[]"}
                    }
                },
                uploadProgress: {
                    parameters: {
                        lengthComputable: {type: "boolean"},
                        loaded: {type: "float"},
                        total: {type: "float"},
                        fileName: {type: "string"},
                        requestHeaders: {type: "object[]"}
                    }
                },
                uploadComplete: {
                    parameters: {
                        fileName: {type: "string"},
                        response: {type: "string"},
                        readyStateXHR: {type: "string"},
                        status: {type: "string"},
                        responseRaw: {type: "string"},
                        headers: {type: "object"},
                        requestHeaders: {type: "object[]"}
                    }
                },
                uploadAborted: {
                    parameters: {
                        fileName: {type: "string"},
                        requestHeaders: {type: "object[]"}
                    }
                },
                beforeDialogOpen: {},
                afterDialogClose: {},
                // Custom events
                /**
                 * @name ixult.ui.unified.FileUploader#upload
                 * @type {sap.ui.base.Event}
                 * @description An event fired during an HTML `upload`
                 *  Event of a file, allowing to process the file
                 *  before an upload has been completed in the
                 *  `FileUploader`
                 * @param {map} mParameters
                 *  the paremeters passed onto this Event instance
                 * @param {object} mParameters.file
                 *  the file object that is uploaded, containing
                 *  information like the file name modified date
                 * @param {ProgessEvent} mParameters.event
                 *  an HTML ProgessEvent instance created when
                 *  reading the uploaded file
                 * @param {string} mParameters.result
                 *  the resulting file contents as string
                 * @param {object|null} mParameters.content
                 *  a content object that is created when parsing
                 *  the file, if `parseOnUpload` has been set
                 *  to `true`
                 * @public
                 */
                upload: {
                    parameters: {
                        file: {
                            type: "object"
                        },
                        event: {
                            type: "ProgressEvent"
                        },
                        result: {
                            type: "string"
                        },
                        content: {
                            type: "object"
                        }
                    }
                }
            }
        },

        // Attributes
        _oView: null,
        _oFileUploader: null,
        _aFileTypes: array([]),

        /**
         * @constructor
         * @summary Creates a new Control instance
         * @param {string} sId
         *  ID for the new control, generated automatically if no ID is given
         * @param {map|object} mSettings
         *  Initial settings for the new Control
         * @public
         * @hideconstructor
         */
        constructor: function (sId, mSettings) {
            FlexBox.prototype.constructor.apply(this, arguments);
        },

        /**
         * @summary Initializes the Control
         * @returns {void}
         * @protected
         */
        init: function () {
            FlexBox.prototype.init.apply(this, arguments);

            // Create an array containing matching file- and MIME types
            for (var sFileType in MyLibrary.FileType) {
                this._aFileTypes.push({
                    sFileType: MyLibrary.FileType[sFileType],
                    sMimeType: MyLibrary.MimeType[sFileType]
                })
            }

            // Set a model into this Control to
            // store and exchange data
            this.setModel(new JSONModel({
                Files: []
            }), "Data");
        },

        /**
         * @summary Handles the `beforeRendering` Event for the UI Control,
         *  that is executed when the DOM elements are created but not yet shown
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onBeforeRendering: function (oEvent) {
            // Return if the content has already been rendered
            if (this.getItems() && this.getItems().length > 0) {
                oEvent.preventDefault();
                return;
            }
            FlexBox.prototype.onBeforeRendering.apply(this, arguments);

            // Save a reference to the surrounding view
            if (!this._oView) {
                this._oView = Library.getViewFor(this);
            }

            // Load the texts into this Control
            this.setModel(new ResourceModel({
                bundle: Library.getResourceBundle()
            }), "messagebundle");

            var sIcon = this.getIcon() || "sap-icon://fluent/folder-open";
            this.setModel(new JSONModel({
                additionalData: this.getAdditionalData(),
                buttonOnly: this.getButtonOnly(),
                buttonText: this.getButtonText() ||
                    Library.getBundleText("fileuploader.button.choose"),
                directory: this.getDirectory(),
                enabled: this.getEnabled(),
                fileType: this.getFileType(),
                httpRequestMethod: this.getHttpRequestMethod(),
                icon: sIcon,
                iconOnly: this.getIconOnly(),
                iconFirst: this.getIconFirst(),
                iconHovered: this.getIconHovered() || sIcon,
                iconSelected: this.getIconSelected() || sIcon,
                maximumFilenameLength: this.getMaximumFilenameLength(),
                maximumFileSize: this.getMaximumFileSize(),
                mimeType: this.getMimeType(),
                name: this.getName(),
                placeholder: this.getPlaceholder(),
                sameFilenameAllowed: this.getSameFilenameAllowed(),
                sendXHR: this.getSendXHR(),
                style: this.getStyle(),
                uploadOnChange: this.getUploadOnChange(),
                uploadUrl: this.getUploadUrl(),
                useMultipart: this.getUseMultipart(),
                value: this.getValue(),
                valueState: this.getValueState(),
                valueStateText: this.getValueStateText(),
                width: this.getWidth(),
                height: this.getHeight(),
                dragDrop: this.getDragDrop(),
                dragActive: false,
                showLabel: this.getShowLabel(),
                labelText: this.getLabelText() ||
                    Library.getBundleText("fileuploader.button.choose"),
                uploadText: this.getUploadText() ||
                    Library.getBundleText("fileuploader.button.upload"),
                uploadEnabled: false
            }), "properties");

            // Load the Fragment and insert it into this Control
            Fragment.load({
                name: "ixult.ui.unified.FileUploader",
                type: "XML",
                controller: this
            }).then(function (oFragment) {
                this.removeAllItems();
                this.addItem(oFragment);

                // Setup the content, i.e. the `FileUploader` Control
                this.createContent(oFragment);

                // Initializes handler for the HTML rendering
                this.onRendering();
            }.bind(this));
        },

        /**
         * @summary Enables a MutationObserver to determine, when this
         *  Control has been fully rendered and added to the DOM
         * @description Used to ensure the correct initialization
         *  of the `Drag & Drop` functionality
         * @returns {void}
         * @protected
         */
        onRendering: function () {
            var oObserver = new MutationObserver(function (aMutations) {
                var oHTMLElement = $("#" + this.getId());
                if (oHTMLElement.length > 0 && document.contains(oHTMLElement[0])) {
                    if (this.getDragDrop()) {
                        this.createDragDrop({
                            HTMLElement: oHTMLElement
                        });
                    }
                    oObserver.disconnect();
                }
            }.bind(this));
            oObserver.observe(document, {
                attributes: false,
                childList: true,
                characterData: false,
                subtree: true
            });

            if (!assert(this._oFileUploader, "No FileUploader Control found!")) {
                return;
            }

            // Bind the properties that are bound to
            // this Control to the FileUploader instead
            var aFileUploaderProperties = array(Object.keys(
                this._oFileUploader.getMetadata().getAllProperties()
            ));
            array(Object.keys(this.mBindingInfos)).loop(function (sKey) {
                if (aFileUploaderProperties.contains(sKey)) {
                    this._oFileUploader.bindProperty(sKey, this.mBindingInfos[sKey]);
                }
            }.bind(this));
        },

        /**
         * @summary Handles the `afterRendering` Event for the UI Control,
         *  that is executed when the Control is completely rendered and shown
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onAfterRendering: function (oEvent) {
            // FlexBox.prototype.onAfterRendering.apply(this, arguments);
        },

        /**
         * @summary Creates the contents for this `FlexBox`
         * @param {sap.ui.core.Fragment} oFragment
         *  the `Fragment` Control containing the `FileUploader`
         * @returns {void}
         * @private
         */
        createContent: function (oFragment) {
            // Save the `FileUploader` instance
            this._oFileUploader = ixult.ui.core.Element.findElement({
                Parent: oFragment,
                Type: "sap.ui.unified.FileUploader"
            });
            if (!assert(this._oFileUploader, "No FileUploader Control found!")) {
                return;
            }

            // Save the chosen file to allow `uploadOnChange`
            // getting the file(s) when set to `true`
            this._oFileUploader.attachChange(this.onFileUploaderChange, this);

            // Pass all applicable events in this Control
            // down to the embedded `FileUploader` Control
            var mFileUploaderEvents =
                this._oFileUploader.getMetadata().getAllEvents();
            for (var sEventName in this.mEventRegistry) {
                // the Event Registry contains arrays with the respective Listeners
                var aRegisteredEvents = array(this.mEventRegistry[sEventName]);
                if (mFileUploaderEvents[sEventName]) {
                    // Attach each Listener to the `FileUploader`
                    aRegisteredEvents.loop(function (oRegEvent) {
                        // noinspection JSReferencingMutableVariableFromClosure
                        this._oFileUploader.attachEvent(
                            sEventName,
                            oRegEvent.oData,
                            oRegEvent.fFunction,
                            oRegEvent.oListener
                        )
                    }.bind(this));
                }
            }
        },

        /**
         * @summary Handles the `change` Event on the `FileUploader` Control
         *  Allows to upload and process a file with `Drag & Drop` support
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onFileUploaderChange: function (oEvent) {
            // Save the chosen file(s)
            var aFiles = oEvent.getParameter("files");
            this.setFiles(aFiles);

            // Activate the `upload` Button
            this.getModel("properties").setProperty("/uploadEnabled", true);

            // Read the file(s) on change
            if (this.getUploadOnChange()) {
                this.readFiles();
            }
        },

        /**
         * @summary Handles the `press` Event on the `Upload` Button
         *  that is displayed when `uploadOnChange` is set to `false`
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onUploadButtonPress: function (oEvent) {
            this.readFiles();
        },

        /**
         * @summary Sets up the `Drag & Drop` area and events for this Control
         * @param {map} mParameters
         *  a parameter map, containing the following attributes
         * @param {object} mParameters.HTMLElement
         *  the HTML Control for which to set up the `Drag & Drop` functionality
         * @returns {void}
         * @private
         */
        createDragDrop: function (mParameters) {
            var oHTMLElement = mParameters.HTMLElement;
            if (!oHTMLElement || oHTMLElement.length === 0) {
                return;
            }

            // Apply the HTML Drag & Drop functionality to the Control
            oHTMLElement.attr("draggable", true);
            oHTMLElement.attr("data-type", "");

            /**
             * @summary Handles the event triggered when
             *  something enters the drag area
             */
            oHTMLElement[0].ondragenter = function (oEvent) {
                oEvent.preventDefault();
                oEvent.dataTransfer.effectAllowed = "copyMove";

                // var sFileType = this.getFileIcon({
                //         sMimeType: oEvent.dataTransfer.items[0].type
                //     }),
                //     oDragImage = new Image();
                // oDragImage.src = LibraryHelper.register("ixult")
                //     .getResourcePath("/assets/icons/filetypes/" + sFileType + ".svg");
                // oEvent.dataTransfer.setDragImage(oDragImage, 10, 10);

                this.addStyleClass("ixultUiFileUploaderDragArea");

                this.getModel("properties").setProperty("/enabled", false);
                this.getModel("properties").setProperty("/dragActive", true);
            }.bind(this);

            /**
             * @summary Handles the event triggered when
             *  something leaves the drag area
             */
            oHTMLElement[0].ondragleave = function (oEvent) {
                oEvent.preventDefault();

                this.removeStyleClass("ixultUiFileUploaderDragArea");

                this.getModel("properties").setProperty("/enabled", true);
                this.getModel("properties").setProperty("/dragActive", false);
            }.bind(this);

            /**
             * @summary Handles the event triggered when
             *  something hovers over the drag area
             *  (having it entered already and not being dropped)
             */
            oHTMLElement[0].ondragover = function (oEvent) {
                oEvent.preventDefault();
                oEvent.dataTransfer.dropEffect = "move";

                this.getModel("properties").setProperty("/enabled", false);
                this.getModel("properties").setProperty("/dragActive", true);
            }.bind(this);

            /**
             * @summary Handles the event triggered when
             *  the drag operation has been completed
             */
            oHTMLElement[0].ondragend = function (oEvent) {
                oEvent.preventDefault();

                this.removeStyleClass("ixultUiFileUploaderDragArea");

                this.getModel("properties").setProperty("/enabled", true);
                this.getModel("properties").setProperty("/dragActive", false);
            }.bind(this);

            /**
             * @summary Handles the event triggered when
             *  the dragged element has been dropped onto the area
             */
            oHTMLElement[0].ondrop = function (oEvent) {
                oEvent.preventDefault();

                this.removeStyleClass("ixultUiFileUploaderDragArea");

                this.getModel("properties").setProperty("/enabled", true);
                this.getModel("properties").setProperty("/dragActive", false);

                this.handleOnDrop(oEvent);
            }.bind(this);
        },

        /**
         * @summary Handles the HTML `ondrop` Event
         *  for this Control's HTML Element
         * @param {DragEvent} oEvent
         *  the Drag & Drop event instance
         * @param {DataTransfer} oEvent.dataTransfer
         *  the DataTransfer instance containing the transfer
         *  information as well as the file contents
         * @returns {void}
         * @protected
         */
        handleOnDrop: function (oEvent) {
            if (!assert(oEvent.dataTransfer && oEvent.dataTransfer.files &&
                oEvent.dataTransfer.files.length > 0, "No files for import found.")) {
                return;
            }

            var sFileName = oEvent.dataTransfer.files[0].name;
            this.getModel("properties").setProperty("/value", sFileName);

            this._oFileUploader.fireChange({
                files: oEvent.dataTransfer.files
            });
        },

        /**
         * @summary Sets the given file(s) into this Control,
         *  e.g. when files have been dragged and dropped
         * @param {object[]} aFiles
         *  an array with the information about the selected file(s)
         * @returns {void}
         * @public
         */
        setFiles: function (aFiles) {
            this.getModel("Data").setProperty("/Files", aFiles);
        },

        /**
         * @summary Returns the uploaded files
         *  if multiple can be chosen
         * @returns {object[]}
         *  an array with information about
         *  the chosen and uploaded files
         * @public
         */
        getFiles: function () {
            return this.getModel("Data").getProperty("/Files");
        },

        /**
         *
         * @param mParameters
         */
        getFileIcon: function (mParameters) {
            mParameters = mParameters || {};
            var aTypeInfo = [];

            if (mParameters.sMimeType) {
                aTypeInfo = this._aFileTypes.grep(oType => {
                    return oType.sMimeType === mParameters.sMimeType
                }) || [];

            }
            return aTypeInfo && aTypeInfo.length > 0 ?
                aTypeInfo[0].sFileType.toLowerCase() : "unknown";
        },

        /**
         * @summary Reads the uploaded file(s); Fires the corresponding
         *  Control events to allow correct upload processing
         * @returns {void}
         * @public
         */
        readFiles: function () {
            Array.from(this.getFiles()).forEach(function (oFile) {
                // File read is progressing
                var fnProgress = function (oEvent) {
                    this.setBusy(true);
                    this._oFileUploader.fireUploadProgress({
                        file: oFile
                    });
                }.bind(this);

                // File has been loaded
                var fnLoad = function (oEvent) {
                    this.setBusy(true);
                    this.processFile({
                        oFile: oFile,
                        oResult: oEvent.target.result
                    }).then(function (oResult) {
                        this.fireUpload(object({
                            file: oFile,
                            event: oEvent,
                            result: oEvent.target.result
                        }).extend(oResult));
                        this.setBusy(false);
                    }.bind(this));
                }.bind(this);

                // File has finished loading
                var fnLoadEnd = function (oEvent) {
                    this.setBusy(false);
                    this._oFileUploader.fireUploadComplete({
                        file: oFile,
                        event: oEvent
                    });
                }.bind(this);

                // File upload was aborted
                var fnAbort = function (oEvent) {
                    this.setBusy(false);
                    this._oFileUploader.fireUploadAborted({
                        file: oFile,
                        event: oEvent
                    });
                }.bind(this);

                // Loading failed
                var fnError = function (oError) {
                    this.setBusy(false);
                    this._oFileUploader.fireUploadAborted({
                        file: oFile,
                        error: oError
                    });
                }.bind(this);

                // Create a `FileReader` or a `XMLHttpRequest`
                // depending on the settings
                var oReader = this.getSendXHR() ?
                    new XMLHttpRequest() : new FileReader();

                // The event handler are the same for both
                oReader.onprogress = fnProgress;
                oReader.onload = fnLoad;
                oReader.onloadend = fnLoadEnd;
                oReader.onabort = fnAbort;
                oReader.onerror = fnError;

                // Read the files as XML or raw contents
                if (this.getSendXHR()) {
                    var sPath = URL.createObjectURL(oFile)
                    oReader.open("GET", sPath, true);
                    if (oReader.overrideMimeType) {
                        oReader.overrideMimeType("text/xml");
                    }
                    var oFormData = new FormData();
                    oFormData.append("file", oFile)
                    oReader.send(oFormData);
                } else {
                    oReader.readAsBinaryString(oFile);
                }
            }.bind(this));
        },

        /**
         * @summary Post-Processes an uploaded file and allows
         *  for parsing its contents if the filetype can be determined
         *  and is supported
         * @param {map} mParameters
         *  a parameter map, containing the following attributes
         * @param {map} mParameters.oFile
         *  the file object used during file upload
         * @param {map} mParameters.oResult
         *  the target result data from the FileReader's `load` event
         * @returns {Promise}
         *  a Promise object with resolve and reject handler methods
         *  to asynchronously post process the result; contains a
         *  `then` function
         * @protected
         */
        processFile: function (mParameters) {
            var oDeferred = $.Deferred(),
                oResult = {
                    content: null,
                    document: null
                };

            // Return right away if the file shouldn't be processed
            if (!this.getParseOnUpload()) {
                oDeferred.resolve();
                return oDeferred.promise();
            }

            // Create an ExcelDocument from the uploaded file; Will
            // only be parsed, if the type is supported and recognized
            var oDocument = new ExcelDocument({
                oFile: mParameters.oFile,
                oData: mParameters.oResult
            });

            // Parse XLSX File
            if (oDocument.isXLSX()) {
                var oContent = oDocument.parseXlsx();
                setTimeout(function () {
                    // Delayed response to wait for the parser
                    oDeferred.resolve({
                        file: mParameters.oFile,
                        content: oContent,
                        document: oDocument
                    });
                }.bind(this), 2000);
            } else if (oDocument.isCSV()) {
                // Parse CSV Files
                debugger;
                // var oCSVParser = new SimpleExcel.Parser.CSV();
                // oCSVParser.loadFile(mParameters.oFile, function () {
                //     debugger;
                // });
                // $.csv.parsers
                oDeferred.resolve(oResult);
            } else {
                // Return with nothing as `content`
                // if the file type is not supported
                oDeferred.resolve();
            }

            // Return the Promise handler
            return oDeferred.promise();
        }

    });
});