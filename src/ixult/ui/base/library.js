sap.ui.define([
    "sap/ui/core/Core",
    "sap/ui/core/library"
], function (
    Core,
    Library
) {
    "use strict";

    /**
     * @namespace base
     * @memberOf ixult.ui
     *
     * @description UI5 Control related base objects
     *  and functions, containing e.g. an 'Error' class to
     *  allow enhanced processing of (UI) errors
     *
     * @public
     */
    return sap.ui.getCore().initLibrary({
        name: "ixult.ui.base",
        noLibraryCSS: true,
        dependencies: [
            "sap.ui.base",
            "sap.m",
        ],
        types: [],
        interfaces: [],
        controls: [],
        elements: [],
        version: "0.8.1"
    });

}, /* bExport= */ false);