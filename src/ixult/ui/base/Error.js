sap.ui.define([
    "ixult/base/assert",
    "ixult/base/util/array",
    "ixult/base/util/object",
    "ixult/base/util/LibraryHelper",
    "ixult/ui/core/theming/ThemeHelper",
    "sap/m/Button",
    "sap/m/Dialog",
    "sap/m/Image",
    "sap/m/MessageBox",
    "sap/ui/base/ManagedObject",
    "sap/ui/core/Fragment",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/odata/v2/ODataModel",
    "sap/ui/model/resource/ResourceModel"
], function (
    assert,
    array,
    object,
    LibraryHelper,
    ThemeHelper,
    Button,
    Dialog,
    Image,
    MessageBox,
    ManagedObject,
    Fragment,
    JSONModel,
    ODataModel,
    ResourceModel
) {
    "use strict";
    const Library = LibraryHelper.register("ixult/ui/base").loadStyleSheet();
    const sLocalHttpErrorDesc = "localHttpErrorDesc";

    /**
     * @function _getJqXHRResponseHeaders
     * @summary Parses a given jqXHR object to extract its
     *  header fields, and returns them as a map
     * @param {jqXHR} jqXHR
     *  the jqXHR object
     * @returns {map}
     *  the parsed header fields
     * @private
     * @internal
     */
    var _getJqXHRResponseHeaders = function (jqXHR) {
        var mHeaders = {};
        var aFields = [];

        jQuery.each(jqXHR.getAllResponseHeaders().split("\r\n"), function (iIndex, sHeader) {
            aFields = sHeader.split(": ");
            if (aFields.length === 2) {
                var sKey = aFields[0].trim();
                var sValue = aFields[1].trim();
                mHeaders[sKey] = sValue;
                // Try to convert date strings (to allow something like session timer checks)
                if (sKey.indexOf("date") !== -1) {
                    try {
                        mHeaders[sKey] = !isNaN(Date.parse(sValue)) ? Date(Date.parse(sValue)) : sValue;
                    } catch (oException) {
                        console.error("'" + aFields[1] + "' could not be parsed into a date.", oException);
                    }
                }
            }
        }.bind(this));

        return mHeaders;
    };

    /**
     * @function _formatJqXHRResponse
     * @summary Formats a given parameter map coming from a
     *  successful or failed jQuery ajax request
     * @param {map} mParameters
     *  the parameter map containing the following attributes
     * @param {string} mParameters.textStatus
     *  the ajax call status text
     * @param {jqXHR} mParameters.jqXHR
     *  the ajax call jqXHR object
     * @param {object} [mParameters.data]
     *  the ajax call data object if the call was successful
     * @param {string} [mParameters.errorThrown]
     *  the ajax call error text if the call failed
     * @param {string} [mParameters.username]
     *  an optional username
     * @returns {{
     *     jqXHR: object|Promise,
     *     oHeaders: object,
     *     oData: object,
     *     iStatus: int|Number,
     *     sStatusText: string,
     *     sStatusText: string,
     *     sUsername: string,
     *     sErrorThrown: string
     *  }}
     *  a formatted map containing the source attributes
     *  as well as processed data
     * @private
     * @internal
     */
    var _formatJqXHRResponse = function (mParameters) {
        if (!mParameters) {
            console.error("Error processing jqXHR response: No parameters found!");
            return null;
        }

        // Create the response object from every possible option
        var mResponse = {
            jqXHR: mParameters.jqXHR,
            oHeaders: mParameters.jqXHR ? _getJqXHRResponseHeaders(mParameters.jqXHR) : null,
            oData: mParameters.data,
            iStatus: mParameters.jqXHR ? mParameters.jqXHR.status : undefined,
            sStatusText: mParameters.textStatus,
            sUsername: mParameters.username,
            sErrorThrown: mParameters.errorThrown
        }

        // And then delete every attribute that has no values
        $.each(Object.keys(mResponse), function (iIndex, sKey) {
            if (mResponse[sKey] === null || mResponse[sKey] === undefined) {
                delete mResponse[sKey];
            }
        });

        return mResponse;
    }

    /**
     * @function _parseLocalHttpError
     * @summary Parses an HTTP error object saved in the
     *  local storage object 'localHttpErrorDesc'
     * @param {map} mParameters
     *  a parameter map containing the following attributes:
     * @param {object} mParameters.oLocalHttpErrorDesc
     *  the LocalStorage object containing the Error details
     * @param {object} mParameters.oError
     *  the Error object
     * @param {object} mParameters.oParams
     *  additional Error parameters
     * @returns {string}
     *  the Error message
     * @private
     * @internal
     */
    var _parseLocalHttpError = function (mParameters) {
        if (!assert([
            mParameters && mParameters.oLocalHttpErrorDesc && mParameters.oLocalHttpErrorDesc.length > 0,
            "'" + sLocalHttpErrorDesc + "' not found.",
            mParameters && mParameters.oError,
            "Error object not found or not specified.",
            mParameters && mParameters.oParams,
            "Error parameters not found or not specified."
        ])) {
            return "";
        }

        var oLocalHttpErrorDesc = mParameters.oLocalHttpErrorDesc,
            oError = mParameters.oError,
            oParams = mParameters.oParams,
            sInnerMessage = "";
        for (var i = 0; i < oLocalHttpErrorDesc.length; i++) {
            if (parseInt(oLocalHttpErrorDesc[i].Msgnr) === oParams.statusCode) {
                sInnerMessage = parseInt(oLocalHttpErrorDesc[i].Msgnr) + " : " + oLocalHttpErrorDesc[i].Errmsg;
            }
            // Inner error found
            if (oError && oError.response && oError.response.body) {
                var oErrorJSON = JSON.parse(oError.response.body);
                if (oErrorJSON && oErrorJSON.error) {
                    sInnerMessage = oErrorJSON.error.message.value + " (" + oErrorJSON.error.code + ")";
                    // Inner error replaced by customizing
                    if (oErrorJSON.error.code.trim() === oLocalHttpErrorDesc[i].ErrorCode.trim()) {
                        sInnerMessage = oLocalHttpErrorDesc[i].Errmsg + " (" + oLocalHttpErrorDesc[i].ErrorCode + ")";
                        break;
                    }
                }
            } else {
                sInnerMessage = "Error parsing '" + sLocalHttpErrorDesc + "' object";
            }
        }

        return sInnerMessage;
    }

    /**
     * @class Error
     * @memberOf ixult.ui.base
     * @extends sap.ui.base.ManagedObject
     *
     * @classdesc Extended Error class to provide
     *  additional information and utility functions
     *  for an Error object
     *
     * @param {Error|jQuery.Error|object|string} oError
     *  the error object containing attributes below;
     *  can be of various instances
     *  a parameter map with addition information,
     *  or a simple error message string
     * @param {int|Number|string} [oError.status]
     *  The Error status code; alternative attribute depending
     *  on the Error object passed to this class
     * @param {int|Number|string} [oError.statusCode]
     *  The Error status code; alternative attribute depending
     *  on the Error object passed to this class
     * @param {int|Number} [oError.iStatus]
     *  The Error status code; alternative attribute depending
     *  on the Error object passed to this class
     * @param {map|string} [oError.message]
     *  The message text; alternative attribute depending
     *  on the Error object passed to this class
     * @param {map|string} [oError.sMessageText]
     *  The message text; alternative attribute depending
     *  on the Error object passed to this class
     * @param {...*} [arguments]
     *  optional arguments to be passed on to the function
     *
     * @public
     */
    return ManagedObject.extend("ixult.ui.base.Error", /** @lends ixult.ui.base.Error.prototype */ {
        _oInternalError: null,
        _aErrorArguments: [],
        _oDialog: null,
        _oJSONViewer: null,

        /**
         * @summary Instantiates the Error handler instance
         * @param {Error|XMLHttpRequest|jQuery.Error|object|string} oError
         *  the error object containing attributes below;
         *  can be of various Error instances, a parameter
         *  map with addition information or a simple error
         *  message string
         * @param {int|Number|string} [oError.status]
         *  The Error status code; alternative attribute depending
         *  on the Error object passed to this class
         * @param {int|Number|string} [oError.statusCode]
         *  The Error status code; alternative attribute depending
         *  on the Error object passed to this class
         * @param {int|Number} [oError.iStatus]
         *  The Error status code; alternative attribute depending
         *  on the Error object passed to this class
         * @param {map|string} [oError.message]
         *  The message text; alternative attribute depending
         *  on the Error object passed to this class
         * @param {map|string} [oError.sMessageText]
         *  The message text; alternative attribute depending
         *  on the Error object passed to this class
         * @param {...*} [arguments]
         *  optional arguments to be passed on to the function
         * @public
         * @hideconstructor
         */
        constructor: function (oError) {
            if (!assert(oError, "No Error object or message found.")) {
                return;
            }

            // Return a standard Error object
            // if it was given as a message
            if (typeof oError === "string") {
                this.message = oError.toString().trim();
                return new Error({
                    message: this.message
                });
            }

            // Save the actual Error object and arguments if given
            this._oInternalError = oError;
            if (arguments.length > 1) {
                this._aErrorArguments = Array.prototype.slice.call(arguments, 1);
            }

            // Evaluate the Error data set them
            // as attributes into this Error instance
            this._createErrorAttributes(oError);
        },

        /**
         * @summary Adds the given map of attributes
         *  directly into this error instance to allow direct
         *  access thereby ensuring to handle legacy
         *  compatibility with standard errors
         * @param {Error|XMLHttpRequest|jQuery.Error|object|string} oError
         *  the error object containing attributes below;
         *  can be of various Error instances, a parameter
         *  map with addition information or a simple error
         *  message string
         * @returns {void}
         * @private
         */
        _createErrorAttributes: function (oError) {
            // Add the attributes directly to this error class instance
            Object.keys(oError).forEach(function (sKey) {
                if (sKey === "results" && Array.isArray(oError[sKey])) {
                    this.results = this._parseErrorResults(oError[sKey]);
                } else if (!this.hasOwnProperty(sKey)) {
                    this[sKey] = oError[sKey];
                }
            }.bind(this));
            this.status = oError.iStatus || oError.statusCode || oError.status;
            this.message = oError.sMessageText || oError.message;
        },

        /**
         * @summary Parses a given result array with messages
         *  coming from e.g. OData services, and thereby strips
         *  the result objects down to simple value maps
         * @param {object[]} aResults
         *  a result array with error messages
         * @returns {object[]}
         *  the parsed results array
         * @private
         */
        _parseErrorResults: function (aResults) {
            var aParsedResults = [],
                oParseResult = null;
            aResults.forEach(function (oResult) {
                oParseResult = object($.extend({}, oResult)).toMap();
                delete oParseResult.processor;
                aParsedResults.push(oParseResult);
            });
            return aParsedResults;
        },

        /**
         * @summary Returns an array of result messages
         * @description This function is intended to be used with the
         *  `MessageModel` attached to an `ODataModel` (v2) and
         *  filled during OData service responses; The messages can
         *  be extracted by using
         *  ```javascript
         *  sap.ui.getCore().getMessageManager().getMessageModel().getData()
         *  ```
         * @see {@link https://ui5.sap.com/sdk/#/api/sap.ui.model.message.MessageModel sap.ui.model.message.MessageModel}
         * @returns {object[]}
         *  the result messages
         */
        getResults: function () {
            return this.results;
        },

        /**
         * @summary Parses a given Error object and returns
         *  detailed information for it
         * @returns {{
         *      message: string,
         *      innerMessage: string,
         *      statusText: string,
         *      statusCode: number,
         *      json: string
         *  }}
         *  Map with error information
         * @public
         */
        getDetails: function () {
            var oError = object(this._oInternalError),
                aArguments = this._aErrorArguments,
                mErrorData = {
                    message: "",
                    innerMessage: "",
                    statusCode: 0,
                    statusText: ""
                };

            // Extract an 'inner error' object and move the attributes to the 'outer error'
            if (oError.oError && oError.oError instanceof "object") {
                $.each(Object.keys(oError.oError), function (iIndex, sKey) {
                    oError[sKey] = oError.oError[sKey];
                });
                delete oError.oError;
            }

            // Fill Error information depending on the given Error object
            if (oError.results && Array.isArray(oError.results) && oError.results.length > 0) {
                var mResult = oError.results[0],
                    aResultTargets = array(mResult.aTargets);

                // Error details map
                mErrorData = {
                    message: mResult.message,
                    innerMessage: "",
                    statusCode: mResult.technicalDetails.statusCode,
                    statusText: ""
                }

                // Get detail information from the result target array supplied with the error
                Array.from(aResultTargets).forEach(oResult => {
                    mErrorData.statusText += typeof oResult === "object" ?
                        Array.from(oResult).toString() + "\r\n" : oResult.toString();
                });

                try {
                    mErrorData.json = JSON.stringify(Object.fromEntries(object(mResult).toMap()));
                } catch (oException) {
                    console.warn("Parsing of Error information failed.", oException);
                    delete mErrorData.json;
                }
            } else if (oError.getParameters && typeof oError.getParameters === "function") {
                // Parse error class instances
                mErrorData = oError.getParameters();
            } else if (oError.response) {
                // Parse OData service responses
                mErrorData = {
                    innerMessage: oError.message || "",
                    statusCode: oError.getNestedProperty(oError, "response.statusCode") || -1,
                    statusText: oError.getNestedProperty(oError, "response.statusText") || "Error"
                }
            } else if (oError.abort && oError.getAllResponseHeaders) {
                // Parse jqXHR (XMLHttpRequest) objects returned from jQuery.ajax requests
                var sTextStatus = "",
                    sErrorThrown = "Error";
                // Try to the additional arguments if
                // they were given with the response
                if (aArguments && aArguments.length > 0) {
                    sTextStatus = aArguments[0] || "";
                    sErrorThrown = aArguments[1] || "";
                }
                // Extract the data from the jqXHR object
                var mErrorResponse = _formatJqXHRResponse({
                    jqXHR: jqXHR,
                    textStatus: sTextStatus,
                    errorThrown: sErrorThrown
                });
                mErrorData = {
                    innerMessage: mErrorResponse.sErrorThrown,
                    statusCode: mErrorResponse.iStatus,
                    statusText: mErrorResponse.sStatusText
                };
            } else if (localStorage && localStorage.getItem(sLocalHttpErrorDesc)) {
                // parse an HTTP error if set into the local storage
                mErrorData.innerMessage = _parseLocalHttpError({
                    oLocalHttpErrorDesc: localStorage.getItem(sLocalHttpErrorDesc),
                    oError: oError,
                    oParams: mErrorData
                });
            } else if (oError.checkNestedProperty("response.body")) {
                try {
                    mErrorData.json = JSON.stringify(oError.valueOf().response.body);
                    if (mErrorData.json && mErrorData.json.error) {
                        mErrorData.innerMessage =
                            oError.getNestedProperty("error.message.value") || "Error" + " (" +
                            oError.getNestedProperty("error.code") || "-1" + ")";
                    }
                } catch (oException) {
                    console.warn("Parsing of Error information failed.", oException);
                    mErrorData.innerMessage = oError.getNestedProperty("response.body") || "Error";
                }
            } else {
                // Try to get everything else
                mErrorData = {
                    innerMessage: oError.sErrorThrown || oError.responseText || "",
                    statusCode: oError.iStatus || oError.statusCode || -1,
                    statusText: oError.sStatus || oError.statusText || ""
                };
            }
            mErrorData.message = mErrorData.message || oError.sMessageText || oError.message || "Error";

            return mErrorData;
        },

        /**
         * @summary Returns the error message
         *  Convenience method, if not all details are needed
         * @returns {string}
         *  the error message
         * @public
         */
        getMessage: function () {
            const mDetails = this.getDetails();
            return mDetails.message;
        },

        /**
         * @summary Shows an error message dialog for a failed OData service request
         * @param {map} mParameters
         *  a parameter map with the following attributes:
         * @param {string} [mParameters.sStyleClass=sapUiSizeCompact]
         *  CSS style class to apply to the MessageBox dialogs
         * @param {boolean} [mParameters.showDetails=false]
         *  shows error details if true; false by default
         * @param {boolean} [mParameters.sendToSupport=false]
         *  shows a "send to support" message if true; action can be configured
         *  using the `controller` parameter either providing a `sap.ui.core.mvc.Controller`
         *  instance or an object with the functions listed below
         * @param {sap.ui.core.mvc.Controller|ixult.ui.core.mvc.Controller|{
         *      onBeforeOpen: function,
         *      onAfterOpen: function,
         *      onCloseButtonPress: function,
         *      onSendButtonPress: function
         *  }} [mParameters.controller]
         *  the controller instance or an object with functions that
         *  can be executed on the Error Dialog
         * @returns {Promise}
         *  a Promise instance that can be evaluated and handled
         *  in the calling instance using 'then' and 'fail' methods
         * @public
         */
        showDialog: function (mParameters) {
            mParameters = mParameters || {};
            var oDeferred = $.Deferred();

            // Create and show the dialog
            this._createDialog({
                showDetails: mParameters.showDetails !== undefined ?
                    mParameters.showDetails : false,
                sendToSupport: mParameters.sendToSupport !== undefined ?
                    mParameters.sendToSupport : false,
                controller: mParameters.controller
            }).then(function (oDialog) {
                oDialog.open();
                oDeferred.resolve(oDialog);
            }.bind(this));

            return oDeferred.promise();
        },

        /**
         * @summary Creates and error dialog
         * @param {map} mParameters
         *  a parameter map containing the following attributes:
         * @param {boolean} [mParameters.showDetails]
         *  shows error details if true; false by default
         * @param {boolean} [mParameters.sendToSupport]
         *  shows a "send to support" message if true; if the 'SupportServiceUrl'
         *  and 'ODataEntityName' parameters are set it provides a button for
         *  sending a support email (experimental)
         * @param {sap.ui.core.mvc.Controller|ixult.ui.core.mvc.Controller|{
         *      onBeforeOpen: function,
         *      onAfterOpen: function,
         *      onCloseButtonPress: function,
         *      onSendButtonPress: function
         *  }} [mParameters.controller]
         *  the controller instance or an object with functions that
         *  can be executed on the Error Dialog
         * @returns {Promise}
         *  a Promise instance that can be evaluated and handled
         *  in the calling instance using 'then' and 'fail' methods
         * @private
         */
        _createDialog: function (mParameters) {
            var oDeferred = $.Deferred(),
                mErrorData = this.getDetails() || {};

            // Set up the Dialog data
            var _setupErrorDialog = function () {
                this._oDialog.setModel(new JSONModel(object(mErrorData).extend({
                    sendToSupport: mParameters.sendToSupport,
                    showDetails: !!(mParameters.showDetails && mErrorData.json),
                })), "PopupData");

                if (mErrorData.json) {
                    this._oDialog.getModel("PopupData").setData({
                        json: mErrorData.json
                    }, true);
                }

                // Set the texts for the Popover
                this._oDialog.setModel(new ResourceModel({
                    bundle: Library.getResourceBundle()
                }), "messagebundle");

                try {
                    // Try to apply the View's theme to the Dialog
                    var oView = sap.ui.getCore().byId($("[class*='sapUiView']")[0].id);
                    ThemeHelper.applyContentDensityClass(this, oView, this._oDialog);
                } catch (oException) {
                    console.error("Error while applying content density class.");
                }
            }.bind(this);

            // Create or retrieve the Column Header Popover Control
            if (!this._oDialog) {
                Fragment.load({
                    name: "ixult.ui.base.ErrorDialog",
                    type: "XML",
                    controller: {
                        onBeforeOpen: this.onDialogBeforeOpen.bind(this, mParameters),
                        onAfterOpen: this.onDialogAfterOpen.bind(this, mParameters),
                        onSendButtonPress: this.onDialogSendButtonPress.bind(this, mParameters),
                        onCloseButtonPress: this.onDialogCloseButtonPress.bind(this, mParameters)
                    }
                }).then(function (oFragment) {
                    this._oDialog = oFragment;
                    _setupErrorDialog();
                    oDeferred.resolve(this._oDialog);
                }.bind(this));
            } else {
                _setupErrorDialog();
                oDeferred.resolve(this._oDialog);
            }

            return oDeferred.promise();
        },

        /**
         * @summary Shows a 'hacker' error dialog with a nice Meme image and
         *  the bahaviour of blocking the application completely, so that no
         *  further inpout
         * @param {map} mParameters
         *  a parameter map containing the following attributes:
         * @param {sap.ui.core.mvc.Controller|ixult.ui.core.mvc.Controller|{
         *      onCloseButtonPress: function
         *  }} [mParameters.controller]
         *  the controller instance or an object with functions that
         *  can be executed on the Error Dialog
         * @returns {void}
         * @public
         */
        showHackerError: function (mParameters) {
            var sErrorImagePath = Library.getManifestEntry("/sap.ui5/config/hackerErrorImage"),
                sErrorImage = LibraryHelper.register("ixult").getResourcePath(sErrorImagePath);
            new Dialog({
                showHeader: false,
                content: [new Image({
                    src: sErrorImage,
                    press: function (oEvent) {
                        if (mParameters.controller && mParameters.controller.onCloseButtonPress) {
                            mParameters.controller.onCloseButtonPress(oEvent);
                        }
                        setTimeout(function () {
                            window.close();
                        }, 1000);
                    }.bind(this)
                })]
            }).open();
        },

        /**
         * @summary Handles the `beforeOpen` Event in the Error Dialog
         * @param {map} mParameters
         *  a parameter map containing the following attributes:
         * @param {sap.ui.core.mvc.Controller|ixult.ui.core.mvc.Controller|{
         *      onCloseButtonPress: function
         *  }} [mParameters.controller]
         *  the controller instance or an object with functions that
         *  can be executed on the Error Dialog
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onDialogBeforeOpen: function (mParameters, oEvent) {
            if (mParameters.controller && mParameters.controller.onBeforeOpen) {
                mParameters.controller.onBeforeOpen(oEvent);
            }
        },

        /**
         * @summary Handles the `afterOpen` Event in the Error Dialog
         * @param {map} mParameters
         *  a parameter map containing the following attributes:
         * @param {sap.ui.core.mvc.Controller|ixult.ui.core.mvc.Controller|{
         *      onCloseButtonPress: function
         *  }} [mParameters.controller]
         *  the controller instance or an object with functions that
         *  can be executed on the Error Dialog
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onDialogAfterOpen: function (mParameters, oEvent) {
            if (mParameters.controller && mParameters.controller.onAfterOpen) {
                mParameters.controller.onAfterOpen(oEvent);
            }
        },

        /**
         * @summary Handles the `press` Event on
         *  the 'send to support' Button in the Error Dialog
         * @param {map} mParameters
         *  a parameter map containing the following attributes:
         * @param {sap.ui.core.mvc.Controller|ixult.ui.core.mvc.Controller|{
         *      onCloseButtonPress: function
         *  }} [mParameters.controller]
         *  the controller instance or an object with functions that
         *  can be executed on the Error Dialog
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onDialogSendButtonPress: function (mParameters, oEvent) {
            if (mParameters.controller && mParameters.controller.onSendButtonPress) {
                mParameters.controller.onSendButtonPress(oEvent, this._oDialog);
            }
        },

        /**
         * @summary Handles the `press` Event on
         *  the 'close' Button in the Error Dialog
         * @param {map} mParameters
         *  a parameter map containing the following attributes:
         * @param {sap.ui.core.mvc.Controller|ixult.ui.core.mvc.Controller|{
         *      onCloseButtonPress: function
         *  }} [mParameters.controller]
         *  the controller instance or an object with functions that
         *  can be executed on the Error Dialog
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onDialogCloseButtonPress: function (mParameters, oEvent) {
            if (mParameters.controller && mParameters.controller.onCloseButtonPress) {
                mParameters.controller.onCloseButtonPress(oEvent, this._oDialog);
            }
            this._oDialog.close();
        }


    });
});