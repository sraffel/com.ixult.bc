sap.ui.define([
    "ixult/ui/base/Error",
    "sap/ui/base/ManagedObject",
    "sap/ui/util/Storage"
], function (
    Error,
    ManagedObject,
    Storage
) {
    "use strict";

    /**
     * @summary the real local storage object
     * @type {localStorage}
     * @private
     * @internal
     */
    var _oStorage = null;

    /**
     * @summary Creates a new local storage
     * @param {localStorage} oStorage - the storage
     * @private
     * @internal
     */
    var _setStorage = function (oStorage) {
        _oStorage = oStorage;
    }

    /**
     * @summary Returns the local storage
     * @returns {localStorage} - the storage
     * @private
     * @internal
     */
    var _getStorage = function () {
        return _oStorage;
    }

    /**
     * @summary Handles an exception thrown by a try catch block
     * @param {object} oError
     *  the error object
     * @returns {void}
     * @private
     * @internal
     */
    var _handleError = function (oError) {
        console.error("Error in LocalStorage: " + new Error(oError).getMessage(), oError);
    };

    /**
     * @class LocalStorage
     * @memberOf ixult.ui.util
     * @classdesc LocalStorage handler class
     * @see {@link https://ui5.sap.com/sdk/#/api/module:sap/ui/util/Storage sap.ui.util.Storage}
     * @see {@link https://ui5.sap.com/sdk/#/api/module:sap/ui/util/Storage.Type sap.ui.util.Storage.Type}
     * @summary Creates the LocalStorage handler instance
     * @param {map} mParameters
     *  a parameter map containing the following attributes
     * @param {string} mParameters.name
     *  the local storage name
     * @param {ixult.ui.util.LocalStorage.Type} [mParameters.type]
     *  the local storage type; e.g. 'local' or 'session', 'local' by default
     * @public
     */
    var oLocalStorage = ManagedObject.extend("ixult.ui.util.LocalStorage", /** @lends ixult.ui.util.LocalStorage.prototype */ {

        // Metadata for this class; Used to define custom properties
        metadata: {
            properties: {
                /**
                 * @name ixult.ui.util.LocalStorage#name
                 * @description the local storage's name
                 * @type {string}
                 * @default 'ixult.ui.util.LocalStorage'
                 */
                name: {
                    type: "string",
                    defaultValue: "ixult.ui.util.LocalStorage"
                },
                /**
                 * @name ixult.ui.util.LocalStorage#type
                 * @description The scope of the local storage; can be 'local' or 'session'
                 * @see ixult.ui.util.LocalStorage#Type
                 * @type {string}
                 * @default 'local'
                 */
                type: {
                    type: "string",
                    defaultValue: "local"
                }
            }
        },

        /**
         * @summary Creates the LocalStorage handler instance
         * @param {map} mParameters
         *  a parameter map containing the following attributes
         * @param {string} mParameters.name
         *  the local storage name
         * @param {ixult.ui.util.LocalStorage.Type} [mParameters.type]
         *  the local storage type; e.g. 'local' or 'session', 'local' by default
         * @public
         * @hideconstructor
         */
        constructor: function (mParameters) {
            ManagedObject.prototype.constructor.apply(this);

            // Populate the properties inside the metadata
            if (mParameters && mParameters.name) {
                this.setName(mParameters.name);
            }
            if (mParameters && mParameters.type) {
                this.setType(mParameters.type);
            }

            // Create the storage
            _setStorage(new Storage(this.getType(), this.getName()));
        }

    });

    /**
     * @description Enumeration of possible storage types;
     *  Convenience map with values from {sap.ui.util.Storage.Type}
     * @enum {string}
     * @memberOf ixult.ui.util.LocalStorage.LocalStorage
     * @see {sap.ui.util.Storage.Type}
     * @public
     * @static
     */
    oLocalStorage.Type = {
        Local: "local",
        Session: "session"
    }

    /**
     * @summary Retrieves data from the local storage for the given key
     * @function get
     * @memberOf ixult.ui.util.LocalStorage.prototype
     * @param {string} sKey
     *  the key, for which the data should be retrieved
     * @returns {object}
     *  the OData object if found, null otherwise
     * @public
     */
    oLocalStorage.prototype.get = function (sKey) {
        try {
            var vData = _getStorage().get(sKey);
            return typeof vData === "string" && vData.indexOf("{") === 0 && vData.indexOf("}") === vData.length - 1 ?
                JSON.parse(vData) : vData;
        } catch (oException) {
            _handleError(oException);
            return null;
        }
    }

    /**
     * @summary Stores data values or objects into the local storage
     * @function set
     * @memberOf ixult.ui.util.LocalStorage.prototype
     * @param {string} sKey
     *  the key, for which the data should be set
     * @param {object|string|any} vData
     *  the data object or scalar value (string, integer, etc.)
     *  that should be put into the storage
     * @returns {boolean}
     *  true, if the data could be saved, false otherwise
     * @public
     */
    oLocalStorage.prototype.set = function (sKey, vData) {
        try {
            var sData = typeof vData === "object" ?
                JSON.stringify(vData) :
                vData;
            _getStorage().put(sKey, sData);

            return true;
        } catch (oException) {
            _handleError(oException);
            return false;
        }
    }

    /**
     * @summary Stores local storage compliant data into the local storage
     * @function put
     * @memberOf ixult.ui.util.LocalStorage.prototype
     * @param {string} sKey
     *  the key, for which the data should be set
     * @param {object|string|any} vData
     *  the data string or scalar value (string, integer, etc.)
     *  that should be put into the storage
     * @returns {boolean}
     *  true, if the data could be saved, false otherwise
     * @public
     */
    oLocalStorage.prototype.put = function (sKey, vData) {
        this.set(sKey, vData);
    }

    /**
     * @summary Removes data from the local storage for the given key (if found)
     * @function remove
     * @memberOf ixult.ui.util.LocalStorage.prototype
     * @param {string} sKey
     *  the key, for which the data should be removed
     * @returns {boolean}
     *  true, if the removal was successful, false otherwise
     * @public
     */
    oLocalStorage.prototype.remove = function (sKey) {
        try {
            return _getStorage().remove(sKey);
        } catch (oException) {
            _handleError(oException);
            return false;
        }
    }

    /**
     * @summary Clears the local storage
     * @function clear
     * @memberOf ixult.ui.util.LocalStorage.prototype
     * @returns {void}
     * @public
     */
    oLocalStorage.prototype.clear = function () {
        try {
            _getStorage().removeAll();
        } catch (oException) {
            _handleError(oException);
        }
    }


    return oLocalStorage;
});