sap.ui.define([
    "sap/ui/core/Core",
    "sap/ui/core/library"
], function (
    Core,
    Library
) {
    "use strict";

    /**
     * @namespace util
     * @memberOf ixult.ui
     *
     * @description Utility library for UI related classes and functions
     *
     * @public
     */
    sap.ui.getCore().initLibrary({
        name: "ixult.ui.util",
        noLibraryCSS: true,
        dependencies: [
            "sap.ui.base",
            "sap.ui.core",
            "sap.ui.util",
            "sap.m"
        ],
        types: [],
        interfaces: [],
        controls: [],
        elements: [],
        version: "0.8.2"
    });

    var thisLib = ixult.ui.util;

    /**
     * @name ixult.ui.util.LocalStorage#Type
     * @type {{
     *     Local: string,
     *     Session: string
     * }}
     * @summary Enumeration of possible storage types;
     *  Convenience map with values from {sap.ui.util.Storage.Type}
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.ui.util.Storage.Type sap.ui.util.Storage.Type}
     * @public
     * @static
     */
    thisLib.LocalStorage.Type = {
        Local: "local",
        Session: "session"
    }


    return thisLib;

}, /* bExport= */ false);