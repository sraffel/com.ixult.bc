sap.ui.define([
    "sap/m/Dialog",
    "sap/m/Image",
    "sap/m/MessageBox",
    "sap/ui/core/Fragment",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/odata/v2/ODataModel",
    "sap/ui/model/resource/ResourceModel",
    "ixult/base/assert",
    "ixult/base/util/LibraryHelper",
    "ixult/base/util/ObjectHelper"
], function (
    Dialog,
    Image,
    MessageBox,
    Fragment,
    JSONModel,
    ODataModel,
    ResourceModel,
    assert,
    LibraryHelper,
    ObjectHelper
) {
    "use strict"
    const Library = LibraryHelper.register("ixult/ui/util").loadStyleSheet();
    const sLocalHttpErrorDesc = "localHttpErrorDesc";

    /**
     * @summary Error Dialog instance
     * @type {sap.m.Dialog}
     * @private
     * @internal
     */
    var _oDialog = null;

    /**
     * @summary JSONViewer instance
     * @type {object}
     * @private
     * @internal
     */
    var _oJSONViewer = null;

    /**
     * @summary Parses an HTTP error object saved in the
     *  local storage object 'localHttpErrorDesc'
     * @param {map} mParameters
     *  a parameter map containing the following attributes:
     * @param {object} mParameters.oLocalHttpErrorDesc
     *  the LocalStorage object containing the Error details
     * @param {object} mParameters.oError
     *  the Error object
     * @param {object} mParameters.oParams
     *  additional Error parameters
     * @returns {string}
     *  the Error message
     * @private
     * @internal
     */
    var _parseLocalHttpError = function (mParameters) {
        if (!assert([
            mParameters && mParameters.oLocalHttpErrorDesc && mParameters.oLocalHttpErrorDesc.length > 0,
            "'" + sLocalHttpErrorDesc + "' not found.",
            mParameters && mParameters.oError,
            "Error object not found or not specified.",
            mParameters && mParameters.oParams,
            "Error parameters not found or not specified."
        ])) {
            return "";
        }

        var oLocalHttpErrorDesc = mParameters.oLocalHttpErrorDesc,
            oError = mParameters.oError,
            oParams = mParameters.oParams,
            sInnerMessage = "";
        for (var i = 0; i < oLocalHttpErrorDesc.length; i++) {
            if (parseInt(oLocalHttpErrorDesc[i].Msgnr) === oParams.statusCode) {
                sInnerMessage = parseInt(oLocalHttpErrorDesc[i].Msgnr) + " : " + oLocalHttpErrorDesc[i].Errmsg;
            }
            // Inner error found
            if (oError && oError.response && oError.response.body) {
                var oErrorJSON = JSON.parse(oError.response.body);
                if (oErrorJSON && oErrorJSON.error) {
                    sInnerMessage = oErrorJSON.error.message.value + " (" + oErrorJSON.error.code + ")";
                    // Inner error replaced by customizing
                    if (oErrorJSON.error.code.trim() === oLocalHttpErrorDesc[i].ErrorCode.trim()) {
                        sInnerMessage = oLocalHttpErrorDesc[i].Errmsg + " (" + oLocalHttpErrorDesc[i].ErrorCode + ")";
                        break;
                    }
                }
            } else {
                sInnerMessage = "Error parsing '" + sLocalHttpErrorDesc + "' object";
            }
        }

        return sInnerMessage;
    }

    /**
     * @namespace ErrorHelper
     * @memberOf ixult.ui.util
     * @summary Static helper for Error evaluation and display
     * @public
     * @static
     * @deprecated since 0.7.4; use ixult.ui.base.Error instead
     */
    var oErrorHelper = {};

    /**
     * @summary Parses a given Error object and returns
     *  detailed information for it
     * @param {object} oError
     *  the Error object
     * @returns {{
     *      message: string,
     *      innerMessage: string,
     *      statusText: string,
     *      statusCode: number
     *  }}
     *  Map with error information
     * @public
     * @static
     */
    oErrorHelper.getErrorDetails = function (oError) {
        var mErrorData = {
            message: "",
            innerMessage: "",
            statusCode: 0,
            statusText: ""
        };

        // Extract an 'inner error' object and move the attributes to the 'outer error'
        if (oError.oError) {
            $.each(Object.keys(oError.oError), function (iIndex, sKey) {
                oError[sKey] = oError.oError[sKey];
            });
            delete oError.oError;
        }

        // Fill Error information depending on the given Error object
        if (oError && oError.getParameters) {
            mErrorData = oError.getParameters();
        } else if (oError.response) {
            mErrorData = {
                innerMessage: oError.message || "",
                statusCode: ObjectHelper.getNestedProperty(oError, "response.statusCode") || -1,
                statusText: ObjectHelper.getNestedProperty(oError, "response.statusText") || "Error"
            }
        } else {
            mErrorData = {
                innerMessage: oError.sErrorThrown || oError.responseText || "",
                statusCode: oError.iStatus || oError.statusCode || -1,
                statusText: oError.sStatus || oError.statusText || ""
            };
        }
        mErrorData.message = mErrorData.message || oError.sMessageText || oError.message || "Error";

        // Get inner error message and/or body
        if (mErrorData.statusText && mErrorData.statusCode !== 0) {
            mErrorData.innerMessage = mErrorData.statusText + " / " + mErrorData.statusCode;
        } else if (localStorage && localStorage.getItem(sLocalHttpErrorDesc)) {
            // parse an HTTP error if set into the local storage
            mErrorData.innerMessage = _parseLocalHttpError({
                oLocalHttpErrorDesc: localStorage.getItem(sLocalHttpErrorDesc),
                oError: oError,
                oParams: mErrorData
            });
        } else if (ObjectHelper.checkNestedProperty(oError, "response.body")) {
            try {
                var oErrorJSON = JSON.parse(oError.response.body);
                if (oErrorJSON && oErrorJSON.error) {
                    mErrorData.innerMessage =
                        ObjectHelper.getNestedProperty(oError, "error.message.value") || "Error" + " (" +
                        ObjectHelper.getNestedProperty(oError, "error.code") || "-1" + ")";
                }
            } catch (oException) {
                console.warn("Parsing of Error information failed.", oException);
                mErrorData.innerMessage = ObjectHelper.getNestedProperty(oError, "response.body") || "Error";
            }
        }

        return mErrorData;
    }

    /**
     * @summary Shows an error message dialog for a failed OData service request
     * @param {object|map} oError
     *  Error object or map with additional information
     * @param {map} mParameters
     *  a parameter map with the following attributes:
     * @param {string} [mParameters.sStyleClass=sapUiSizeCompact]
     *  CSS style class to apply to the MessageBox dialogs
     * @param {boolean} [mParameters.bShowDetails=false]
     *  shows error details if true; false by default
     * @param {boolean} [mParameters.bSendToSupport=false]
     *  shows a "send to support" message if true; if the 'SupportServiceUrl'
     *  and 'ODataEntityName' parameters are set it provides a button for
     *  sending a support email (experimental)
     * @param {string} [mParameters.sSupportServiceUrl]
     *  URL of the (OData) service that is used to send support emails
     * @param {map} [mParameters.mODataParameters]
     *  a map with optional parameters to attach to the ODataModel
     * @param {string} [mParameters.sODataEntityName]
     *  the name of the ODataModel Set used to send the Support Mail Service
     *  request (experimental)
     * @returns {Promise}
     *  a Promise instance that can be evaluated and handled
     *  in the calling instance using 'then' and 'fail' methods
     * @public
     * @static
     */
    oErrorHelper.showErrorDialog = function (oError, mParameters) {
        mParameters = mParameters || {};

        var oDeferred = $.Deferred(),
            mErrorData = this.getErrorDetails(oError),
            sErrorJSON,
            oErrorData,
            bShowDetails = mParameters.bShowDetails !== undefined ? mParameters.bShowDetails : false,
            bSendToSupport = mParameters.bSendToSupport !== undefined ? mParameters.bSendToSupport : false;
        mParameters.sStyleClass = mParameters.sStyleClass || "sapUiSizeCompact";
        mParameters.mODataParameters = mParameters.mODataParameters || {};

        // Construct the error message
        var sMessage = Library.getBundleText("error.dialog.message") + ": \n\n" + mErrorData.message;
        sMessage += mErrorData.innerMessage ? "\n" + mErrorData.innerMessage + "" : "";
        if (mParameters.bSendToSupport) {
            sMessage += "\n\n" + Library.getBundleText("error.support.contact");
        }

        // Add displayMessage and prepare JSONified details
        oError.sDisplayMessage = sMessage;
        oErrorData = oError;
        oErrorData.oSource = null; // disable reference to allow JSON.stringify
        try {
            sErrorJSON = JSON.stringify(oErrorData, null, 4);
        } catch (oException) {
            console.warn("Parsing error mail object failed.", oException);
        }

        // Show popup with serialized "details" section
        MessageBox.error(sMessage, {
            actions: bSendToSupport && mParameters.sSupportServiceUrl && mParameters.sODataEntityName ?
                [Library.getBundleText("error.support.button.send"), MessageBox.Action.CLOSE] :
                MessageBox.Action.CLOSE,
            icon: MessageBox.Icon.ERROR,
            title: Library.getBundleText("error.dialog.title"),
            styleClass: mParameters.sStyleClass,
            details: bShowDetails ? sErrorJSON : "",
            onClose: function (mFnParameters, sAction) {
                // Send the error to a support Email Address, if set
                if (sAction === Library.getBundleText("error.support.button.send")) {
                    oErrorHelper.sendSupportMail($.extend(true, mFnParameters, {
                        sMailContent: sErrorJSON,
                        oErrorData: oErrorData
                    })).then(function (mResult) {
                        oDeferred.resolve(mResult);
                    }.bind(this)).fail(function (oError) {
                        oDeferred.reject(oError);
                    }.bind(this));
                } else {
                    oDeferred.resolve(mFnParameters);
                }
            }.bind(this, mParameters)
        });

        return oDeferred.promise();
    }

    /**
     * @summary Sends a mail to a support address using the specified parameters
     * @param {map} mParameters
     *  parameter map with the following attributes:
     * @param {string} mParameters.sMailContent
     *  the contents for the Error Email
     * @param {map} [mParameters.oErrorData]
     *  additional Information to include in the Email
     * @param {string} [mParameters.sStyleClass=sapUiSizeCompact]
     *  CSS style class to apply to the MessageBox dialogs
     * @param {string} mParameters.sSupportServiceUrl
     *  URL of the (OData) service that is used to send support emails
     * @param {map} [mParameters.mODataParameters]
     *  a map with optional parameters to attach to the ODataModel
     * @param {string} [mParameters.sODataEntityName]
     *  the name of the ODataModel Set used to send the Support
     *  Mail Service request (experimental)
     * @returns {Promise}
     *  a Promise handler to be evaluated by the calling function
     * @todo experimental
     * @since 0.2.1
     * @public
     * @static
     */
    oErrorHelper.sendSupportMail = function (mParameters) {
        var oDeferred = $.Deferred();

        mParameters = mParameters || {};
        if (!assert(mParameters.sMailContent, "Email should have contents, don't you think?")) {
            oDeferred.reject();
            return oDeferred.promise();
        }
        mParameters.sStyleClass = mParameters.sStyleClass || "sapUiSizeCompact";
        mParameters.mODataParameters = mParameters.mODataParameters || {};

        // Create the ODataModel and its Entity and send the support mail request
        try {
            var oDataModel = new ODataModel(mParameters.sSupportServiceUrl, mODataParameters);
            if (!assert(oDataModel && oDataModel.create, "Could not create ODataModel to send support email.")) {
                oDeferred.reject();
                return oDeferred.promise();
            }

            // Try to send the support email
            oDataModel.create(mParameters.sODataEntityName, {
                Mailtext: mParameters.sErrorJSON
            }, {
                success: function (mFnParameters, oData, oResponse) {
                    MessageBox.alert(Library.getBundleText("error.support.send.success"), {
                        title: Library.getBundleText("error.support.success"),
                        styleClass: mParameters.sStyleClass,
                        onClose: function () {
                            oDeferred.resolve($.extend(true, mFnParameters, {
                                oData: oData,
                                oResponse: oResponse
                            }));
                        }.bind(this)
                    });
                }.bind(this, mParameters),
                error: function (oError) {
                    MessageBox.alert(Library.getBundleText("error.support.send.failed"), {
                        title: Library.getBundleText("error.dialog.title"),
                        styleClass: mParameters.sStyleClass,
                        onClose: function () {
                            oDeferred.reject(oError);
                        }.bind(this)
                    });
                }.bind(this)
            });
        } catch (oException) {
            console.error("At the moment only ODataModels can be used to send Support Mail requests.");
            oDeferred.reject(mParameters)
        }

        return oDeferred.promise();
    }

    /**
     * @summary Shows a 'hacker' error dialog with a nice Meme image and
     *  the bahaviour of blocking the application completely, so that no
     *  further inpout
     * @param {function} [fnCallback]
     *  optional callback function to execute on
     *  'press' Event on the 'hacker' image
     * @returns {void}
     * @public
     * @static
     */
    oErrorHelper.showHackerError = function (fnCallback) {
        var sErrorImagePath = Library.getManifestEntry("/sap.ui5/config/hackerErrorImage"),
            sErrorImage = LibraryHelper.register("ixult").getResourcePath(sErrorImagePath);
        new Dialog({
            showHeader: false,
            content: [new Image({
                src: sErrorImage,
                press: function (oEvent) {
                    if (fnCallback && typeof fnCallback == "function") {
                        fnCallback(oEvent);
                    } else {
                        setTimeout(function () {
                            window.close();
                        }, 1000);
                    }
                }.bind(this)
            })]
        }).open();
    }

    return oErrorHelper;
});