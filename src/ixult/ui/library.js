sap.ui.define([
    "sap/ui/core/Core",
    "sap/ui/core/library"
], function (
    Core,
    Library
) {
    "use strict";

    /**
     * @namespace ui
     * @memberOf ixult
     * @summary Main library for UI controls and helper classes
     * @public
     */
    return sap.ui.getCore().initLibrary({
        name: "ixult.ui",
        noLibraryCSS: true,
        dependencies: [
            "sap.ui.core",
            "sap.m",
        ],
        types: [],
        interfaces: [],
        controls: [],
        elements: [],
        version: "0.8.1"
    });

}, /* bExport= */ false);