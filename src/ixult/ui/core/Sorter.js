sap.ui.define([
    "ixult/ui/core/Element",
    "sap/ui/core/SortOrder",
    "sap/ui/model/Sorter"
], function (
    Element,
    SortOrder,
    Sorter
) {
    "use strict";

    /**
     * @class Sorter
     * @memberof ixult.ui.core
     * @extends sap.ui.core.Element
     *
     * @summary Sorter for a UI Control, e.g. {ixult.m.Column}
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.ui.model.Sorter sap.ui.model.Sorter}
     *
     * @param {map} mParameters
     *  a map of parameters for the `Sorter` element,
     *  containing the properties set into the `metadata`
     *  for this Class
     *
     * @public
     *
     * @since 0.8.1
     */
    return Element.extend("ixult.ui.core.Sorter", /** @lends ixult.ui.core.Sorter.prototype */ {

        // Metadata for this class; Used to define custom properties
        metadata: {
            properties: {
                /**
                 * @name ixult.ui.core.Sorter#path
                 * @type {string|object|*}
                 * @description The model context path pointing to the bound
                 *  model attributes, that will be used for sorting
                 * @public
                 */
                path: {
                    type: "string"
                },
                /**
                 * @name ixult.ui.core.Sorter#type
                 * @type {string|object|*}
                 * @description The type of the `path` property;
                 *  Can be one of the following values:
                 *  <ul>
                 *      <li>Date</li>
                 *      <li>`Time`</li>
                 *      <li>`Integer`</li>
                 *      <li>`String`</li>
                 *  </ul>
                 *  `String` by default
                 * @default `String`
                 * @public
                 */
                type: {
                    type: "string",
                    defaultValue: "String"
                },
                /**
                 * @name ixult.ui.core.Sorter#default
                 * @type {boolean}
                 * @description Specifies the sort order,
                 *  `true` for `Descending`, false by default
                 * @default `false`
                 * public
                 */
                descending: {
                    type: "boolean",
                    defaultValue: false
                },
                /**
                 * @name ixult.ui.core.Sorter#default
                 * @type {boolean}
                 * @description Specifies if this Sorter should be set to
                 *  default, e.g. to have a corresponding {ixult.m.Column}
                 *  being sorted, when an applicable parent {ixult.m.Table}
                 *  is initialized
                 * @default `false`
                 * @public
                 */
                default: {
                    type: "boolean",
                    defaultValue: false
                },
                /**
                 * @name ixult.ui.core.Sorter#group
                 * @type {boolean}
                 * @description Sets the `group` option for the Sorter;
                 *  Intended, to be forwarded to a {sap.ui.model.Sorter},
                 *  that uses this Sorter's values and attributes
                 * @default `false`
                 * @public
                 */
                group: {
                    type: "boolean",
                    defaultValue: false
                }
            }
        },

        /**
         * @summary Constructor for the Sorter Element
         * @param {map} mParameters
         *  a map of parameters for the `Sorter` element,
         *  containing the properties set into the `metadata`
         *  for this Class
         * @public
         * @hideconstructor
         */
        constructor: function (mParameters) {
            Element.prototype.constructor.apply(this, arguments);

            return this;
        },

        /**
         * @summary Retrieves binding information for a given property
         * @param {string} sProperty
         *  the property to look up the binding for
         * @returns {sap.ui.model.Binding|null}
         *  the property binding, or `null` if not found
         * @protected
         */
        getPropertyBinding: function (sProperty) {
            var oBindingInfo = this.getBindingInfo(sProperty);

            return oBindingInfo && oBindingInfo.binding ?
                oBindingInfo.binding : null;
        },

        /**
         * @summary Returns the model context path for the
         *  `path` property as a string, if the property is
         *  bound (which it should be); Looks for the property
         *  value for `path` instead if it is just a string
         * @returns {string}
         *  The `path` property's model context path
         * @public
         * @override
         */
        getPath: function () {
            return this.getPropertyBinding("path") ?
                this.getPropertyBinding("path").getPath() :
                this.getProperty("path");
        }

    });
});