sap.ui.define([
    "sap/ui/core/UIComponent",
    "sap/ui/events/KeyCodes"
], function (
    UIComponent,
    KeyCodes
) {
    "use strict";

    /**
     * @class UIComponent
     * @memberOf ixult.ui.core
     * @extends sap.ui.core.UIComponent
     *
     * @classdesc Core Class for UIComponents
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.ui.core.UIComponent sap.ui.core.UIComponent}
     *
     * @public
     */
    return UIComponent.extend("ixult.ui.core.UIComponent", /** @lends ixult.ui.core.UIComponent.prototype */ {

        // Metadata for the UI element; Used to define custom properties
        metadata: {
            events: {
                /**
                 * @name ixult.ui.core.UIComponent#updateStarted
                 * @type {sap.ui.base.Event}
                 * @summary Handles an event to initiate an update,
                 *  e.g. by calling an OData service to get new data
                 * @public
                 */
                updateStarted: {
                    allowPreventDefault: true
                },
                /**
                 * @name ixult.ui.core.UIComponent#updateStarted
                 * @type {sap.ui.base.Event}
                 * @summary Used to handle an event when an update has finished
                 *  e.g. when an OData call has returned and the view is about
                 *  to be refreshed
                 * @public
                 */
                updateFinished: {
                    allowPreventDefault: true
                },
            }
        },

        // Attributes
        mConfig: {},
        oModel: null,

        /**
         * @summary The component is initialized by UI5 automatically during
         *  the startup of the app and calls the init method once.
         * @param {map} mParameters
         *  a parameter map, containing the following attributes:
         * @param {ixult.ui.model.Model|sap.ui.model.Model} mParameters.oModel
         *  the main Data Model used for the Application (optionally)
         * @public
         * @override
         */
        init: function (mParameters) {
            // Evaluate and save the Parameters if given
            mParameters = mParameters || {};
            if (mParameters.oModel) {
                this.oModel = mParameters.oModel;
            }

            // Call the base component's init function
            UIComponent.prototype.init.apply(this);

            // Enable Routing
            var oRouter = this.getRouter();
            if (oRouter) {
                oRouter.initialize();
            }

            // Apply WebApp capability (for Apple devices)
            this.setWebAppCapable(this.getConfig("/webAppCapable"));
        },

        /**
         * @summary Returns the Manifest file's settings stored in the `sap.ui5/config` section
         * @see {manifest.json#sap.ui5}
         * @param {string} sPath
         *  an optional sub-path underneath the `config` section to extract
         * @returns {object|map}
         *  the `config` map
         * @public
         */
        getConfig: function (sPath) {
            sPath = sPath !== undefined ? sPath.indexOf("/") === 0 ? sPath : "/" + sPath : "";
            return this.getManifestEntry("/sap.ui5/config" + sPath);
        },

        /**
         * @summary Retrieves a named JSON model if given or the main application model
         * @param {string} sName
         *  the name of the model to be retrieved
         * @returns {sap.ui.model.json.JSONModel|ixult.ui.model.Model|null}
         *  the Data Model instance
         * @public
         * @override
         */
        getModel: function (sName) {
            return sName ? UIComponent.prototype.getModel.apply(this, arguments) : this.oModel;
        },

        /**
         * @summary Attaches an Event Handler function for the `routeMatched` Event
         * @see {ixult.m.routing.Router~handleRoutePatternMatched}
         * @param {function} fnFunction
         *  the Event Handler function
         * @param {object} oListener
         *  the Handler Class instance
         * @returns {void}
         * @public
         */
        attachRouteMatched: function (fnFunction, oListener) {
            this.attachEvent("routeMatched", fnFunction, oListener);
        },

        /**
         * @summary Fires the custom `routeMatched` event coming from the Router
         * @see {ixult.m.routing.Router~handleRoutePatternMatched}
         * @param {sap.ui.base.Event|map|object} oEvent
         *  either the original routing Event instance or a map of custom parameters
         * @returns {void}
         * @public
         */
        fireRouteMatched: function (oEvent) {
            var mParameters = oEvent && oEvent.isA && oEvent.isA("sap.ui.base.Event") ? oEvent.getParameters() :
                oEvent && typeof oEvent === "object" ? oEvent : {};

            this.fireEvent("routeMatched", mParameters);
        },

        /**
         * @summary Attaches an Event Handler function for the `targetMatched` Event
         * @see {ixult.m.routing.Router~handleTargetDisplayMatched}
         * @param {function} fnFunction
         *  the Event Handler function
         * @param {object} oListener
         *  the Handler Class instance
         * @returns {void}
         * @public
         */
        attachTargetMatched: function (fnFunction, oListener) {
            var oView = oListener.getView();
            if (!oView) {
                return;
            }
            var sViewName = oView.getViewName() || oView.getProperty("viewName") || oView.sViewName;
            sViewName = sViewName.substring(
                sViewName.lastIndexOf(".") + 1,
                sViewName.length
            );
            var sEventName = "target" + sViewName + "Matched";
            this.attachEvent(sEventName, fnFunction, oListener);
        },

        /**
         * @summary Fires the custom `targetMatched` event coming from the router
         * @see {ixult.m.routing.Router~handleTargetDisplayMatched}
         * @param {sap.ui.base.Event} oEvent
         *  the original target display event
         * @returns {void}
         * @public
         */
        fireTargetMatched: function (oEvent) {
            var mParameters = oEvent.getParameters(),
                sEventName = "target" + mParameters.config.viewName + "Matched";

            this.fireEvent(sEventName, mParameters);
        },

        /**
         * Attaches an Event Handler function for the `keyPressed` Event
         * @param {function} fnFunction
         *  the Event Handler function
         * @param {object} oListener
         *  the Handler Class instance
         * @returns {void}
         * @public
         */
        attachKeyPressed: function (fnFunction, oListener) {
            this.attachEvent("keyPressed", fnFunction, oListener);

            // Define the handler function for the keypress events
            // that will forward this by firing the UI5 custom event
            const fnFireKeyPressed = function (mParameters) {
                try {
                    mParameters.oKeyEvent.data.oListener
                        .getComponent().fireKeyPressed(mParameters);
                } catch (oException) {
                    console.error("Error during keypress event handling.", oException);
                }
            };

            // Attach the jQuery `keydown`, `keypress` and `keyup` events that
            // actually are fired when a key has been pressed
            $(document).keydown({oListener: oListener}, function (oKeyEvent) {
                fnFireKeyPressed({
                    sType: "keydown",
                    oKeyEvent: oKeyEvent,
                    oKeyboardEvent: oKeyEvent.originalEvent,
                    sKey: oKeyEvent.which
                });
            }.bind(oListener));
            $(document).keypress({oListener: oListener}, function (oKeyEvent) {
                fnFireKeyPressed({
                    sType: "keypress",
                    oKeyEvent: oKeyEvent,
                    oKeyboardEvent: oKeyEvent.originalEvent,
                    sKey: oKeyEvent.which
                });
            }.bind(oListener));
            $(document).keyup({oListener: oListener}, function (oKeyEvent) {
                fnFireKeyPressed({
                    sType: "keyup",
                    oKeyEvent: oKeyEvent,
                    oKeyboardEvent: oKeyEvent.originalEvent,
                    sKey: oKeyEvent.which
                });
            }.bind(oListener));
        },

        /**
         * @summary Fires the custom `keyPressed` Event to notify all Listeners
         *  whenever a keystroke has been recognized
         * @description Add a `mKeyCodes` map of possible keys to allow
         *  for easier identification of the pressed key
         * @see {@link https://ui5.sap.com/sdk/#/api/module:sap/ui/events/KeyCodes sap/ui/events/KeyCodes}
         * @param {map} mParameters
         *  a map with custom Parameters to apply to the Event
         * @param {Event} mParameters.oEvent
         *  the jQuery original keydown, keypress or keyup event
         * @param {string} mParameters.sType
         *  the type of keypress event;
         *  can be `keydown`, `keypress` or `keyup`
         * @returns {void}
         * @public
         */
        fireKeyPressed: function (mParameters) {
            this.fireEvent("keyPressed", $.extend(true, mParameters, {
                mKeyCodes: KeyCodes
            }));
        },

        /**
         * @summary Attaches an Event Handler function for the `requestCompleted` Event
         *  to be applied e.g. after ODataService requests to allow for success handling
         * @param {function} fnFunction
         *  the Event Handler function
         * @param {object} oListener
         *  the Handler Class instance
         * @returns {void}
         * @public
         */
        attachRequestCompleted: function (fnFunction, oListener) {
            this.attachEvent("requestCompleted", fnFunction, oListener);
        },

        /**
         * @summary Fires the custom `requestCompleted` Event to notify Views about
         *  e.g. successful OData requests and allow postprocessing the results
         * @param {map} mParameters
         *  a map with custom Parameters to apply to the Event
         * @returns {void}
         * @public
         */
        fireRequestCompleted: function (mParameters) {
            this.fireEvent("requestCompleted", mParameters);
        },

        /**
         * @summary Attaches an Event Handler function for the `requestFailed` Event
         *  to be applied e.g. after ODataService requests to allow for error handling
         * @param {function} fnFunction
         *  the Event Handler function
         * @param {object} oListener
         *  the Handler Class instance
         * @returns {void}
         * @public
         */
        attachRequestFailed: function (fnFunction, oListener) {
            this.attachEvent("requestFailed", fnFunction, oListener);
        },

        /**
         * @summary Fires the custom `requestFailed` Event to notify Views about
         *  e.g. failed OData requests and allow error handling
         * @param {map} mParameters
         *  a map with custom Parameters to apply to the Event
         * @returns {void}
         * @public
         */
        fireRequestFailed: function (mParameters) {
            this.fireEvent("requestFailed", mParameters);
        },

        /**
         * @summary Attaches an Event Handler function to react on thrown errors
         *  from anywhere inside the application; Errors can be thrown using the
         *  function `throwError`
         * @see {ixult.ui.core.UIComponent~throwError}
         * @param {function} fnFunction
         *  the Event Handler function
         * @param {object} oListener
         *  the Handler Class instance
         * @returns {void}
         * @public
         */
        attachErrorThrown: function (fnFunction, oListener) {
            this.attachEvent("errorThrown", fnFunction, oListener);
        },

        /**
         * @summary Fires the custom `errorThrown` Event to notify
         *  about an Error inside the Application; can be thrown
         *  from virtually anywhere inside the Application
         * @param {jQuery.Error|object} oError
         *  the Error instance or object with Error information
         * @param {map} mParameters
         *  a map with custom Parameters to apply to the Event
         * @returns {void}
         * @public
         */
        throwError: function (oError, mParameters) {
            this.fireEvent("errorThrown", oError, mParameters);
        },

        /**
         * @summary Shows a simple MessageToast for single messages
         * @param {string} sText
         *  the i18n text id or the text string to show
         * @param {map} mParameters
         *  a map of Parameters applied to the {sap.m.MessageToast}
         *  Control, containing the corresponding values valid for
         *  {sap.m.MessageToast}
         * @see {@link https://ui5.sap.com/sdk/#/api/sap.m.MessageToast%23methods/sap.m.MessageToast.show sap.m.MessageToast#.show}
         * @returns {void}
         * @public
         */
        showMessage: function (sText, mParameters) {
            if (!sText) {
                console.error("Can't show empty message");
                return;
            }

            // Get and show the message
            sap.ui.require(["sap/m/MessageToast", "sap/ui/core/Popup"], function (MessageToast, Popup) {
                MessageToast.show(this._getText(sText), mParameters);
            }.bind(this));
        },

        /**
         * @summary Enables or disables the Application to be loaded in full screen mode
         *  after added to home screen from iOS Safari or mobile Chrome from version 31
         * @see [/sap/ui/util/Mobile.setWebAppCapable]
         *  (https://ui5.sap.com/sdk/#/api/module:sap/ui/util/Mobile%23methods/sap/ui/util/Mobile.setWebAppCapable)
         * @param {boolean} bWebAppCapable
         *  true, if the App should be a WebApp
         * @returns {void}
         * @protected
         */
        setWebAppCapable: function (bWebAppCapable) {
            bWebAppCapable = bWebAppCapable !== undefined && bWebAppCapable !== null ? bWebAppCapable : false;
            sap.ui.require(["sap/ui/util/Mobile"], function (Mobile) {
                Mobile.setWebAppCapable(bWebAppCapable);
            }.bind(this));
        },

        /**
         * @summary Retrieve the text using the Model,
         *  and it's `getText` method if set;
         *  Otherwise try to get the `i18n` model and
         *  use it directly
         * @param {string} sTextId
         *  the text id or text string
         * @returns {string}
         *  the text for the given id, or the text itself
         * @private
         * @internal
         */
        _getText: function (sTextId) {
            var sText = sTextId;

            if (this.getModel() && this.getModel().getText) {
                sText = this.getModel().getText(sTextId);
            } else if (this.getModel("i18n")) {
                var oBundle =
                    this.getModel("i18n").getResourceBundle();
                sText = oBundle && oBundle.hasText(sTextId) ?
                    oBundle.getText(sTextId) : sTextId;
            } else {
                console.warn("Could not retrieve text for text id '" +
                    sTextId + "'. Using it as text instead.");
                sText = sTextId;
            }

            return sText;
        }

    });
});