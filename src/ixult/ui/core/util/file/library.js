sap.ui.define([
    "sap/ui/core/Core",
    "sap/ui/core/library"
], function (
    Core,
    Library
) {
    "use strict";

    /**
     * @namespace file
     * @memberOf ixult.ui.core.util
     *
     * @description Library to handle files,
     *  their data and types
     *
     * @public
     */
    sap.ui.getCore().initLibrary({
        name: "ixult.ui.core.util.file",
        noLibraryCSS: true,
        dependencies: [
            "sap.ui.core"
        ],
        types: [],
        interfaces: [],
        controls: [],
        elements: [],
        version: "0.8.2"
    });

    var thisLib = ixult.ui.core.util.file;

    /**
     * @name ixult.ui.core.util.file#FileType
     * @type {{
     *     CSV: string,
     *     XLSX: string
     * }}
     * @summary List of supported file types, can be
     *  compared against the type of an uploaded file
     * @public
     * @static
     */
    thisLib.FileType = {
        "CSV": "csv",
        "XLS": "xls",
        "XLSX": "xlsx"
    }

    /**
     * @name ixult.ui.core.util.file#MimeType
     * @type {{
     *     XLSX: string
     * }}
     * @summary List of supported MIME types, to be
     *  compared against the MIME type from an uploaded file
     * @public
     * @static
     */
    thisLib.MimeType = {
        "CSV": "text/csv",
        "XLS": "application/vnd.ms-excel",
        "XLSX": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    }


    return thisLib;

}, /* bExport= */ false);