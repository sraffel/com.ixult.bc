sap.ui.define([
    "sap/base/util/uid",
    "sap/ui/base/ManagedObject",
    "sap/ui/core/util/File",
    "ixult/base/assert",
    "ixult/base/util/array",
    "ixult/base/util/string",
    "ixult/base/util/DateHelper",
    "ixult/base/util/LibraryHelper",
    "ixult/lib/ui/unified/jquery.csv",
    "ixult/lib/ui/unified/simple-excel",
    "ixult/lib/ui/unified/xlsx-populate",
    "ixult/lib/ui/unified/xlsx",
    "ixult/lib/ui/unified/jszip",
    "ixult/ui/core/util/file/library"
], function (
    uid,
    ManagedObject,
    File,
    assert,
    array,
    string,
    DateHelper,
    LibraryHelper,
    LibJQueryCsv,
    LibSimpleExcel,
    LibXlsxPopulate,
    LibXlsx,
    LibJsZip,
    FileLibrary
) {
    "use strict";
    const Library = LibraryHelper.register("ixult/ui/core/util/file/type");


    /**
     * @name dBeginDate
     * @memberOf ixult.ui.core.util.file.type.ExcelDocument
     * @type {Date}
     * @summary Global starting Date for Date numbers in Excel;
     *  Excel counts all date values beginning with the
     *  1st of January 1900; If the computer standardized
     *  1st of January *1970* should be used, the difference
     *  value in Excel is `25569`
     * @protected
     * @static
     * @internal
     */
    const dBeginDate = DateHelper.toDate({
        iYear: 1900,
        iMonth: 1,
        iDay: 1
    });

    /**
     * @name iDateThreshold
     * @memberOf ixult.ui.core.util.file.type.ExcelDocument
     * @type {Number}
     * @summary Artificial Threshold for begin dates to
     *  eliminate false positives, when looking for  dates
     *  in the Excel data; Set to the Excel value
     *  corresponding to the `01.01.2000`
     * @protected
     * @static
     * @internal
     */
    const iDateThreshold = 10957;

    /**
     * @class ExcelDocument
     * @memberof ixult.ui.core.util.file.type
     * @extends sap.ui.base.ManagedObject
     *
     * @summary Document class for parsing and evaluates
     *  Microsoft Excel documents, including as `XLSX` or `CSV` files
     *
     * @param {map} mParameters
     *  a parameter map containing the file and its data
     * @param {map} mParameters.oFile
     *  the File object read by a `FileReader` instance,
     *  containing the raw data needed for parsing
     * @param {map} mParameters.oData
     *  the result data read from the file
     *
     * @public
     *
     * @since 0.8.2
     */
    return ManagedObject.extend("ixult.ui.core.util.file.type.ExcelDocument", /** @lends ixult.ui.core.util.file.type.ExcelDocument */ {
        mCellType: {
            Date: "Date",
            Duration: "Duration",
            String: "String"
        },
        sFileType: "",
        sMimeType: "",
        aSheetNames: [],
        mSheets: {},
        oFile: null,
        oData: null,
        oWorkbook: null,

        /**
         * @summary Creates a new ExcelDocument instance based on a given File object
         * @param {map} mParameters
         *  a parameter map containing the file and its data
         * @param {object} mParameters.oFile
         *  the File object read by a `FileReader` instance,
         *  containing the raw data needed for parsing
         * @param {object} mParameters.oData
         *  the result data read from the file
         * @returns {ixult.ui.core.util.file.type.ExcelDocument}
         *  a new document instance of this
         * @public
         * @hideconstructor
         */
        constructor: function (mParameters) {
            ManagedObject.prototype.constructor.apply(this);

            this.oFile = mParameters.oFile;
            this.oData = mParameters.oData;

            var aFilenameParts = mParameters.oFile.name.toLowerCase().split(".");
            this.sFileType = aFilenameParts[aFilenameParts.length - 1];
            this.sMimeType = mParameters.oFile.type;

            // Parse the workbook if the file is supported
            this._createDocument();

            return this;
        },

        /**
         * @summary Creates the document (workbook)
         *  from the given file contents
         * @returns {void}
         * @private
         */
        _createDocument: function () {
            if (this.isXLSX()) {
                this.oWorkbook = XLSX.read(this.oData, {
                    type: "binary"
                });
            }
        },

        /**
         * @summary Checks if the given document is of type `XLSX`
         * @returns {boolean}
         *  true, if the document is an `XLSX` file, false otherwise
         * @public
         */
        isXLSX: function () {
            return this.sFileType === FileLibrary.FileType.XLSX ||
                this.sMimeType === FileLibrary.MimeType.XLSX;
        },

        /**
         * @summary Checks if the given document is of type `CSV`
         * @returns {boolean}
         *  true, if the document is an `CSV` file, false otherwise
         * @public
         */
        isCSV: function () {
            return this.sFileType === FileLibrary.FileType.CSV ||
                this.sMimeType === FileLibrary.MimeType.CSV;
        },

        /**
         * @summary Parses the contents of an `XLSX` file
         * @description Uses the XLSX parser from `sheetjs.com` to
         *  analyze the given document and extract the information
         * @see {@link https://sheetjs.com/ https://sheetjs.com/}
         * @returns {{
         *      workbook: object,
         *      sheetNames: string[],
         *      sheets: object
         *  }|null}
         *  the data extracted from the given Excel document;
         *  `null` if the Workbook type is not supported
         * @public
         */
        parseXlsx: function () {
            if (!this.isXLSX() || !this.oWorkbook) {
                return null;
            }

            // Save the sheet names (but don't sort them alphabetically)
            this.aSheetNames = Object.keys(this.oWorkbook.Sheets);

            // Parse every sheet of the workbook to a JSON data table
            this.oWorkbook.SheetNames.forEach(function (sSheetName) {
                this.mSheets[sSheetName] = this.parseXlsxSheet({
                    oSheet: this.oWorkbook.Sheets[sSheetName],
                    sSheetName: sSheetName
                });
            }.bind(this));

            return {
                workbook: this.oWorkbook,
                sheetNames: this.aSheetNames,
                sheets: this.mSheets
            };
        },

        /**
         * @summary Parses a single Sheet of an `XLSX` Workbook;
         *  Returns an object including the list of found rows
         *  and columns as well as a parsed results table
         * @param {map} mParameters
         *  a parameter map, containing the following attributes
         * @param {object} mParameters.oSheet
         *  the Workbook Sheet instance containing the columns and rows
         * @param {string} mParameters.sSheetName
         *  the name of the Workbook Sheet that is about to be parsed
         * @returns {{
         *      name: string,
         *      columns: string[],
         *      rows: string[],
         *      results: object[],
         *      json: object
         *  }}
         *  the parsed data from the sheet
         * @protected
         */
        parseXlsxSheet: function (mParameters) {
            var sSheetName = mParameters.sSheetName,
                oSheet = mParameters.oSheet,
                aRows = array([]),
                aColumns = array([]);

            // Add a JSON representation extracted by
            // the XLSX parser, just for good measure :-)
            var oRowObject = XLSX.utils.sheet_to_row_object_array(oSheet),
                sRowJSON = JSON.stringify(oRowObject),
                oRowJSON = JSON.parse(sRowJSON);

            // Extract the column ids and row numbers
            array(Object.keys(oSheet)).grep(sKey => {
                // Exclude technical data from the Workbook Parser
                return !sKey.startsWith("!");
            }).loop(sKey => {
                // The Sheet contains the row-column-id
                // in Excel format, e.g. `C4` or `N69`
                var aMatches = sKey.match(/(\D*)(\d*)/i);
                // Extract the row and column ids using a RegEx
                if (aMatches && aMatches.length === 3) {
                    if (!aColumns.contains(aMatches[1])) {
                        aColumns.push(aMatches[1]);
                    }
                    if (!aRows.contains(aMatches[2])) {
                        aRows.push(aMatches[2]);
                    }
                }
            });

            // Sort the column ids alphabetically
            aColumns = aColumns.sort();

            // Parse the Sheet into a results table
            var aTable = array([]);
            aRows.loop(sRowId => {
                // Create an object for the current Sheet row
                var oRow = {};
                // Parse all the columns of this row
                aColumns.loop((sColId, iIndex) => {
                    // Get the cell using the row number and column id
                    var oCell = oSheet[sColId + "" + sRowId],
                        // and create the column data object
                        oColumn = {
                            value: oCell ? oCell.v : null,
                            text: oCell ? oCell.w : "",
                            type: this.mCellType.String,
                            cell: oCell
                        };

                    // Try to parse number (`n`) values into time or date
                    if (oCell && oCell.t === "n") {
                        var fNumber = parseFloat(oCell.v),
                            iNumber = parseInt(oCell.v);
                        if (oCell.v < 1 && fNumber.toString() !== "NaN") {
                            oColumn.value = this.floatToMinutes(fNumber);
                            oColumn.type = this.mCellType.Duration;
                        } else if (iNumber > iDateThreshold) {
                            oColumn.value = this.integerToDate(iNumber);
                            oColumn.type = this.mCellType.Date;
                        }
                    }

                    // Add the column by its index
                    // to ensure correct numbering
                    oRow[sColId] = oColumn;
                });

                aTable.push(oRow);
            })

            // Return the parsed Sheet data
            return {
                name: sSheetName,
                columns: aColumns.sort((a, b) => a.localeCompare(b)),
                rows: aRows.sort((a, b) => a - b),
                results: aTable,
                json: oRowJSON
            }
        },

        /**
         * @summary Returns a list of all the names for the
         *  Sheets fround in the Workbook for a parsed Excel
         *  document file
         * @returns {string[]}
         *  the array of Sheet names
         * @public
         */
        getSheetNames: function () {
            return this.aSheetNames;
        },

        /**
         * @summary Returns the sheet instance from an Excel Workbook
         *  with the given name, or null if not found
         * @param {string} sName
         *  the name of the Workbook Sheet to look for;
         *  Hint: Use `getSheetNames` to get a list of all the names
         * @returns {object}
         *  the Excel Sheet
         * @public
         */
        getSheet: function (sName) {
            return this.mSheets[sName];
        },

        /**
         * @summary Parses a given Excel number into a Javascript Date
         * @description Excel dates are given as 5-digit integers,
         *  that represent the number of days passed since the
         *  1st of January 1900, where Excel time begins. This can be
         *  verified by changing the format of a corresponding Excel
         *  date cell to 'Text'. To calculate a Javascript date for that,
         *  the value is multiplied by
         *  <ul>
         *      <li>`24` for the hours in a day</li>
         *      <li>`60` for the minutes in an hour</li>
         *      <li>`60` for the seconds in a minute</li>
         *      <li>`1000` for the milliseconds in a seconds</li>
         *  </ul>
         *  This value is then added to the timestamp representing
         *  Excel's calculatory begin date (1st of January 1900), which
         *  is `-2208992400000`, because computer time begins with
         *  the 1st of January *1970* (though you might have to account
         *  for the GMT deviation here when testing that).
         *  *Note*: If calculating with the 01.01.1970 instead the
         *  difference between that and the 01.01.1900 would be `25569`
         *  in Excel
         * @param {int} iNumber
         *  the Excel date number
         * @returns {Date}
         *  the parsed Javascript date
         * @public
         */
        integerToDate: function (iNumber) {
            return new Date(dBeginDate.getTime() + iNumber * 1000 * 3600 * 24);
        },

        /**
         * @summary Parses an Excel float value into a minutes number
         * @description Excel uses float values <= 1 to indicate a Time.
         *  These values can be shown in Excel by changing the format
         *  of a corresponding time value cell to 'Text'. The values
         *  follow the rule of being a fraction of the number of seconds
         *  in a day, where each day has exactly `86400` seconds.
         * @example
         *  <ul>
         *      <li>Excel `0.010416667` times `86400` is around `900`
         *       seconds, which equals `60` minutes</li>
         *      <li>Excel `0.03125` times `86400` equals `2.700`
         *       seconds which is `45` minutes</li>
         *      <li> One day in Excel is `1`, i.e. `86400`
         *       seconds, i.e. `1440` minutes
         *  <ul>
         * @param {float|number} fNumber
         *  the float value to parse
         * @returns {int}
         *  the number of minutes that the given value has been parsed into
         * @public
         */
        floatToMinutes: function (fNumber) {
            return Math.round(fNumber * 86400 / 60);
        }

    });
});