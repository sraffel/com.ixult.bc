sap.ui.define([
    "sap/ui/core/Core",
    "sap/ui/core/library"
], function (
    Core,
    Library
) {
    "use strict";

    /**
     * @namespace type
     * @memberOf ixult.ui.core.util.file
     *
     * @description Library to handle file types
     *
     * @public
     */
    return sap.ui.getCore().initLibrary({
        name: "ixult.ui.core.util.file.type",
        noLibraryCSS: true,
        dependencies: [
            "sap.ui.core"
        ],
        types: [],
        interfaces: [],
        controls: [],
        elements: [],
        version: "0.8.2"
    });

}, /* bExport= */ false);