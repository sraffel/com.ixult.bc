sap.ui.define([
    "sap/ui/core/Core",
    "sap/ui/core/library"
], function (
    Core,
    Library
) {
    "use strict";

    /**
     * @namespace util
     * @memberOf ixult.ui.core
     *
     * @description Library for core utility functions
     *
     * @public
     */
    return sap.ui.getCore().initLibrary({
        name: "ixult.ui.core.util",
        noLibraryCSS: true,
        dependencies: [
            "sap.ui.core",
            "sap.ui.core.util"
        ],
        types: [],
        interfaces: [],
        controls: [],
        elements: [],
        version: "0.8.2"
    });

}, /* bExport= */ false);