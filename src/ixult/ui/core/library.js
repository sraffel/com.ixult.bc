sap.ui.define([
    "sap/ui/core/Core",
    "sap/ui/core/library",
    "ixult/base/util/array"
], function (
    Core,
    Library,
    array
) {
    "use strict";

    /**
     * @namespace core
     * @memberOf ixult.ui
     *
     * @description UI5 core namespace, containing
     *  main UI handler classes and functions
     *
     * @public
     */
    return sap.ui.getCore().initLibrary({
        name: "ixult.ui.core",
        noLibraryCSS: true,
        dependencies: [
            "sap.ui.core",
            "sap.m",
        ],
        types: [],
        interfaces: [],
        controls: [],
        elements: [
            "ixult.ui.core.Element",
            "ixult.ui.core.Filter",
            "ixult.ui.core.Sorter",
            "ixult.ui.core.UIComponent"
        ],
        version: "0.8.2"
    });

}, /* bExport= */ false);