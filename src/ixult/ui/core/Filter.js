sap.ui.define([
    "ixult/ui/core/Element",
    "sap/ui/core/format/DateFormat",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    "sap/ui/model/type/String",
    "ixult/base/assert",
    "ixult/base/util/array",
    "ixult/base/util/DateHelper"
], function (
    Element,
    DateFormat,
    Filter,
    FilterOperator,
    StringType,
    assert,
    array,
    DateHelper
) {
    "use strict";

    /**
     * @class Filter
     * @memberof ixult.ui.core
     * @extends sap.ui.core.Element
     *
     * @summary Filter for a UI Control, e.g. {ixult.m.Column}
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.ui.model.Filter sap.ui.model.Filter}
     *
     * @param {map} mParameters
     *  a map of parameters for the `Filter` element,
     *  containing the properties set into the `metadata`
     *  for this Class
     *
     * @public
     *
     * @since 0.8.1
     */
    return Element.extend("ixult.ui.core.Filter", /** @lends ixult.ui.core.Filter.prototype */ {

        // Metadata for this class; Used to define custom properties
        metadata: {
            properties: {
                /**
                 * @name ixult.ui.core.Filter#path
                 * @type {string|object|*}
                 * @description The model context path pointing to
                 *  the bound model property, that will be used
                 *  for filter comparison
                 * @public
                 */
                path: {
                    type: "string"
                },
                /**
                 * @name ixult.ui.core.Filter#type
                 * @type {string|object|*}
                 * @description The type of the `path` property;
                 *  Can be one of the following values:
                 *  <ul>
                 *      <li>`Date`</li>
                 *      <li>`Time`</li>
                 *      <li>`Integer`</li>
                 *      <li>`String`</li>
                 *  </ul>
                 *  `String` by default
                 * @default `String`
                 * @public
                 */
                type: {
                    type: "string",
                    defaultValue: "String"
                },
                /**
                 * @name ixult.ui.core.Filter#filterOptions
                 * @type {string|object|*}
                 * @description filter options, specified in the
                 *  SAP UI5 compatible `formatOptions` form that
                 *  is used for `sap.ui.model.Type` instances;
                 *  Supports a `pattern` attribute, that specifies,
                 *  how the filter values selected in the filter
                 *  suggestion list should be applied to the
                 *  comparator function
                 * @public
                 */
                filterOptions: {
                    type: "object"
                },
                /**
                 * @name ixult.ui.core.Filter#operator
                 * @type {sap.ui.model.FilterOperator}
                 * @description The Filter Operator to use for filtering;
                 *  Convenience attribute used e.g. in applicable
                 *  {ixult.m.Column} instances for creating the
                 *  {sap.ui.model.Filter} instances
                 * @default `EQ`
                 * @public
                 */
                operator: {
                    type: "sap.ui.model.FilterOperator",
                    defaultValue: FilterOperator.EQ
                },
                /**
                 * @name ixult.ui.core.Filter#text
                 * @type {string|object|*}
                 * @description The text that should be displayed
                 *  for each filter item; Formatted using the
                 *  `formatOptions` property
                 * @public
                 */
                text: {
                    type: "string",
                    defaultValue: ""
                },
                /**
                 * @name ixult.ui.core.Filter#formatOptions
                 * @type {string|object|*}
                 * @description Format options, specified in the
                 *  SAP UI5 compatible `formatOptions` form that
                 *  is used for {sap.ui.model.Type} instances;
                 *  Supports a `pattern` attribute, that specifies,
                 *  how the `text` values in the filter suggestion
                 *  list should be formatted
                 * @public
                 */
                formatOptions: {
                    type: "object"
                },
                /**
                 * @name ixult.ui.core.Filter#icon
                 * @type {string}
                 * @description Specifies an icon that can be set into
                 *  the Header Popover filter suggestion list
                 * @public
                 */
                icon: {
                    type: "string",
                    defaultValue: "string"
                }
            }
        },

        /**
         * @summary Constructor for the Filter Element
         * @param {map} mParameters
         *  a map of parameters for the `Filter` element,
         *  containing the properties set into the `metadata`
         *  for this Class
         * @public
         * @hideconstructor
         */
        constructor: function (mParameters) {
            Element.prototype.constructor.apply(this, arguments);

            return this;
        },

        /**
         * @summary Retrieves binding information for a given property
         * @param {string} sProperty
         *  the property to look up the binding for
         * @returns {sap.ui.model.Binding|null}
         *  the property binding, or `null` if not found
         * @protected
         */
        getPropertyBinding: function (sProperty) {
            const oBindingInfo = this.getBindingInfo(sProperty);
            return oBindingInfo && oBindingInfo.binding ?
                oBindingInfo.binding : null;
        },

        /**
         * @summary Returns the model context path for the
         *  `path` property as a string, if the property is
         *  bound (which it should be); Looks for the property
         *  value for `path` instead if it is just a string
         * @returns {string}
         *  The `path` property's model context path
         * @public
         * @override
         */
        getPath: function () {
            return this.getPropertyBinding("path") ?
                this.getPropertyBinding("path").getPath() :
                this.getProperty("path");
        },

        /**
         * @summary Returns a distinct list of possible filter values
         * @description Uses the `path` property binding to look
         *  inside the bound model data for possible values to filter
         * @returns {object[]}
         *  a list of filter values
         * @public
         */
        getValues: function () {
            // Get the binding info if set
            var oValueBinding = this.getPropertyBinding("path"),
                oModel = oValueBinding.getModel(),
                aFilterValues = array([]);

            // Get the values using the data model bindings
            // and searching for every entry that matches this Filter's
            // `path` value and has a bound Context
            array(oModel.getBindings()).grep(oBinding => {
                return oBinding.getPath() === oValueBinding.getPath() &&
                    oBinding.getContext() !== undefined;
            }).loop(function (oData) {
                // Get the values of the bound properties for this Filter
                var mValues = this._getContextValues({
                    oModel: oModel,
                    sPath: oData.getContext().getPath()
                });
                // Add a more logical `Value` attribute,
                // to be used instead of the `Path`
                mValues.Value = mValues.Path;
                if (aFilterValues.grep(oValue => {
                    return oValue.Text === mValues.Text;
                }).length === 0) {
                    aFilterValues.push(mValues);
                }
            }.bind(this));

            // Return a filtered list of filter values
            return aFilterValues.sort((a, b) => a.vValue - b.vValue);
        },

        /**
         * @summary Retrieves the data specified at the given context path
         *  from the given (JSON) data model, searching for this Filter's
         *  `path`, `text` and `icon` properties
         * @param {map} mParameters
         *  A parameter map containing the following attributes
         * @param {sap.ui.model.json.JSONModel|sap.ui.model.Model} mParameters.oModel
         *  The data model to look up the property in
         * @param {string} mParameters.sPath
         *  The oData context path
         * @returns {{
         *     Path: string,
         *     Text: string,
         *     Icon: string
         *  }}
         *  a value map with the attributes extracted
         *  from the Column's filter values
         * @private
         */
        _getContextValues(mParameters) {
            var oModel = mParameters.oModel,
                sContextPath = mParameters.sPath,
                oBindings = {
                    Path: this.getPropertyBinding("path"),
                    Text: this.getPropertyBinding("text") ||
                        this.getPropertyBinding("path"),
                    Icon: this.getPropertyBinding("icon")
                },
                mValue = {};

            for (var sKey in oBindings) {
                var sPath = oBindings[sKey] ?
                        oBindings[sKey].getPath() : null,
                    vValue = sPath ?
                        oModel.getProperty(sContextPath + "/" + sPath) : null;
                mValue[sKey] = vValue && sKey === "Text" ?
                    this._formatValue(vValue) : vValue;
            }

            return mValue;
        },

        /**
         * @summary Returns the pattern used e.g. to format Date and Time values
         * @returns {string|null}
         *  The formatter pattern, null if not found or specified
         * @private
         */
        _getFormatPattern: function () {
            var oOptions = this.getFilterOptions() || this.getFormatOptions();
            return oOptions && oOptions.pattern ? oOptions.pattern :
                this.getType() === "Date" ? DateHelper.getDatePattern("short") :
                    this.getType() === "Time" ? DateHelper.getTimePattern("short") : "";
        },

        /**
         * @summary Formats the given value according to the `type`
         *  of this Filter and using the `pattern` (if applied)
         *  when formatting types `Date` or `Time`
         * @param {*} vValue
         *  the unformatted value
         * @returns {string|int}
         *  the formatted value
         * @private
         */
        _formatValue: function (vValue) {
            // Get the format and/or filter options to build the distinct filter values
            const sPattern = this._getFormatPattern();

            switch (this.getType()) {
                case "Date":
                case "Time":
                    // Format Dates and Times according to the given pattern
                    // (e.g. 'EEEE', 'HH:mm'); Uses the `pattern` property within
                    // `formatOptions` if it wasn't provided with `filterOptions`
                    return sPattern ? DateHelper.format(vValue, sPattern) : vValue;
                case "Integer":
                    return parseInt(vValue);
                default:
                    return vValue.toString();
            }
        },

        /**
         * @summary Returns the comparator function that will
         *  execute the filter comparison
         * @returns {function}
         *  The function that executes the comparison;
         *  Uses parameters `vValue` and `vArray`;
         *  Returns `0` if the value was found inside
         *  the array, `-1` otherwise
         * @public
         */
        getComparator: function () {
            return function (vValue, aArray) {
                return array(aArray).format(function (vValue) {
                    return this._formatValue(vValue);
                }.bind(this)).contains(this._formatValue(vValue)) ? 0 : -1;
            }.bind(this);
        }


    });
});