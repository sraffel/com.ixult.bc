sap.ui.define([
    "sap/ui/core/Element",
    "ixult/base/assert",
    "ixult/base/util/array"
], function (
    Element,
    assert,
    array
) {
    "use strict";

    /**
     * @class Element
     * @memberof ixult.ui.core
     * @extends sap.ui.core.Element
     *
     * @summary Abstract `Element` class for the ixult UI5 framework
     *  Used e.g. to identify Controls using the `byId` method
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.ui.core.Element sap.ui.core.Element}
     *
     * @public
     *
     * @since 0.8.2
     */
    var oElement = Element.extend("ixult.ui.core.Element", /** @lends ixult.ui.core.Element.prototype */ {

        /**
         * @summary Constructor for the Filter Element
         * @param {map} mParameters
         *  a map of parameters for the `Filter` element,
         *  containing the properties set into the `metadata`
         *  for this Class
         * @public
         * @hideconstructor
         */
        constructor: function (mParameters) {
            Element.prototype.constructor.apply(this, arguments);

            return this;
        },

        /**
         * @summary Initializes the element instance after creation.
         * @description Applications must not call this hook method directly,
         *  it is called by the framework while the constructor of an element
         *  is executed. Subclasses of Element should override this hook to
         *  implement any necessary initialization.
         * @protected
         */
        init: function () {
            Element.prototype.init.apply(this, arguments);
        }
    });

    /**
     * @function findElement
     * @memberOf ixult.ui.core
     * @summary Finds an Element or UI Control using given parameters
     *  Extends the functionality of functions like `sap.ui.core.Core.byId`
     *  or `sap.ui.core.Element.getElementbyId` (since 1.119)
     * @param {string|map} sId
     *  the unique identifier of a Control; Will be used to directly
     *  find a single Control instance; Can be replaced using the
     *  `Parameters` map instead
     * @param {map} [mParameters]
     *  a parameter map, containing the following attributes
     * @param {sap.ui.core.Control} [mParameters.Parent]
     *  a parent Control that should be used as starting
     *  point to find a child Control
     * @param {string} [mParameters.Type]
     *  Specifies a Control type that is used to identify a
     *  found Control instance by its class type
     * @param {string} [mParameters.UiRole]
     *  the HTML `ui-role` attribute to look for; this will be set
     *  into the HTML element's tag as `data-ui-role` and can be set
     *  into the SAPUI5 Control using `customData` and its
     *  `writeToDom` property, e.g.:
     *  <customData>
     *      <core:CustomData key="ui-role"
     *          value="button-for-specific-task"
     *          writeToDom="true" />
     *  </customData>
     * @returns {sap.ui.core.Element[]|sap.ui.core.Element|undefined}
     *  the Control instance(s), or null if not found
     * @public
     * @static
     */
    oElement.findElement = function (sId, mParameters) {
        // Adjust the parameters
        var mParams = typeof sId === "object" ? sId : mParameters !== undefined ? mParameters : {},
            sElementId = typeof sId === "string" ? sId : undefined;

        /**
         * @summary Recursively finds all child Controls for a given Parent Control
         * @param {sap.ui.Element.Control[]} aChildren
         *  the array of child elements, internally used to
         *  add Child elements during recursive calls
         * @returns {sap.ui.Element.Control[]}
         *  the array of found child Controls
         * @private
         * @internal
         */
        var fnGetChildren = function (aChildren) {
            array(aChildren).loop(oControl => {
                var aControls = oControl.getItems ? oControl.getItems() :
                        oControl.getContent ? oControl.getContent() :
                            oControl.getFormContainers ? oControl.getFormContainers() :
                                oControl.getFormElements ? oControl.getFormElements() :
                                    oControl.getFields ? oControl.getFields() : [],
                    bExists = array(aChildren).grep(oChild => {
                        return oChild.getId() === oControl.getId();
                    }).length > 0;
                if (aControls.length === 0 && !bExists) {
                    aChildren.push(oControl)
                } else {
                    aChildren = aChildren.concat(fnGetChildren(aControls));
                }
            });
            return aChildren;
        }

        // Find the Control by its Identifier
        if (sElementId) {
            var oCore = sap.ui.getCore ? sap.ui.getCore() : null;

            // New functionality, will be used beginning in SAPUI5 version 1.119
            return sap.ui.core.Element.getElementById ?
                sap.ui.core.Element.getElementById(sElementId) :
                // 'Legacy' functionality using the `Core` instance
                oCore && oCore.byId ? oCore.byId(sElementId) :
                    undefined;
        }

        // Find the children if a `Parent` Control has been given
        if (mParams.Parent && mParams.Type) {
            var aChildren = fnGetChildren([mParams.Parent]),
                aResults = array(aChildren).grep(oChild => {
                    return oChild.isA(mParams.Type);
                });
            return aResults.length > 1 ? aResults :
                aResults.length === 1 ? aResults[0] : null;
        }


        // Look for the Control using the `data-ui-role` attribute
        // that can be set into UI5 Controls using `customData`
        if (mParams.UiRole) {
            var sSearchString = "[data-ui-role='" + mParams.UiRole + "']",
                oElement = $(sSearchString);
            if (oElement && oElement[0]) {
                return this.findElement(oElement[0].id);
            } else {
                console.error("Couldn't find element using " +
                    "pattern '" + sSearchString + "'");
            }
        }

        return null;
    }


    return oElement;
});