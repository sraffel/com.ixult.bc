sap.ui.define([
    "sap/ui/core/Core",
    "sap/ui/core/library"
], function (
    Core,
    Library
) {
    "use strict";

    /**
     * @namespace theming
     * @memberOf ixult.ui.core
     *
     * @description Contains classes and helper functions
     *  to get or set theming options and attributes in
     *  SAPUI5 controls, e.g. used to apply Fiori theme
     *  options to controls
     *
     * @public
     */
    return sap.ui.getCore().initLibrary({
        name: "ixult.ui.core.theming",
        noLibraryCSS: true,
        dependencies: [
            "sap.ui.core",
            "sap.m",
        ],
        types: [],
        interfaces: [],
        controls: [],
        elements: [],
        version: "0.8.1"
    });

}, /* bExport= */ false);