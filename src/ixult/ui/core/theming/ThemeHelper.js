sap.ui.define([
    "ixult/base/assert",
    "sap/ui/Device",
    "sap/ui/core/syncStyleClass"
], function (
    assert,
    Device,
    syncStyleClass
) {

    /**
     * @namespace ThemeHelper
     * @memberOf ixult.ui.core.theming
     *
     * @summary Helper class for Fiori Design and Theme related functions
     *
     * @public
     * @static
     */
    var oThemeHelper = {};

    /**
     * @summary Returns the company logo path using the global 'themeroots' setting
     * @returns {string} the path to the company logo image file
     * @public
     * @static
     */
    oThemeHelper.getCompanyLogo = function () {
        try {
            var sThemeName = sap.ui.getCore().getConfiguration().getTheme();
            if (window["sap-ui-config"] && window["sap-ui-config"]["themeroots"]
                && window["sap-ui-config"]["themeroots"][sThemeName]) {
                var sThemeRoot = window["sap-ui-config"]["themeroots"][sThemeName];
            }
            return sThemeRoot.replace("/UI5/", "/Base/baseLib/") +
                sThemeName + "/img/misc/sapCompanyLogo.png";
        } catch (oException) {
            console.error("Company Logo not found.", oException);
            return "";
        }
    }

    /**
     * @summary Updates the 'favIcon' images using the given parameter map
     * @description This method can be used to update the icons of an application with the given
     *  icon map; this usually can be set up in the app's manifest file in the `sap.ui./icons`
     *  section and read at runtime using the statement
     *  `this.getComponent().getManifestEntry("/sap.ui/icons")`
     * @param {map} mIcons
     *  the icon map read from the manifest file or set up manually
     * @param {string} mIcons.favIcon
     *  the source path to the app's favIcon that will e.g. be used to
     *  display in the browser and also be applied to a browser bookmark;
     *  this will be at least required as an icon to set
     * @param {string} [mIcons.phone]
     *  the source path to the icon, that will be displayed on phones;
     *  if not set the 'favIcon' parameter will be used
     * @param {string} [mIcons.phone@2]
     *  the source path to an additional icon, that will be displayed on phones;
     *  this may only apply to Apple devices; if not set the 'phone' or
     *  'favIcon' parameter will be used
     * @param {string} [mIcons.tablet]
     *  the source path to the icon, that will be displayed on tablets;
     *  if not set the 'favIcon' parameter will be used
     * @param {string} [mIcons.tablet@2]
     *  the source path to an additional icon, that will be displayed on tablets;
     *  this may only apply to Apple devices; if not set the 'tablet' or
     *  'favIcon' parameter will be used
     * @returns {void}
     * @public
     * @static
     */
    oThemeHelper.setAppIcons = function (mIcons) {
        if (!assert(mIcons && mIcons.favIcon, "This method need at least a 'favIcon' to set!")) {
            return;
        }

        sap.ui.require(["sap/ushell/components/applicationIntegration/AppLifeCycle"], function (AppLifeCycle) {
            var oMetadataConfig = {
                "homeScreenIconPhone": mIcons["phone"] || mIcons["favIcon"],
                "homeScreenIconPhone@2": mIcons["phone@2"] || mIcons["phone"] || mIcons["favIcon"],
                "homeScreenIconTablet": mIcons["tablet"] || mIcons["favIcon"],
                "homeScreenIconTablet@2": mIcons["tablet@2"] || mIcons["tablet"] || mIcons["favIcon"],
                "favIcon": mIcons["favIcon"],
            }
            AppLifeCycle.getAppMeta().setAppIcons(oMetadataConfig);
        }.bind(this));
    }

    /**
     * @summary Applies the current content density class
     *  (for 'cozy' or 'compact' mode)
     *  to the given control (e.g. a dialog)
     * @param {sap.ui.core.mvc.Controller|ixult.ui.core.mvc.Controller} oController
     *  the view controller class that should most likely call this method;
     *  if specified, the function will try to get the component manifest settings for
     *  'contentDensities' when no current content density has been found in
     *  the rendered view
     * @param {sap.ui.core.mvc.View} oView
     *  the view instance to get the content density class from
     * @param {sap.ui.core.Control} oControl
     *  the given control, e.g. a dialog
     * @returns {void}
     * @public
     * @static
     */
    oThemeHelper.applyContentDensityClass = function (oController, oView, oControl) {
        syncStyleClass(this.getContentDensityClass(oController), oView, oControl);
    }

    /**
     * @summary This method can be called to determine whether the 'sapUiSizeCompact' or 'sapUiSizeCozy'
     *  design mode class should be set, which influences the size appearance of some controls.
     * @param {sap.ui.core.mvc.Controller|ixult.ui.core.mvc.Controller} [oController]
     *  the view controller class that should most likely call this method;
     *  if specified, the function will try to get the component manifest settings for
     *  'contentDensities' when no current content density has been found in
     *  the rendered view
     * @returns {string}
     *  CSS class, either 'sapUiSizeCompact' or 'sapUiSizeCozy'
     * @public
     * @static
     */
    oThemeHelper.getContentDensityClass = function (oController) {
        // Define the supported CSS class strings (to prevent typos below)
        var mClasses = {
            cozy: "sapUiSizeCozy",
            compact: "sapUiSizeCompact"
        };

        // This variable determines, whether a Device should be blessed with the 'compact' Design mode;
        // This should apply to Smartphones and Desktop PCs or Laptops without touch
        var bIsCompactDensityDevice = Device && (Device.system.phone || Device.system.desktop || !Device.support.touch),
            sContentDensityClass = "";
        try {
            // Get the 'compact' or 'cozy' styles from the document.body first
            // (if set e.g. via the Fiori Launchpad)
            var oBody = $(document.body);
            if (oBody && oBody.hasClass(mClasses.compact)) {
                sContentDensityClass = mClasses.compact;
            } else if (oBody && oBody.hasClass(mClasses.cozy)) {
                sContentDensityClass = mClasses.cozy;
            } else if (oController && oController.getComponent) {
                // Otherwise: Look up the supported content densities in the
                // manifest of the App (if given by the View Controller Parameter)
                var mContentDensities = oController.getComponent().getManifestEntry("/sap.ui5/contentDensities");
                if (mContentDensities && mContentDensities.compact === true && bIsCompactDensityDevice) {
                    // Here we also want to set the 'compact' mode only on applicable devices
                    sContentDensityClass = mClasses.compact;
                } else if (mContentDensities && mContentDensities.cozy === true) {
                    sContentDensityClass = mClasses.cozy;
                }
            } else if (bIsCompactDensityDevice) {
                // Otherwise: Set the 'compact' or 'cozy' mode depending on the used device
                sContentDensityClass = mClasses.compact;
            } else {
                sContentDensityClass = mClasses.cozy;
            }
        } catch (oException) {
            console.error("Could not determine content density. Falling back to defaults.", oException);
            // And that's just a Fallback
            sContentDensityClass = mClasses.cozy;
        }

        return sContentDensityClass;
    }

    return oThemeHelper;
});