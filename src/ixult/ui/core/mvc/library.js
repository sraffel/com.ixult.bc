sap.ui.define([
    "sap/ui/core/Core",
    "sap/ui/core/library"
], function (
    Core,
    Library
) {
    "use strict";

    /**
     * @namespace mvc
     * @memberOf ixult.ui.core
     *
     * @description Contains Model-View-Controlles
     *  classes and functions, e.g. UI5 application
     *  controllers
     *
     * @public
     */
    return sap.ui.getCore().initLibrary({
        name: "ixult.ui.core.mvc",
        noLibraryCSS: true,
        dependencies: [
            "sap.ui.core",
            "sap.m",
        ],
        types: [],
        interfaces: [],
        controls: [],
        elements: [],
        version: "0.8.1"
    });

}, /* bExport= */ false);