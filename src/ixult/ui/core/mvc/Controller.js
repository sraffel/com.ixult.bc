sap.ui.define([
    "ixult/base/assert",
    "ixult/base/util/array",
    "ixult/ui/core/theming/ThemeHelper",
    "sap/ui/Device",
    "sap/ui/core/Component",
    "sap/ui/core/Fragment",
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/syncStyleClass"
], function (
    assert,
    array,
    ThemeHelper,
    Device,
    Component,
    Fragment,
    Controller,
    syncStyleClass
) {
    "use strict";

    /**
     * @class Controller
     * @memberof ixult.ui.core.mvc
     * @extends sap.ui.core.mvc.Controller
     *
     * @classdesc UI core Controller class with
     *  general getter, setter and other common methods
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.ui.core.mvc.Controller sap.ui.core.mvc.Controller}
     *
     * @public
     */
    return Controller.extend("ixult.ui.core.mvc.Controller", /** @lends ixult.ui.core.mvc.Controller */ {
        _oComponent: null,
        _oEventBus: null,
        _oRouter: null,
        _sContentDensityClass: undefined,

        /**
         * @summary Called when a controller is instantiated and
         *  its View controls (if available) are already created.
         * @description Can be used to modify the View before it is
         *  displayed, to bind event handlers and do other one-time
         *  initialization.
         * @see {@link https://ui5.sap.com/sdk/#/api/sap.ui.core.mvc.Controller sap.ui.core.mvc.Controller}
         * @see {@link https://ui5.sap.com/sdk/#/api/sap.ui.base.Event sap.ui.base.Event}
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onInit: function (oEvent) {
            this._oComponent = Component.getOwnerComponentFor(this.getView());
            this._oEventBus = this._oComponent.getEventBus();
            this._oRouter = this._oComponent.getRouter();

            // Attach Event Handler (if they are implemented in subclasses)
            if (this.onRouteMatched) {
                this.getComponent().attachRouteMatched(this.onRouteMatched, this);
            }
            if (this.onTargetMatched) {
                this.getComponent().attachTargetMatched(this.onTargetMatched, this);
            }
            if (this.onKeyPressed) {
                this.getComponent().attachKeyPressed(this.onKeyPressed, this);
            }
        },

        /**
         * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
         * (NOT before the first rendering! onInit() is used for that one!).
         * @param {sap.ui.base.Event} oEvent - the UI event instance
         * @returns {void}
         * @protected
         */
        onBeforeRendering: function (oEvent) {
            // nothing to do here at the moment
        },

        /**
         * Called when the View has been rendered
         * Post-rendering manipulations of the HTML could be done here.
         * This hook is the same one that SAPUI5 controls get after being rendered.
         * @param {sap.ui.base.Event} oEvent - the UI event instance
         * @returns {void}
         * @protected
         */
        onAfterRendering: function (oEvent) {
            // Set the content density class based on the manifest settings and the used device
            // (i.e. switch to compact mode if the device is a phone or desktop and if enabled)
            this.syncContentDensityClass();
        },

        /**
         * Handles the 'routeMatched' event thrown by the Router; Allows for custom Postprocessing
         * when the View has been called by a Routing Event through a matched Route defined in the
         * manifest.json file;
         * Method Stub, will be attached to this Controller when implemented in deriving Classes
         * @function
         * @name ixult.ui.core.mvc.Controller.prototype.onRouteMatched
         * @param {sap.ui.base.Event} oEvent - the routing Event
         * @returns {void}
         * @protected
         * @abstract
         */

        /**
         * Handles the 'targetMatched' event thrown by the Router; Allows for custom Postprocessing
         * when the View has been called directly as a Target by the Router or indirectly using
         * the 'navTo' routing method;
         * Method Stub, will be attached to this Controller when implemented in deriving Classes
         * @function
         * @name ixult.ui.core.mvc.Controller.prototype.onTargetMatched
         * @param {sap.ui.base.Event} oEvent
         *  the routing Event
         * @returns {void}
         * @protected
         * @abstract
         */

        /**
         * Handle the 'keyPressed' event that is fired - unsurprisingly :-) - when a key has been pressed;
         * Method Stub, will be attached to this Controller when implemented in deriving Classes
         * @function
         * @name ixult.ui.core.mvc.Controller.prototype.onKeyPressed
         * @param {sap.ui.base.Event} oEvent
         *  the UI event containing the original jQuery 'keydown' Event
         * @returns {void}
         * @protected
         * @abstract
         */

        /**
         * @summary Returns the Applications Component
         * @returns {sap.ui.core.UIComponent}
         *  the App's Component
         * @protected
         * @instance
         */
        getComponent: function () {
            return this._oComponent ? this._oComponent : this.getOwnerComponent();
        },

        /**
         * @summary Returns the Router for the Application
         * @returns {ixult.m.routing.Router|sap.m.routing.Router|sap.ui.core.routing.Router}
         *  the Router instance
         * @protected
         */
        getRouter: function () {
            return this._oRouter;
        },

        /**
         * @summary Returns a local JSON data model for
         *  the given name or the main data model instance
         * @description Convenience method redirecting
         *  to the Component's 'getModel' method
         * @param {string} [sName]
         *  the model name; optionally
         * @returns {sap.ui.model.json.JSONModel|ixult.ui.model.Model}
         *  the named JSON model if given,
         *  or the data model instance
         * @protected
         */
        getModel: function (sName) {
            return this.getComponent().getModel(sName);
        },

        /**
         * @summary Creates a local JSONModel and
         *  sets it into the Component
         * @description Convenience method that
         *  forwards into the core/ui/Model
         * @see {@link ixult.ui.model.Model#setModel}
         * @param {map} mParameters
         *  a parameter map with the following attributes
         * @param {string} mParameters.sName
         *  the model name
         * @param {object} [mParameters.oData]
         *  the OData to set into the model, optionally
         * @param {boolean} [mParameters.bMerge]
         *  true, if the oData should be merged with
         *  existing data, false otherwise; false by default
         * @returns {sap.ui.model.json.JSONModel|null}
         *  the JSONModel instance, for method chaining or null
         * @protected
         */
        setModel: function (mParameters) {
            return this.getComponent().getModel().setModel(mParameters);
        },

        /**
         * @summary Looks for a UI Control in the current view
         *  based on the given parameters
         * @param {map} mParameters
         *  a parameter map, containing the following attributes
         * @param {string} mParameters.sId
         *  the SAPUI5 Control Id to look for
         * @param {string} mParameters.sUiRole
         *  the HTML `ui-role` attribute to look for; this will be set
         *  into the HTML element's tag as `data-ui-role` and can be set
         *  into the SAPUI5 Control using `customData` and its
         *  `writeToDom` property, e.g.:
         *  <customData>
         *      <core:CustomData key="ui-role"
         *          value="button-for-specific-task"
         *          writeToDom="true" />
         *  </customData>
         * @returns {sap.ui.core.Control}
         *  The UI Control instance, if found
         * @public
         */
        findControl: function (mParameters) {
            var oControl = null,
                oElement = null;

            // There have to be parameters to find the Control
            if (!assert(mParameters, "findControl: No parameters given.")) {
                return oControl;
            }

            // Look for the Control by its SAPUI5 Id
            if (mParameters.sId) {
                oControl = this.byId(mParameters.sId) ||
                    sap.ui.getCore().byId(mParameters.sId);
            }

            // Look for the Control using the `data-ui-role` attribute
            // that can be set into UI5 Controls using `customData`
            if (oControl && mParameters.sUiRole) {
                oElement = $("[data-ui-role='" + mParameters.sUiRole + "']");
                if (oElement && oElement[0]) {
                    oControl = sap.ui.getCore().byId(oElement[0].id);
                }
            }

            return oControl;
        },

        /**
         * @summary Retrieves a text from the language model
         * @description Convenience method that forwards to
         *  the {ixult.ui.model.Model} if set; If no `ixult`
         *  Model is used, the method relies on the `i18n`
         *  Model and uses its ResourceBundle
         * @param {string} sTextId
         *  the text id to search for in the language model
         * @returns {string}
         *  the text for the given id
         * @public
         */
        getText: function (sTextId) {
            var sText = sTextId;

            if (this.getModel() && this.getModel().getText) {
                sText = this.getModel().getText(sTextId);
            } else if (this.getModel("i18n")) {
                var oBundle = this.getModel("i18n").getResourceBundle();
                sText = oBundle && oBundle.hasText(sTextId) ?
                    oBundle.getText(sTextId) : sTextId;
            } else {
                console.warn("Could not retrieve text for text id '" +
                    sTextId + "'. Using it as text instead.");
                sText = sTextId;
            }

            return sText;
        },

        /**
         * @summary Returns the local storage handler
         * @description Convenience method that
         *  forwards into the core/ui/Model
         * @returns {ixult.util.LocalStorage}
         *  the local storage handler
         * @public
         */
        getLocalStorage: function () {
            return this.getModel().getLocalStorage();
        },

        /**
         * @summary Adds the style class for the desired
         *  content density ('cozy' or 'compact') to the View
         * @returns {void}
         * @protected
         */
        syncContentDensityClass: function () {
            // Return when there's no View instance or the
            // content density class has already been synced
            if (!this.getView && !this.getView() || this._sContentDensityClass) {
                return;
            }

            // Get the current Content Density Class and sync the View to it
            this._sContentDensityClass = ThemeHelper.getContentDensityClass(this);
            if (!this.getView().hasStyleClass(this._sContentDensityClass)) {
                this.getView().addStyleClass(this._sContentDensityClass);
            }
        },

        /**
         * @summary Applies the current content density
         *  class (for 'cozy' or 'compact' mode)
         *  to the given control (e.g. a dialog)
         * @description Convenience method forwarding
         *  to a corresponding ThemeHelper method
         * @param {sap.ui.core.Control} oControl
         *  the given control, e.g. a Dialog
         * @returns {void}
         * @protected
         */
        applyContentDensityClass: function (oControl) {
            ThemeHelper.applyContentDensityClass(this, this.getView(), oControl);
        },

        /**
         * @summary Creates a Dialog from the given XML fragment information
         * @param {map} mParameters
         *  a parameter map, containing the following attributes:
         * @param {string} mParameters.sFragmentName
         *  the XML fragment's name
         * @param {string} mParameters.sId
         *  the Dialog-Id
         * @param {sap.ui.core.mvc.Controller} [mParameters.oController]
         *  the UI controller of the fragment; or 'this'
         * @param {sap.ui.core.mvc.View} [mParameters.oView]
         *  the containing view, optionally;
         *  Should be omitted if `oController` is given as an object
         * @returns {Promise}
         *  a Promise Handler which resolves when
         *  the Dialog was successfully created
         * @protected
         */
        createDialog: function (mParameters) {
            var oDeferred = $.Deferred(),
                oView = mParameters.oView ? mParameters.oView : this.getView();

            if (!mParameters || !mParameters.sFragmentName) {
                oDeferred.reject(new Error("Can not create Dialog without parameters!"));
                return oDeferred.promise();
            }
            mParameters.sId = oView.createId(mParameters.sId);
            mParameters.oController = mParameters.oController || this;

            // Create the Dialog based on the XML Fragment
            Fragment.load({
                id: mParameters.sId,
                name: mParameters.sFragmentName,
                type: "XML",
                controller: mParameters.oController
                // containingView: mParameters.oView
            }).then(function (oDialog) {
                // Switch the dialog to compact mode if the hosting view has compact mode
                syncStyleClass(ThemeHelper.getContentDensityClass(this), oView, oDialog);

                // Remove the dialog if it exists
                // and add the new instance to the view
                oView.removeDependent(mParameters.sId);
                oView.addDependent(oDialog);

                // Resolve the Deferred Handler when the Dialog was created
                // This can be handled in calling methods using 'then' to open the Dialog
                oDeferred.resolve(oDialog);
            }.bind(this));

            // Return the Promise object
            return oDeferred.promise();
        }

    });
});