sap.ui.define([
    "sap/ui/core/Core",
    "sap/ui/core/library"
], function (
    Core,
    Library
) {
    "use strict";

    /**
     * @namespace commons
     * @memberOf ixult.suite.ui
     *
     * @description Namespace for common suite UI controls,
     *  containing e.g. Chart Container Controls to allow
     *  displaying charts in SAPUI5 apps
     *
     * @public
     */
    return sap.ui.getCore().initLibrary({
        name: "ixult.suite.ui.commons",
        noLibraryCSS: true,
        dependencies: [
            "sap.m",
            "sap.suite.ui.commons",
            "sap.viz.ui5.controls"
        ],
        types: [],
        interfaces: [],
        controls: [
            "ixult.suite.ui.commons.ChartContainer"
        ],
        elements: [],
        version: "0.8.1"
    });

}, /* bExport= */ false);