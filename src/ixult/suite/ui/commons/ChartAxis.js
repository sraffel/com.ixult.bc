sap.ui.define([
    "sap/base/util/uid",
    "sap/ui/base/ManagedObject",
    "ixult/base/assert",
    "ixult/base/util/array",
    "ixult/base/util/DateHelper",
    "ixult/base/util/LibraryHelper"
], function (
    uid,
    ManagedObject,
    assert,
    array,
    DateHelper,
    LibraryHelper
) {
    "use strict";
    const Library = LibraryHelper.register("ixult/suite/ui/commons");

    /**
     * @class ChartAxis
     * @extends sap.ui.base.ManagedObject
     * @memberOf ixult.suite.ui.commons
     *
     * @summary Axis definition for the Chart in a
     *  {ixult.suite.ui.commons.ChartContainer}
     *
     * @public
     */
    return ManagedObject.extend("ixult.suite.ui.commons.ChartAxis", /** @lends ixult.suite.ui.commons.ChartAxis.prototype */ {

        // Metadata for the UI element; Used to define custom properties
        metadata: {
            properties: {
                /**
                 * @name ixult.suite.ui.commons.ChartAxis#type
                 * @type {string}
                 * @description The type of Axis for the Chart;
                 *  can be either `Dimension` or `Measure`
                 * @public
                 */
                type: {
                    type: "string",
                    defaultValue: ""
                },
                /**
                 * @name ixult.suite.ui.commons.ChartAxis#uid
                 * @type {string}
                 * @description the uid descriptor for the Chart
                 *  content to define the role of an Axis; can be
                 *  either `categoryAxis` or `color` for Dimensions,
                 *  or `valueAxis` or `size` for Measures
                 * @public
                 */
                uid: {
                    type: "string",
                    defaultValue: ""
                },
                /**
                 * @name ixult.suite.ui.commons.ChartAxis#name
                 * @type {object|string}
                 * @description the Text that should be displayed
                 *  on this axis and/or the legend for the Chart
                 * @public
                 */
                name: {
                    type: "string"
                },
                /**
                 * @name ixult.suite.ui.commons.ChartAxis#value
                 * @type {object|string}
                 * @description the Value that should be
                 *  displayed in the Chart
                 * @public
                 */
                value: {
                    type: "string"
                }
            }
        },

        /**
         * @summary Returns the value as a binding string to
         *  be used by a surrounding ChartContainer VizFrame
         * @returns {string}
         *  the bound value as a string in the form of a
         *  binding, i.e. `{path}`
         * @public
         * @override
         */
        getValue: function () {
            var oBindingInfo = this.getBindingInfo("value"),
                oBinding = oBindingInfo.parts[0];

            return "{" + oBinding.path + "}";
        }

    });
});