sap.ui.define([
    "sap/base/util/uid",
    "sap/m/Column",
    "sap/m/ColumnListItem",
    "sap/m/CustomListItem",
    "sap/m/OverflowToolbar",
    "sap/m/ScrollContainer",
    "sap/m/StandardListItem",
    "sap/m/Label",
    "sap/m/List",
    "sap/m/MultiComboBox",
    "sap/suite/ui/commons/ChartContainer",
    "sap/ui/core/Fragment",
    "sap/ui/core/ListItem",
    "sap/ui/export/Spreadsheet",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/resource/ResourceModel",
    "sap/viz/ui5/controls/common/feeds/FeedItem",
    "sap/viz/ui5/data/DimensionDefinition",
    "sap/viz/ui5/data/FlattenedDataset",
    "sap/viz/ui5/data/MeasureDefinition",
    "ixult/base/assert",
    "ixult/base/util/array",
    "ixult/base/util/DateHelper",
    "ixult/base/util/LibraryHelper",
    "ixult/viz/ui5/controls/library"
], function (
    uid,
    Column,
    ColumnListItem,
    CustomListItem,
    OverflowToolbar,
    ScrollContainer,
    StandardListItem,
    Label,
    List,
    MultiComboBox,
    ChartContainer,
    Fragment,
    ListItem,
    Spreadsheet,
    Filter,
    FilterOperator,
    JSONModel,
    ResourceModel,
    FeedItem,
    DimensionDefinition,
    FlattenedDataset,
    MeasureDefinition,
    assert,
    array,
    DateHelper,
    LibraryHelper,
    VizControlsLibrary
) {
    "use strict";
    const Library = LibraryHelper.register("ixult/suite/ui/commons").loadStyleSheet();

    /**
     * @class ChartContainer
     * @extends sap.suite.ui.commons.ChartContainer
     * @memberOf ixult.suite.ui.commons
     *
     * @summary ChartContainer UI control for an advanced display
     *  of charts with additional options
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.suite.ui.commons.ChartContainer sap.suite.ui.commons.ChartContainer}
     *
     * @public
     */
    return ScrollContainer.extend("ixult.suite.ui.commons.ChartContainer", /** @lends ixult.suite.ui.commons.ChartContainer.prototype */ {
        // The renderer to use; Important: Leave this empty,
        // so that the default Renderer is used
        renderer: {},

        // Metadata for the UI element; Used to define custom properties
        metadata: {
            properties: {
                data: {
                    /**
                     * @name ixult.suite.ui.commons.ChartContainer#data
                     * @type {sap.ui.model.Model|object|*}
                     * @description The data model containing the Chart data
                     * @public
                     */
                    type: "object",
                    group: "Misc"
                },
                /**
                 * @name ixult.suite.ui.commons.ChartContainer#chartType
                 * @type {ixult.viz.ui5.controls.VizType}
                 * @default `Column`
                 * @description the Chart type, e.g.
                 *  `Column`, `Line`, `Bar`, `Pie` or `Donut`
                 * @public
                 */
                chartType: {
                    type: "ixult.viz.ui5.controls.VizType",
                    group: "Misc",
                    defaultValue: "Column"
                },
                /**
                 * @name ixult.suite.ui.commons.ChartContainer#showTable
                 * @type {boolean}
                 * @default `true`
                 * @description Shows or hides the Table icon to
                 *  allow switching between Chart and Table view.
                 * @public
                 */
                showTable: {
                    type: "boolean",
                    group: "Misc",
                    defaultValue: true
                },
                /**
                 * @name ixult.suite.ui.commons.ChartContainer#showNotes
                 * @type {boolean}
                 * @default `true`
                 * @description Shows or hides an additional view that
                 *  can contain further information regarding the statistics
                 * @public
                 */
                showNotes: {
                    type: "boolean",
                    group: "Misc",
                    defaultValue: true
                },
                /**
                 * @name ixult.suite.ui.commons.ChartContainer#showFilter
                 * @type {boolean}
                 * @default `true`
                 * @description Shows a Filter icon that opens a Popover
                 *  with filter options for the current data.
                 * @public
                 */
                showFilter: {
                    type: "boolean",
                    group: "Misc",
                    defaultValue: true
                },
                /**
                 * @name ixult.suite.ui.commons.ChartContainer#showExport
                 * @type {boolean}
                 * @default `true`
                 * @description Shows an Icon to allow
                 *  exporting the data as a Spreadsheet.
                 * @public
                 */
                showExport: {
                    type: "boolean",
                    group: "Misc",
                    defaultValue: true
                },
                /**
                 * @name ixult.suite.ui.commons.ChartContainer#showPersonalization
                 * @type {boolean}
                 * @default `false`
                 * @description Set to true to display the personalization icon.
                 *  Set to false to hide it
                 * @public
                 * @public
                 */
                showPersonalization: {
                    type: "boolean",
                    group: "Misc",
                    defaultValue: false
                },
                /**
                 * @name ixult.suite.ui.commons.ChartContainer#showFullScreen
                 * @type {boolean}
                 * @default `false`
                 * @description Set to true to display the full screen icon.
                 *  Set to false to hide it
                 * @public
                 */
                showFullScreen: {
                    type: "boolean",
                    group: "Misc",
                    defaultValue: false
                },
                /**
                 * @name ixult.suite.ui.commons.ChartContainer#fullScreen
                 * @type {boolean}
                 * @default `false`
                 * @description Display the chart and the toolbar
                 *  in full screen or normal mode.
                 * @public
                 */
                fullScreen: {
                    type: "boolean",
                    group: "Misc",
                    defaultValue: false
                },
                /**
                 * @name ixult.suite.ui.commons.ChartContainer#showLegend
                 * @type {boolean}
                 * @default `true`
                 * @description Set to true to display the charts' legends.
                 *  Set to false to hide them. See also showLegendButton.
                 * @public
                 */
                showLegend: {
                    type: "boolean",
                    group: "Misc",
                    defaultValue: true
                },
                /**
                 * @name ixult.suite.ui.commons.ChartContainer#title
                 * @type {string}
                 * @description String shown if there are no dimensions to display.
                 * @public
                 */
                title: {
                    type: "string",
                    group: "Misc",
                    defaultValue: ""
                },
                /**
                 * @name ixult.suite.ui.commons.ChartContainer#autoAdjustHeight
                 * @type {boolean}
                 * @default `false`
                 * @description Determine whether to stretch the chart height to
                 *  the maximum possible height of ChartContainer's parent container.
                 *  As a prerequisite, the parent container needs to have a fixed
                 *  value height or be able to determine height from its parent.
                 * @public
                 */
                autoAdjustHeight: {
                    type: "boolean",
                    group: "Misc",
                    defaultValue: false
                },
                /**
                 * @name ixult.suite.ui.commons.ChartContainer#showZoom
                 * @type {boolean}
                 * @default `true`
                 * @description Set to true to display zoom icons.
                 *  Set to false to hide them.
                 * @public
                 */
                showZoom: {
                    type: "boolean",
                    group: "Misc",
                    defaultValue: true
                },
                /**
                 * @name ixult.suite.ui.commons.ChartContainer#showLegendButton
                 * @type {boolean}
                 * @default `true`
                 * @description Set to true or false to display or hide a button
                 *  for controlling the visibility of the chart's legend. Please
                 *  be aware that setting this property to true indirectly is
                 *  setting showLegend to false. If you need to hide the button
                 *  but to show the legend, you need to set showLegend at a
                 *  later point in time (onBeforeRendering). The execution order
                 *  of the combined properties is not guaranteed by the control.
                 * @public
                 */
                showLegendButton: {
                    type: "boolean",
                    group: "Misc",
                    defaultValue: true
                },
                /**
                 * @name ixult.suite.ui.commons.ChartContainer#showSelectionDetails
                 * @type {boolean}
                 * @default `false`
                 * @description Set to true to display the 'Details' button that
                 *  opens a popup dialog with details about the selected data
                 *  from the VizFrame based chart.
                 * @public
                 * @since 1.48.0
                 */
                showSelectionDetails: {
                    type: "boolean",
                    group: "Behavior",
                    defaultValue: false
                },
                /**
                 * @name ixult.suite.ui.commons.ChartContainer#wrapLabels
                 * @type {boolean}
                 * @default `false`
                 * @description Set to true to wrap text labels in the dialog
                 *  that opens when the user clicks or taps the 'Details' button.
                 * @public
                 * @since 1.58.0
                 */
                wrapLabels: {
                    type: "boolean",
                    group: "Misc",
                    defaultValue: false
                },
                /**
                 * @name ixult.suite.ui.commons.ChartContainer#enableScroll
                 * @type {boolean}
                 * @default `true`
                 * @description If set to <code>true</code>, the Container control
                 *  has its own scroll bar, with the scrolling taking place within
                 *  the Container control itself
                 * @public
                 */
                enableScroll: {
                    type: "boolean",
                    group: "Misc",
                    defaultValue: true
                },
                /**
                 * @name ixult.suite.ui.commons.ChartContainer#width
                 * @type {sap.ui.core.CSSSize}
                 * @default `100%`
                 * @description Defines the width of the Container
                 * @public
                 */
                width: {
                    type: "sap.ui.core.CSSSize",
                    defaultValue: "100%"
                }
            },
            aggregations: {
                /**
                 * @name  ixult.suite.ui.commons.ChartContainer#axis
                 * @type {ixult.suite.ui.commons.ChartAxis[]}
                 * @description the Axis definitions that should
                 *  make up the Dimensions and Values for the Chart
                 *  content; can be either `Dimension` or `Measure`
                 * @public
                 */
                axis: {
                    type: "ixult.suite.ui.commons.ChartAxis",
                    multiple: true,
                    singularName: "axis",
                    bindable: "bindable",
                    selector: "#{id}",
                    dnd: false
                },
                /**
                 * @name  ixult.suite.ui.commons.ChartContainer#columns
                 * @type {ixult.suite.ui.commons.ChartColumn[]}
                 * @description the Column definitions for the Table Control
                 *  that cane be displayed alongside the Chart
                 * @public
                 */
                columns: {
                    type: "ixult.suite.ui.commons.ChartColumn",
                    multiple: true,
                    singularName: "column",
                    bindable: "bindable",
                    selector: "#{id}",
                    dnd: false
                },
                /**
                 * @name  ixult.suite.ui.commons.ChartContainer#notes
                 * @type {sap.ui.core.Control[]}
                 * @description additional content that can
                 *  provide further information
                 * @public
                 */
                notes: {
                    type: "sap.ui.core.Control",
                    multiple: true,
                    singularName: "note",
                    bindable: "bindable",
                    selector: "#{id}",
                    dnd: false
                }
            }
        },

        // Attributes
        _oView: null,
        _oToolbar: null,
        _oPopover: null,
        _oVizFrame: null,
        _oTable: null,
        _oNotes: null,

        /**
         * @constructor
         * @summary Creates a new ChartContainer Control
         * @param {string} sId
         *  ID for the new control, generated automatically if no ID is given
         * @param {map|object} mSettings
         *  Initial settings for the new Control
         * @public
         * @hideconstructor
         */
        constructor: function (sId, mSettings) {
            ScrollContainer.prototype.constructor.apply(this, arguments);
        },

        /**
         * @summary Initializes the ChartContainer Control
         * @returns {void}
         * @protected
         */
        init: function () {
            ScrollContainer.prototype.init.apply(this, arguments);
        },

        /**
         * @summary Handles the `beforeRendering` Event for the UI Control,
         *  that is executed when the DOM elements are created but not yet shown
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onBeforeRendering: function (oEvent) {
            // Return if the content has already been rendered
            if (this.getContent() && this.getContent().length > 0) {
                oEvent.preventDefault();
                return;
            }
            ScrollContainer.prototype.onBeforeRendering.apply(this, arguments);

            // Save a reference to the surrounding view
            if (!this._oView) {
                this._oView = Library.getViewFor(this);
            }

            // Load the XML fragment containing the ChartContainer
            // template and set its contents into this Control
            Fragment.load({
                name: "ixult.suite.ui.commons.ChartContainer",
                type: "XML",
                controller: this
            }).then(function (oFragment) {
                // Set the texts into the Fragment
                oFragment.setModel(new ResourceModel({
                    bundle: Library.getResourceBundle()
                }), "messagebundle");

                // Apply the properties into the Fragment
                oFragment.setModel(new JSONModel({
                    autoAdjustHeight: this.getAutoAdjustHeight(),
                    enableScroll: this.getEnableScroll(),
                    fullScreen: this.getFullScreen(),
                    showFilter: this.getShowFilter(),
                    showExport: this.getShowExport(),
                    showFullScreen: this.getShowFullScreen(),
                    showLegend: this.getShowLegend(),
                    showLegendButton: this.getShowLegendButton(),
                    showPersonalization: this.getShowPersonalization(),
                    showSelectionDetails: this.getShowSelectionDetails(),
                    showZoom: this.getShowZoom(),
                    showTable: this.getShowTable(),
                    showNotes: this.getShowNotes(),
                    title: this.getTitle(),
                    width: this.getWidth(),
                    wrapLabels: this.getWrapLabels(),
                    chartType: this.getChartType(),
                    chartIcon: this.getChartIcon(),
                    tableIcon: this.getTableIcon()
                }), "properties");

                // Insert the Fragment's Contents into this Container
                this.removeAllContent();
                this.addContent(oFragment);

                // Create the Chart and Table contents
                this.createContent(oFragment);
            }.bind(this));
        },

        /**
         * @summary Handles the `afterRendering` Event for the UI Control,
         *  that is executed when the Control is completely rendered and shown
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onAfterRendering: function (oEvent) {
            // ScrollContainer.prototype.onAfterRendering.apply(this, arguments);
        },

        /**
         * @summary Creates the chart and table view contents
         * @param {sap.ui.core.Fragment} oFragment
         *  the XML Fragment containing the Container and its contents
         * @returns {void}
         * protected
         */
        createContent: function (oFragment) {
            // Save the Content area's Controls
            var oChartContainer = oFragment.getContent()[0],
                oChartContainerVizContent = oChartContainer.getContent()[0],
                oChartContainerTableContent = oChartContainer.getContent()[1],
                oChartContainerNotesContent = oChartContainer.getContent()[2],
                oChartContentBox = oChartContainerVizContent.getContent();

            this._oToolbar = oChartContainer.getToolbar();
            this._oPopover = oChartContentBox.getItems()[0];
            this._oVizFrame = oChartContentBox.getItems()[1];
            this._oTable = oChartContainerTableContent.getContent();
            this._oNotes = oChartContainerNotesContent.getContent();

            // Get the bound data for Chart and Table
            var oDataBindingInfo = this.getBindingInfo("data"),
                oDataModel = oDataBindingInfo.binding.getModel(),
                sDataPath = oDataBindingInfo.binding.getPath(),
                oChartModel = new JSONModel({
                    results: oDataModel.getProperty(sDataPath)
                });

            // Evaluate the chart axis properties
            var aChartAxis = array(this.getAggregation("axis")),
                aChartDimensions = array([]),
                aChartMeasures = array([]),
                aChartFeeds = array([]);
            aChartAxis.loop(function (oAxis) {
                if (oAxis.getType() === "Measure") {
                    aChartMeasures.push(new MeasureDefinition({
                        name: oAxis.getName(),
                        value: oAxis.getValue()
                    }));
                } else if (oAxis.getType() === "Dimension") {
                    aChartDimensions.push(new DimensionDefinition({
                        name: oAxis.getName(),
                        value: oAxis.getValue()
                    }));
                }
                aChartFeeds.push({
                    uid: oAxis.getUid(),
                    type: oAxis.getType(),
                    value: oAxis.getName()
                });
            }.bind(this));

            // Build the chart
            this._oVizFrame.destroyDataset();
            this._oVizFrame.setDataset(new FlattenedDataset({
                dimensions: aChartDimensions,
                measures: aChartMeasures,
                data: {
                    path: "/results"
                }
            }));
            this._oVizFrame.setModel(oChartModel);
            this._oVizFrame.setVizProperties({
                plotArea: {
                    dataLabel: {
                        visible: true
                    }
                },
                legend: {
                    title: {
                        visible: false
                    }
                },
                title: {
                    visible: false,
                    text: this.getTitle()
                }
            });
            this._oVizFrame.removeAllFeeds();
            aChartFeeds.loop(function (oFeed) {
                this._oVizFrame.addFeed(new FeedItem({
                    uid: oFeed.uid,
                    type: oFeed.type,
                    values: [oFeed.value]
                }));
            }.bind(this));
            if (this._oPopover) {
                this._oPopover.connect(this._oVizFrame.getVizUid());
            }

            // Create the data table columns and cells
            var aChartColumns = array(this.getAggregation("columns") || []),
                aTableCells = array([]);
            if (this.getShowTable() && this._oTable && aChartColumns.length > 0) {
                this._oTable.removeAllColumns();
                aChartColumns.loop(function (oColumn) {
                    this._oTable.addColumn(new Column({
                        header: new Label({
                            text: oColumn.getText()
                        }),
                        mergeDuplicates: oColumn.getMergeDuplicates()
                    }));
                    aTableCells.push(new Label({
                        text: oColumn.getValue()
                    }));
                }.bind(this));
                this._oTable.bindItems("/results", new ColumnListItem({
                    type: "Inactive",
                    cells: aTableCells
                }));
                this._oTable.setModel(oChartModel);
            }

            // Insert custom content into the additional `notes` section
            var aNoteContents = array(this.getAggregation("notes") || []);
            if (this.getShowNotes() && this._oNotes && aNoteContents.length > 0) {
                this._oNotes.removeAllItems();
                aNoteContents.loop(function (oNote) {
                    this._oNotes.addItem(oNote);
                }.bind(this));
            }
        },

        /**
         * @summary Returns the icon displayed for the chart view;
         *  changes depending on the chart type
         * @returns {string}
         *  the chart icon string
         * @protected
         */
        getChartIcon: function () {
            switch (this.getChartType()) {
                case VizControlsLibrary.VizType.Column:
                    return "sap-icon://bar-chart";
                case VizControlsLibrary.VizType.Bar:
                case VizControlsLibrary.VizType.Bubble:
                case VizControlsLibrary.VizType.Donut:
                case VizControlsLibrary.VizType.Line:
                case VizControlsLibrary.VizType.Pie:
                    return "sap-icon://" + this.getChartType().toLowerCase() + "-chart";
                case VizControlsLibrary.VizType.StackedBar:
                    return "sap-icon://horizontal-stacked-chart";
                case VizControlsLibrary.VizType.StackedColumn:
                    return "sap-icon://vertical-stacked-chart";
                default:
                    return "sap-icon://chart-axis";
            }
        },

        /**
         * @summary Returns the table view icon
         * @returns {string}
         *  the icon string for the table icon,
         *  `sap-icon://table-view` by default
         * @protected
         */
        getTableIcon: function () {
            return "sap-icon://table-view";
        },

        /**
         * @summary Handles the `filterButtonPress` Event
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onFilterButtonPress: function (oEvent) {
            // debugger;
        },

        /**
         * @summary Handles the `exportButtonPress` Event
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onExportButtonPress: function (oEvent) {
            // debugger;
        },

        /**
         * @summary Handles the `fullScreenButtonPress` Event
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onFullScreenButtonPress: function (oEvent) {
            // debugger;
        },

        /**
         * @summary Handles the `vizFrameDataSelect` Event
         * @param {sap.ui.base.Event} oEvent
         *  the UI event instance
         * @returns {void}
         * @protected
         */
        onVizFrameDataSelect: function (oEvent) {
            // debugger;
        }


    });
});