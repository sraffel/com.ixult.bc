sap.ui.define([
    "sap/base/util/uid",
    "sap/ui/base/ManagedObject",
    "ixult/base/assert",
    "ixult/base/util/array",
    "ixult/base/util/DateHelper",
    "ixult/base/util/LibraryHelper"
], function (
    uid,
    ManagedObject,
    assert,
    array,
    DateHelper,
    LibraryHelper
) {
    "use strict";
    const Library = LibraryHelper.register("ixult/suite/ui/commons");

    /**
     * @class ChartColumn
     * @extends sap.ui.base.ManagedObject
     * @memberOf ixult.suite.ui.commons
     *
     * @summary Column definition for a Table inside a
     *  {ixult.suite.ui.commons.ChartContainer}
     *
     * @public
     */
    return ManagedObject.extend("ixult.suite.ui.commons.ChartColumn", /** @lends ixult.suite.ui.commons.ChartColumn.prototype */ {

        // Metadata for the UI element; Used to define custom properties
        metadata: {
            properties: {
                /**
                 * @name ixult.suite.ui.commons.ChartColumn#value
                 * @type {object|string}
                 * @description the Value that should be
                 *  displayed in the Table Control for a Chart
                 * @public
                 */
                value: {
                    type: "string"
                },
                /**
                 * @name ixult.suite.ui.commons.ChartColumn#text
                 * @type {object|string}
                 * @description the Text that should be displayed
                 *  in the Header of the Table for a Chart
                 * @public
                 */
                text: {
                    type: "string"
                },
                /**
                 * @name ixult.suite.ui.commons.ChartColumn#mergeDuplicates
                 * @type {boolean}
                 * @default false
                 * @description Merges repeating/duplicate cells into one
                 *  cell block, if set to `true`; `false` by default
                 * @public
                 */
                mergeDuplicates: {
                    type: "boolean",
                    defaultValue: false
                }
            }
        },

        /**
         * @summary Returns the value as a binding string to
         *  be used by a surrounding ChartContainer VizFrame
         * @returns {string}
         *  the bound value as a string in the form of a
         *  binding, i.e. `{path}`
         * @public
         * @override
         */
        getValue: function () {
            var oBindingInfo = this.getBindingInfo("value"),
                oBinding = oBindingInfo.parts[0];

            return "{" + oBinding.path + "}";
        }

    });
});