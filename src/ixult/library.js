sap.ui.define([
    "sap/ui/core/Core",
    "sap/ui/core/library"
], function (
    Core,
    Library
) {
    "use strict";

    /**
     * @namespace ixult
     *
     * @description conovum UI5 common library
     *
     * @public
     */
    return sap.ui.getCore().initLibrary({
        name: "ixult",
        noLibraryCSS: true,
        dependencies: [
            "sap.m",
            "sap.ui.core"
        ],
        types: [],
        interfaces: [],
        controls: [],
        elements: [],
        version: "0.8.1"
    });

}, /* bExport= */ false);