sap.ui.define([], function () {
    "use strict";

    /**
     * @function assert
     * @memberOf ixult.base
     *
     * @summary Executes an assertion check against a given condition
     *  and throws an Error if the condition was not 'true'
     *
     * @param {boolean|Array<boolean|string>} vCondition
     *  either a single condition check resulting in a boolean value
     *  (optionally followed by the message specified in sMessage)
     *  or an array of conditions and messages;
     *  The array can consist of comma separated conditions and messages
     *  in any order, e.g. first containing all the conditions, and then
     *  the messages corresponding to the conditions, or each condition
     *  followed by its check message
     * @param {string} [sMessage]
     *  an optional message to display if the check fails
     * @returns {boolean}
     *  the condition check result (true or false)
     *
     * @public
     */
    return function (vCondition, sMessage) {
        var bChk = vCondition;
        var sMsg = sMessage || "Assertion failed";

        // Handle condition arrays
        if (Array.isArray(vCondition) && vCondition.length >= 1) {
            bChk = vCondition[0];

            // Split the given conditions array in what is really the
            // conditions to check for and the messages to display
            var aConditions = $.grep(vCondition, function (vValue, iIndex) {
                return typeof vValue !== "string";
            });
            var aMessages = $.grep(vCondition, function (vValue, iIndex) {
                return typeof vValue === "string";
            });

            // If there's a message that follows the check in
            // the given 'vCondition' parameter, then the message
            // will be added, if the check fails
            for (var i = 0; i < aConditions.length; i++) {
                if (!aConditions[i] && aMessages[i]) {
                    sMsg += " | " + aMessages[i];
                }
                bChk = bChk && aConditions[i];
            }
        }

        // Here's the real evaluation, returning its boolean value
        if (!bChk) {
            console.error(sMsg);
            return false;
        }
        return true;
    }

});