sap.ui.define([], function () {
    "use strict";

    /**
     * @function validate
     * @memberOf ixult.base
     *
     * @summary Executes a given function and returns a Promise
     *  to handle an exception that may have occurred
     *
     * @param {function} fnFunction
     *  a function to execute
     * @returns {Promise}
     *  a Promise handler with `resolve` (`then`)
     *  and `reject` (`fail`) handlers
     *
     * @public
     */
    return function (fnFunction) {
        var oDeferred = $.Deferred();
        if (fnFunction && typeof fnFunction === "function") {
            try {
                fnFunction();
                oDeferred.resolve();
            } catch (oException) {
                console.error("An Error occurred.", oException);
                oDeferred.reject(oException);
            }
        } else {
            oDeferred.reject(new Error("No function given."));
        }

        return oDeferred.promise();
    }

});