sap.ui.define([
    "sap/ui/core/Core",
    "sap/ui/core/library"
], function (
    Core,
    Library
) {
    "use strict";

    /**
     * @namespace base
     * @memberOf ixult
     *
     * @description Namespace for basic functions
     *
     * @public
     */
    return sap.ui.getCore().initLibrary({
        name: "ixult.base",
        noLibraryCSS: true,
        dependencies: [
            "sap.ui.core",
            "sap.m",
        ],
        types: [],
        interfaces: [],
        controls: [],
        elements: [],
        version: "0.8.1"
    });

}, /* bExport= */ false);