sap.ui.define([
    "sap/ui/core/Core",
    "sap/ui/core/library"
], function (
    Core,
    Library
) {
    "use strict";

    /**
     * @namespace util
     * @memberOf ixult.base
     *
     * @description Namespace for basic utility functions
     *
     * @public
     */
    sap.ui.getCore().initLibrary({
        name: "ixult.base.util",
        noLibraryCSS: true,
        dependencies: [
            "ixult.base",
            "sap.m",
            "sap.ui.core"
        ],
        types: [],
        interfaces: [],
        controls: [],
        elements: [],
        version: "0.8.1"
    });

    var thisLib = ixult.base.util;

    /**
     * @name ixult.base.util#DateTimePattern
     * @type {{
     *     Short: string,
     *     Medium: string,
     *     Long: string,
     *     Full: string
     * }}
     * @summary Allowed date or time patterns
     * @public
     * @static
     */
    thisLib.DateTimePattern = {
        Short: "short",
        Medium: "medium",
        Long: "long",
        Full: "full"
    };


    return thisLib;

}, /* bExport= */ false);