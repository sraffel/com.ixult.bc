sap.ui.define([
    "ixult/base/assert",
    "ixult/base/util/FunctionHelper",
    "ixult/ui/model/resource/ResourceModel",
    "sap/ui/model/json/JSONModel"
], function (
    assert,
    FunctionHelper,
    ResourceModel,
    JSONModel
) {
    "use strict";

    /**
     * @class LibraryHelper
     * @memberOf ixult.base.util
     *
     * @classdesc A helper class used for library specific purposes, e.g. to get resource
     *  bundle texts, load custom library styles or retrieve the library specific resource path
     * @description Note: The helper class needs to be instantiated using the
     *  `register` method, because using a static class causes
     *  conflicts when used in multiple namespaces
     * @see {@link ixult.base.util.LibraryHelper.html#.register #.register}
     *
     * @param {string} sNamespace
     *  the namespace of the calling class or library instance;
     *  can be given in "." or "/" notation
     * @param {map} [mParameters]
     *  convenience parameters allowing for easier library loading
     * @param {boolean} [mParameters.loadStyleSheet=false]
     *  loads the stylesheets directly after initializing the library;
     *  Executes `loadStyleSheet` method; Default is `false`
     * @param {boolean} [mParameters.loadFonts=false]
     *  loads the fonts directly after initializing the library;
     *  Executes `loadFonts` method; Default is `false`
     *
     * @returns {this|ixult.base.util.LibraryHelper}
     *  a new LibraryHelper instance
     *
     * @protected
     */
    var oLibraryHelper = function (sNamespace, mParameters) /** @lends ixult.base.util.LibraryHelper.prototype */ {
        this.sNamespace = sNamespace.toString().trim();
        this.sResourcePath = "";
        this.oResourceBundle = null;
        this.oFontModel = null;
        this.oManifest = null;

        try {
            // If the namespace separator is given in "." notation it needs to be converted to "/"
            // to correctly retrieve the library's ResourceBundle and path
            var sNamespaceWithDashes = this.sNamespace.indexOf("\/") !== -1 ? this.sNamespace :
                this.sNamespace.replace(/\./g, "\/");

            // Get the resource path (i.e. where the calling class
            // or library is located) and resource bundle therein
            this.sResourcePath = sap.ui.require.toUrl(sNamespaceWithDashes + "/");
            this.oResourceBundle = sap.ui.getCore().getLibraryResourceBundle(sNamespaceWithDashes);

            // Try to load the manifest if existing
            var sManifestPath = this.getResourcePath("manifest.json");
            $.ajax({
                url: sManifestPath,
                success: function (oResult) {
                    this.oManifest = new JSONModel(oResult);
                }.bind(this)
            });
        } catch (oError) {
            console.error(oError);
        }

        // Adjust the additional parameters
        mParameters = mParameters || {};
        mParameters.loadStyleSheet = mParameters.loadStyleSheet === true;
        mParameters.loadFonts = mParameters.loadFonts === true;

        // Load stylesheets directly if requested
        if (mParameters.loadStyleSheet) {
            this.loadStyleSheet();
        }

        // Load fonts directly if requested
        if (mParameters.loadFonts) {
            this.loadFonts();
        }
    }

    /**
     * @function register
     * @memberOf ixult.base.util.LibraryHelper.prototype
     * @summary Registers this library with a given namespace
     * @description Crucially required to allow setting up resource paths
     *  and ResourceBundles relative to the calling class or library
     * @param {string} sNamespace
     *  the namespace of the calling class or library instance;
     *  can be given in "." or "/" notation
     * @param {map} [mParameters]
     *  convenience parameters allowing for easier library loading
     * @param {boolean} [mParameters.loadStyleSheet=false]
     *  loads the stylesheets directly after initializing the library;
     *  Executes `loadStyleSheet` method; Default is `false`
     * @param {boolean} [mParameters.loadFonts=false]
     *  loads the fonts directly after initializing the library;
     *  Executes `loadFonts` method; Default is `false`
     * @returns {this|ixult.base.util.LibraryHelper}
     *  a new LibraryHelper instance
     * @public
     * @static
     */
    oLibraryHelper.register = function (sNamespace, mParameters) {
        if (!assert(sNamespace && typeof sNamespace === "string", "Namespace must be a valid string.")) {
            return null;
        }

        return new oLibraryHelper(sNamespace, mParameters);
    }

    /**
     * @function getResourcePath
     * @memberOf ixult.base.util.LibraryHelper.prototype
     * @summary Returns a constructed path to a resource with the given name
     *  relative to the registered library's path
     * @param {string} sResourceName
     *  the resource file name
     * @returns {string}
     *  the complete (absolute) resource path to the file
     * @public
     */
    oLibraryHelper.prototype.getResourcePath = function (sResourceName) {
        return (this.sResourcePath + sResourceName).toString().trim().replaceAll("//", "/");
    }

    /**
     * @function getResourceBundle
     * @memberOf ixult.base.util.LibraryHelper.prototype
     * @summary Returns the current Library's ResourceBundle
     * @returns {sap.base.i18n.ResourceBundle}
     *  the ResourceBundle
     * @public
     */
    oLibraryHelper.prototype.getResourceBundle = function () {
        return this.oResourceBundle;
    }

    /**
     * @function getBundleText
     * @memberOf ixult.base.util.LibraryHelper.prototype
     * @summary Retrieves a text for the given text id in
     *  the Library's Message (Resource) Bundle
     * @param {string} sTextId
     *  the text id to search for in the Message (Resource) Bundle
     * @returns {string}
     *  the text for the given id, or the given text/id if not found
     * @public
     */
    oLibraryHelper.prototype.getBundleText = function (sTextId) {
        if (!assert(this.oResourceBundle, "ResourceBundle needs to be set up first.")) {
            return sTextId;
        }
        return this.oResourceBundle && this.oResourceBundle.hasText(sTextId) ?
            this.oResourceBundle.getText(sTextId) : sTextId;
    }

    /**
     * @function getViewFor
     * @memberOf ixult.base.util.LibraryHelper.prototype
     * @summary Returns the View for the given Control by traversing
     *  upwards in the DOM hierarchy using the 'getParent' method
     * @param {sap.ui.core.Control} oElement
     *  the given UI Control
     * @returns {sap.ui.mvc.View|sap.ui.core.Control}
     *  the (most probably XML-) View instance (if found)
     * @public
     */
    oLibraryHelper.prototype.getViewFor = function (oElement) {
        while (!(oElement.isA("sap.ui.core.mvc.View")) && oElement.getParent) {
            oElement = oElement.getParent();
        }
        return oElement;
    }

    /**
     * @function getManifestEntry
     * @memberOf ixult.base.util.LibraryHelper.prototype
     * @summary Returns an entry of the specified path
     *  from the library's manifest model
     * @see {@link https://ui5.sap.com/sdk/#/api/sap.ui.core.Component%23methods/getManifestEntry https://ui5.sap.com/sdk/#/api/sap.ui.core.Component#getManifestEntry}
     * @param {string} sPath
     *  the path to look up, e.g. `/sap.ui5/config/`
     * @returns {string|int|boolean|object|null}
     *  the value or config section object if found, null otherwise
     * @public
     */
    oLibraryHelper.prototype.getManifestEntry = function (sPath) {
        return this.oManifest ? this.oManifest.getProperty(sPath) : null;
    }

    /**
     * @function loadStyleSheet
     * @memberOf ixult.base.util.LibraryHelper.prototype
     * @summary Loads a custom CSS file into the website
     * @description The file name is a path has to be relative to the currently
     *  loaded library, by default it is the 'library.css' in the library's folder
     * @param {string} [sFileName=library.css]
     *  the CSS file name
     * @param {map} [mAttributes]
     *  optional attributes to set into the resulting '<link>' tag that contains th stylesheet
     * @param {map} [mParameters]
     *  additional parameters, including the following attributes
     * @param {function} [mParameters.fnSuccess]
     *  a callback handler executed when the stylesheet was loaded successfully
     * @param {function} [mParameters.fnError]
     *  a callback handler executed when the stylesheet loading returned an error
     * @returns {ixult.base.util.LibraryHelper}
     *  this, for method chaining
     * @public
     */
    oLibraryHelper.prototype.loadStyleSheet = function (sFileName, mAttributes, mParameters) {
        // Check if the filename was omitted, but not the other
        // attributes, and set up the variables accordingly
        if (sFileName && typeof sFileName === "object") {
            var vAttribute = sFileName;
            // No attributes but parameters
            if (vAttribute.fnSuccess || vAttribute.fnError) {
                mParameters = vAttribute;
            } else {
                mAttributes = $.extend({}, sFileName);
            }
            sFileName = undefined;
        }

        // Set up the parameters
        sFileName = sFileName || "library.css";
        mAttributes = mAttributes || {};
        mAttributes.id = mAttributes.id || this.sNamespace.replaceAll("/", ".");
        var sPath = this.sResourcePath + sFileName;

        // Execute the callback function if given, depending on the given error flag
        var fnCallback = function (mFnParams) {
            var bError = mFnParams.bError !== undefined ? mFnParams.bError : false,
                mEventParams = mFnParams.mParameters;
            if (bError && mParameters && mParameters.fnError) {
                FunctionHelper.executeCallback(mParameters.fnError, mEventParams);
            } else if (!bError && mParameters && mParameters.fnSuccess) {
                FunctionHelper.executeCallback(mParameters.fnSuccess, mEventParams);
            }
        }

        // Create the '<link>' tag that contains the stylesheet path
        var fnCreateLink = function () {
            var oLink = document.createElement("link");
            oLink.rel = "stylesheet";
            oLink.href = sPath;
            // Set the attributes into the link
            if (mAttributes && typeof mAttributes === "object") {
                Object.keys(mAttributes).forEach(function (sKey) {
                    if (mAttributes[sKey] != null) {
                        oLink.setAttribute(sKey, mAttributes[sKey]);
                    }
                });
            }
            // Event listener to handle the success or error after loading the stylesheet
            var fnListener = function (oEvent) {
                var bError = oEvent.type === "error";
                oLink.setAttribute("data-sap-ui-ready", !bError);
                oLink.removeEventListener("load", fnListener);
                oLink.removeEventListener("error", fnListener);
                fnCallback({
                    bError: bError,
                    mParameters: oEvent
                });
            }.bind(this);
            oLink.addEventListener("load", fnListener);
            oLink.addEventListener("error", fnListener);
            return oLink;
        };

        // Check for existence of the link
        var oOldLink = document.getElementById(mAttributes.id);
        if (oOldLink && oOldLink.tagName === "LINK" && oOldLink.rel === "stylesheet") {
            // Resolve with the already created link
            fnCallback({
                mParameters: oOldLink
            });
        } else {
            // Create and add the stylesheet link
            var oNewLink = fnCreateLink();
            document.head.appendChild(oNewLink);
            fnCallback({
                mParameters: oNewLink
            });
        }

        // Return this for method chaining
        return this;
    }

    /**
     * @function loadFonts
     * @memberOf ixult.base.util.LibraryHelper.prototype
     * @summary Loads a `ResourceModel` instance to e.g. allow integrating fonts
     * @returns {ixult.base.util.LibraryHelper}
     *  this, for method chaining
     *  @public
     */
    oLibraryHelper.prototype.loadFonts = function () {
        this.oFontModel = new ResourceModel({
            bundleName: "ixult.assets.fonts",
            type: "fonts",
            supportedLocales: [""],
            fallbackLocale: ""
        })

        return this;
    }


    return oLibraryHelper;
});
