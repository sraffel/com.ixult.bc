sap.ui.define([], function () {

    /**
     * @namespace FunctionHelper
     * @memberOf ixult.base.util
     *
     * @summary Static helper functions for use with JavaScript functions
     *
     * @public
     * @static
     */
    var oFunctionHelper = {};

    /**
     * @summary Executes a callback function and applies any arguments to it
     *  that will be given as additional parameters
     * @param {function} fnCallback
     *  the callback function
     * @param {...*} [arguments]
     *  optional arguments to be passed on to the function
     * @returns {void}
     * @public
     * @static
     */
    oFunctionHelper.executeCallback = function (fnCallback) {
        if (!fnCallback || typeof fnCallback !== "function") {
            console.warn("Callback function not found or not a function. Probably not specified?");
            return;
        }
        if (arguments.length > 1) {
            var aArguments = [];
            for (var i = 1; i < arguments.length; i++) {
                aArguments.push(arguments[i]);
            }
            fnCallback.apply(this, aArguments);
        } else {
            fnCallback();
        }
    };

    return oFunctionHelper;
});