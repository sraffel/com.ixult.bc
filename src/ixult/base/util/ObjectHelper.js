sap.ui.define([
    "sap/base/util/isEmptyObject",
    "sap/base/util/isPlainObject"
], function (
    isEmptyObject,
    isPlainObject
) {
    "use strict"

    /**
     * @namespace ObjectHelper
     * @memberOf ixult.base.util
     *
     * @summary Static helper class for handling JavaScript and/or SAPUI5 Objects
     * @description Deprecated as of 0.7.0.
     *  Use `ixult.base.util.object` instead
     * @see {@link ixult.base.util.Object}
     *
     * @public
     * @static
     * @deprecated
     */
    var oObjectHelper = {};

    /**
     * @function isPlainObject
     * @memberOf ixult.base.util.ObjectHelper
     * @summary Checks a given Object if is created empty
     * @see {@link https://ui5.sap.com/sdk/#/api/module:sap/base/util/isPlainObject sap/base/util/isPlainObject}
     * @param {object} oObject
     *  the object to check
     * @returns {boolean}
     *  true if the Object is a plain object
     *  (created using `new Object` or `{}`)
     */
    oObjectHelper.isPlainObject = function (oObject) {
        return isPlainObject(oObject);
    }

    /**
     * @function isEmptyObject
     * @memberOf ixult.base.util.ObjectHelper
     * @summary Checks a given Object if it has contents;
     *  Also returns true, if all attributes are undefined or null
     * @see {@link https://ui5.sap.com/sdk/#/api/module:sap/base/util/isEmptyObject sap/base/util/isEmptyObject}
     * @param {object} oObject
     *  the object to check
     * @returns {boolean}
     *  true if the Object is empty, false otherwise
     */
    oObjectHelper.isEmptyObject = function (oObject) {
        return isEmptyObject(oObject) || $.grep(Object.keys(oObject), function (sKey) {
            return oObject[sKey] !== undefined && oObject[sKey] !== null;
        }).length === 0;
    }

    /**
     * @function checkNestedProperty
     * @memberOf ixult.base.util.ObjectHelper
     * @summary Checks for nested properties inside an object
     * @param {object} oObject
     *  the object to check
     * @param {string|arguments[]} arguments
     *  the properties to check for, looked up using the 'arguments' property;
     *  can be either comma separated argument strings or a dot separated property string
     * @returns {boolean}
     *  true, if the property was found; false otherwise
     * @public
     * @static
     */
    oObjectHelper.checkNestedProperty = function (oObject) {
        var aArguments = Array.prototype.slice.call(arguments, 1);
        if (aArguments.length === 1 && aArguments[0].indexOf(".") !== -1) {
            aArguments = aArguments[0].split(".");
        }

        for (var i = 0; i < aArguments.length; i++) {
            if (!oObject || !oObject.hasOwnProperty(aArguments[i])) {
                return false;
            }
            oObject = oObject[aArguments[i]];
        }

        return true;
    };

    /**
     * @function getNestedProperty
     * @memberOf ixult.base.util.ObjectHelper
     * @summary Retrieves a nested property from a given object
     * @param {object} oObject
     *  the object to extract the property from
     * @param {string|arguments[]} arguments
     *  the properties to check for, looked up using the 'arguments' property;
     *  can be either comma separated argument strings or a dot separated property string
     * @returns {any|undefined}
     *  the value if it was found, or undefined otherwise
     * @public
     * @static
     */
    oObjectHelper.getNestedProperty = function (oObject) {
        var aArguments = Array.prototype.slice.call(arguments, 1);
        if (aArguments.length === 1 && aArguments[0].indexOf(".") !== -1) {
            aArguments = aArguments[0].split(".");
        }

        var vData = $.extend(true, {}, oObject);
        for (var i = 0; i < aArguments.length; i++) {
            if (!vData || !vData.hasOwnProperty(aArguments[i])) {
                return undefined;
            }
            vData = vData[aArguments[i]];
        }

        return vData;
    };

    return oObjectHelper;
});