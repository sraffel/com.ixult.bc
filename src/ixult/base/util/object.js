sap.ui.define([
    "sap/base/util/deepClone",
    "sap/base/util/deepExtend",
    "sap/base/util/isEmptyObject",
    "sap/base/util/isPlainObject",
    "./array"
], function (
    deepClone,
    deepExtend,
    isEmptyObject,
    isPlainObject,
    array
) {
    "use strict";

    /**
     * @summary Executes the removing of all
     *  non-data parts in the given object
     * @param {object|map} oObject
     *  the source object to analyze
     * @returns {object|map}
     *  the extracted object
     * @private
     * @internal
     */
    const _toMap = function (oObject, iMaxDepth, iDepth) {
        const mMap = new Map(Object.entries(oObject));
        mMap.forEach(function (vValue, sKey) {
            if (typeof vValue === "function") {
                mMap.delete(sKey)
            } else if (typeof vValue === "object" && iDepth < iMaxDepth) {
                mMap.set(sKey, _toMap(vValue, iMaxDepth, iDepth++));
            }
        });
        return mMap;
    };

    /**
     * @class object
     * @memberOf ixult.base.util
     *
     * @classdesc Handles and/or manipulate object; Uses a given
     *  object instance as parameter and enhances it with custom functions
     * @description Usage: to create the object handler instance, this constructor can be called in a
     *  static way like `object(oObject)` where the function `object` is
     *  the namespace instance of `ixult/base/util/object` included in the
     *  `sap.ui.define` or `sap.ui.require` statement in the source file
     *  and `oObject` is the source object to enhance.
     *  Methods can then directly be applied to the result, like `object(oObject).isEmpty()`
     * @example
     *  // Gets the nested property of an object by recursively traversing its sub-attributes
     *  var object = sap.ui.require("ixult/base/util/object");
     *  var sObjValue = object({
     *          data: {
     *              value: '123'
     *          }
     *      }).getNestedProperty("data.value"); // returns '123'
     *
     * @param {object} oObject
     *  the source object
     * @returns {object}
     *  the source object enhanced with the functions below that can be applied to it
     *
     * @public
     */
    var object = function (oObject) {
        // Special case: return an ixult.base.util.array
        // instance for arrays; should avoid problems with
        // arrays being identified as an Object instance
        if (Array.isArray(oObject)) {
            return array(oObject);
        }

        oObject = oObject || {};
        Object.setPrototypeOf(oObject, _object);

        return oObject;
    };


    /**
     * @summary The extended object containing
     *  additional functions to enhance the source object with
     * @type {object}
     * @private
     * @internal
     */
    var _object = {

        /**
         * @function valueOf
         * @memberOf ixult.base.util.object.prototype
         * @summary Returns this object as an
         *  attribute map without its functions
         * @description Useful when used with `extend`
         *  functions to avoid getting the
         *  functions inside the extended object
         * @returns {object}
         *  this object's values
         * @public
         * @override
         */
        valueOf: function () {
            var oObject = $.extend({}, this);

            Object.keys(oObject).forEach(function (sKey) {
                if (typeof oObject[sKey] === "function") {
                    delete oObject[sKey];
                }
            }.bind(this));

            return oObject;
        },

        /**
         * @function isPlain
         * @memberOf ixult.base.util.object.prototype
         * @summary Checks, if the object was created empty
         * @see {@link https://ui5.sap.com/sdk/#/api/module:sap/base/util/isPlainObject sap/base/util/isPlainObject}
         * @returns {boolean}
         *  true if the Object is a plain object
         *  (created using `new Object` or `{}`)
         */
        isPlain: function () {
            return isPlainObject(this);
        },

        /**
         * @function isEmpty
         * @memberOf ixult.base.util.object.prototype
         * @summary Checks, if the object has contents;
         *  Also returns true, if all attributes are undefined or null
         * @see {@link https://ui5.sap.com/sdk/#/api/module:sap/base/util/isEmptyObject sap/base/util/isEmptyObject}
         * @returns {boolean}
         *  true if the Object is empty, false otherwise
         */
        isEmpty: function () {
            return isEmptyObject(this) || $.grep(Object.keys(this), function (sKey) {
                return this[sKey] !== undefined && this[sKey] !== null;
            }.bind(this)).length === 0;
        },

        /**
         * @function isArray
         * @memberOf ixult.base.util.object.prototype
         * @summary Checks, if the object is an array
         * @description Convenience method forwarding to
         *  `jQuery.isArray` (deprecated since 3.2)
         *  or the native JavaScript `Array.isArray` function
         * @see {@link https://api.jquery.com/jQuery.isArray/ jQuery.isArray}
         * @see {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/isArray Array.isArray}
         * @returns {boolean}
         *  true if the object is an array, false otherwise;
         *  returns undefined, if none of the check functions could be found (shouldn't happen)
         */
        isArray: function () {
            return Array.isArray ? Array.isArray(this) :
                Array.isArray ? Array.isArray(this) : undefined;
        },

        /**
         * @function clone
         * @memberOf ixult.base.util.object.prototype
         * @summary Clones this object
         * @description Convenience method forwarding to
         *  the SAPUI5 `deepClone` function
         * @see {@link https://ui5.sap.com/sdk/#/api/module:sap/base/util/deepClone sap/base/util/deepClone}
         * @param {int} [iMaxDepth=10]
         *  Maximum recursion depth for the clone operation, deeper structures will throw an error
         * @returns {any}
         *  A clone of the source value
         */
        clone: function (iMaxDepth) {
            try {
                return deepClone(this.valueOf(), iMaxDepth);
            } catch (oException) {
                console.error("Error while cloning object, " +
                    "returning original instead. " +
                    "Try using 'extend()'.", oException);
                return this.valueOf();
            }
        },

        /**
         * @function extend
         * @memberOf ixult.base.util.object.prototype
         * @summary Performs object extension by merging source objects
         *  into a target object. Copies are always deep.
         * @description Convenience method forwarding to
         *  the SAPUI5 `deepExtend` function
         * @see {@link https://ui5.sap.com/sdk/#/api/module:sap/base/util/deepExtend sap/base/util/deepExtend}
         * @param {object} [oSource]
         *  One or more objects which get merged into this object
         * @returns {object}
         *  this object, merged and extended with the source object(s)
         */
        extend: function (oSource) {
            return deepExtend(this.valueOf(), oSource.valueOf());
        },

        /**
         * @function toMap
         * @memberOf ixult.base.util.object.prototype
         * @summary Transfers this object's properties into
         *  a key-value-map structure
         * @param {int|Number} [iMaxDepth=2]
         *  the maximum iteration level; Note; should not exceed
         *  2 or 3 levels when dealing with class objects having
         *  sub object instances, i.e. don't use this to parse
         *  SAPUI5 objects
         * @returns {object}
         *  this object as an extracted map
         */
        toMap: function (iMaxDepth) {
            iMaxDepth = iMaxDepth > 0 ? iMaxDepth : 2;
            return Object.fromEntries(_toMap($.extend({}, this), iMaxDepth, 0));
        },

        /**
         * @function deepExtend
         * @memberOf ixult.base.util.object.prototype
         * @summary Performs object extension by merging a
         *  source object into a target object;
         *  Includes the extension of arrays
         * @param {object} oSource
         *  the (one) object which get merged into this object
         * @returns {object}
         *  this object, merged and extended with the source object
         * @protected
         * @experimental
         */
        deepExtend: function (oSource) {
            var oMergedObject = deepExtend(this.valueOf(), oSource.valueOf()),
                aKeys = [...Object.keys(this), ...Object.keys(oSource)],
                aSharedKeys = $.grep(aKeys, function (sKey) {
                    return Array.isArray(this[sKey]) && Array.isArray(oSource[sKey]);
                });
            aSharedKeys.forEach(function (sKey) {
                oMergedObject[sKey] = {};
                var iLength = this[sKey].length > oSource[sKey].length ?
                    this[sKey].length : oSource[sKey].length;
                for (var i = 0; i < iLength; i++) {
                    if (this[sKey][i] && oSource[sKey][i] &&
                        typeof this[sKey][i] === "object" &&
                        typeof oSource[sKey][i] === "object"
                    ) {
                        oMergedObject[sKey][i] = object(this[sKey][i]).deepExtend(oSource[sKey][i]);
                    } else if (this[sKey][i]) {
                        oMergedObject[sKey][i] = this[sKey][i];
                    } else if (oSource[sKey][i]) {
                        oMergedObject[sKey][i] = oSource[sKey][i];
                    }
                }
            });

            return oMergedObject;
        },

        /**
         * @function setNestedProperty
         * @memberOf ixult.base.util.object.prototype
         * @summary Creates a nested property in this
         *  object and fills it with the given data
         * @param {string|string[]|int[]} vProperty
         *  the array of properties to create, can be a `.` separated string
         *  or an array of property names to create
         * @param {any} vValue
         *  the value to set into the newly created property
         * @returns {ixult.base.util.object}
         *  this, for method chaining
         * @public
         * @static
         */
        setNestedProperty: function (vProperty, vValue) {
            var aArguments = [],
                vData = this;
            if (typeof vProperty === "string" && vProperty.indexOf(".") !== -1) {
                aArguments = vProperty.split(".");
            } else if (Array.isArray(vProperty)) {
                aArguments = array(vProperty);
            }

            // Traverse the object levels, create new property objects if necessary
            for (var i = 0; i < aArguments.length; i++) {
                if (!vData || !vData.hasOwnProperty(aArguments[i])) {
                    vData[aArguments[i]] = {};
                    if (i === aArguments.length - 1) {
                        vData[aArguments[i]] = vValue;
                    }
                }
                vData = vData[aArguments[i]];
            }

            return this;
        },

        /**
         * @function getNestedProperty
         * @memberOf ixult.base.util.object.prototype
         * @summary Retrieves a nested property from this object
         * @param {string|arguments[]} arguments
         *  the properties to check for, looked up using the 'arguments' property;
         *  can be either comma separated argument strings or a dot separated property string
         * @returns {any|undefined}
         *  the value if it was found, or undefined otherwise
         * @public
         * @static
         */
        getNestedProperty: function () {
            var aArguments = arguments;
            if (aArguments.length === 1 && aArguments[0].indexOf(".") !== -1) {
                aArguments = aArguments[0].split(".");
            }

            var vData = $.extend(true, {}, this);
            for (var i = 0; i < aArguments.length; i++) {
                if (!vData || !vData.hasOwnProperty(aArguments[i])) {
                    return undefined;
                }
                vData = vData[aArguments[i]];
            }

            return vData;
        },

        /**
         * @function checkNestedProperty
         * @memberOf ixult.base.util.object.prototype
         * @summary Checks for nested properties inside an object
         * @param {string|arguments[]} arguments
         *  the properties to check for, looked up using the 'arguments' property;
         *  can be either comma separated argument strings or a dot separated property string
         * @returns {boolean}
         *  true, if the property was found; false otherwise
         * @public
         * @static
         */
        checkNestedProperty: function () {
            var aArguments = arguments;
            if (aArguments.length === 1 && aArguments[0].indexOf(".") !== -1) {
                aArguments = aArguments[0].split(".");
            }

            var oObject = $.extend({}, this);
            for (var i = 0; i < aArguments.length; i++) {
                if (!oObject || !oObject.hasOwnProperty(aArguments[i])) {
                    return false;
                }
                oObject = oObject[aArguments[i]];
            }

            return true;
        }

    }


    return object;
});
