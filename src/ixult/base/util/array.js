sap.ui.define([
    "sap/base/util/array/diff",
    "sap/base/util/array/uniqueSort",
    "ixult/base/util/DateHelper"
], function (
    arrayDiff,
    arrayUniqueSort,
    DateHelper
) {
    "use strict";

    /**
     * @function _fnArrayFromMap
     * @summary Converts an object or map into an array
     * @param {object|map} oObject
     *  the object to convert
     * @returns {Array}
     *  an array instance
     * @private
     * @internal
     */
    const _fnArrayFromMap = function (oObject) {
        if (typeof oObject !== "object") {
            return [];
        }
        debugger;
        var aArray = [],
            aKeys = Object.keys(oObject),
            oIterator = oObject.values ? oObject.values() : null,
            oNext;

        if (aKeys.length > 0) {
            aKeys.forEach(sKey => {
                aArray.push(oObject[sKey]);
            });
        } else if (oIterator && oIterator.next) {
            do {
                oNext = oIterator.next();
                if (oNext.done !== true) {
                    aArray.push(oNext.value);
                }
            } while (oNext && oNext.done !== true);
        }

        return aArray;
    }

    /**
     * @class array
     * @memberOf ixult.base.util
     *
     * @classdesc Handles and/or manipulate arrays; Uses a given
     *  array as parameter and enhances it with custom functions
     * @summary Usage: to create the array handler instance, this constructor can be called in a
     *  static way like `array([aValues])` where the function `array` is
     *  the namespace instance of `ixult/base/util/array` included in the
     *  `sap.ui.define` or `sap.ui.require` statement in the source file
     *  and `aValues` is the source array to enhance.
     *  Methods can then directly be applied to the result, like `array(aValues).contains(vValue)`
     * @example
     *  // Determines whether the Date is a weekend day, i.e. Saturday = '6' or Sunday = '0' (Javascript standard)
     *  var dDate = new Date();
     *  var array = sap.ui.require("ixult/base/util/array");
     *  var isWeekDay = array([6, 0]).contains(dDate.getDay());
     *
     * @param {any[]} aArray
     *  the source array
     * @returns {any[]}
     *  the source array enhanced with the functions below that can be applied to it
     *
     * @public
     */
    var array = function (aArray) {
        aArray = aArray === undefined ? [] : Array.isArray(aArray) ? aArray : [aArray];

        /**
         * @function isEmpty
         * @memberOf ixult.base.util.array.prototype
         * @summary Checks, if the array contains any elements
         * @returns {boolean}
         *  true, if the array is empty, false otherwise
         * @public
         * @instance
         */
        aArray.isEmpty = function () {
            return $.isEmptyObject(aArray) || aArray.length === 0;
        }

        /**
         * @function diff
         * @memberOf ixult.base.util.array.prototype
         * @summary Calculates delta of this array compared to a given list.
         * @description Convenience method forwarding to
         *  {@link https://ui5.sap.com/sdk/#/api/module:sap/base/util/array/diff sap/base/util/array/diff}
         * @param {any[]} aCompare
         *  The array to compare this one to
         * @param {object|function} [vConfigOrSymbol]
         *  Configuration object or a function to calculate substitute symbols for array items
         * @returns {Array<{
         *      type:string,
         *      index:int
         *  }>}
         *  List of update operations
         * @public
         * @instance
         */
        aArray.diff = function (aCompare, vConfigOrSymbol) {
            return arrayDiff(aArray, aCompare, vConfigOrSymbol);
        }

        /**
         * @function uniqueSort
         * @memberOf ixult.base.util.array.prototype
         * @summary Sorts the given array in-place and removes any duplicates (identified by "===").
         * @description Convenience method forwarding to {sap/base/util/array/uniqueSort}
         * @see {@link https://ui5.sap.com/sdk/#/api/module:sap/base/util/array/uniqueSort sap/base/util/array/uniqueSort}
         * @returns {any[]}
         *  Same array as given (for method chaining)
         * @public
         * @instance
         */
        aArray.uniqueSort = function () {
            return arrayUniqueSort(aArray);
        }

        /**
         * @function format
         * @memberOf ixult.base.util.array.prototype
         * @summary Formats the array using the given format function
         * @param {function} fnFormat
         *  the formatter function; has to take one array value
         *  and return the formatted one
         * @returns {ixult.base.util.array}
         *  this array instance, to allow method chaining
         */
        aArray.format = function (fnFormat) {
            for (var i = 0; i < aArray.length; i++) {
                aArray[i] = fnFormat(aArray[i]);
            }
            return this;
        }

        /**
         * @function contains
         * @memberOf ixult.base.util.array.prototype
         * @summary Checks a given array if it contains a given value;
         * @description Convenience method forwarding to jQuery.inArray
         * @see {@link https://api.jquery.com/jquery.inarray/ jQuery.inArray}
         * @param {*} vValue
         *  the value to check for
         * @returns {boolean}
         *  true if the array contains the value, false otherwise
         * @public
         * @instance
         */
        aArray.contains = function (vValue) {
            return $.inArray(vValue, aArray) !== -1;
        }

        /**
         * @function indexOf
         * @memberOf ixult.base.util.array.prototype
         * @summary Returns the index of a value inside an array;
         * @description Convenience method forwarding to jQuery.inArray
         * @see {@link https://api.jquery.com/jquery.inarray/ jQuery.inArray}
         * @param {*} vValue
         *  the value to check for
         * @returns {int}
         *  the index of the value if found, -1 if the value was not found
         * @public
         * @instance
         */
        aArray.indexOf = function (vValue) {
            return $.inArray(vValue, aArray);
        }

        /**
         * @function grep
         * @memberOf ixult.base.util.array.prototype
         * @summary Filters the array using the specified function
         * @see {@link https://api.jquery.com/jquery.grep/ jQuery.grep}
         * @see {@link https://www.w3schools.com/jsref/jsref_filter.asp Array.filter}
         * @param {function} fnCompare
         *  the function using to compare the items; it will provide the parameters
         *  item and index to be used inside the comparision; returning true or
         *  false inside the function results in the item being included in or
         *  excluded from the array
         * @returns {ixult.base.util.array}
         *  the filtered array
         * @public
         * @instance
         */
        aArray.grep = function (fnCompare) {
            return aArray.filter ? array(aArray.filter(fnCompare)) :
                $.grep ? array($.grep(aArray, fnCompare)) : this;
        }

        /**
         * @function remove
         * @memberOf ixult.base.util.array.prototype
         * @summary Removes an item from an array by its value
         * @param {any} vValue
         *  the value to search for and remove
         * @param {boolean} [bAll=true]
         *  removes all instances of the value, true by default
         * @returns {ixult.base.util.array}
         *  the remaining array
         * @public
         * @instance
         */
        aArray.remove = function (vValue, bAll) {
            return array(aArray).grep(function (vItem) {
                return vItem !== vValue;
            });
        }

        /**
         * @function loop
         * @memberOf ixult.base.util.array.prototype
         * @summary Allows to loop over an array using a single function
         *  Determines if the array supports either `forEach` (ES5 standard),
         *  `each` (jQuery selectors) or none of those; the last case will
         *  then be handled by a simple for `loop`
         * @param {function} fnLoop
         *  the function that will be executed for each item in the array;
         *  it should come with the parameters for `vElement` and `iIndex`
         *  in that order
         * @returns {ixult.base.util.array}
         *  the array instance, for method chaining
         * @public
         * @instance
         */
        aArray.loop = function (fnLoop) {
            if (aArray.forEach) {
                aArray.forEach(function (vElement, iIndex) {
                    fnLoop(vElement, iIndex);
                });
            } else if (aArray.each) {
                aArray.each(function (iIndex, vElement) {
                    fnLoop(vElement, iIndex);
                });
            } else {
                for (var i = 0; i < aArray.length; i++) {
                    fnLoop(aArray[i], i);
                }
            }

            return this;
        }

        /**
         * @function sortByProperty
         * @memberOf ixult.base.util.array.prototype
         * @summary Sorts an array of objects by a given property name
         * @description Can add object attributes if they are
         *  missing in certain array objects
         * @param {string|string[]} vProperties
         *  the property name, or an array of property names
         *  for hierarchical sorting; required (for obvious reasons)
         * @param {string} [sType='string']
         *  the attribute type, can be 'integer', 'float', 'date' or 'string'
         * @param {boolean} [bDescending=false]
         *  sorts the list in descending order, false by default
         * @returns {object[]}
         *  the sorted object array
         * @public
         * @instance
         * @deprecated function is not reliable when it comes to duplicate
         *  number values, use built-in javascript `sort` directly instead
         */
        aArray.sortByProperty = function (vProperties, sType, bDescending) {
            var aKeyArray = array([]),
                mValueMap = {},
                aResultArray = array([]),
                aProperties = array(vProperties) || array("__sortid");

            // Adjust the parameters if some were omitted
            if (sType && typeof sType === "boolean") {
                bDescending = sType;
                sType = "string";
            }
            sType = sType ? sType.toString().toLowerCase() : "string";
            bDescending = bDescending !== undefined ? bDescending : false;

            // Build up a key array and a value map based
            // on the sort attributes and their type
            aArray.forEach(oObject => {
                var vIndex = sType === "string" ? "" : 0;
                aProperties.loop(sProperty => {
                    switch (sType) {
                        case "integer":
                            vIndex += oObject[sProperty] ? parseInt(oObject[sProperty]) : 0;
                            break;
                        case "float":
                            vIndex += oObject[sProperty] ? parseFloat(oObject[sProperty]) : 0;
                            break;
                        case "date":
                            vIndex += oObject[sProperty] && DateHelper.isDate(oObject[sProperty]) ?
                                new Date(oObject[sProperty]).valueOf() : 0;
                            break;
                        default:
                            vIndex += oObject[sProperty] ? oObject[sProperty].toString().trim() : "";
                    }
                });
                aKeyArray.push(vIndex);
                mValueMap[vIndex] = oObject;
            });

            // Sort the key array, using existing internal Javascript functions
            aKeyArray = bDescending === true ?
                aKeyArray.sort((a, b) => a - b).reverse() :
                aKeyArray.sort((a, b) => a - b);

            // Create the result array by adding the objects
            // from the value map depending on the key order
            aKeyArray.forEach(vKey => {
                aResultArray.push(mValueMap[vKey]);
            });

            return aResultArray;
        }

        /**
         * @function extractValues
         * @memberOf ixult.base.util.array.prototype
         * @summary Searches inside an object array for a specific property
         *  and returns the values for this property
         * @param {string} sProperty
         *  the property to look for
         * @param {boolean} [bDistinct=true]
         *  looks only for distinct values
         * @returns {ixult.base.util.array}
         *  an array instance for the extracted value array;
         *  for method chaining
         * @public
         * @instance
         */
        aArray.extractValues = function (sProperty, bDistinct) {
            var aExtract = [];
            bDistinct = bDistinct !== undefined ? bDistinct : true;

            aArray.forEach(function (oItem) {
                // Continue if the item does not have the specified value
                if (typeof oItem !== "object" || !oItem[sProperty]) {
                    return true;
                }
                // Continue if the array already has this value
                if (bDistinct && $.inArray(oItem[sProperty], aExtract) !== -1) {
                    return true;
                }
                // Add the value to the extract list
                aExtract.push(oItem[sProperty]);
            });

            return array(aExtract);
        }


        return aArray;
    }

    return array;
});
