sap.ui.define([
    "sap/base/security/encodeURL",
    "sap/ui/core/Locale",
    "sap/ui/core/LocaleData",
    "sap/ui/core/format/DateFormat",
    "sap/ui/model/type/DateTime",
    "sap/ui/model/type/Time",
    "ixult/base/assert",
    "ixult/base/util/array",
    "ixult/base/util/object",
    "ixult/base/util/LibraryHelper",
    "./library"
], function (
    encodeURL,
    Locale,
    LocaleData,
    DateFormat,
    DateTime,
    Time,
    assert,
    array,
    object,
    LibraryHelper,
    MyLibrary
) {
    "use strict";
    const Library = LibraryHelper.register("ixult/base/util");
    var _sLocale = undefined;

    /**
     * @summary Returns a new Locale instance from the given or current locale settings
     * @description Methods accepts
     *   - short locales like `de` or `en`
     *   - long locales like`de_DE` or `en-US`
     *   - empty values (will look for current locale)
     *  To ensure returning the correct Unicode CLDR configuration for short
     *  locales the function will use the `navigator.languages` array to look up
     *  the next applicable region specific locale
     *  For example the locale `de` in combination with the `navigator.languages`
     *  array being like `['de', 'de-DE', 'en', 'en-GB', 'en-US']`, it will use
     *  `de-DE` for further locale evaluations. For ´en´ it will use `en-GB` though.
     *  Note: the method only evaluates the locales and region specific locales
     *  again, when the locale has changed.
     * @param {string} [sLocale]
     *  the Locale string, optionally; uses the current Locale if not given
     * @returns {sap.ui.core.Locale}
     *  the Locale instance
     * @private
     * @static
     * @internal
     */
    var _getLocale = function (sLocale) {
        // Check if the locale has changed since the last evaluation
        if (!_sLocale || _sLocale.indexOf(sLocale) !== 0) {
            // Save the new locale (to avoid later checks)
            _sLocale = sLocale || sap.ui.getCore().getConfiguration().getLanguage();
            // Get a correct sub locale to ensure initializing real locales
            // by comparing the given locale to the values the browser offers
            // and returning the first locale that is a sub locale of the given one
            // e.g. `de` will return `de-DE`, `en1` will return `en-GB`
            if (_sLocale.length === 2 && navigator.languages) {
                var aLocales = $.grep(navigator.languages, function (sLanguage) {
                    return sLanguage.indexOf(_sLocale) === 0 &&
                        sLanguage.length > _sLocale.length;
                });
                _sLocale = aLocales.length > 0 ? aLocales[0] : _sLocale;
            }
        }

        return new Locale(_sLocale);
    }

    /**
     * @summary Creates a new Locale Data instance using a Locale instance
     * @param {string} [sLocale]
     *  the Locale string, optionally;
     *  uses the current Locale if not given
     * @returns {sap.ui.core.LocaleData}
     *  the LocaleData instance
     * @private
     * @static
     * @internal
     */
    var _getLocaleData = function (sLocale) {
        return new LocaleData(_getLocale(sLocale));
    }

    /**
     * @summary Reads the public holidays for a given year
     *  using a public internet web service
     * @param {map} mParameters
     *  a parameter map containing the following attributes
     * @param {int|Number} mParameters.sServiceUrl
     *  the ServiceUIrl used to read the holidays
     *  preferably a local URL with already stored data;
     *  can be a public service URL though, both
     *  stored in library's manifest file
     * @param {int|Number} mParameters.iYear
     *  the year to read the holidays for
     * @param {string} [mParameters.sCountry]
     *  the country code or locale to read the data for
     * @returns {Promise}
     *  A Promise instance with `resolve` and `reject`
     *  handlers including the corresponding service result,
     *  i.e. the data or an error
     * @private
     * @static
     * @internal
     */
    var _readHolidays = function (mParameters) {
        var oDeferred = $.Deferred(),
            iYear = mParameters.iYear || this.toMap().iYear,
            sCountry = (mParameters.sCountry || _getLocale().getRegion()).toUpperCase(),
            sHolidayServiceUrl = mParameters.sServiceUrl
                .replace("%year%", iYear.toString())
                .replace("%country%", sCountry);

        // Call the service and handle the
        // Promise according to the result
        $.ajax({
            url: sHolidayServiceUrl,
            success: function (vResult) {
                oDeferred.resolve(vResult);
            }.bind(this),
            error: function (oError) {
                oDeferred.reject(oError);
            }.bind(this)
        });

        return oDeferred.promise();
    }


    /**
     * @namespace DateHelper
     * @memberOf ixult.base.util
     * @summary Static helper providing methods
     *  for advanced handling of Dates and DateTimes
     * @public
     */
    var oDateHelper = function () {
        /**
         * @summary Defines the handling of Dates in UTC format
         * @type {boolean}
         * @default false
         * @protected
         */
        const bUTC = false;
    }

    /**
     * @function isDate
     * @memberOf ixult.base.util.DateHelper
     * @summary Checks if a given value is a Date
     * @param {any} vValue
     *  the value to check
     * @returns {boolean}
     *  true if the value is a Date, false otherwise
     * @public
     * @static
     */
    oDateHelper.isDate = function (vValue) {
        return vValue !== undefined && vValue !== null ?
            Object.prototype.toString.call(vValue) === "[object Date]" :
            false;
    }

    /**
     * @function toDate
     * @memberOf ixult.base.util.DateHelper
     * @summary Creates a UTC date from Year, Month and Day values
     *  either by using the given values or using values of the current Date
     * @param {map} mDate
     *  the Date value map containing the following attributes
     * @param {Number} [mDate.iYear]
     *  the Year value; the current Year if not given
     * @param {Number} [mDate.iMonth]
     *  the Month value, given in 'human' format (ranging from 1 to 12);
     *  the current one if not given
     * @param {Number} [mDate.iDay]
     *  the Day value; the current Day if not given
     * @param {Number} [mDate.iHours]
     *  the Hours value; the current Hour if not given
     * @param {Number} [mDate.iMinutes]
     *  the Minutes value; the current Minute if not given
     * @param {Number} [mDate.iSeconds]
     *  the Seconds value; the current Second if not given
     * @param {Number} [mDate.iTimeMs]
     *  a Milliseconds value for the time duration since 00:00;
     *  will be used to extract the Hours, Minutes and Seconds
     *  values if set
     * @param {boolean} [mDate.bUTC=true]
     *  returns the date in UTC format, true by default
     * @returns {Date}
     *  the Date
     * @public
     * @static
     */
    oDateHelper.toDate = function (mDate) {
        mDate = mDate || {};
        var dNewDate = new Date(),
            mNewDate = object().extend(mDate);
        mNewDate.bUTC = mDate && mDate.bUTC !== undefined ? mDate.bUTC : oDateHelper.bUTC;

        // Take the current year if no value was given
        mNewDate.iYear = mDate.iYear || dNewDate.getFullYear();

        // -1 for the Month, because Javascript
        mNewDate.iMonth = mDate.iMonth > 0 ? mDate.iMonth - 1 : 0;

        // 'Day' can actually be '0'; see function {#getEndOfMonth}
        mNewDate.iDay = !isNaN(mDate.iDay) ? mDate.iDay : 1;

        // Hours, Minutes and Seconds also can be '0' (obviously)
        mNewDate.iHours = !isNaN(mDate.iHours) ? mDate.iHours : 0;       // dDate.getHours();
        mNewDate.iMinutes = !isNaN(mDate.iMinutes) ? mDate.iMinutes : 0; // dDate.getMinutes();
        mNewDate.iSeconds = !isNaN(mDate.iSeconds) ? mDate.iSeconds : 0; // dDate.getSeconds();

        // Calculate hours, minutes and seconds from a milliseconds
        // value (e.g. coming from an Edm.Time object)
        if (mDate.iTimeMs) {
            var dTimeDate = new Date(new Date(
                mNewDate.iYear,
                mNewDate.iMonth,
                mNewDate.iDay,
                0,
                0,
                0
            ).getTime() + mDate.iTimeMs);
            mNewDate.iHours = dTimeDate.getHours();
            mNewDate.iMinutes = dTimeDate.getMinutes();
            mNewDate.iSeconds = dTimeDate.getSeconds();
        }

        // Return the Date in UTC format
        return mNewDate.bUTC ? new Date(Date.UTC(
            mNewDate.iYear,
            mNewDate.iMonth,
            mNewDate.iDay,
            mNewDate.iHours,
            mNewDate.iMinutes,
            mNewDate.iSeconds
        )) : new Date(
            mNewDate.iYear,
            mNewDate.iMonth,
            mNewDate.iDay,
            mNewDate.iHours,
            mNewDate.iMinutes,
            mNewDate.iSeconds
        );
    }

    /**
     * @function toMap
     * @memberOf ixult.base.util.DateHelper
     * @summary Converts a given Date to a map with
     *  - Year
     *  - Month
     *  - Day
     *  - Hours
     *  - Minutes
     *  - Seconds
     *  - Milliseconds for the date (with time being 00:00 / 12:00 am)
     *  - Milliseconds for the date (just the time, without the date)
     *  values
     * @param {date} dDate
     *  the input Date
     * @returns {{
     *      iYear: Number,
     *      iMonth: Number,
     *      iDay: Number,
     *      iHours: Number,
     *      iMinutes: Number,
     *      iSeconds: Number,
     *      iDateMs: Number,
     *      iTimeMs: Number
     *  }
     *  the Date value map containing the single values
     * @public
     * @static
     */
    oDateHelper.toMap = function (dDate) {
        dDate = dDate || new Date();

        // Create the date/time map
        var mDate = {
            dDate: dDate,                   // Add the Date itself
            iYear: dDate.getFullYear(),
            iMonth: dDate.getMonth() + 1,   // + 1 because we like human-readable format :-P
            iDay: dDate.getDate(),
            iHours: dDate.getHours(),
            iMinutes: dDate.getMinutes(),
            iSeconds: dDate.getSeconds()
        }

        // Calculate milliseconds for date and time separately
        // for use with e.g. OData Edm-Time values
        mDate.iDateMs = new Date(
            mDate.iYear,
            mDate.iMonth - 1,
            mDate.iDay,
            0,
            0,
            0
        ).getTime();
        mDate.iTimeMs = dDate.getTime() - mDate.iDateMs;

        return mDate;
    }

    /**
     * @function getDatePattern
     * @memberOf ixult.base.util.DateHelper
     * @summary Returns the Date Pattern for a given Format Type using the current Locale
     * @param {ixult.base.DateTimePattern} sType
     *  the Pattern to use, possible values are 'short', 'medium', 'long' and 'full'
     * @returns {string}
     *  the Date Format, or an empty string if not found
     * @public
     * @static
     */
    oDateHelper.getDatePattern = function (sType) {
        if (!assert($.grep(Object.keys(MyLibrary.DateTimePattern), function (sKey) {
            return MyLibrary.DateTimePattern[sKey] === sType;
        }.bind(this)).length > 0, "Date pattern '" + sType + "' is not allowed")) {
            return "";
        }
        return _getLocaleData().getDatePattern(sType);
    }

    /**
     * @function getTimePattern
     * @memberOf ixult.base.util.DateHelper
     * @summary Returns the Time Pattern for a given Format Type using the current Locale
     * @param {ixult.base.DateTimePattern} sType
     *  the Pattern to use, possible values are 'short', 'medium', 'long' and 'full'
     * @returns {string}
     *  the Time Format, or an empty string if not found
     * @public
     * @static
     */
    oDateHelper.getTimePattern = function (sType) {
        if (!assert($.grep(Object.keys(MyLibrary.DateTimePattern), function (sKey) {
            return MyLibrary.DateTimePattern[sKey] === sType;
        }.bind(this)).length > 0, "Date pattern '" + sType + "' is not allowed")) {
            return "";
        }
        return _getLocaleData().getTimePattern(sType);
    }

    /**
     * @function getDateRange
     * @memberOf ixult.base.util.DateHelper
     * @summary Returns a range of Dates for a given year and month,
     *  i.e. returns the begin and end of month (at the moment)
     * @description Should (later) be extended to return the
     *  days for a complete year or in between given days
     * @param {map} mParameters
     *  a parameter map, containing the following attributes
     * @param {map} mParameters.iYear
     *  the year to return the dates for
     * @param {map} [mParameters.iMonth]
     *  the month in the given year to return the dates for;
     *  if omitted, the whole year will be processed
     * @param {map} [mParameters.iDayFrom]
     *  the month in the given year to return the dates for;
     *  if omitted the begin of month will be used
     * @param {map} [mParameters.iDayTo]
     *  the month in the given year to return the dates for;
     *  if omitted the end of month will be used
     * @returns {Array<{
     *      dDate: Date,
     *      iYear: Number,
     *      iMonth: Number,
     *      iDay: Number
     *  }>}
     *  an array of Date maps created by the `toMap` function
     * @public
     * @static
     */
    oDateHelper.getDateRange = function (mParameters) {
        var iYear = mParameters && mParameters.iYear ? mParameters.iYear : new Date().getFullYear(),
            aDates = array([]),
            iBeginDay,
            iEndDay;

        if (!mParameters.iMonth) {
            // Loop over the whole year if only a year has been given
            for (var m = 1; m <= 12; m++) {
                aDates = aDates.concat(oDateHelper.getDateRange({
                    iYear: iYear,
                    iMonth: m
                }));
            }
            return aDates;
        } else {
            // Use the given month if it is between valid month values
            // or set the month to the current one
            var iMonth = mParameters.iMonth >= 1 && mParameters.iMonth <= 12 ?
                mParameters.iMonth : new Date().getMonth() + 1;

            // Get the begin and end of month or use the given `from` and `to` days
            // if they are within the first and last day of the month
            var dBeginOfMonth = oDateHelper.getBeginOfMonth({
                    iYear: iYear,
                    iMonth: iMonth
                }),
                dEndOfMonth = oDateHelper.getEndOfMonth({
                    iYear: iYear,
                    iMonth: iMonth
                });

            iBeginDay = mParameters.iDayFrom && mParameters.iDayFrom >= 1 ?
                mParameters.iDayFrom : 1;
            iEndDay = mParameters.iDayTo && mParameters.iDayTo <= dEndOfMonth.getDate() ?
                mParameters.iDayTo : dEndOfMonth.getDate();
        }

        // Loop over the calculated dates and add the date objects
        for (var i = iBeginDay; i <= iEndDay; i++) {
            aDates.push(oDateHelper.toMap(oDateHelper.toDate({
                iYear: iYear,
                iMonth: mParameters.iMonth,
                iDay: i
            })));
        }

        // Return the array of date objects
        return aDates;
    }

    /**
     * @function getBeginOfMonth
     * @memberOf ixult.base.util.DateHelper
     * @summary Returns the begin-date either for a given
     *  year and/or month or based on the current Date
     * @param {map} mParameters
     *  a parameter map, containing the following attributes
     * @param {Number} mParameters.iYear
     *  the Year value; the current Year if not given
     * @param {Number} mParameters.iMonth
     *  the Month value, given in 'human' format
     *  (ranging from 1 to 12); the current one if not given
     * @returns {Date}
     *  the begin-date for the given (or current) Month
     * @public
     * @static
     */
    oDateHelper.getBeginOfMonth = function (mParameters) {
        return oDateHelper.toDate({
            iYear: parseInt(mParameters.iYear),
            iMonth: parseInt(mParameters.iMonth),
            iDay: 1
        });
    }

    /**
     * @function getEndOfMonth
     * @memberOf ixult.base.util.DateHelper
     * @summary Calculates the Date value for the end of a Month
     *  either for a given Year and/or Month or based on the current Date
     * @param {Number} mParameters.iYear
     *  the Year value; the current Year if not given
     * @param {Number} mParameters.iMonth
     *  the Month value, given in 'human' format
     *  (ranging from 1 to 12); the current one if not given
     * @returns {Date}
     *  the end Date for the given (or current) Month
     * @public
     * @static
     */
    oDateHelper.getEndOfMonth = function (mParameters) {
        // 'End of month' is achieved by using the given Month (ranging from 1 to 12)
        // and adding +1 to get it as a 'next Month' and the Day set to '0'
        return oDateHelper.toDate({
            iYear: parseInt(mParameters.iYear),
            iMonth: parseInt(mParameters.iMonth) + 1,
            iDay: 0,
            iHours: 23,
            iMinutes: 59,
            iSeconds: 59,
            bUTC: false // 'false' allows to correctly get the time zone dependent end of month
        });
    }

    /**
     * @function getMonthNames
     * @memberOf ixult.base.util.DateHelper
     * @summary Returns the month names in width "narrow", "abbreviated" or "wide"
     *  specified by the currently given Locale
     * @param {string} sWidth
     *  the required width for the month names
     * @returns {string[]}
     *  the array of month names (starting with January)
     * @public
     * @static
     */
    oDateHelper.getMonthNames = function (sWidth) {
        if (!assert(array(["narrow", "abbreviated", "wide"]).contains(sWidth),
            "sWidth must be 'narrow', 'abbreviated' or 'wide', " +
            "not '" + sWidth.toString() + "'")) {
            return [];
        }
        var oLocaleData = _getLocaleData();
        return oLocaleData ? oLocaleData.getMonths(sWidth) : [];
    }

    /**
     * @function getWeekNumber
     * @memberOf ixult.base.util.DateHelper
     * @summary Returns the Calendar Week Number for a given date or date map
     * @param {Date} dDate
     *  the date to get the calendar week from
     * @param {boolean} [bUTC]
     *  uses the DateFormat with UTC
     * @returns {string}
     *  the Calendar Week Number as a string
     * @public
     * @static
     */
    oDateHelper.getWeekNumber = function (dDate, bUTC) {
        return oDateHelper.format(dDate, "w", bUTC);
    }

    /**
     * @function isWeekday
     * @memberOf ixult.base.util.DateHelper
     * @summary Checks, if the given Date is not a Saturday or Sunday
     * @param {Date} dDate
     *  the Date to check
     * @returns {boolean|undefined}
     *  true if the date is a weekday, false otherwise
     * @public
     * @static
     */
    oDateHelper.isWeekday = function (dDate) {
        return !oDateHelper.isWeekend(dDate);
    }

    /**
     * @function isWeekend
     * @memberOf ixult.base.util.DateHelper
     * @summary Checks if the given Date is a Saturday or Sunday
     * @param {Date} dDate
     *  the Date to check
     * @returns {boolean|undefined}
     *  true if the date is a weekend date, false otherwise
     * @public
     * @static
     */
    oDateHelper.isWeekend = function (dDate) {
        return array([6, 7]).contains(oDateHelper.getWeekDay(dDate));
    }

    /**
     * @function getWeekDay
     * @memberOf ixult.base.util.DateHelper
     * @summary Returns the Day for a given Data as a human-readable weekday
     * @description The date will be returns in localized format, e.g.
     * <ul>
     *  <li> for 'en-US' the week goes from Sunday = 1 to Saturday = 7</li>
     *  <li> for 'de-DE' the week goes from Monday = 1 to Sunday = 7</li>
     * </ul>
     * @param {Date} dDate
     *  the date to evaluate
     * @returns {Number}
     *  the weekday from 1 to 7, depending on the current Locale
     * @public
     * @static
     */
    oDateHelper.getWeekDay = function (dDate) {
        var iDay = dDate.getDay() - _getLocaleData().getFirstDayOfWeek() + 1;
        return iDay <= 0 ? iDay + 7 : iDay;
    }

    /**
     * @function format
     * @memberOf ixult.base.util.DateHelper
     * @summary Formats a given date using the specified pattern
     * @see {@link ixult.base.util.html#.DateTimePattern ixult.base.util.DateTimePattern}
     * @see {@link ixult.base.util.DateHelper.html#.getDatePattern ixult.base.util.DateHelper#.getDatePattern}
     * @param {Date} dDate
     *  the date to format
     * @param {string} sPattern
     *  the Unicode CLDR date format pattern to use;
     *  alternatively can be one of the format strings
     *  used with function 'getDatePattern'
     * @param {boolean} [bUTC=false]
     *  uses the DateFormat with UTC
     * @returns {string}
     *  the formatted Date (as a string)
     * @public
     * @static
     */
    oDateHelper.format = function (dDate, sPattern, bUTC) {
        // Fallback: Check if the Date is really a Date
        if (!oDateHelper.isDate(dDate)) {
            return dDate.toString();
        }

        // Special case: Format for the short name of the weekday ('Mo', 'Tu', 'We' etc.)
        // Is valid in Unicode CLDR, but not implemented in SAPUI5 DateFormat (for some reason)
        if (sPattern === "e") {
            return oDateHelper.getWeekDay(dDate).toString();
        }

        // Check if the pattern is actually a format string
        // like 'short', 'medium', 'long' or 'full'
        var bIsFormat = $.grep(Object.keys(MyLibrary.DateTimePattern), function (sKey) {
            return MyLibrary.DateTimePattern[sKey] === sPattern;
        }.bind(this)).length > 0;

        // if it is, get the correct pattern to use
        if (bIsFormat) {
            sPattern = oDateHelper.getDatePattern(sPattern);
        }

        // Return the formatted string
        return DateFormat.getDateInstance({
            UTC: bUTC !== undefined ? bUTC : oDateHelper.bUTC,
            pattern: sPattern
        }, _getLocale()).format(dDate);
    }

    /**
     * @function getTimeFromString
     * @memberOf ixult.base.util.DateHelper
     * @summary Evaluates an arbitrary input string and tries
     *  to create a valid time value out of it
     * @description This method evaluates a given input to 'guess' a time value from it, e.g.
     *  - `8` is converted to `8:00`
     *  - `83` will become `8:03`
     *  - `17a` will result in `1:07` am resp. `1:07` (for 24h Locales)
     * @param {string} sInputValue
     *  the given input string
     * @param {map} [mParameters]
     *  a map with additional parameters including the following attributes
     * @param {Date} [mParameters.dDate]
     *  a base date on which the evaluated time value will be set
     * @param {boolean} [mParameters.bUTC=true]
     *  whether to use UTC when converting the time value
     * @returns {Date|null}
     *  the time object, which uses the given or current date as a base,
     *  or null if given input could not be evaluated successfully
     * @public
     * @static
     */
    oDateHelper.getTimeFromString = function (sInputValue, mParameters) {
        mParameters = mParameters || {};
        mParameters.bUTC = mParameters.bUTC !== undefined ? mParameters.bUTC : true;

        // Create the base date time as a map (for easier handling)
        var mBaseDate = mParameters && mParameters.dDate ?
                oDateHelper.toMap(mParameters.dDate) :
                oDateHelper.toMap(new Date()),
            // only copy the date values
            mDate = {
                iYear: mBaseDate.iYear,
                iMonth: mBaseDate.iMonth,
                iDay: mBaseDate.iDay,
            };

        // Extract the time value by removing all non number characters
        var sValue = sInputValue.replace(/\D/g, ""),
            bIsMorning = sInputValue.toLowerCase().indexOf("am") !== -1,
            bIsAfternoon = sInputValue.toLowerCase().indexOf("pm") !== -1;

        // Determine the values for hours and minutes;
        // Supports up to 4 numbers to be evaluated as time
        var iHour1 = parseInt(sValue.substring(0, 1), 10),
            iHour2 = (sValue.length >= 2) ? parseInt(sValue.substring(1, 2), 10) : null,
            iMinute1 = (sValue.length >= 3) ? parseInt(sValue.substring(2, 3), 10) : null,
            iMinute2 = (sValue.length >= 4) ? parseInt(sValue.substring(3, 4), 10) : null,
            iHour = 0,
            iMinute = 0;

        // Check the hour numbers for valid input
        iHour = (iHour1 !== null && iHour2 !== null) ? iHour1 * 10 + iHour2 : 0;
        if (iHour > 23 || iHour2 === null) {
            iHour = iHour1;
            iMinute2 = iMinute1;
            iMinute1 = iHour2;
        }

        // Check the minute numbers for valid input
        iMinute = (iMinute1 !== null && iMinute2 !== null) ?
            iMinute1 * 10 + iMinute2 :
            iMinute1 ? iMinute1 : 0;
        if (iMinute > 59 || iMinute2 === null) {
            iMinute = iMinute1 || 0;
        }

        // Set the result into the date time object
        mDate.iHours = bIsAfternoon && iHour <= 11 ? iHour + 12 :
            bIsMorning && iHour === 12 ? 0 : iHour;
        mDate.iMinutes = iMinute;

        return oDateHelper.toDate(mDate);
    }

    /**
     * @function readPublicHolidays
     * @memberOf ixult.base.util.DateHelper
     * @summary Reads the public holidays for a given year
     *  using a public internet web service
     * @description This method uses the free service API at
     *  the URL specified above to get the public and regional
     *  holidays for a given year and country; it returns an
     *  array of JSON formatted holidays which follow the
     *  structure given in the example below.
     *  ```json
     *  {
     *    "date": "2021-01-06",                 // the date
     *    "localName": "Heilige Drei Könige",   // local description
     *    "name": "Epiphany",                   // English description
     *    "countryCode": "DE",
     *    "fixed": true,                        // true for fixed date, false for calculated dates
     *    "global": false,
     *    "counties": [                         // list of counties, for regional holidays
     *      "DE-BW",
     *      "DE-BY",
     *      "DE-ST"
     *    ],
     *    "launchYear": 1967,                   // first year of occurrence
     *    "types": [
     *       "Public"                           // 'Public' for public holidays
     *    ]
     *  }
     *  ```
     * @see {@link https://date.nager.at date.nager.at}
     * @param {map} mParameters
     *  a parameter map containing the following attributes
     * @param {int|Number} [mParameters.iYear]
     *  the year to read the holidays for
     * @param {string} [mParameters.sCountry]
     *  the country code or locale to read the data for
     * @returns {Promise}
     *  A Promise instance with `resolve` and `reject`
     *  handlers including the corresponding service result,
     *  i.e. the data or an error
     * @public
     * @static
     */
    oDateHelper.readPublicHolidays = function (mParameters) {
        var oDeferred = $.Deferred(),
            iYear = mParameters.iYear || this.toMap().iYear,
            sCountry = (mParameters.sCountry || _getLocale().getRegion()).toUpperCase();

        // Try reading from locally existing data
        _readHolidays({
            iYear: iYear,
            sCountry: sCountry,
            sServiceUrl: Library.getManifestEntry("/sap.app/dataSources/" +
                "localHolidayService/uri"),
        }).then(function (oResult) {
            oDeferred.resolve(oResult);
        }.bind(this)).fail(function (oError) {
            // If not existing use the public service
            _readHolidays({
                iYear: iYear,
                sCountry: sCountry,
                sServiceUrl: Library.getManifestEntry("/sap.app/dataSources/" +
                    "publicHolidayService/uri"),
            }).then(function (oResult) {
                oDeferred.resolve(oResult);
            }.bind(this)).fail(function (oError) {
                oDeferred.reject(oError);
            }.bind(this));
        }.bind(this));

        return oDeferred.promise();
    }


    return oDateHelper;
});
