sap.ui.define([
    "ixult/base/assert",
    "ixult/base/util/array",
    "ixult/base/util/DateHelper"
], function (
    assert,
    array,
    DateHelper
) {
    "use strict";

    /**
     * @class string
     * @memberOf ixult.base.util
     *
     * @classdesc Handles and/or manipulate strings; Uses a given
     *  string as parameter and enhances it with custom functions
     * @description Usage: to create the string handler instance, this constructor
     *  can be called in a static way like `string(sString)` where the function
     *  `string` is the namespace instance of `ixult/base/util/string` included in
     *  the `sap.ui.define` or `sap.ui.require` statement in the source file
     *  and `sString` is the source string to enhance. Methods can then directly
     *  be applied to the result, like `string(sString).contains(vValue)`
     * @example
     *  // Determines whether the String contains another string, optional with non-case-sensitive checks
     *  var string = sap.ui.require("ixult/base/util/string");
     *  var bContains1 = string("Hello World!").contains("world", true); // returns true; non case-sensitive
     *  var bContains2 = string("Hello World!").contains("world", false); // returns false; case-sensitive
     *
     * @param {string} sString
     *  the source string
     * @returns {String<string>}
     *  a `String` object including the source string and
     *  the additional functions below that can be applied to it
     *
     * @public
     */
    var string = function (sString) {
        var _sString = sString || "",
            _oString = Object(_sString);

        /**
         * @function contains
         * @memberOf ixult.base.util.string.prototype
         * @summary Checks whether a given string contains a substring
         * @description Convenience method using `indexOf`,
         *  enhanced to allow non-case-sensitive checks
         * @param {string|any} vValue
         *  the string to look for inside the source string
         * @param {boolean} [bIgnoreCase=true]
         *  executes the checks non-case-sensitive; true by default
         * @returns {boolean}
         *  true, if the string contains the compare value, false otherwise
         * @public
         * @instance
         */
        _oString.contains = function (vValue, bIgnoreCase) {
            var bContains = false,
                fnCheck = function (sValue) {
                    return bIgnoreCase !== false ?
                        _sString.toLowerCase().indexOf(sValue.toString().toLowerCase()) !== -1 :
                        _sString.indexOf(sValue.toString()) !== -1;
                }

            array(vValue).loop(sValue => {
                bContains = bContains || fnCheck(sValue);
            });

            return bContains;
        }

        /**
         * @function toCamelCase
         * @memberOf ixult.base.util.string.prototype
         * @summary Converts the string value into 'camelCase' format
         * @returns {string}
         *  the converted string
         * @public
         */
        _oString.toCamelCase = function () {
            return _sString
                .replace(/\s(.)/g, function ($1) {
                    return $1.toUpperCase();
                })
                .replace(/\s/g, '')
                .replace(/^(.)/, function ($1) {
                    return $1.toLowerCase();
                });
        }

        /**
         * @function toTitleCase
         * @memberOf ixult.base.util.string.prototype
         * @summary Converts the string value into 'TitleCase' format
         * @returns {string}
         *  the converted string
         * @public
         */
        _oString.toTitleCase = function () {
            return _sString
                .replace(/\s(.)/g, function ($1) {
                    return $1.toUpperCase();
                })
                .replace(/\s/g, '')
                .replace(/^(.)/, function ($1) {
                    return $1.toUpperCase();
                });
        }

        return _oString;
    }

    return string;
});
