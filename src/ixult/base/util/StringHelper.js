sap.ui.define([], function () {
    "use strict"

    /**
     * @namespace StringHelper
     * @memberOf ixult.base.util
     *
     * @summary Static helper providing methods
     *  for advanced handling of strings
     * @description Deprecated as of 0.6.9.
     *  Use `ixult.base.util.string` instead
     *
     * @public
     * @static
     * @deprecated
     */
    var oStringHelper = {};

    /**
     * @function toCamelCase
     * @memberOf ixult.base.util.StringHelper.prototype
     * @summary Converts the string value in to 'camelCase' format
     * @param {string} sString
     *  the source string
     * @returns {string}
     *  the converted string
     * @public
     */
    oStringHelper.toCamelCase = function (sString) {
        return sString
            .replace(/\s(.)/g, function ($1) {
                return $1.toUpperCase();
            })
            .replace(/\s/g, '')
            .replace(/^(.)/, function ($1) {
                return $1.toLowerCase();
            });
    }

    /**
     * @function toTitleCase
     * @memberOf ixult.base.util.StringHelper.prototype
     * @summary Converts the string value in to 'TitleCase' format
     * @param {string} sString
     *  the source string
     * @returns {string}
     *  the converted string
     * @public
     */
    oStringHelper.toTitleCase = function (sString) {
        return sString
            .replace(/\s(.)/g, function ($1) {
                return $1.toUpperCase();
            })
            .replace(/\s/g, '')
            .replace(/^(.)/, function ($1) {
                return $1.toUpperCase();
            });
    }


    return oStringHelper;
});
