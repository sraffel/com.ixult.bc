sap.ui.define([
    "sap/ui/core/Core",
    "sap/ui/core/library"
], function (
    Core,
    Library
) {
    "use strict";

    /**
     * @namespace ui
     * @memberOf ixult.lib
     *
     * @description Utility library used for
     *  third party UI Controls and libraries
     *
     * @public
     */
    return sap.ui.getCore().initLibrary({
        name: "ixult.lib.ui",
        noLibraryCSS: true,
        dependencies: [
            "sap.ui.base",
            "sap.ui.core",
            "sap.ui.util",
            "sap.m"
        ],
        types: [],
        interfaces: [],
        controls: [],
        elements: [],
        version: "0.8.1"
    });

}, /* bExport= */ false);