sap.ui.define([
    "sap/ui/core/Core",
    "sap/ui/core/library"
], function (
    Core,
    Library
) {
    "use strict";

    /**
     * @namespace unified
     * @memberOf ixult.lib.ui
     *
     * @description 3rd party utility
     *  library used for unified Controls
     *
     * @public
     */
    return sap.ui.getCore().initLibrary({
        name: "ixult.lib.ui.unified",
        noLibraryCSS: true,
        dependencies: [
            "sap.ui.base",
            "sap.ui.core",
            "sap.ui.util",
            "sap.m"
        ],
        types: [],
        interfaces: [],
        controls: [],
        elements: [],
        version: "0.8.2"
    });

}, /* bExport= */ false);