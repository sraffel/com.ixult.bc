sap.ui.define([
    "sap/ui/core/Core",
    "sap/ui/core/library"
], function (
    Core,
    Library
) {
    "use strict";

    /**
     * @namespace lib
     * @memberOf ixult
     *
     * @description Library used to
     *  include third party functionality
     *
     * @public
     */
    return sap.ui.getCore().initLibrary({
        name: "ixult.lib",
        noLibraryCSS: true,
        dependencies: [
            "sap.ui.base",
            "sap.ui.core",
            "sap.ui.util",
            "sap.m"
        ],
        types: [],
        interfaces: [],
        controls: [],
        elements: [],
        version: "0.8.1"
    });

}, /* bExport= */ false);