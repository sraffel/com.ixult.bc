| ❗❗ Note : This documentation is not final. It will be continued successively. |
|:---|

# conovum UI5 common library

<img src="../assets/icons/robot.svg" width="128px" />

[Hi there, hello](https://www.youtube.com/ryangeorge). 😊 

Welcome to the conovum UI5 common library. Here you can find a collection of enhanced classes and controls derived from SAPUI5 sources.

This library and its classes contain attributes, methods and event handlers that have proven to be useful in SAPUI5 apps. So, to prevent copy-and-paste-coding this collection is intended to be used as a framework helping to structure and manage your SAPUI5 apps.
